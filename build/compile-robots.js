const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const loadDotEnv = require('cmc-load-dot-env');
const oneLineTrim = require('common-tags').oneLineTrim;

const options = {
    srcPath: path.resolve(__dirname, '../app'),
    outputPath: path.resolve(__dirname, '../dist'),
    srcs: ['robots.txt.tmpl', 'sitemap.xml.tmpl', '_headers.tmpl'],
};

if (!{}.hasOwnProperty.call(process.env, 'CORS_ORIGIN_TLD')) {
    // eslint-disable-next-line no-console
    console.log('no CORS_ORIGIN_TLD set, will load .env');
    loadDotEnv();
}

options.srcs.forEach((fileName) => {
    fs.readFile(`${options.srcPath}/${fileName}`, 'utf8', (readTemplateError, template) => {
        if (readTemplateError) {
            // eslint-disable-next-line no-console
            console.log(`error reading template file, ${fileName}`, readTemplateError);
        } else {
            const compiled = _.template(template, { variable: 'data' });
            const result = compiled(process.env);
            const outputFileName = fileName.replace('.tmpl', '');
            if (!fs.existsSync(options.outputPath)) {
                fs.mkdirSync(options.outputPath);
            }
            fs.writeFile(
                `${options.outputPath}/${outputFileName}`,
                result,
                'utf8',
                (writeConfigError) => {
                    if (writeConfigError) {
                        // eslint-disable-next-line no-console
                        console.log(`error writing file, ${outputFileName}`, writeConfigError);
                    } else {
                        // eslint-disable-next-line no-console
                        console.log(oneLineTrim`Generated new ${outputFileName} 
                            file in folder: ${options.outputPath}`);
                    }
                }
            );
        }
    });
});

