// from React Starter Kit (https://www.reactstarterkit.com/)
/* eslint-disable import/no-extraneous-dependencies */
import hotClient from 'webpack-hot-middleware/client';
import launchEditorEndpoint from 'react-dev-utils/launchEditorEndpoint';
import formatWebpackMessages from 'react-dev-utils/formatWebpackMessages';
import {
    reportBuildError,
    dismissBuildError,
    startReportingRuntimeErrors,
    stopReportingRuntimeErrors,
} from 'react-error-overlay';

hotClient.useCustomOverlay({
    showProblems(type, errors) {
        const formatted = formatWebpackMessages({
            errors,
            warnings: [],
        });

        reportBuildError(formatted.errors[0]);
    },
    clear() {
        dismissBuildError();
    },
});

hotClient.setOptionsAndConnect({
    name: 'cmc-site',
    reload: true,
});

startReportingRuntimeErrors({
    launchEditorEndpoint,
    filename: '/dist/cmc-site.js',
});

if (module.hot) {
    module.hot.dispose(stopReportingRuntimeErrors);
}
