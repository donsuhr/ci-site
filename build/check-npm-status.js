'use strict';

const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const { spawn } = require('child_process');

const hashFile = path.resolve(process.cwd(), 'npmhash.json');

let hashFileJson;
try {
    hashFileJson = JSON.parse(fs.readFileSync(hashFile));
} catch (e) {
    hashFileJson = {};
}

const npmFile = path.resolve(process.cwd(), 'package-lock.json');
const npmFileExists = fs.existsSync(npmFile);
let npmFileHash;

if (npmFileExists){
    const fileData = fs.readFileSync(npmFile);

    npmFileHash = crypto
        .createHash('md5')
        .update(fileData)
        .digest('hex');
}

if (!npmFileExists || npmFileHash !== hashFileJson.hash) {
    const npmInstall = spawn(
        /^win/.test(process.platform) ? 'npm.cmd' : 'npm',
        ['install'],
        { stdio: 'inherit' },
    );
    npmInstall.on('exit', () => {
        // process.exit();
        hashFileJson.hash = npmFileHash;
        fs.writeFileSync(hashFile, JSON.stringify(hashFileJson, null, 4));
    });
} else {
    // eslint-disable-next-line no-console
    console.log('no npm update');
}
