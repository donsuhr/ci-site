// write front-end SAFE variables to config file
const writeConfig = require('cmc-write-config');
const path = require('path');

const outputPath = path.resolve(__dirname, '../');
writeConfig({ outputPath });
