module.exports = function awsS3(grunt, options) {
    return {
        'search-index-docs--get-general': {
            options: {
                bucket: 'cmcv3-general--ci',
                differential: true,
            },
            files: [
                {
                    dest: '/',
                    cwd: 'search-index/cmcv3-general--ci/',
                    action: 'download',
                },
            ],
        },
    };
};
