'use strict'; // eslint-disable-line strict, lines-around-directive

const dotEnv = require('dotenv');
const path = require('path');
const fs = require('fs');
const argv = require('yargs').argv;
const _ = require('lodash');

dotEnv.config();

// eslint-disable-next-line import/no-extraneous-dependencies
const bs = argv.serve ? require('browser-sync').create() : null;

const Metalsmith = require('metalsmith');
const layouts = require('metalsmith-layouts');
const inPlace = require('metalsmith-in-place');
const helpers = require('metalsmith-register-helpers');
const publish = require('metalsmith-publish');
const chokidar = argv.watch ? require('chokidar') : null;
const collections = require('metalsmith-collections');
const defaultValues = require('metalsmith-default-values');
const permalinks = require('metalsmith-permalinks');
const partials = require('metalsmith-register-partials');
const metalsmithEnv = require('metalsmith-env');
const sitemap = require('metalsmith-sitemap');
const addOriginalFilename = require('metalsmith-add-orig-file-name');
const addFormatedDate = require('cmc-site/app/metalsmith/middleware/metalsmith--add-pub-date-formatted');
const webpackAssets = require('cmc-site/app/metalsmith/middleware/metalsmith-webpack-assets');

const addPageImgMetadata =
    require('cmc-site/app/metalsmith/middleware/metalsmith--add-page-img-metadata');
const handlebars = require('handlebars');
const handlebarsLayouts = require('handlebars-layouts');
// assemble helpers have the handlebars-layouts in it. assemble first

require('handlebars-helpers')({
    handlebars,
}); // http://assemble.io/helpers/

// overwrite second
handlebars.registerHelper(handlebarsLayouts(handlebars));

require('@babel/register')({
    only: [/config.js/],
    presets: ['@babel/preset-env'],
})

const modulesDir = path.resolve(require.resolve('cmc-load-dot-env'), '../..');

const config = require('../config').default;

const production = process.env.NODE_ENV === 'production';

const publishConfig = {
    draft: !production,
    private: !production,
    future: !production,
};

const t1 = process.hrtime();
const webpackAssetsFilePath = 'webpack-manifest.json';
let webpackManifestJson = JSON.parse(
    fs.readFileSync(path.resolve(process.cwd(), webpackAssetsFilePath)),
);

function compareWebpackManifest() {
    const newJson = JSON.parse(
        fs.readFileSync(path.resolve(process.cwd(), webpackAssetsFilePath)),
    );
    const result = isEqual(webpackManifestJson, newJson);
    webpackManifestJson = newJson;
    return result;
}

function build(clean, collectionsObj, changedFile) {
    const ms = new Metalsmith(path.resolve(`${__dirname}/../`));
    console.log('Metalsmith build start'); // eslint-disable-line no-console
    ms.clean(!!clean)
        .source('app/pages')
        .destination('dist')
        .metadata({
            config,
        })
        .ignore((currentPath, lstat) => {
            // true to ignore
            if (changedFile) {
                if (lstat.isDirectory()) {
                    // don't ignore directories, need to search sub dirs
                    return false;
                }
                let parent = changedFile.split(path.sep).slice(0, -1).join(path.sep);
                while (parent.length) {
                    if (currentPath.indexOf(`${parent}${path.sep}index.html`) !== -1) {
                        // is parent index file, dont ignore
                        return false;
                    }
                    parent = parent.split(path.sep).slice(0, -1).join(path.sep);
                }
                // check for same directory sibling files and below
                return currentPath.indexOf(changedFile) === -1;
            }
            // no file changed, ignore nothing
            return false;
        })
        .use(metalsmithEnv())
        .use(
            defaultValues([
                {
                    pattern: 'dev/**/*.html',
                    defaults: {
                        'show-in-left-nav': process.env.NODE_ENV !== 'production',
                        private: true,
                    },
                },
                {
                    pattern: 'admin-3/**/*.html',
                    defaults: {
                        private: true,
                    },
                },
                {
                    pattern: '**/*.html',
                    defaults: {
                        layout: 'ci-site.hbs',
                        language: 'en-us',
                        'url-lang-prefix': '',
                        'menu-priority': 0.5,
                        'show-in-quick-launch': true,
                        'show-in-left-nav': true,
                        'og-type': 'article',
                        leftNavStartFrom: 'pages',
                    },
                },

            ])
        )
        .use(publish(publishConfig))
        .use(addOriginalFilename({}))
        .use(addFormatedDate({}))
        .use(addPageImgMetadata({}))
        .use(webpackAssets({}))
        .use(collections(collectionsObj))
        .use(
            permalinks({
                relative: false,
            })
        )
        .use(
            sitemap({
                hostname: process.env.CORS_ORIGIN_CMC,
                omitIndex: true,
                omitExtension: true,
                output: 'pages.xml',
            })
        )
        .use(
            helpers({
                engine: 'handlebars',
                directory: path.relative(
                    '.', path.resolve(modulesDir, 'cmc-site/app/metalsmith/helpers')
                ),
            })
        )
        .use(
            helpers({
                engine: 'handlebars',
                directory: 'app/metalsmith/helpers',
            })
        )
        .use(
            partials({
                directory: path.relative(
                    '.', path.resolve(modulesDir, 'cmc-site/app/metalsmith/partials')
                ),
                handlebars,
            })
        )
        .use(
            partials({
                directory: 'app/metalsmith/partials',
                handlebars,
            })
        )
        .use(
            inPlace({
                engine: 'handlebars',
                partials: handlebars.partials,
            })
        )
        .use(
            layouts({
                // files using cmc-site layouts folder
                engine: 'handlebars',
                directory: 'app/metalsmith/layouts',
                pattern: ['**/*'],
            })
        )
        .build((err, files) => {
            if (err) {
                console.log(err); // eslint-disable-line no-console
                console.log('Error rendering files. Stack:'); // eslint-disable-line no-console
                console.log(err.stack); // eslint-disable-line no-console
            } else {
                // eslint-disable-next-line no-console
                console.log('Metalsmith Done', Object.keys(files).length, 'files written');
            }
        });
}

function getdirSync(dirPath, parentName, result) {
    const stat = fs.lstatSync(dirPath);
    result = result || {};
    if (stat.isDirectory()) {
        const name = parentName
            ? `${parentName}==>${path.basename(dirPath)}`
            : path.basename(dirPath);
        const localName = name.replace(/^pages==>/, '');
        if (parentName) {
            // ignore root dir
            result[localName] = {
                path: dirPath,
                name: localName,
                refer: false,
                pattern: `${localName.replace(/==>/g, '/')}/*.html`,
            };
        }

        fs.readdirSync(dirPath).map(child => getdirSync(`${dirPath}/${child}`, name, result));
    }
    return result;
}

const autoCollections = getdirSync(path.resolve(`${__dirname}/../app/pages`));
const specificCollections = {
    // add links and other 'sub-site' metadata here; page metadata in defaultValues
};

const collectionsObj = _.merge({}, autoCollections, specificCollections);

if (!argv.watch_only) {
    build(!!argv.clean, collectionsObj);
}

if (argv.watch) {
    console.log('Metalsmith watching files...'); // eslint-disable-line no-console
    chokidar.watch([
        'app/pages/**/*.html',
        'app/metalsmith/layouts/**/*.hbs',
        'app/metalsmith/partials/**/*.hbs',
        'app/metalsmith/helpers/**/*.js',
    ])
        .on('change', (file) => {
            const folder = path.dirname(file);
            const type = path.extname(file);
            console.log('Metalsmith Watch: change', file); // eslint-disable-line no-console
            const limitToFolder = type === '.html' ? folder : false;
            build(false, collectionsObj, limitToFolder);
        });
}

if (argv.serve) {
    bs.init({
        server: './dist',
        files: ['./dist/**/*.html'],
        open: false,
    });
}
