'use strict';

const critical = require('critical');
const fs = require('fs');
const path = require('path');

// https://stackoverflow.com/questions/13542667/create-directory-when-writing-to-file-in-node-js
function ensureDirectoryExistence(filePath) {
    const dirname = path.dirname(filePath);
    if (!fs.existsSync(dirname)) {
        ensureDirectoryExistence(dirname);
        fs.mkdirSync(dirname);
    }
}

function loadForceFile(src) {
    return new Promise((resolve, reject) => {
        const file = path.resolve(src);
        fs.readFile(file, 'utf8', (rfErr, data) => {
            if (rfErr) {
                return reject(rfErr);
            }
            return resolve(data);
        });
    });
}

function generate({ src, name }) {
    return critical
        .generate({
            base: './dist/',
            src,
            css: ['./.tmp/sass-out/main.css'],
            // dest: `../.tmp/styles/critical-${name}.css`,
            minify: false,
            inline: false,
            extract: false,
            ignore: [
                '@font-face',
                /^\.search-form/,
                /modal/,
                /\.products-and-solutions/,
                /^\.left-nav-menu__list/,
            ],
            dimensions: [
                {
                    width: 320,
                    height: 500,
                },
                {
                    width: 1200,
                    height: 600,
                },
            ],
        })
        .then((result) => ({
            src,
            name,
            result,
        }));
}

const items = [
    { src: 'index.html', name: 'home' },
];

loadForceFile('./.tmp/sass-out/crit-forced.css').then((forceCss) => {
    Promise.all(items.map(generate)).then((results) => {
        const combinedResults = `${forceCss} \n${results.map((x) => x.result).join('\n')}`;
        const combinedOutputFile = path.resolve(
            process.cwd(),
            './.tmp/sass-out/critical.css',
        );
        fs.writeFileSync(combinedOutputFile, combinedResults, 'utf8');

        ensureDirectoryExistence(
            path.resolve(process.cwd(), './.tmp/crit-out'),
        );

        results.forEach(({ src, name, result }) => {
            const outputFile = path.resolve(
                process.cwd(),
                `./.tmp/crit-out/critical-${name}.css`,
            );
            ensureDirectoryExistence(outputFile);
            fs.writeFileSync(outputFile, result, 'utf8');
        });
    });
});
