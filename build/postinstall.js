/* eslint-disable no-console */
// https://gist.github.com/DavidWells/e886bff1fdb543a707b39451535b1369
const path = require('path');
const exec = require('child_process').exec;

const cwd = process.cwd();

if (
    cwd.indexOf('node_modules') === -1 &&
    process.env.NODE_ENV !== 'production'
) {
    console.log('in local dev context. Build DLL');
    const webpackPath = path.join(cwd, 'node_modules', '.bin', 'webpack');
    exec(
        `${webpackPath} --display-chunks --color --config webpack.dll.config.js`,
        { cwd },
        (error, stdout, stderr) => {
            if (error) {
                console.warn(error);
            }
            console.log(stdout);
            console.log('Built dll for local DEV');
        }
    );
} else {
    console.log('in node_modules context, stop DLL build on postinstall');
}
