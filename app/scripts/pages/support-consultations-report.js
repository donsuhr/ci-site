import getDomDataAttributes from '../../components/util/domAttributes-util';

require.ensure(['../firebase'], () => {
    require.ensure(['../react-app'], () => {
        require.ensure(['../../components/SupportConsultationsReport'], (require) => {
            const App = require('../../components/SupportConsultationsReport').default;
            const el = document.getElementById('SupportConsultationsReport');
            const dataAttributes = getDomDataAttributes(el);
            App.config(dataAttributes, el);
        }, 'supportConsultationsReportChunk');
    }, 'reactChunk');
}, 'fbChunk');
