import $ from 'jquery';
import queryString from 'query-string';
import form from 'cmc-site/app/components/form';

const $el = $('.js--simple-form:first');
const formDomID = $el.attr('id');
const successRedirectUrl =
    $el.data('success-redirect-url') || '/contact-us/contact-complete/';
const leadSource = $el.data('lead-source');
const endpoint = $el.attr('action');

const parsed = queryString.parse(window.location.search);
const { level } = parsed;
if (level) {
    $('[name="level"]').val(level);
}

form.createForm({
    leadSource,
    successRedirectUrl,
    formDomID,
    endpoint,
});
