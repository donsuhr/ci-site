import getDomDataAttributes from '../../components/util/domAttributes-util';

require.ensure(['../firebase'], () => {
    require.ensure(['../react-app'], () => {
        require.ensure(['../../components/PassportReport'], (require) => {
            const App = require('../../components/PassportReport').default;
            const el = document.getElementById('PassportReport');
            const dataAttributes = getDomDataAttributes(el);
            App.config(dataAttributes, el);
        }, 'passportReportChunk');
    }, 'reactChunk');
}, 'fbChunk');
