import $ from 'jquery';
import 'jquery-ui/ui/core';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/widgets/accordion';

$('.award-category-radio-group').accordion({
    animate: 200,
    heightStyle: 'content',
    header: '.award-category__more-info-icon',
    collapsible: true,
    active: false,
});
