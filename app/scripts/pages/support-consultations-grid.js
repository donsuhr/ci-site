import getDomDataAttributes from '../../components/util/domAttributes-util';

require.ensure(['../firebase'], () => {
    require.ensure(['../react-app'], () => {
        require.ensure(['../../components/SupportConsultationsGrid'], (require) => {
            const App = require('../../components/SupportConsultationsGrid').default;
            const el = document.getElementById('CiAppMountPoint');
            const dataAttributes = getDomDataAttributes(el);
            App.config(dataAttributes, el);
        }, 'scheduleChunk');
    }, 'reactChunk');
}, 'fbChunk');
