import $ from 'jquery';
import form from 'cmc-site/app/components/form';

const $el = $('#requestDemoForm');
const formDomID = $el.attr('id');
const successRedirectUrl = $el.data('success-redirect-url') || '/contact-us/contact-complete/';
const endpoint = $el.attr('action');

form.createForm({
    leadSource: 'ci executive activities registration',
    successRedirectUrl,
    formDomID,
    endpoint,
});

function getShoeVal() {
    return $('[name="needs-golf-shoes"]').val().toLowerCase();
}

function onShoeChange() {
    const show = getShoeVal() !== 'no';
    $('#golf-form__shoe-size-li').toggle(show);
    $('#golf-form__shoe-size-li label').toggleClass('required', show);
}

function onGolfChange() {
    const checked = $('[name="will-golf"]').is(':checked');
    $('.golf-form').toggle(checked);
}

function onDinnerChange() {
    const checked = $('[name="will-executive-dinner"]').is(':checked');
    $('.executive-dinner-form').toggle(checked);
}

$('[name^="needs-golf-shoes"]').on('change', (event) => {
    onShoeChange();
});

$('[name="will-executive-dinner"]').on('change', (event) => {
    onDinnerChange();
});

$('[name="will-golf"]').on('change', (event) => {
    onGolfChange();
});

onShoeChange();
onGolfChange();
onDinnerChange();

window.Parsley.addValidator('requiredIf', {
    validateString: (value, requirement) => {
        if (getShoeVal() !== 'no') {
            return !!value;
        }
        return true;
    },
    priority: 33,
    messages: {
        en: 'Please provide a shoe size',
    },
});
