import getDomDataAttributes from '../../components/util/domAttributes-util';

require.ensure(['../firebase'], () => {
    require.ensure(['../react-app'], () => {
        require.ensure(['../../components/EvaluationReport'], (require) => {
            const App = require('../../components/EvaluationReport').default;
            const el = document.getElementById('EvaluationReport');
            const dataAttributes = getDomDataAttributes(el);
            App.config(dataAttributes, el);
        }, 'evaluationReportChunk');
    }, 'reactChunk');
}, 'fbChunk');
