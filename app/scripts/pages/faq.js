import $ from 'jquery';
import 'jquery-ui/ui/core';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/widgets/accordion';

$('.faqs').accordion({
    animate: 200,
    heightStyle: 'content',
});
