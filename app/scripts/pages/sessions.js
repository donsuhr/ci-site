import $ from 'jquery';
import getDomDataAttributes from '../../components/util/domAttributes-util';

$('.session-browser').on('click', '.session-browser__session-details-button', (event) => {
    event.preventDefault();
    $(event.target)
        .toggleClass('show')
        .closest('li')
        .find('.session-browser__session-details')
        .slideToggle({
            duration: 200,
        });
});

require.ensure(['../firebase'], () => {
    require.ensure(['../react-app'], () => {
        require.ensure(['../../components/SessionBrowser'], (require) => {
            const App = require('../../components/SessionBrowser').default;
            const el = document.getElementById('SessionsMountPoint');
            const dataAttributes = getDomDataAttributes(el);
            dataAttributes.basename = document.location.pathname.endsWith('/') ?
                document.location.pathname.slice(0, -1) : document.location.pathname;
            App.config(dataAttributes, el);
        }, 'sessionBrowserChunk');
    }, 'reactChunk');
}, 'fbChunk');
