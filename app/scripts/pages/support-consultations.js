import getDomDataAttributes from '../../components/util/domAttributes-util';

require.ensure(['../firebase'], () => {
    require.ensure(['../react-app'], () => {
        require.ensure(['../../components/SupportConsultations'], (require) => {
            const App = require('../../components/SupportConsultations').default;
            const el = document.getElementById('SupportConsultationsMountPoint');
            const dataAttributes = getDomDataAttributes(el);
            App.config(dataAttributes, el);
        }, 'supportConsultationsChunk');
    }, 'reactChunk');
}, 'fbChunk');
