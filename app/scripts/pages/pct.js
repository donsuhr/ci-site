import $ from 'jquery';
import getDomDataAttributes from '../../components/util/domAttributes-util';

$('.session-browser').on('click', '.pct-browser__session-details-button', (event) => {
    event.preventDefault();
    $(event.target)
        .toggleClass('show')
        .closest('li')
        .find('.pct-browser__session-details')
        .slideToggle({
            duration: 200,
        });
});

require.ensure(['../firebase'], () => {
    require.ensure(['../react-app'], () => {
        require.ensure(['../../components/PctBrowser'], (require) => {
            // import App from '../../components/PctBrowser';
            const App = require('../../components/PctBrowser').default;
            const el = document.getElementById('PctMountPoint');
            const dataAttributes = getDomDataAttributes(el);
            App.config(dataAttributes, el);
        }, 'pctChunk');
    }, 'reactChunk');
}, 'fbChunk');
