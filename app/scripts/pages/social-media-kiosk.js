/* eslint-disable */

import $ from 'jquery';
import _ from 'underscore';
require('slick-carousel');

const template = document.getElementById('facebookTemplate').innerHTML;

window.fbAsyncInit = function () {
    doResize();
};
setInterval(doResize, 60000);

function doResize() {
    const width = $('.facebook-wrapper').width();
    const height = $('.facebook-wrapper').height();
    const compiled = _.template(template, { variable: 'data' });
    const html = compiled({ width, height });
    $('.facebook-wrapper').html(html);
    console.log('should rebuild')
    FB.init({
        appId: '537400829608466',
        xfbml: true,
        version: 'v2.5',
    });
    setTimeout(function () {
        $('#twitter-widget-0')
            .contents()
            .find('head')
            .append('<style type="text/css">.timeline-Header{display:none;}</style>');
    }, 1000);
}

// load twitter widget
!(function (d, s, id) {
    var js,
        fjs = d.getElementsByTagName(s)[0],
        p = /^http:/.test(d.location) ? 'http' : 'https';
    if (!d.getElementById(id)) {
        js = d.createElement(s);
        js.id = id;
        js.src = p + '://platform.twitter.com/widgets.js';
        fjs.parentNode.insertBefore(js, fjs);
    }
})(document, 'script', 'twitter-wjs');

// load facebook widget
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = '//connect.facebook.net/en_US/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
})(document, 'script', 'facebook-jssdk');

$('#social-media-page-carousel').slick({
    vertical: true,
    autoplay: true,
    autoplaySpeed: 15000,
    pauseOnFocus: false,
    pauseOnHover: false,
    arrows: false,
    accessibility: false,
    infinite: true,
});

