import $ from 'jquery';
import getDomDataAttributes from '../../components/util/domAttributes-util';

$('.schedule').on('click', '.event-schedule-table__day-heading', (event) => {
    event.preventDefault();
    $(event.target).toggleClass('show').next().slideToggle({
        duration: 200,
    });
});

require.ensure(['../firebase'], () => {
    require.ensure(['../react-app'], () => {
        require.ensure(['../../components/Schedule'], (require) => {
            const App = require('../../components/Schedule').default;
            const el = document.getElementById('ScheduleMountPoint');
            const dataAttributes = getDomDataAttributes(el);
            App.config(dataAttributes, el);
        }, 'scheduleChunk');
    }, 'reactChunk');
}, 'fbChunk');
