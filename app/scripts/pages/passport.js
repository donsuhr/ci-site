import getDomDataAttributes from '../../components/util/domAttributes-util';

require.ensure(['../firebase'], () => {
    require.ensure(['../react-app'], () => {
        require.ensure(['../../components/Passport'], (require) => {
            const App = require('../../components/Passport').default;
            const el = document.getElementById('PassportMountPoint');
            const dataAttributes = getDomDataAttributes(el);
            App.config(dataAttributes, el);
        }, 'passportChunk');
    }, 'reactChunk');
}, 'fbChunk');
