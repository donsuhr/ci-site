import getDomDataAttributes from '../../components/util/domAttributes-util';

require.ensure(['../firebase'], () => {
    require.ensure(['../react-app'], () => {
        require.ensure(['../../components/ScheduleReport'], (require) => {
            const App = require('../../components/ScheduleReport').default;
            const el = document.getElementById('ScheduleReport');
            const dataAttributes = getDomDataAttributes(el);
            App.config(dataAttributes, el);
        }, 'scheduleReportChunk');
    }, 'reactChunk');
}, 'fbChunk');
