import $ from 'jquery';
import queryString from 'query-string';
import Basil from 'basil.js';

import config from 'config';
import { checkStatus, parseJSON } from 'fetch-json-helpers';
import form from 'cmc-site/app/components/form';
import { gleanDefaultProfile } from '../../components/firebase/profile.util';

const $el = $('.js--simple-form:first');
const formDomID = $el.attr('id');
const leadSource = $el.data('lead-source');
const endpoint = $el.attr('action');
const year = $el.data('year');
const $loadingEl = $('.cmc-article .loading--placeholder');
const $titleEl = $('.cmc-article .cmc-article__title--app');

const parsed = queryString.parse(window.location.search);
const sessionId = parsed.id;
const returnTo = parsed.f || '/sessions/';
$('#session_id').val(parsed.id);
$('#year').val(year);

const basil = new Basil();

const evalForm = form.createForm({
    leadSource,
    formDomID,
    endpoint,
    cb: (error, result) => {
        if (error) {
            return;
        }
        const current = basil.get('evaluations') || {};
        const newVal = {
            ...current,
            [sessionId]: true,
        };
        basil.set('evaluations', newVal, { storages: ['local', 'cookie'] });
        window.location.href = returnTo;
    },
});

function getSessionTitle(id) {
    $loadingEl.css({ display: 'block' });
    return fetch(`${config.domains.api}/campusInsight/ci/sessions/${id}/`,
        {
            method: 'GET',
        })
        .then(checkStatus)
        .then(parseJSON)
        .then(response => response);
}

if (basil.get(`authenticated-${year}`)) {
    $('#email').attr('placeholder', 'loading...');
    require.ensure(['../firebase'], (require) => {
        const createFirebaseApp = require('../firebase').default;
        const fb = createFirebaseApp(year);
        const firebaseDb = fb.db;
        const firebaseAuth = fb.auth;

        firebaseAuth.onAuthStateChanged((user) => {
            if (user) {
                const userRef = firebaseDb.ref(`users/${user.uid}/profile`);
                userRef.on('value',
                    (data) => {
                        const profileData = gleanDefaultProfile(user, data.val());
                        $('#email')
                            .attr('placeholder', '')
                            .val(profileData.email);
                    },
                    (error) => {
                        $('#email').attr('placeholder', '');
                    }
                );
            }
        });
    }, 'fbChunk');
}

Promise.resolve()
    .then(() => {
        evalForm.showLoading();
    })
    .then(() => getSessionTitle(sessionId))
    .then((result) => {
        if (result.data.length === 0) {
            throw new Error('Not Found');
        }
        $loadingEl.css({ display: 'none' });
        evalForm.hideLoading();
        $titleEl.text(result.data[0].title);
        $('#session_name').val(result.data[0].title);
        $('.cmc-form').css({ display: 'block' });
    })
    .catch((error) => {
        $titleEl.text('Error Loading Evaluation');
        const subtitle = $('.session-eval-form__sub-title');
        subtitle.text('Please try again later.');
        if (error.message.toLowerCase().indexOf('not found') !== -1) {
            subtitle.text('Please try again later. We were unable to find a session with the supplied ID.');
        }
        $('.session-eval-form__sub-title-two').css({ display: 'none' });
        $('.cmc-form').css({ display: 'none' });
    })
    .then(() => {
        $loadingEl.css({ display: 'none' });
        evalForm.hideLoading();
    });
