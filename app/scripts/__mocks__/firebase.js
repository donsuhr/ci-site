import firebasemock from 'firebase-mock'; // eslint-disable-line import/no-extraneous-dependencies

const mockDatabase = new firebasemock.MockFirebase();
const mockAuth = new firebasemock.MockFirebase();
const mocksdk = firebasemock.MockFirebaseSdk(
    path => (path ? mockDatabase.child(path) : mockDatabase),
    () => mockAuth
);

const firebaseDb = mocksdk.database();

function createFirebaseApp() {
    return {
        db: firebaseDb,
        auth: mockDatabase,
    };
}

export { createFirebaseApp as default, mockDatabase };
