/* global FontFaceObserver */
/* eslint no-var:0 prefer-arrow-callback:0  */

var oswald = new FontFaceObserver('Oswald', {
    weight: 400,
});

var openSans300 = new FontFaceObserver('Open Sans', {
    weight: 300,
});

var openSans400 = new FontFaceObserver('Open Sans', {
    weight: 400,
});

if (sessionStorage.getItem('fonts-loaded')) {
    // fonts cached, add class to document
    document.documentElement.classList.add('font-loaded--secondary');
    document.documentElement.classList.add('font-loaded--primary');
    document.documentElement.classList.add('font-loaded--primary-400');
}

oswald.load().then(function onOswaldLoaded() {
    // document.documentElement.className += ' font-loaded--secondary';
    document.documentElement.classList.add('font-loaded--secondary');
    sessionStorage.setItem('fonts-loaded', true);
}, function onOswaldError() {
    // eslint-disable-next-line no-console
    console.log('Font is not available: oswald');
});

openSans300.load().then(function onOpenSans300loaded() {
    // document.documentElement.className += ' font-loaded--primary';
    document.documentElement.classList.add('font-loaded--primary');
}, function onOpenSans300error() {
    // eslint-disable-next-line no-console
    console.log('Font is not available: open-sans 300');
});

openSans400.load().then(function onOpenSans400loaded() {
    // document.documentElement.className += ' font-loaded--primary-400';
    document.documentElement.classList.add('font-loaded--primary-400');
}, function onOpenSans400error() {
    // eslint-disable-next-line no-console
    console.log('Font is not available: open-sans 400');
});
