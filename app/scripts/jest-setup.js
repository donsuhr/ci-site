import 'isomorphic-fetch'; // eslint-disable-line import/no-extraneous-dependencies
import $ from 'jquery';

global.$ = $;
global.jQuery = $;
window.$ = $;
window.jQuery = $;

global.requestAnimationFrame = (cb) => {
    setTimeout(cb, 0);
};

require('jquery-ui/ui/widget');
