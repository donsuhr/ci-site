// separate to keep index from loading firebase on pages with no
// firebase functionality
import { EventEmitter } from 'events';

const firebaseEvents = new EventEmitter();

export default firebaseEvents;
