import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import config from '../../config';
import firebaseEvents from './firebaseEvents';

const appCache = {};

function createFirebaseApp(year = '2017') {
    if (Object.hasOwnProperty.call(appCache, year)) {
        return appCache[year];
    }
    const app = firebase.initializeApp(
        {
            apiKey: config.firebase[year].apiKey,
            databaseURL: config.firebase[year].databaseURL,
            authDomain: config.firebase[year].authDomain,
        },
        `ci-app-${year}`
    );
    const auth = app.auth();
    auth.onAuthStateChanged(user => {
        if (user) {
            firebaseEvents.emit('login', {
                year,
                user,
            });
        } else {
            firebaseEvents.emit('logout', { year });
        }
    });
    firebaseEvents.emit('loaded', { year });

    appCache[year] = {
        app,
        db: app.database(),
        auth,
        ns: firebase,
    };
    return appCache[year];
}

export { createFirebaseApp as default };
