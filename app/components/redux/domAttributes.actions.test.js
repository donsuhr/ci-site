/* global it,expect,describe */
import * as actions from './domAttributes.actions';

describe('domAttributes actions', () => {
    it('should create an action to set dom attributes', () => {
        const data = {};
        const expectedAction = {
            type: actions.INIT_DOM_ATTRIBUTES,
            data,
        };
        expect(actions.initDomAttributes(data)).toEqual(expectedAction);
    });
});
