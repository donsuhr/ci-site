import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import DevTools from './DevTools';

export default function configureStore(initialState, rootReducer) {
    const enhancer = compose(
        applyMiddleware(thunk),
        DevTools.instrument()
    );

    return createStore(rootReducer, initialState, enhancer);
}
