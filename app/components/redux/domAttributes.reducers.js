import * as actions from './domAttributes.actions';

const defaultState = {};

const domAttributes = (state = defaultState, action) => {
    switch (action.type) {
        case actions.INIT_DOM_ATTRIBUTES:
            return {
                ...state,
                ...action.data,
            };
        default:
            return state;
    }
};

export default domAttributes;

