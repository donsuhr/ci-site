/* global it,expect,describe */
import domAttributes from './domAttributes.reducers';
import * as actions from './domAttributes.actions';

describe('domAttributes reducer', () => {
    describe('domAttributes', () => {
        it('should return the initial state', () => {
            expect(domAttributes(undefined, {})).toEqual({});
        });

        it('should handle INIT_DOM_ATTRIBUTES', () => {
            const state = { group: 'foo' };
            const action = {
                type: actions.INIT_DOM_ATTRIBUTES,
                data: { year: 123 },
            };
            expect(domAttributes(state, action)).toEqual({
                group: 'foo',
                year: 123,
            });
        });
    });
});
