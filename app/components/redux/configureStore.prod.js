import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

export default function configureStore(initialState, rootReducer) {
    const enhancer = applyMiddleware(thunk);
    // Note: only Redux >= 3.1.0 supports passing enhancer as third argument.
    // See https://github.com/rackt/redux/releases/tag/v3.1.0
    return createStore(rootReducer, initialState, enhancer);
}
