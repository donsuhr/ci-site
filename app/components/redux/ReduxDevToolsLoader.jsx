/* eslint-disable global-require */

let dt;

if (process.env.NODE_ENV === 'production') {
    dt = () => null;
} else {
    dt = require('./DevTools').default;
}
const ReduxDevToolsLoader = dt;

export default ReduxDevToolsLoader;
