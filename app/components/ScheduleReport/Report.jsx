import React from 'react';
import SessionChart from './charts/SessionChart.container';
import PctSessionChart from './charts/PctSessionChart.container';
import PctTopicChart from './charts/PctTopicChart.container';
import TopicChart from './charts/TopicChart.container';
import UserInfo from './charts/UserInfo.container';

const Report = () => (
    <div>
        <PctSessionChart />
        <PctTopicChart />
        <SessionChart />
        <TopicChart />
        <UserInfo />
    </div>
);

export default Report;
