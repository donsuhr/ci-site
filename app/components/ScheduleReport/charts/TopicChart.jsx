import React from 'react';
import PropTypes from 'prop-types';
import {
    ResponsiveContainer,
    Radar,
    RadarChart,
    PolarGrid,
    PolarAngleAxis,
    PolarRadiusAxis,
    Tooltip,
} from 'recharts';
import Loading from '../../Loading';

const TopicChart = ({
    data,
    loading,
    title = 'Topics',
    className = 'topics-chart',
}) => {
    if (loading) {
        return (
            <div className={className}>
                <h2 className="cmc-article__intro-p">{title}</h2>
                <Loading>{loading}</Loading>
            </div>
        );
    }
    return (
        <div className={className}>
            <h2 className="cmc-article__intro-p">{title}</h2>
            <ResponsiveContainer>
                <RadarChart data={data}>
                    <Radar
                        name="Topic"
                        dataKey="d1"
                        stroke="#78A22F"
                        fill="#78A22F"
                        fillOpacity={0.6}
                    />
                    <PolarGrid />
                    <PolarAngleAxis
                        dataKey="topic"
                        tick={{ fontSize: '12px' }}
                    />
                    <PolarRadiusAxis />
                    <Tooltip />
                </RadarChart>
            </ResponsiveContainer>
        </div>
    );
};

TopicChart.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    data: PropTypes.array,
    loading: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
    ]),
    title: PropTypes.string,
    className: PropTypes.string,
};

export default TopicChart;
