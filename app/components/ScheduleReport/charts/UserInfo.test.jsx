/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import UserInfo from './UserInfo';

describe('ScheduleReport charts UserInfo', () => {
    it('renders', () => {
        const wrapper = shallow(
            <UserInfo
                loading={false}
                data={[
                    {
                        id: 123,
                        sessions: 3,
                        pct: 2,
                        total: 5,
                    },
                    {
                        id: 321,
                        sessions: 3,
                        pct: 4,
                        total: 7,
                    },
                ]}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const wrapper = shallow(<UserInfo loading="Foo Loading" />);
        expect(wrapper.find('Loading').dive().text()).toContain('Foo Loading');
    });

    it('renders total users', () => {
        const wrapper = shallow(
            <UserInfo
                loading={false}
                data={[
                    {
                        id: 123,
                        sessions: 3,
                        pct: 2,
                        total: 5,
                    },
                    {
                        id: 321,
                        sessions: 3,
                        pct: 4,
                        total: 7,
                    },
                ]}
            />
        );
        expect(wrapper.find('p').text()).toContain('Total Users: 2');
        expect(wrapper.find('p').text()).toContain('1 or more PCT: 2 of 2');
        expect(wrapper.find('p').text()).toContain(
            '1 or more Breakout Session: 2 of 2'
        );
    });
});
