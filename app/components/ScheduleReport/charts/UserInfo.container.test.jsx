/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { UserInfoContainer } from './UserInfo.container';

describe('SupportConsultations UserInfo.container', () => {
    it('renders', () => {
        const wrapper = shallow(
            <UserInfoContainer
                allUsers={{ hasEverLoaded: true, fetching: false }}
            />
        );
        expect(wrapper.find('UserInfo')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading data', () => {
        const wrapper = shallow(
            <UserInfoContainer allUsers={{ hasEverLoaded: false }} />
        );
        expect(wrapper.find('UserInfo').props().loading).toContain('Loading');
    });
});
