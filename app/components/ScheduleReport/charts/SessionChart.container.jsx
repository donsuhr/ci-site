import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import sortBy from 'lodash/sortBy';
import SessionChart from './SessionChart';
import { fetchSessions } from '../../SessionBrowser/actions';
import {
    countSessionSignups,
    PropType as sessionsPropType,
} from '../../SessionBrowser/reducer';
import { listenToAllUsers } from '../../firebase/allUsers.actions';
import { PropType as allUsersPropType } from '../../firebase/allUsers.reducer';

export class SessionChartContainer extends React.Component {
    static propTypes = {
        sessions: sessionsPropType,
        // eslint-disable-next-line react/forbid-prop-types
        sessionSignupCount: PropTypes.object,
        fetchSessions: PropTypes.func,
        listenToAllUsers: PropTypes.func,
        allUsers: allUsersPropType,
    };

    componentDidMount() {
        if (
            this.props.sessions.hasEverLoaded === false &&
            this.props.sessions.fetching === false
        ) {
            this.props.fetchSessions();
        }
        if (
            this.props.allUsers.hasEverLoaded === false &&
            this.props.allUsers.fetching === false
        ) {
            this.props.listenToAllUsers();
        }
    }

    render() {
        let loading = false;
        if (
            !this.props.sessions.hasEverLoaded ||
            this.props.sessions.fetching
        ) {
            loading = 'Loading Data...';
        }
        const data = Object.keys(
            this.props.sessionSignupCount
        ).reduce((acc, x) => {
            const item = this.props.sessionSignupCount[x];
            acc.push({
                name: item.title,
                d1: item.count,
            });
            return acc;
        }, []);
        const total = Object.keys(this.props.sessionSignupCount).reduce(
            (acc, x) => acc + this.props.sessionSignupCount[x].count,
            0
        );
        const sorted = sortBy(data, 'd1');
        return (
            <SessionChart
                data={sorted.reverse()}
                title={`Breakout Sessions (${total} signups)`}
                loading={loading}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        sessions: state.sessions,
        allUsers: state.allUsers,
        sessionSignupCount: countSessionSignups(state.sessions, state.allUsers),
    };
}

const SessionChartConnected = connect(mapStateToProps, {
    fetchSessions,
    listenToAllUsers,
})(SessionChartContainer);

export default SessionChartConnected;
