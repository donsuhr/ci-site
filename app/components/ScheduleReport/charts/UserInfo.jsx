import React from 'react';
import PropTypes from 'prop-types';
import sortBy from 'lodash/sortBy';
import Loading from '../../Loading';
import ActiveUserChart from './ActiveUserChart';

const UserInfo = ({ data, loading }) => {
    if (loading) {
        return (
            <div className="user-info">
                <h2 className="cmc-article__intro-p">User Info</h2>
                <Loading>{loading}</Loading>
            </div>
        );
    }

    const usersWithPct = data.filter(x => x.pct > 0);
    const usersWithPctCount = usersWithPct.length;
    const PctSum = usersWithPct.reduce((acc, x) => acc + x.pct, 0);
    const PctAvg = Math.floor(PctSum / usersWithPctCount);

    const usersWithBS = data.filter(x => x.sessions > 0);
    const usersWithBSCount = usersWithBS.length;
    const SessionSum = usersWithBS.reduce((acc, x) => acc + x.sessions, 0);
    const SessionAvg = Math.floor(SessionSum / usersWithBSCount);

    const usersWithNone = data.filter(x => x.sessions === 0 && x.pct === 0).length;

    let chartData = sortBy(data, 'total');
    chartData = chartData.reverse();
    chartData.length = 20;

    return (
        <div className="user-info">
            <h2 className="cmc-article__intro-p">User Info</h2>
            <p>
                Total Users: {data.length} - Does not count users with no activity ever<br />
                Users with 1 or more PCT:{'\u00A0'}
                {usersWithPctCount} of {data.length}{'\u00A0'}
                ({Math.floor((usersWithPctCount / data.length) * 100)}%)
                - average {PctAvg} per user
                <br />
                Users with 1 or more Breakout Session:{'\u00A0'}
                {usersWithBSCount} of {data.length}{'\u00A0'}
                ({Math.floor((usersWithBSCount / data.length) * 100)}%)
                - average {SessionAvg} per user
                <br />
                Users with no schedule:{'\u00A0'}
                {usersWithNone} of {data.length}{'\u00A0'}
                ({Math.floor((usersWithNone / data.length) * 100)}%)
                - these users may be admins or have canceled{'\u00A0'}
                a support conciliation or emptied schedule.
                <br />
            </p>
            <ActiveUserChart
                data={chartData}
            />
        </div>
    );
};

UserInfo.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    data: PropTypes.array,
    loading: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
    ]),
};

export default UserInfo;
