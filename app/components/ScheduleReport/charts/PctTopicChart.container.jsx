import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import sortBy from 'lodash/sortBy';
import TopicChart from './TopicChart';
import { fetchPcts } from '../../PctBrowser/actions';
import {
    countPctTopicSignups,
    PropType as pctPropType,
} from '../../PctBrowser/reducer';
import { listenToAllUsers } from '../../firebase/allUsers.actions';
import { PropType as allUsersPropType } from '../../firebase/allUsers.reducer';

export class PctTopicChartContainer extends React.Component {
    static propTypes = {
        // eslint-disable-next-line react/forbid-prop-types
        topicCount: PropTypes.object,
        fetchPcts: PropTypes.func,
        listenToAllUsers: PropTypes.func,
        allUsers: allUsersPropType,
        pct: pctPropType,
    };

    componentDidMount() {
        if (
            this.props.pct.hasEverLoaded === false &&
            this.props.pct.fetching === false
        ) {
            this.props.fetchPcts();
        }
        if (
            this.props.allUsers.hasEverLoaded === false &&
            this.props.allUsers.fetching === false
        ) {
            this.props.listenToAllUsers();
        }
    }

    render() {
        let loading = false;
        if (!this.props.pct.hasEverLoaded || this.props.pct.fetching) {
            loading = 'Loading Data...';
        }
        if (
            !this.props.allUsers.hasEverLoaded ||
            this.props.allUsers.fetching
        ) {
            loading = 'Loading Users...';
        }
        const data = loading
            ? []
            : Object.keys(this.props.topicCount).reduce((acc, x) => {
                const item = this.props.topicCount[x];
                acc.push({
                    topic: item.title,
                    d1: item.count,
                });
                return acc;
            }, []);
        const sorted = sortBy(data, 'd1');
        return (
            <TopicChart
                data={sorted.reverse()}
                title="PCT Topics"
                className="pct-topic-chart"
                loading={loading}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        pct: state.pct,
        allUsers: state.allUsers,
        topicCount: countPctTopicSignups(state.pct, state.allUsers),
    };
}

const PctTopicChartConnected = connect(mapStateToProps, {
    fetchPcts,
    listenToAllUsers,
})(PctTopicChartContainer);

export default PctTopicChartConnected;
