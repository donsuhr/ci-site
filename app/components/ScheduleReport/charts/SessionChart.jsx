import React from 'react';
import PropTypes from 'prop-types';
import {
    ResponsiveContainer,
    Bar,
    BarChart,
    XAxis,
    YAxis,
    Tooltip,
    CartesianGrid,
} from 'recharts';
import Loading from '../../Loading';

const SessionChart = ({
    data,
    loading,
    className = 'session-chart',
    title = '',
}) => {
    if (loading) {
        return (
            <div className={className}>
                <h2 className="cmc-article__intro-p">{title}</h2>
                <Loading>{loading}</Loading>
            </div>
        );
    }
    return (
        <div className={className}>
            <h2 className="cmc-article__intro-p">{title}</h2>
            <ResponsiveContainer>
                <BarChart data={data} layout="vertical">
                    <XAxis type="number" />
                    <YAxis
                        dataKey="name"
                        type="category"
                        interval={0}
                        width={325}
                        tick={{ fontSize: '10px' }}
                    />
                    <CartesianGrid strokeDasharray="3 3" />
                    <Tooltip />
                    <Bar
                        dataKey="d1"
                        fill="#78A22F"
                        maxBarSize={30}
                        name="Sign Ups"
                        isAnimationActive={false}
                    />
                </BarChart>
            </ResponsiveContainer>
        </div>
    );
};

SessionChart.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    data: PropTypes.array,
    title: PropTypes.string,
    loading: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
    ]),
    className: PropTypes.string,
};

export default SessionChart;
