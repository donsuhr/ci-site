/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ActiveUserChart from './ActiveUserChart';

describe('ScheduleReport charts ActiveUserChart', () => {
    it('renders', () => {
        const wrapper = shallow(<ActiveUserChart />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const wrapper = shallow(<ActiveUserChart loading="Foo Loading" />);
        expect(wrapper.find('Loading').dive().text()).toContain('Foo Loading');
    });
});
