import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import sortBy from 'lodash/sortBy';
import SessionChart from './SessionChart';
import { fetchPcts } from '../../PctBrowser/actions';
import {
    countPctSignups,
    PropType as pctPropType,
} from '../../PctBrowser/reducer';
import { listenToAllUsers } from '../../firebase/allUsers.actions';
import { PropType as allUsersPropType } from '../../firebase/allUsers.reducer';

export class PctSessionChartContainer extends React.Component {
    static propTypes = {
        // eslint-disable-next-line react/forbid-prop-types
        sessionSignupCount: PropTypes.object,
        listenToAllUsers: PropTypes.func,
        fetchPcts: PropTypes.func,
        pct: pctPropType,
        allUsers: allUsersPropType,
    };

    componentDidMount() {
        if (
            this.props.pct.hasEverLoaded === false &&
            this.props.pct.fetching === false
        ) {
            this.props.fetchPcts();
        }
        if (
            this.props.allUsers.hasEverLoaded === false &&
            this.props.allUsers.fetching === false
        ) {
            this.props.listenToAllUsers();
        }
    }

    render() {
        let loading = false;
        if (!this.props.pct.hasEverLoaded || this.props.pct.fetching) {
            loading = 'Loading Data...';
        }
        const data = Object.keys(
            this.props.sessionSignupCount
        ).reduce((acc, x) => {
            const item = this.props.sessionSignupCount[x];
            acc.push({
                name: item.title,
                d1: item.count,
            });
            return acc;
        }, []);
        const sorted = sortBy(data, 'd1');
        const total = Object.keys(this.props.sessionSignupCount).reduce(
            (acc, x) => acc + this.props.sessionSignupCount[x].count,
            0
        );
        return (
            <SessionChart
                data={sorted.reverse()}
                title={`Pre Conference Training (${total} signups)`}
                className="preconf-chart"
                loading={loading}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        pct: state.pct,
        allUsers: state.allUsers,
        sessionSignupCount: countPctSignups(state.pct, state.allUsers),
    };
}

const PctSessionChartConnected = connect(mapStateToProps, {
    fetchPcts,
    listenToAllUsers,
})(PctSessionChartContainer);

export default PctSessionChartConnected;
