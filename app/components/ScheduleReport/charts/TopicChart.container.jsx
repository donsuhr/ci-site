import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import sortBy from 'lodash/sortBy';
import TopicChart from './TopicChart';
import { fetchSessions } from '../../SessionBrowser/actions';
import {
    countSessionTopicSignups,
    PropType as sessionsPropType,
} from '../../SessionBrowser/reducer';
import { listenToAllUsers } from '../../firebase/allUsers.actions';
import { PropType as allUsersPropType } from '../../firebase/allUsers.reducer';

export class TopicChartContainer extends React.Component {
    static propTypes = {
        sessions: sessionsPropType,
        // eslint-disable-next-line react/forbid-prop-types
        topicCount: PropTypes.object,
        fetchSessions: PropTypes.func,
        listenToAllUsers: PropTypes.func,
        allUsers: allUsersPropType,
    };

    componentDidMount() {
        if (
            this.props.sessions.hasEverLoaded === false &&
            this.props.sessions.fetching === false
        ) {
            this.props.fetchSessions();
        }
        if (
            this.props.allUsers.hasEverLoaded === false &&
            this.props.allUsers.fetching === false
        ) {
            this.props.listenToAllUsers();
        }
    }

    render() {
        let loading = false;
        if (
            !this.props.sessions.hasEverLoaded ||
            this.props.sessions.fetching
        ) {
            loading = 'Loading Data...';
        }
        if (
            !this.props.allUsers.hasEverLoaded ||
            this.props.allUsers.fetching
        ) {
            loading = 'Loading Users...';
        }
        const data = loading
            ? []
            : Object.keys(this.props.topicCount).reduce((acc, x) => {
                const item = this.props.topicCount[x];
                acc.push({
                    topic: item.title,
                    d1: item.count,
                });
                return acc;
            }, []);
        const sorted = sortBy(data, 'd1');
        return (
            <TopicChart
                data={sorted.reverse()}
                title="Session Topics"
                className="session-topic-chart"
                loading={loading}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        sessions: state.sessions,
        allUsers: state.allUsers,
        topicCount: countSessionTopicSignups(state.sessions, state.allUsers),
    };
}

const TopicChartConnected = connect(mapStateToProps, {
    fetchSessions,
    listenToAllUsers,
})(TopicChartContainer);

export default TopicChartConnected;
