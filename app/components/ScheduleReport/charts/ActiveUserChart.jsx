import React from 'react';
import PropTypes from 'prop-types';
import {
    ResponsiveContainer,
    Bar,
    BarChart,
    Legend,
    XAxis,
    YAxis,
    Tooltip,
    CartesianGrid,
} from 'recharts';
import Loading from '../../Loading';

const Chart = ({ data, loading }) => {
    if (loading) {
        return (
            <div className="active-user-chart">
                <Loading>{loading}</Loading>
            </div>
        );
    }
    return (
        <div className="active-user-chart">
            <ResponsiveContainer>
                <BarChart data={data} layout="vertical">
                    <XAxis type="number" />
                    <YAxis
                        dataKey="id"
                        type="category"
                        interval={0}
                        width={200}
                        tick={{ fontSize: '10px' }}
                    />
                    <CartesianGrid strokeDasharray="3 3" />
                    <Tooltip />
                    <Legend />

                    <Bar
                        dataKey="sessions"
                        stackId="a"
                        fill="#00468A"
                        name="Breakout"
                        maxBarSize={30}
                        isAnimationActive={false}
                    />
                    <Bar
                        dataKey="pct"
                        stackId="a"
                        fill="#78A22F"
                        name="PCT"
                        maxBarSize={30}
                        isAnimationActive={false}
                    />
                </BarChart>
            </ResponsiveContainer>
        </div>
    );
};

Chart.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    data: PropTypes.array,
    loading: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
    ]),
};

export default Chart;
