/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { TopicChartContainer } from './TopicChart.container';

describe('SupportConsultations charts TopicChart.container', () => {
    it('renders', () => {
        const wrapper = shallow(
            <TopicChartContainer
                allUsers={{ hasEverLoaded: true, fetching: false }}
                sessions={{ hasEverLoaded: true, fetching: false }}
                topicCount={{
                    'topic 1': { topic: 'topic 1', count: 2 },
                    'topic 3': { topic: 'topic 3', count: 1 },
                }}
            />
        );
        expect(wrapper.find('TopicChart')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading data', () => {
        const wrapper = shallow(
            <TopicChartContainer
                sessions={{ hasEverLoaded: false, fetching: true }}
                allUsers={{ hasEverLoaded: true, fetching: false }}
            />
        );
        expect(wrapper.find('TopicChart').props().loading).toContain(
            'Loading Data'
        );
    });
    it('renders loading users', () => {
        const wrapper = shallow(
            <TopicChartContainer
                sessions={{ hasEverLoaded: true, fetching: false }}
                allUsers={{ hasEverLoaded: false, fetching: true }}
            />
        );
        expect(wrapper.find('TopicChart').props().loading).toContain(
            'Loading Users'
        );
    });
});
