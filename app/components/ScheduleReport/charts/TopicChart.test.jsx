/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import TopicChart from './TopicChart';

describe('ScheduleReport charts TopicChart', () => {
    it('renders', () => {
        const wrapper = shallow(<TopicChart />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const wrapper = shallow(<TopicChart loading="Foo Loading" />);
        expect(wrapper.find('Loading').dive().text()).toContain('Foo Loading');
    });
});
