import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import UserInfo from './UserInfo';
import { fetchSessions } from '../../SessionBrowser/actions';
import {
    countUserActivity,
    PropType as allUsersPropType,
} from '../../firebase/allUsers.reducer';
import { listenToAllUsers } from '../../firebase/allUsers.actions';

export class UserInfoContainer extends React.Component {
    static propTypes = {
        // eslint-disable-next-line react/forbid-prop-types
        userActivityCount: PropTypes.array,
        listenToAllUsers: PropTypes.func,
        allUsers: allUsersPropType,
    };

    componentDidMount() {
        if (
            this.props.allUsers.hasEverLoaded === false &&
            this.props.allUsers.fetching === false
        ) {
            this.props.listenToAllUsers();
        }
    }

    render() {
        let loading = false;
        if (
            !this.props.allUsers.hasEverLoaded ||
            this.props.allUsers.fetching
        ) {
            loading = 'Loading Users...';
        }
        return (
            <UserInfo data={this.props.userActivityCount} loading={loading} />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        allUsers: state.allUsers,
        userActivityCount: countUserActivity(state.allUsers),
    };
}

const SessionChartConnected = connect(mapStateToProps, {
    fetchSessions,
    listenToAllUsers,
})(UserInfoContainer);

export default SessionChartConnected;
