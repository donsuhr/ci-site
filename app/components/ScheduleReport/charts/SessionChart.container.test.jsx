/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { SessionChartContainer } from './SessionChart.container';

describe('SupportConsultations charts SessionChart.container', () => {
    it('renders', () => {
        const wrapper = shallow(
            <SessionChartContainer
                sessions={{ hasEverLoaded: true, fetching: false }}
                sessionSignupCount={{
                    123: { title: 'session 1', count: 2 },
                    456: { topic: 'sesison 3', count: 1 },
                }}
            />
        );
        expect(wrapper.find('SessionChart')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading data', () => {
        const wrapper = shallow(
            <SessionChartContainer
                sessions={{ hasEverLoaded: false, fetching: true }}
                sessionSignupCount={{
                    123: { title: 'session 1', count: 2 },
                    456: { topic: 'sesison 3', count: 1 },
                }}
            />
        );
        expect(wrapper.find('SessionChart').props().loading).toContain(
            'Loading Data'
        );
    });
});
