/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { PctTopicChartContainer } from './PctTopicChart.container';

describe('SupportConsultations charts PctTopicChart.container', () => {
    it('renders', () => {
        const wrapper = shallow(
            <PctTopicChartContainer
                allUsers={{ hasEverLoaded: true, fetching: false }}
                pct={{ hasEverLoaded: true, fetching: false }}
                topicCount={{
                    'topic 1': { topic: 'topic 1', count: 2 },
                    'topic 3': { topic: 'topic 3', count: 1 },
                }}
            />
        );
        expect(wrapper.find('TopicChart')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading data', () => {
        const wrapper = shallow(
            <PctTopicChartContainer
                pct={{ hasEverLoaded: false, fetching: true }}
                allUsers={{ hasEverLoaded: true, fetching: false }}
            />
        );
        expect(wrapper.find('TopicChart').props().loading).toContain(
            'Loading Data'
        );
    });
    it('renders loading users', () => {
        const wrapper = shallow(
            <PctTopicChartContainer
                pct={{ hasEverLoaded: true, fetching: false }}
                allUsers={{ hasEverLoaded: false, fetching: true }}
            />
        );
        expect(wrapper.find('TopicChart').props().loading).toContain(
            'Loading Users'
        );
    });
});
