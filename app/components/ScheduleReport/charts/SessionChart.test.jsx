/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SessionChart from './SessionChart';

describe('ScheduleReport charts SessionChart', () => {
    it('renders', () => {
        const wrapper = shallow(<SessionChart />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const wrapper = shallow(<SessionChart loading="Foo Loading" />);
        expect(wrapper.find('Loading').dive().text()).toContain('Foo Loading');
    });
});
