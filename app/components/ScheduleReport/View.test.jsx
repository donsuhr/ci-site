/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { ReportContainer } from './View';

describe('ScheduleReport ReportContainer', () => {
    it('renders', () => {
        const wrapper = shallow(
            <ReportContainer
                listenToAuth={jest.fn()}
                listenToAllUsers={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    initting: false,
                    isAuth: true,
                    loadingRoot: false,
                    isAdmin: true,
                }}
                allUsers={{
                    hasEverLoaded: true,
                    fetching: false,
                }}
            />
        );
        expect(wrapper.find('Report')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading data', () => {
        const wrapper = shallow(
            <ReportContainer
                listenToAuth={jest.fn()}
                listenToAllUsers={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    initting: true,
                }}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
        expect(wrapper.find('Loading').dive().text()).toContain('Loading');
    });

    it('renders authenticating', () => {
        const wrapper = shallow(
            <ReportContainer
                listenToAuth={jest.fn()}
                listenToAllUsers={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    initting: false,
                    loadingRoot: true,
                }}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
        expect(wrapper.find('Loading').dive().text()).toContain(
            'Authenticating'
        );
    });

    it('renders sign in button', () => {
        const wrapper = shallow(
            <ReportContainer
                listenToAuth={jest.fn()}
                listenToAllUsers={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    initting: false,
                    loadingRoot: false,
                    isAuth: false,
                }}
            />
        );
        expect(wrapper.find('button').text()).toContain('Sign In');
    });

    it('renders auth required', () => {
        const wrapper = shallow(
            <ReportContainer
                listenToAuth={jest.fn()}
                listenToAllUsers={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    initting: false,
                    loadingRoot: false,
                    isAuth: true,
                }}
            />
        );
        expect(wrapper.find('p').text()).toContain('Admin rights required');
    });

    it('renders loading data', () => {
        const wrapper = shallow(
            <ReportContainer
                listenToAuth={jest.fn()}
                listenToAllUsers={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    initting: false,
                    loadingRoot: false,
                    isAuth: true,
                    isAdmin: true,
                }}
                allUsers={{
                    hasEverLoaded: false,
                    fetching: true,
                }}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
        expect(wrapper.find('Loading').dive().text()).toContain('Loading Data');
    });
});
