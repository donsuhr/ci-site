import { combineReducers } from 'redux';

import sessions from '../SessionBrowser/reducer';
import pct from '../PctBrowser/reducer';
import user from '../firebase/auth.reducer';
import listeners from '../firebase/listeners.reducer';
import allUsers from '../firebase/allUsers.reducer';
import domAttributes from '../redux/domAttributes.reducers';

export default combineReducers({
    sessions,
    pct,
    user,
    allUsers,
    listeners,
    domAttributes,
});
