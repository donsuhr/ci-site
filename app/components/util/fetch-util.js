import { checkStatus, parseJSON } from 'fetch-json-helpers';

export default function fetchItems({
    dispatch,
    getState,
    startAction,
    endpoint,
    endAction,
    errorAction,
    key,
    token,
    credentials = 'omit',
}) {
    const state = getState ? getState()[key] : false;
    if (!state || (state && !state.fetching)) {
        dispatch(startAction());
        const headers = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        };
        if (token) {
            headers.authorization = `Bearer ${token}`;
        }
        return fetch(endpoint, {
            credentials,
            method: 'GET',
            headers,
        })
            .then(checkStatus)
            .then(parseJSON)
            .then(result => dispatch(endAction(result)))
            .catch(err => {
                dispatch(errorAction(err));
            });
    }
    return null;
}
