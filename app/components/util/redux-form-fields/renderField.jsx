/* eslint-disable react/forbid-prop-types */

import React from 'react';
import PropTypes from 'prop-types';

class renderField extends React.Component {
    static propTypes = {
        input: PropTypes.object,
        meta: PropTypes.object,
        label: PropTypes.string.isRequired,
        type: PropTypes.string.isRequired,
        inputRef: PropTypes.func,
        labelClassName: PropTypes.string,
    };

    render() {
        const {
            input,
            label,
            type,
            labelClassName,
            meta: { touched, error },
            inputRef,
        } = this.props;
        return (
            <div>
                <label className={labelClassName} htmlFor={input.name}>
                    {label}
                </label>
                <div className="input-wrapper">
                    <input
                        {...input}
                        ref={inputRef}
                        placeholder={label}
                        type={type}
                        id={input.name}
                    />
                    {touched && error && <span>{error}</span>}
                </div>
            </div>
        );
    }
}

export default renderField;
