/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import renderField from './renderField';

describe('util redux-form-fields renderField', () => {
    it('renders', () => {
        const wrapper = shallow(<renderField label="label" type="text" />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
