const moment = require('moment-timezone/moment-timezone');
moment.tz.load(require('./custom-moment-tz.json'));

module.exports = moment;
