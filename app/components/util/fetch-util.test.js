/* global it,expect,describe,beforeEach,afterEach,jest */

import nock from 'nock';
import config from '../../../config';
import fetchItems from './fetch-util';

const endpoint = `${config.domains.api}/test-endpoint`;

describe('util fetch-util', () => {
    beforeEach(() => {
        nock.disableNetConnect();
        nock(`${config.domains.api}/`)
            .get('/test-endpoint')
            .reply(200, '{"success": true}');
    });

    afterEach(() => {
        nock.cleanAll();
    });

    it('dispatches startAction when starting', () => {
        const startAction = jest.fn(() => 'startAction');
        const errorAction = jest.fn();
        const dispatch = jest.fn();
        fetchItems({
            errorAction,
            dispatch,
            startAction,
            endpoint,
        });
        expect(startAction).toBeCalled();
        expect(dispatch).toBeCalledWith('startAction');
    });

    it('does not refetch currently fetching items', () => {
        const startAction = jest.fn(() => 'startAction');
        const dispatch = jest.fn();
        const getState = () => ({ test: { fetching: true } });
        const errorAction = jest.fn();

        fetchItems({
            errorAction,
            dispatch,
            startAction,
            endpoint,
            getState,
            key: 'test',
        });
        expect(dispatch).not.toBeCalled();
    });

    it('dispatches endAction when its done', () => {
        const startAction = jest.fn();
        const errorAction = jest.fn();
        const endAction = jest.fn(() => 'endAction');
        const dispatch = jest.fn();

        return fetchItems({
            dispatch,
            startAction,
            endpoint,
            endAction,
            errorAction,
        }).then(() => {
            expect(errorAction).not.toBeCalled();
            expect(endAction).toBeCalledWith({ success: true });
            expect(dispatch).toBeCalledWith('endAction');
        });
    });

    it('dispatches errorAction on error', () => {
        nock.cleanAll();
        nock(`${config.domains.api}/`)
            .get('/test-endpoint')
            .reply(500, '{"error": true}');
        const startAction = jest.fn();
        const errorAction = jest.fn(() => 'errorAction');
        const endAction = jest.fn();
        const dispatch = jest.fn();

        return fetchItems({
            dispatch,
            startAction,
            endpoint,
            endAction,
            errorAction,
        }).then(() => {
            expect(errorAction).toBeCalled();
            expect(dispatch).toBeCalledWith('errorAction');
        });
    });
});
