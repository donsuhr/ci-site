/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ConflictWarning from './ConflictWarning';

describe('ChooseAnalystTime ConflictWarning', () => {
    it('renders', () => {
        const wrapper = shallow(
            <ConflictWarning
                time="2017-04-28T13:00:00.000Z"
                conflicts={[{ _id: 1, title: 'title1' }]}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
