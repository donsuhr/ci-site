/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import DayItemsList from './DayItemsList';

const dayItems = [
    {
        _id: '589e04cb3414ba604118d257',
        end: '2017-03-10T15:22:00.000Z',
        start: '2017-03-10T15:21:00.000Z',
    },
    {
        _id: '5860926699ab5cb274586d00',
        end: '2017-04-28T13:30:00.000Z',
        start: '2017-04-28T13:00:00.000Z',
    },
    {
        _id: '5860926699ab5cb274586d01',
        end: '2017-04-28T14:00:00.000Z',
        start: '2017-04-28T13:30:00.000Z',
    },
];

describe('ChooseAnalystTime DayItemsList', () => {
    it('renders', () => {
        const wrapper = shallow(
            <DayItemsList
                dayItems={dayItems}
                getSupportConsultationConflicts={() => []}
                getScheduleItemIsAvailable={() => true}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('calls onTimeSelected', () => {
        const onTimeSelected = jest.fn();
        const wrapper = shallow(
            <DayItemsList
                dayItems={dayItems}
                getSupportConsultationConflicts={() => []}
                getScheduleItemIsAvailable={() => true}
                onTimeSelected={onTimeSelected}
            />
        );
        expect(onTimeSelected).not.toHaveBeenCalled();
        wrapper
            .find('button')
            .first()
            .simulate('click', {
                preventDefault() {},
            });
        expect(onTimeSelected).toHaveBeenCalled();
    });
});
