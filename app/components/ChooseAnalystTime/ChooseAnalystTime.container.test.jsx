/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { ChooseAnalystTimeContainer } from './ChooseAnalystTime.container';

describe('ChooseAnalystTime ConflictWarning', () => {
    it('renders', () => {
        const history = {
            replace: jest.fn(),
        };
        const wrapper = shallow(
            <ChooseAnalystTimeContainer
                analysts={{ hasEverLoaded: true, fetching: false }}
                sessions={{ hasEverLoaded: true, fetching: false }}
                schedule={{
                    hasEverLoadedSessions: true,
                    fetching: false,
                    hasEverLoadedPcts: true,
                }}
                pct={{ hasEverLoaded: true, fetching: false }}
                supportConsultations={{ hasEverLoadedUsedAvailability: true }}
                history={history}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
