import React from 'react';
import PropTypes from 'prop-types';
import ReactTooltip from 'react-tooltip';
import moment from '../util/load-moment-tz';

const ConflictWarning = ({ conflicts, time }) => {
    const id = moment(time).format('YYYYMMDDHHmm');
    if (conflicts.length > 0) {
        return (
            <span>
                <button
                    className="support-consultations__conflict-warning-trigger"
                    data-tip
                    data-for={id}
                >
                    Warning
                </button>
                <ReactTooltip
                    class="support-consultations__conflict-tooltip"
                    id={id}
                >
                    <p>You have a scheduled item at this time:</p>
                    <ul>
                        {conflicts.map(x => <li key={x._id}>{x.title}</li>)}
                    </ul>
                </ReactTooltip>
            </span>
        );
    }
    return null;
};

ConflictWarning.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    conflicts: PropTypes.array,
    time: PropTypes.oneOfType([
        PropTypes.instanceOf(Date),
        PropTypes.string,
    ]),
};

export default ConflictWarning;
