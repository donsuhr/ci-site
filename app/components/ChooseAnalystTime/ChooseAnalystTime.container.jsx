/* eslint-disable react/forbid-prop-types */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchAnalysts } from '../ChooseAnalyst/actions';
import { availabilityByDay } from '../ChooseAnalyst/reducer';
import { fetchSessions } from '../SessionBrowser/actions';
import { fetchPcts } from '../PctBrowser/actions';
import ChooseAnalystTime from './ChooseAnalystTime';
import { selectionComplete } from '../SupportConsultations/actions';
import { listenToAuth } from '../firebase/auth.actions';
import {
    listenToScheduleSessions,
    listenToSchedulePct,
} from '../firebase/schedule.actions';
import {
    getScheduledSessions,
    getSupportConsultationConflicts,
} from '../firebase/schedule.reducer';
import {
    addSupportCouncilToSchedule,
    listenToUsedAvailability,
} from '../firebase/schedule.support.actions';
import { getScheduleItemIsAvailable } from '../firebase/schedule.support.reducer';

export class ChooseAnalystTimeContainer extends React.Component {
    static propTypes = {
        analysts: PropTypes.object,
        analyst: PropTypes.object,
        sessions: PropTypes.object,
        user: PropTypes.object,
        pct: PropTypes.object,
        schedule: PropTypes.object,
        supportConsultations: PropTypes.object,
        availabilityByDay: PropTypes.object,
        scheduledSessions: PropTypes.object,
        fetchAnalysts: PropTypes.func,
        fetchSessions: PropTypes.func,
        fetchPcts: PropTypes.func,
        listenToAuth: PropTypes.func,
        listenToScheduleSessions: PropTypes.func,
        listenToSchedulePct: PropTypes.func,
        listenToUsedAvailability: PropTypes.func,
        addSupportCouncilToSchedule: PropTypes.func,
        selectionComplete: PropTypes.func,
        getScheduleItemIsAvailable: PropTypes.func,
        chosenAnalystId: PropTypes.string,
        history: PropTypes.object.isRequired,
    };

    componentWillMount() {
        if (!this.props.chosenAnalystId) {
            this.props.history.replace('/choose-analyst');
        }
    }

    componentDidMount() {
        if (
            this.props.analysts.hasEverLoaded === false &&
            this.props.analysts.fetching === false
        ) {
            this.props.fetchAnalysts();
        }
        if (
            this.props.sessions.hasEverLoaded === false &&
            this.props.sessions.fetching === false
        ) {
            this.props.fetchSessions();
        }
        if (
            this.props.pct.hasEverLoaded === false &&
            this.props.pct.fetching === false
        ) {
            this.props.fetchPcts();
        }
        this.props.listenToAuth().then(user => {
            this.props.listenToScheduleSessions(user.uid);
            this.props.listenToSchedulePct(user.uid);
            this.props.listenToUsedAvailability();
        });
        setTimeout(() => {
            if (!this.props.user.isAuth) {
                this.props.history.replace('/');
            }
        }, 1000);
    }

    componentWillUnmount() {
        window.scrollTo(0, 0);
    }

    onTimeSelected = (availabilityId, start, end) => {
        this.props
            .addSupportCouncilToSchedule({
                availabilityId,
                analyst: this.props.analyst,
                user: this.props.user,
                start,
                end,
            })
            .then(ref => {
                this.props.history.push('/');
                this.props.selectionComplete();
            });
    };

    render() {
        const fetching =
            !this.props.analysts.hasEverLoaded ||
            this.props.analysts.fetching ||
            !this.props.sessions.hasEverLoaded ||
            this.props.sessions.fetching ||
            !this.props.schedule.hasEverLoadedSessions ||
            this.props.schedule.fetching ||
            !this.props.schedule.hasEverLoadedPcts ||
            !this.props.pct.hasEverLoaded ||
            this.props.pct.fetching ||
            !this.props.supportConsultations.hasEverLoadedUsedAvailability;
        return (
            <ChooseAnalystTime
                fetching={fetching}
                analyst={this.props.analyst}
                analystLoadStatus={this.props.analysts.loadStatus}
                availabilityByDay={this.props.availabilityByDay}
                getSupportConsultationConflicts={
                    getSupportConsultationConflicts
                }
                scheduledSessions={this.props.scheduledSessions}
                supportConsultations={this.props.supportConsultations}
                onTimeSelected={this.onTimeSelected}
                getScheduleItemIsAvailable={
                    this.props.getScheduleItemIsAvailable
                }
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        sessions: state.sessions,
        pct: state.pct,
        schedule: state.schedule,
        supportConsultations: state.supportConsultations,
        user: state.user,
        analysts: state.analysts,
        analyst: state.analysts.byId[state.ui.chosenAnalystId],
        availabilityByDay: availabilityByDay(
            state.analysts,
            state.ui.chosenAnalystId
        ),
        getScheduleItemIsAvailable: getScheduleItemIsAvailable.bind(
            null,
            state.supportConsultations
        ),
        scheduledSessions: getScheduledSessions(
            state.schedule,
            state.sessions,
            state.pct,
            state.preset
        ),
        chosenAnalystId: state.ui.chosenAnalystId,
        history: ownProps.history,
    };
}

const ChooseAnalystTimeConnected = connect(mapStateToProps, {
    fetchSessions,
    fetchPcts,
    fetchAnalysts,
    listenToAuth,
    listenToScheduleSessions,
    listenToSchedulePct,
    listenToUsedAvailability,
    addSupportCouncilToSchedule,
    selectionComplete,
})(ChooseAnalystTimeContainer);

export default ChooseAnalystTimeConnected;
