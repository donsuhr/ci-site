import React from 'react';
import PropTypes from 'prop-types';
import moment from '../util/load-moment-tz';
import ConflictWarning from './ConflictWarning';

const DayItemsList = ({
    dayItems,
    getSupportConsultationConflicts,
    getScheduleItemIsAvailable,
    onTimeSelected,
    scheduledSessions,
}) => (
    <ul className="support-consultations__select-analyst__day-list">
        {dayItems.map((item, i) => {
            const conflicts = getSupportConsultationConflicts(
                scheduledSessions,
                item.start,
                item.end
            );
            const isAvailable = getScheduleItemIsAvailable(item._id);
            if (isAvailable) {
                return (
                    <li
                        key={item._id}
                        className="support-consultations__select-analyst__day-list-item"
                    >
                        {moment(item.start)
                            .tz('America/New_York')
                            .format('dddd h:mm A')}
                        <ConflictWarning
                            time={item.start}
                            conflicts={conflicts}
                        />
                        <button
                            type="button"
                            className="support-consultations__select-analyst__select-slot-button"
                            onClick={event => {
                                event.preventDefault();
                                onTimeSelected(item._id, item.start, item.end);
                            }}
                        >
                            Select
                        </button>
                    </li>
                );
            }
            return null;
        })}
    </ul>
);

DayItemsList.propTypes = {
    dayItems: PropTypes.array, // eslint-disable-line react/forbid-prop-types
    getSupportConsultationConflicts: PropTypes.func,
    getScheduleItemIsAvailable: PropTypes.func,
    onTimeSelected: PropTypes.func,
    scheduledSessions: PropTypes.object, // eslint-disable-line react/forbid-prop-types
};

export default DayItemsList;
