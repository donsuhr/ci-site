/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ChooseAnalystTime from './ChooseAnalystTime';

const availabilityByDay = {
    Friday: [
        {
            _id: '589e04cb3414ba604118d257',
            end: '2017-03-10T15:22:00.000Z',
            start: '2017-03-10T15:21:00.000Z',
        },
        {
            _id: '5860926699ab5cb274586d00',
            end: '2017-04-28T13:30:00.000Z',
            start: '2017-04-28T13:00:00.000Z',
        },
        {
            _id: '5860926699ab5cb274586d01',
            end: '2017-04-28T14:00:00.000Z',
            start: '2017-04-28T13:30:00.000Z',
        },
    ],
    Thursday: [
        {
            _id: '5860926699ab5cb274586cf6',
            end: '2017-04-27T17:30:00.000Z',
            start: '2017-04-27T17:00:00.000Z',
        },
        {
            _id: '5860926699ab5cb274586cf7',
            end: '2017-04-27T18:00:00.000Z',
            start: '2017-04-27T17:30:00.000Z',
        },
    ],
    Sunday: [
        {
            _id: '5860926799ab5cb274586d0b',
            end: '2017-05-28T08:00:00.000Z',
            start: '2017-05-28T07:30:00.000Z',
        },
    ],
};

describe('ChooseAnalystTime', () => {
    it('renders', () => {
        const wrapper = shallow(
            <ChooseAnalystTime
                fetching={false}
                analyst={{
                    loadStatus: 'success',
                }}
                supportConsultations={{
                    loadStatus: 'success',
                }}
                availabilityByDay={availabilityByDay}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const wrapper = shallow(<ChooseAnalystTime fetching />);
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('Loading')).toHaveLength(1);
    });

    it('renders load error', () => {
        const wrapper = shallow(
            <ChooseAnalystTime
                fetching={false}
                analyst={{}}
                analystLoadStatus={{ message: 'test message' }}
                supportConsultations={{
                    loadStatus: 'success',
                }}
                availabilityByDay={availabilityByDay}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('ShowError')).toHaveLength(1);
    });
});
