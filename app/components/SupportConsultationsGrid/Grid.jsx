/* eslint-disable react/forbid-prop-types */

import React from 'react';
import PropTypes from 'prop-types';
import sortBy from 'lodash/sortBy';
import moment from '../util/load-moment-tz';
import Cell from './Cell.container';

function getAnalystSlotByStartTime(analyst, startTimeString) {
    return analyst.availability.find(x => x.start === startTimeString);
}

class Grid extends React.Component {
    static propTypes = {
        analysts: PropTypes.object,
        uniqueStartTimes: PropTypes.array,
    };

    state = { showOld: true };

    toggleOld = () => {
        this.setState({ showOld: !this.state.showOld });
    };

    render() {
        const { analysts, uniqueStartTimes } = this.props;
        const sortedTopics = sortBy(analysts.byTopic, ['title']);
        const filteredUniqueStartTimes = this.state.showOld
            ? [...uniqueStartTimes]
            : uniqueStartTimes.filter(
                x =>
                    x >=
                      moment()
                          .subtract(31, 'minute')
                          .utc()
                          .format('YYYY-MM-DDTHH:mm')
            );
        // return x >= moment.tz('2017-04-27T14:00:00', 'America/New_York')
        // .utc().format('YYYY-MM-DDTHH:mm');

        const days = filteredUniqueStartTimes.reduce((acc, time) => {
            const day = moment(time)
                .tz('America/New_York')
                .format('YYYY-MM-DD');
            if (!acc.includes(day)) {
                acc.push(day);
            }
            return acc;
        }, []);

        return (
            <div>
                {days.map((day, dayIndex) => {
                    const dayTimes = filteredUniqueStartTimes.filter(x =>
                        x.startsWith(day)
                    );
                    return (
                        <table
                            key={day}
                            className="support-consultations-kiosk-table"
                        >
                            <thead>
                                <tr>
                                    <th
                                        colSpan={
                                            Object.keys(analysts.byId).length +
                                            1
                                        }
                                    >
                                        {moment(dayTimes[0])
                                            .tz('America/New_York')
                                            .format('dddd, MMMM Do')}
                                        <div className="support-con-table__toggle-old-wrapper">
                                            <label htmlFor="toggleOld">
                                                Show Past Appointments
                                            </label>
                                            <input
                                                id="toggleOld"
                                                type="checkbox"
                                                onChange={this.toggleOld}
                                                checked={this.state.showOld}
                                            />
                                        </div>
                                    </th>
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    {sortedTopics.map(topic => (
                                        <th
                                            key={topic.title}
                                            colSpan={topic.analysts.length}
                                        >
                                            {topic.title}
                                        </th>
                                    ))}
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    {sortedTopics.map(topic =>
                                        topic.analysts.map(analyst => (
                                            <th key={analyst._id}>
                                                {analyst.firstName}
                                            </th>
                                        ))
                                    )}
                                </tr>
                            </thead>
                            <tbody>
                                {dayTimes.map((time, timeIndex) => {
                                    const formatted = moment(time)
                                        .tz('America/New_York')
                                        .format('h:mm A');
                                    return (
                                        <tr key={time}>
                                            <td>{formatted}</td>
                                            {sortedTopics.map(
                                                (topic, topicIndex) =>
                                                    topic.analysts.map(
                                                        (
                                                            analyst,
                                                            analystIndex
                                                        ) => {
                                                            const slot = getAnalystSlotByStartTime(
                                                                analyst,
                                                                time
                                                            );
                                                            const availabilityId = slot
                                                                ? slot._id
                                                                : null;
                                                            let popoverPlacement =
                                                                'right';

                                                            if (
                                                                dayIndex ===
                                                                    0 &&
                                                                timeIndex === 0
                                                            ) {
                                                                popoverPlacement =
                                                                    'bottom';
                                                            }

                                                            if (
                                                                topicIndex +
                                                                    1 +
                                                                    analystIndex +
                                                                    1 ===
                                                                Object.keys(
                                                                    analysts.byId
                                                                ).length
                                                            ) {
                                                                popoverPlacement =
                                                                    'left';
                                                            }
                                                            if (
                                                                timeIndex ===
                                                                dayTimes.length -
                                                                    1
                                                            ) {
                                                                popoverPlacement =
                                                                    'top';
                                                            }
                                                            return (
                                                                <Cell
                                                                    key={
                                                                        availabilityId
                                                                    }
                                                                    availabilityId={
                                                                        availabilityId
                                                                    }
                                                                    popoverPlacement={
                                                                        popoverPlacement
                                                                    }
                                                                    analystId={
                                                                        analyst._id
                                                                    }
                                                                />
                                                            );
                                                        }
                                                    )
                                            )}
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    );
                })}
            </div>
        );
    }
}

export default Grid;
