import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchUserProfile } from '../firebase/userProfiles.actions';
import { getRegularBookedByAvailabilityId } from '../firebase/schedule.support.reducer';
import {
    easyBook,
    removeEasyBook,
    removeRegularBook,
} from '../firebase/easybook.actions';
import Cell from './Cell';

class CellContainer extends React.Component {
    static propTypes = {
        availabilityId: PropTypes.string,
        easyBook: PropTypes.func,
        // eslint-disable-next-line react/forbid-prop-types
        easybookItem: PropTypes.object,
        fetchUserProfile: PropTypes.func,
        popoverPlacement: PropTypes.string,
        removeEasyBook: PropTypes.func,
        regularBookedId: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.oneOf([true]),
        ]),
        removeRegularBook: PropTypes.func,
        userId: PropTypes.string,
        // eslint-disable-next-line react/forbid-prop-types
        userProfile: PropTypes.object,
    };

    componentDidMount() {
        this.checkForProfile(this.props);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.userId !== this.props.userId) {
            this.checkForProfile(nextProps);
        }
    }

    checkForProfile(props) {
        const hasUserId =
            props.userId !== undefined && props.userId !== 'easybook';
        const hasProfile = props.userProfile !== undefined;
        const profileLoading = hasProfile && props.userProfile.loading === true;
        const profileLoaded =
            hasProfile && Object.hasOwnProperty.call(props.userProfile, 'val');
        if (
            hasUserId &&
            !profileLoaded &&
            (!hasProfile || (hasProfile && !profileLoading))
        ) {
            this.props.fetchUserProfile(props.userId);
        }
    }

    easyBook = options => {
        this.props.easyBook(options);
    };

    removeEasyBook = easybookItem => {
        this.props.removeEasyBook(easybookItem);
    };

    removeRegularBook = ({ regularBookId, userId, availabilityId }) => {
        this.props.removeRegularBook({ regularBookId, userId, availabilityId });
    };

    render() {
        return (
            <Cell
                availabilityId={this.props.availabilityId}
                easyBook={this.easyBook}
                easybookItem={this.props.easybookItem}
                popoverPlacement={this.props.popoverPlacement}
                regularBookedId={this.props.regularBookedId}
                removeEasyBook={this.removeEasyBook}
                removeRegularBook={this.removeRegularBook}
                userId={this.props.userId}
                userProfile={this.props.userProfile}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    const easybookItem =
        state.easybook.byAvailabilityId[ownProps.availabilityId];
    const regularBooked = getRegularBookedByAvailabilityId(
        state.supportConsultations,
        ownProps.availabilityId
    );
    const userId = regularBooked ? regularBooked.userId : undefined;
    const regularBookedId = regularBooked
        ? regularBooked.scheduleId
        : undefined;
    let userProfile;
    if (userId && userId !== 'easybook') {
        userProfile = state.userProfiles.byId[userId];
    }
    if (userId && userId === 'easybook') {
        if (!state.easybook.hasEverLoaded || state.easybook.fetching) {
            userProfile = {
                val: {
                    firstName: 'loading...',
                },
            };
        } else {
            userProfile = {
                val: easybookItem && easybookItem.val.info,
            };
        }
    }
    return {
        userProfile,
        userId,
        regularBookedId,
        easybookItem,
        availabilityId: ownProps.availabilityId,
        analystId: ownProps.analystId,
        popoverPlacement: ownProps.popoverPlacement,
    };
}

const CellConnected = connect(mapStateToProps, {
    fetchUserProfile,
    easyBook,
    removeEasyBook,
    removeRegularBook,
})(CellContainer);

export default CellConnected;
