import React from 'react';
import PropTypes from 'prop-types';
import Popover from 'react-bootstrap/lib/Popover';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';

class OpenCell extends React.Component {
    static propTypes = {
        easyBook: PropTypes.func,
        availabilityId: PropTypes.string,
        popoverPlacement: PropTypes.string,
    };

    onCloseSlotClicked = event => {
        event.preventDefault();
        this.props.easyBook({
            availabilityId: this.props.availabilityId,
            firstName: 'closed',
            lastName: 'closed',
            institution: 'closed',
        });
        this.overlay.hide();
    };

    onBookSlotClicked = event => {
        event.preventDefault();
        this.props.easyBook({
            availabilityId: this.props.availabilityId,
            firstName: this.firstNameInput.value,
            lastName: this.lastNameInput.value,
            institution: this.institutionInput.value,
        });
        this.overlay.hide();
    };

    render() {
        const { availabilityId, popoverPlacement } = this.props;

        const regularPopover = (
            <Popover
                ref={node => {
                    this.popoverNode = node;
                }}
                id={`popover${availabilityId}`}
                title="Schedule an Appointment"
            >
                <fieldset className="support-con-table__easybook-form">
                    <label htmlFor={`popover${availabilityId}-firstName`}>
                        First Name
                    </label>
                    <input
                        ref={node => {
                            this.firstNameInput = node;
                        }}
                        id={`popover${availabilityId}-firstName`}
                        type="text"
                        placeholder="First Name"
                    />
                    <label htmlFor={`popover${availabilityId}-lastName`}>
                        Last Name
                    </label>
                    <input
                        ref={node => {
                            this.lastNameInput = node;
                        }}
                        id={`popover${availabilityId}-lastName`}
                        type="text"
                        placeholder="Last Name"
                    />

                    <label htmlFor={`popover${availabilityId}-institution`}>
                        Institution
                    </label>
                    <input
                        ref={node => {
                            this.institutionInput = node;
                        }}
                        id={`popover${availabilityId}-institution`}
                        type="text"
                        placeholder="Institution"
                    />
                </fieldset>
                <button
                    className="cmc-article__link support-con-table__schedule-button"
                    type="button"
                    onClick={this.onBookSlotClicked}
                >
                    Schedule
                </button>
                <button
                    className="cmc-article__link support-con-table__close-slot-button"
                    type="button"
                    onClick={this.onCloseSlotClicked}
                >
                    Close Time Slot
                </button>
            </Popover>
        );

        return (
            <td>
                <OverlayTrigger
                    ref={node => {
                        this.overlay = node;
                    }}
                    trigger="click"
                    rootClose
                    placement={popoverPlacement}
                    overlay={regularPopover}
                    onEntered={div => {
                        div.querySelector('input').focus();
                    }}
                >
                    <button
                        type="button"
                        className="support-con-table__open-button"
                    >
                        Open
                    </button>
                </OverlayTrigger>
            </td>
        );
    }
}

export default OpenCell;
