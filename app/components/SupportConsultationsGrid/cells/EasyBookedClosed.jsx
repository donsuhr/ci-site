import React from 'react';
import PropTypes from 'prop-types';
import Popover from 'react-bootstrap/lib/Popover';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';

class EasyBookedClosed extends React.Component {
    static propTypes = {
        availabilityId: PropTypes.string,
        // eslint-disable-next-line react/forbid-prop-types
        easybookItem: PropTypes.object,
        popoverPlacement: PropTypes.string,
        removeEasyBook: PropTypes.func,
    };

    onOpenSlotClicked = event => {
        event.preventDefault();
        this.props.removeEasyBook(this.props.easybookItem);
        this.overlay.hide();
    };

    render() {
        const { availabilityId, popoverPlacement } = this.props;
        const closedPopover = (
            <Popover
                ref={node => {
                    this.popoverNode = node;
                }}
                id={`popover${availabilityId}`}
                title="Reopen Appointment"
            >
                <button
                    className="cmc-article__link support-con-table__reopen-button"
                    type="button"
                    onClick={this.onOpenSlotClicked}
                >
                    Open Time Slot
                </button>
            </Popover>
        );
        return (
            <td>
                <OverlayTrigger
                    ref={node => {
                        this.overlay = node;
                    }}
                    trigger="click"
                    rootClose
                    placement={popoverPlacement}
                    overlay={closedPopover}
                >
                    <button
                        className="cmc-article__link support-con-table__closed-button"
                        type="button"
                    >
                        Closed
                    </button>
                </OverlayTrigger>
            </td>
        );
    }
}

export default EasyBookedClosed;
