import React from 'react';
import PropTypes from 'prop-types';
import Popover from 'react-bootstrap/lib/Popover';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';

class RegularBooked extends React.Component {
    static propTypes = {
        availabilityId: PropTypes.string,
        popoverPlacement: PropTypes.string,
        regularBookedId: PropTypes.string,
        removeRegularBook: PropTypes.func,
        userId: PropTypes.string,
        // eslint-disable-next-line react/forbid-prop-types
        userProfile: PropTypes.object,
    };

    onOpenSlotClicked = event => {
        event.preventDefault();
        this.props.removeRegularBook({
            regularBookId: this.props.regularBookedId,
            userId: this.props.userId,
            availabilityId: this.props.availabilityId,
        });
        this.overlay.hide();
    };

    render() {
        const { userProfile, availabilityId, popoverPlacement } = this.props;
        const popover = (
            <Popover
                id={`popover${availabilityId}`}
                title="Cancel Appointment"
                className="support-con-table__popup--regular-booked"
            >
                <p>
                    * This time slot was booked by a user using the regular
                    Support Consultations sign up. They may not be aware of its
                    cancellation.
                </p>
                <button
                    className="cmc-article__link support-con-table__reopen-button"
                    type="button"
                    onClick={this.onOpenSlotClicked}
                >
                    Open Time Slot
                </button>
            </Popover>
        );

        return (
            <td>
                <OverlayTrigger
                    ref={node => {
                        this.overlay = node;
                    }}
                    trigger="click"
                    rootClose
                    placement={popoverPlacement}
                    overlay={popover}
                >
                    <span className="support-con-table__easy-booked-span">
                        {userProfile.val.firstName}&nbsp;
                        {userProfile.val.lastName}&nbsp; ({userProfile.val.institution})*
                    </span>
                </OverlayTrigger>
            </td>
        );
    }
}

export default RegularBooked;
