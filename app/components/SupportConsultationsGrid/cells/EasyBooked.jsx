import React from 'react';
import PropTypes from 'prop-types';
import Popover from 'react-bootstrap/lib/Popover';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';

class EasyBooked extends React.Component {
    static propTypes = {
        availabilityId: PropTypes.string,
        // eslint-disable-next-line react/forbid-prop-types
        easybookItem: PropTypes.object,
        popoverPlacement: PropTypes.string,
        removeEasyBook: PropTypes.func,
        // eslint-disable-next-line react/forbid-prop-types
        userProfile: PropTypes.object,
    };

    onOpenSlotClicked = event => {
        event.preventDefault();
        this.props.removeEasyBook(this.props.easybookItem);
        this.overlay.hide();
    };

    render() {
        const { userProfile, availabilityId, popoverPlacement } = this.props;
        const popover = (
            <Popover
                ref={node => {
                    this.popoverNode = node;
                }}
                id={`popover${availabilityId}`}
                title="Cancel Appointment"
            >
                <button
                    className="cmc-article__link support-con-table__reopen-button"
                    type="button"
                    onClick={this.onOpenSlotClicked}
                >
                    Open Time Slot
                </button>
            </Popover>
        );
        return (
            <td>
                <OverlayTrigger
                    ref={node => {
                        this.overlay = node;
                    }}
                    trigger="click"
                    rootClose
                    placement={popoverPlacement}
                    overlay={popover}
                >
                    <span className="support-con-table__easy-booked-span">
                        {userProfile.val.firstName}&nbsp;
                        {userProfile.val.lastName}&nbsp; ({userProfile.val.institution})
                    </span>
                </OverlayTrigger>
            </td>
        );
    }
}

export default EasyBooked;
