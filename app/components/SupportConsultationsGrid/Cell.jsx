import React from 'react';
import PropTypes from 'prop-types';
import OpenCell from './cells/OpenCell';
import RegularBooked from './cells/RegularBooked';
import EasyBooked from './cells/EasyBooked';
import EasyBookedClosed from './cells/EasyBookedClosed';

const Cell = ({
    availabilityId,
    easyBook,
    easybookItem,
    popoverPlacement,
    regularBookedId,
    removeEasyBook,
    removeRegularBook,
    userId,
    userProfile,
}) => {
    if (availabilityId === null) {
        return (<td>&nbsp;</td>);
    }

    if (userId === 'easybook') {
        if (userProfile.val === 'closed') {
            return (<EasyBookedClosed
                availabilityId={availabilityId}
                easybookItem={easybookItem}
                popoverPlacement={popoverPlacement}
                removeEasyBook={removeEasyBook}
            />);
        }
        return (<EasyBooked
            availabilityId={availabilityId}
            easybookItem={easybookItem}
            popoverPlacement={popoverPlacement}
            removeEasyBook={removeEasyBook}
            userProfile={userProfile}
        />);
    }

    const loading = userProfile && userProfile.loading;
    if (loading) {
        return (<td>Loading...</td>);
    }
    const loaded = userProfile && !userProfile.loading;
    if (loaded) {
        return (<RegularBooked
            availabilityId={availabilityId}
            popoverPlacement={popoverPlacement}
            regularBookedId={regularBookedId}
            removeRegularBook={removeRegularBook}
            userId={userId}
            userProfile={userProfile}
        />);
    }

    return (<OpenCell
        availabilityId={availabilityId}
        popoverPlacement={popoverPlacement}
        easyBook={easyBook}
    />);
};

Cell.propTypes = {
    availabilityId: PropTypes.string,
    easyBook: PropTypes.func,
    // eslint-disable-next-line react/forbid-prop-types
    easybookItem: PropTypes.object,
    popoverPlacement: PropTypes.string,
    removeEasyBook: PropTypes.func,
    regularBookedId: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.oneOf([true]),
    ]),
    removeRegularBook: PropTypes.func,
    userId: PropTypes.string,
    // eslint-disable-next-line react/forbid-prop-types
    userProfile: PropTypes.object,
};

export default Cell;
