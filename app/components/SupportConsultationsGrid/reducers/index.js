import { combineReducers } from 'redux';

import ui from './ui.reducer';
import analysts from '../../ChooseAnalyst/reducer';
import supportConsultations from '../../firebase/schedule.support.reducer';
import user from '../../firebase/auth.reducer';
import userProfiles from '../../firebase/userProfiles.reducer';
import easybook from '../../firebase/easybook.reducer';
import listeners from '../../firebase/listeners.reducer';
import domAttributes from '../../redux/domAttributes.reducers';

export default combineReducers({
    ui,
    analysts,
    supportConsultations,
    userProfiles,
    user,
    easybook,
    listeners,
    domAttributes,
});
