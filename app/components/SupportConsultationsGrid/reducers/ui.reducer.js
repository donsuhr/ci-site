import * as actions from './ui.actions';

const defaultUiState = {
    confirmedProfile: false,
    chosenAnalystId: null,
};

const ui = (state = defaultUiState, action) => {
    switch (action.type) {
        case actions.CONFIRMED_PROFILE:
            return {
                ...state,
                confirmedProfile: true,
            };
        case actions.CHOOSE_ANALYST:
            return {
                ...state,
                chosenAnalystId: action.id,
            };
        case actions.SELECTION_COMPLETE:
            return {
                ...defaultUiState,
            };
        default:
            return state;
    }
};

export default ui;
