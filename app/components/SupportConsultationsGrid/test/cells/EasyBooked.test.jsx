/* global jest,it,expect,describe */

import React from 'react';
import { shallow, mount, ReactWrapper } from 'enzyme';
import toJson from 'enzyme-to-json';
import EasyBooked from '../../cells/EasyBooked';

const userProfile = {
    val: {
        firstName: 'firstName',
        lastName: 'lastName',
        institution: 'institution',
    },
};

const easybookItem = {};

const removeEasyBook = jest.fn();

describe('SupportConsultationsGrid cells EasyBooked', () => {
    it('renders', () => {
        const wrapper = shallow(
            <EasyBooked
                userProfile={userProfile}
                removeEasyBook={removeEasyBook}
                popoverPlacement="left"
                easybookItem={easybookItem}
                availabilityId="23"
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('calls open time slot', () => {
        const wrapper = mount(
            <table>
                <tbody>
                    <tr>
                        <EasyBooked
                            userProfile={userProfile}
                            removeEasyBook={removeEasyBook}
                            popoverPlacement="left"
                            easybookItem={easybookItem}
                            availabilityId="23"
                        />
                    </tr>
                </tbody>
            </table>
        );
        wrapper.find('.support-con-table__easy-booked-span').simulate('click');
        const popoverWrapper = new ReactWrapper(
            wrapper.find('EasyBooked').node.popoverNode,
            true
        );
        popoverWrapper.find('button').simulate('click');

        expect(removeEasyBook).toHaveBeenCalledWith(easybookItem);
    });
});
