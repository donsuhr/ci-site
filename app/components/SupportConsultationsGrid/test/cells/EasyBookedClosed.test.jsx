/* global jest,it,expect,describe */

import React from 'react';
import { shallow, mount, ReactWrapper } from 'enzyme';
import toJson from 'enzyme-to-json';
import EasyBookedClosed from '../../cells/EasyBookedClosed';


const easybookItem = {};

const removeEasyBook = jest.fn();

describe('SupportConsultationsGrid cells EasyBookedClosed', () => {
    it('renders', () => {
        const wrapper = shallow(
            <EasyBookedClosed
                removeEasyBook={removeEasyBook}
                popoverPlacement="left"
                easybookItem={easybookItem}
                availabilityId="23"
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('calls open time slot', () => {
        const wrapper = mount(
            <table>
                <tbody>
                    <tr>
                        <EasyBookedClosed
                            removeEasyBook={removeEasyBook}
                            popoverPlacement="left"
                            easybookItem={easybookItem}
                            availabilityId="23"
                        />
                    </tr>
                </tbody>
            </table>
        );
        wrapper.find('.support-con-table__closed-button').simulate('click');
        const popoverWrapper = new ReactWrapper(
            wrapper.find('EasyBookedClosed').node.popoverNode,
            true
        );
        popoverWrapper.find('button').simulate('click');

        expect(removeEasyBook).toHaveBeenCalledWith(easybookItem);
    });
});
