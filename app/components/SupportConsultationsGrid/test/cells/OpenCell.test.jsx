/* global jest,it,expect,describe */

import React from 'react';
import { shallow, mount, ReactWrapper } from 'enzyme';
import toJson from 'enzyme-to-json';
import OpenCell from '../../cells/OpenCell';

describe('SupportConsultationsGrid cells OpenCell', () => {
    it('renders', () => {
        const easyBook = jest.fn();
        const wrapper = shallow(
            <OpenCell
                easyBook={easyBook}
                popoverPlacement="left"
                availabilityId="23"
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('calls close time slot', () => {
        const easyBook = jest.fn();
        const wrapper = mount(
            <table>
                <tbody>
                    <tr>
                        <OpenCell
                            easyBook={easyBook}
                            popoverPlacement="left"
                            availabilityId="23"
                        />
                    </tr>
                </tbody>
            </table>
        );
        wrapper.find('.support-con-table__open-button').simulate('click');
        const popoverWrapper = new ReactWrapper(
            wrapper.find('OpenCell').node.popoverNode,
            true
        );
        popoverWrapper
            .find('button.support-con-table__close-slot-button')
            .simulate('click');

        expect(easyBook).toHaveBeenCalledWith({
            availabilityId: '23',
            firstName: 'closed',
            lastName: 'closed',
            institution: 'closed',
        });
    });

    it('calls book time slot', () => {
        const easyBook = jest.fn();
        const wrapper = mount(
            <table>
                <tbody>
                    <tr>
                        <OpenCell
                            easyBook={easyBook}
                            popoverPlacement="left"
                            availabilityId="23"
                        />
                    </tr>
                </tbody>
            </table>
        );
        wrapper.find('.support-con-table__open-button').simulate('click');
        const popoverWrapper = new ReactWrapper(
            wrapper.find('OpenCell').node.popoverNode,
            true
        );
        popoverWrapper
            .find('#popover23-firstName')
            .node.value = 'firstName';
        popoverWrapper
            .find('#popover23-lastName')
            .node.value = 'lastName';
        popoverWrapper
            .find('#popover23-institution')
            .node.value = 'institution';
        popoverWrapper
            .find('button.support-con-table__schedule-button')
            .simulate('click');

        expect(easyBook).toHaveBeenCalledWith({
            availabilityId: '23',
            firstName: 'firstName',
            lastName: 'lastName',
            institution: 'institution',
        });
    });
});
