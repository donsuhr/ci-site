/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { GridContainer } from '../Grid.container';

describe('SupportConsultationsGrid Grid.container', () => {
    it('renders', () => {
        const analysts = {
            hasEverLoaded: true,
            fetching: false,
        };
        const supportConsultations = {
            hasEverLoadedUsedAvailability: true,
        };
        const easybook = {
            hasEverLoaded: true,
        };
        const wrapper = shallow(
            <GridContainer
                analysts={analysts}
                supportConsultations={supportConsultations}
                easybook={easybook}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading data', () => {
        const analysts = {
            hasEverLoaded: true,
            fetching: false,
        };
        const supportConsultations = {
            hasEverLoadedUsedAvailability: false,
        };
        const easybook = {
            hasEverLoaded: true,
        };
        const wrapper = shallow(
            <GridContainer
                analysts={analysts}
                supportConsultations={supportConsultations}
                easybook={easybook}
            />
        );
        expect(wrapper.find('Loading').dive().text()).toContain('Data');
    });

    it('renders loading analysts', () => {
        const analysts = {
            hasEverLoaded: false,
            fetching: false,
        };
        const supportConsultations = {
            hasEverLoadedUsedAvailability: true,
        };
        const easybook = {
            hasEverLoaded: true,
        };
        const wrapper = shallow(
            <GridContainer
                analysts={analysts}
                supportConsultations={supportConsultations}
                easybook={easybook}
            />
        );
        expect(wrapper.find('Loading').dive().text()).toContain('Analysts');
    });
});
