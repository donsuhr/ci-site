/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import Cell from '../Cell';

describe('SupportConsultationsGrid Cell', () => {
    it('renders null', () => {
        const wrapper = shallow(<Cell availabilityId={null} />);
        expect(wrapper.text()).toBe(' ');
    });

    it('renders easybook closed', () => {
        const wrapper = shallow(
            <Cell userId="easybook" userProfile={{ val: 'closed' }} />
        );
        expect(wrapper.find('EasyBookedClosed')).toHaveLength(1);
    });
    it('renders easybooked', () => {
        const wrapper = shallow(
            <Cell userId="easybook" userProfile={{ val: 'notclosed' }} />
        );
        expect(wrapper.find('EasyBooked')).toHaveLength(1);
    });
    it('renders RegularBooked', () => {
        const wrapper = shallow(
            <Cell userId="123" userProfile={{ loading: false }} />
        );
        expect(wrapper.find('RegularBooked')).toHaveLength(1);
    });
    it('renders OpenCell', () => {
        const wrapper = shallow(<Cell userId="123" />);
        expect(wrapper.find('OpenCell')).toHaveLength(1);
    });
});
