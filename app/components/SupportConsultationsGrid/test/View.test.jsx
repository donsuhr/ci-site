/* global it,expect,describe,jest */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { ScheduleGridView } from '../View';

describe('SupportConsultationsGrid View', () => {
    it('renders', () => {
        const wrapper = shallow(
            <ScheduleGridView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    isAuth: false,
                    isAdmin: false,
                    initting: false,
                    loadingRoot: false,
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const wrapper = shallow(
            <ScheduleGridView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    isAuth: false,
                    isAdmin: false,
                    initting: true,
                    loadingRoot: false,
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('Loading').dive().text()).toContain('Loading...');
    });

    it('renders autenticating', () => {
        const wrapper = shallow(
            <ScheduleGridView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    isAuth: false,
                    isAdmin: false,
                    initting: false,
                    loadingRoot: true,
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('Loading').dive().text()).toContain(
            'Authenticating...'
        );
    });

    it('renders sign in button', () => {
        const wrapper = shallow(
            <ScheduleGridView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    isAuth: false,
                    isAdmin: false,
                    initting: false,
                    loadingRoot: false,
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('button').text()).toContain('Sign In');
    });

    it('renders auth required', () => {
        const wrapper = shallow(
            <ScheduleGridView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    isAuth: true,
                    isAdmin: false,
                    initting: false,
                    loadingRoot: false,
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('p').text()).toContain('Admin rights required.');
    });

    it('renders the grid', () => {
        const wrapper = shallow(
            <ScheduleGridView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    isAuth: true,
                    isAdmin: true,
                    initting: false,
                    loadingRoot: false,
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('Connect(GridContainer)')).toHaveLength(1);
    });
});
