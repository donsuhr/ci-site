/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Grid from '../Grid';

const analyst1 = {
    id: 1,
    firstName: 'fname',
    lastName: 'lname',
    availability: [
        { start: '2017-04-27T17:00:00.000Z' },
        { start: '2017-04-27T17:00:30.000Z' },
        { start: '2017-04-28T17:00:00.000Z' },
    ],
};
const analyst2 = {
    id: 2,
    firstName: 'fname',
    lastName: 'lname',
    availability: [
        { start: '2017-04-27T17:00:00.000Z' },
        { start: '2017-04-27T17:00:30.000Z' },
        { start: '2017-04-28T17:00:00.000Z' },
    ],
};
const analysts = {
    byTopic: {
        topic1: {
            title: 'topic1',
            analysts: [analyst1],
        },
        topic2: {
            title: 'topic2',
            analysts: [analyst1, analyst2],
        },
    },
    byId: {
        1: analyst1,
        2: analyst2,
    },
};
const uniqueStartTimes = [
    '2017-04-27T17:00:00.000Z',
    '2017-04-27T17:00:30.000Z',
    '2017-04-28T17:00:00.000Z',
];

describe('SupportConsultationsGrid Grid', () => {
    it('renders', () => {
        const wrapper = shallow(
            <Grid analysts={analysts} uniqueStartTimes={uniqueStartTimes} />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('toggles show old', () => {
        const wrapper = shallow(
            <Grid analysts={analysts} uniqueStartTimes={uniqueStartTimes} />
        );
        expect(wrapper.state('showOld')).toBe(true);
        wrapper
            .find('#toggleOld')
            .first()
            .simulate('change', { target: { checked: false } });
        expect(wrapper.state('showOld')).toBe(false);
    });
});
