import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Grid from './Grid';
import { fetchAnalysts } from '../ChooseAnalyst/actions';
import { uniqueStartTimes } from '../ChooseAnalyst/reducer';
import { listenToAuth } from '../firebase/auth.actions';
import { listenToUsedAvailability } from '../firebase/schedule.support.actions';
import { listenToEasyBook } from '../firebase/easybook.actions';
import Loading from '../Loading';

export class GridContainer extends React.Component {
    static propTypes = {
        analysts: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        // eslint-disable-next-line react/forbid-prop-types
        uniqueStartTimes: PropTypes.array,
        supportConsultations: PropTypes.shape({
            hasEverLoadedUsedAvailability: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        easybook: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        fetchAnalysts: PropTypes.func,
        listenToUsedAvailability: PropTypes.func,
        listenToAuth: PropTypes.func,
        listenToEasyBook: PropTypes.func,
    };

    componentDidMount() {
        if (
            this.props.analysts.hasEverLoaded === false &&
            this.props.analysts.fetching === false
        ) {
            this.props.fetchAnalysts();
        }
        this.props.listenToAuth().then(user => {
            this.props.listenToUsedAvailability();
            this.props.listenToEasyBook();
        });
    }

    render() {
        if (
            !this.props.supportConsultations.hasEverLoadedUsedAvailability ||
            !this.props.easybook.hasEverLoaded
        ) {
            return <Loading>Loading Data...</Loading>;
        }
        if (
            this.props.analysts.hasEverLoaded === false ||
            this.props.analysts.fetching
        ) {
            return <Loading>Loading Analysts...</Loading>;
        }

        return (
            <Grid
                analysts={this.props.analysts}
                uniqueStartTimes={this.props.uniqueStartTimes}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        analysts: state.analysts,
        easybook: state.easybook,
        supportConsultations: state.supportConsultations,
        uniqueStartTimes: uniqueStartTimes(state.analysts),
    };
}

const GridConnected = connect(mapStateToProps, {
    fetchAnalysts,
    listenToAuth,
    listenToUsedAvailability,
    listenToEasyBook,
})(GridContainer);

export default GridConnected;
