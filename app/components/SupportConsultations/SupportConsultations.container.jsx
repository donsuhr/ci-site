import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SupportConsultations from './SupportConsultations';
import { fetchAnalysts } from '../ChooseAnalyst/actions';
import { listenToAuth, requestPopupAuth } from '../firebase/auth.actions';
import { PropType as userPropType } from '../firebase/auth.reducer';
import {
    listenToScheduleSupport,
    removeSupportFromSchedule,
} from '../firebase/schedule.support.actions';
import {
    getAppointmentsByDay,
    getScheduleItemById,
} from '../firebase/schedule.support.reducer';
import Loading from '../Loading';

export class SupportConsultationsContainer extends React.Component {
    static propTypes = {
        // eslint-disable-next-line react/forbid-prop-types
        analysts: PropTypes.object,
        // eslint-disable-next-line react/forbid-prop-types
        supportConsultations: PropTypes.object,
        // eslint-disable-next-line react/forbid-prop-types
        userItemsByDay: PropTypes.object,
        user: userPropType,
        fetchAnalysts: PropTypes.func,
        listenToAuth: PropTypes.func,
        listenToScheduleSupport: PropTypes.func,
        getScheduleItemById: PropTypes.func,
        removeSupportFromSchedule: PropTypes.func,
        year: PropTypes.string,
    };

    componentDidMount() {
        if (
            this.props.analysts.hasEverLoaded === false &&
            this.props.analysts.fetching === false
        ) {
            this.props.fetchAnalysts();
        }
        this.props.listenToAuth().then(user => {
            this.props.listenToScheduleSupport(user.uid);
        });
    }

    onSignInClick = event => {
        event.preventDefault();
        requestPopupAuth(this.props.year);
    };

    removeItem = item => {
        const scheduleItem = this.props.getScheduleItemById(
            item.analystId,
            item.availabilityId
        );
        this.props.removeSupportFromSchedule(scheduleItem);
    };

    render() {
        if (this.props.user.initting) {
            return <Loading>Loading...</Loading>;
        }
        if (!this.props.user.isAuth) {
            return (
                <button
                    type="button"
                    className="cmc-article__link"
                    onClick={this.onSignInClick}
                >
                    Sign In
                </button>
            );
        }
        return (
            <SupportConsultations
                analysts={this.props.analysts}
                supportConsultations={this.props.supportConsultations}
                userItemsByDay={this.props.userItemsByDay}
                removeItem={this.removeItem}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user,
        analysts: state.analysts,
        supportConsultations: state.supportConsultations,
        userItemsByDay: getAppointmentsByDay(
            state.supportConsultations,
            state.analysts
        ),
        getScheduleItemById: getScheduleItemById.bind(
            null,
            state.supportConsultations
        ),
        year: state.domAttributes.year,
    };
}

const SupportConsultationsConnected = connect(mapStateToProps, {
    fetchAnalysts,
    listenToAuth,
    removeSupportFromSchedule,
    listenToScheduleSupport,
})(SupportConsultationsContainer);

export default SupportConsultationsConnected;
