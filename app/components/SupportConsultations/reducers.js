import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import sessions from '../SessionBrowser/reducer';
import pct from '../PctBrowser/reducer';
import preset from '../Schedule/reducer';
import analysts from '../ChooseAnalyst/reducer';
import schedule from '../firebase/schedule.reducer';
import supportConsultations from '../firebase/schedule.support.reducer';
import user from '../firebase/auth.reducer';
import profile from '../firebase/profile.reducer';
import listeners from '../firebase/listeners.reducer';
import ui from './reducer';
import domAttributes from '../redux/domAttributes.reducers';

export default combineReducers({
    sessions,
    pct,
    schedule,
    user,
    preset,
    analysts,
    profile,
    form: formReducer,
    ui,
    supportConsultations,
    listeners,
    domAttributes,
});
