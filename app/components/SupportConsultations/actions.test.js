/* global it,expect,describe */
import * as actions from './actions';

describe('SupportConsultations actions', () => {
    it('should create an action to confirm profile ', () => {
        const expectedAction = {
            type: actions.CONFIRMED_PROFILE,
        };
        expect(actions.confirmProfile()).toEqual(expectedAction);
    });

    it('should create an action to choose analyst', () => {
        const id = 22;
        const expectedAction = {
            type: actions.CHOOSE_ANALYST,
            id,
        };
        expect(actions.chooseAnalyst(id)).toEqual(expectedAction);
    });

    it('should create an action to complete a section', () => {
        const expectedAction = {
            type: actions.SELECTION_COMPLETE,
        };
        expect(actions.selectionComplete()).toEqual(expectedAction);
    });
});
