/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SupportConsultations from './SupportConsultations';

describe('SupportConsultations', () => {
    it('renders', () => {
        const analysts = {
            hasEverLoaded: true,
            fetching: false,
        };
        const supportConsultations = {
            hasEverLoaded: true,
        };
        const userItemsByDay = {};
        const wrapper = shallow(
            <SupportConsultations
                analysts={analysts}
                supportConsultations={supportConsultations}
                userItemsByDay={userItemsByDay}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders an appointment', () => {
        const userItemsByDay = {
            20170310: [
                {
                    analystId: '5860926299ab5cb274586cf5',
                    availabilityId: '589e04cb3414ba604118d257',
                    end: '2017-03-10T15:22:00.000Z',
                    location: { title: 'TBD' },
                    start: '2017-03-10T15:21:00.000Z',
                    title:
                        'Corrine123 Snoddy / Academics — Support Consultation',
                    type: 'support',
                },
            ],
        };
        const analysts = {
            hasEverLoaded: true,
            fetching: false,
        };
        const supportConsultations = {
            hasEverLoaded: true,
        };
        const wrapper = shallow(
            <SupportConsultations
                analysts={analysts}
                supportConsultations={supportConsultations}
                userItemsByDay={userItemsByDay}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('ScheduleDay')).toHaveLength(1);
    });

    it('renders loading data', () => {
        const analysts = {
            hasEverLoaded: false,
            fetching: true,
        };
        const supportConsultations = {
            hasEverLoaded: true,
        };
        const wrapper = shallow(
            <SupportConsultations
                analysts={analysts}
                supportConsultations={supportConsultations}
                userItemsByDay={{}}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
    });

    it('renders loading error', () => {
        const analysts = {
            hasEverLoaded: true,
            fetching: false,
            loadStatus: {
                message: 'load error',
            },
        };
        const supportConsultations = {
            hasEverLoaded: true,
        };
        const wrapper = shallow(
            <SupportConsultations
                analysts={analysts}
                supportConsultations={supportConsultations}
                userItemsByDay={{}}
            />
        );
        expect(wrapper.find('ShowError')).toHaveLength(1);
        expect(wrapper.find('ShowError').dive().text()).toContain('load error');
    });
});
