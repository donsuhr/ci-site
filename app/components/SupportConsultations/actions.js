export const CONFIRMED_PROFILE = 'CONFIRMED_PROFILE';
function confirmProfile() {
    return {
        type: CONFIRMED_PROFILE,
    };
}

export const CHOOSE_ANALYST = 'CHOOSE_ANALYST';
function chooseAnalyst(id) {
    return {
        type: CHOOSE_ANALYST,
        id,
    };
}

export const SELECTION_COMPLETE = 'SELECTION_COMPLETE';
function selectionComplete() {
    return {
        type: SELECTION_COMPLETE,
    };
}

export { confirmProfile, chooseAnalyst, selectionComplete };
