import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ProfileView from '../ProfileView';
import ShowError from '../ShowError';
import Loading from '../Loading';
import ScheduleDay from '../Schedule/ScheduleDay';

const SupportConsultations = ({
    analysts,
    supportConsultations,
    userItemsByDay,
    removeItem,
}) => {
    const { loadStatus } = analysts;
    const showLoadError =
        loadStatus && loadStatus !== 'success' && loadStatus !== '';
    // @formatter:off
    const fetching =
        !analysts.hasEverLoaded ||
        analysts.fetching ||
        !supportConsultations.hasEverLoaded ||
        supportConsultations.fetching;
    // @formatter:on
    if (fetching) {
        return <Loading>Loading...</Loading>;
    }
    const orderedDays = Object.keys(userItemsByDay).sort();
    return (
        <div className="support-consultations__home">
            <div className="support-consultations__scheduled-appointments">
                <h2 className="secondary-header--alt">
                    My Scheduled Appointments
                </h2>
                {showLoadError && <ShowError error={loadStatus} />}
                {Object.keys(userItemsByDay).length > 0 ? (
                    orderedDays.map(x => (
                        <ScheduleDay
                            key={x}
                            dayItems={userItemsByDay[x]}
                            removeItem={removeItem}
                        />
                    ))
                ) : (
                    <div>
                        <p>
                            Use the create appointment button below to meet with
                            a product expert.
                        </p>
                        <Link
                            className="cmc-article__link"
                            to="/verify-registration"
                        >
                            Create an Appointment
                        </Link>
                    </div>
                )}
            </div>
            <ProfileView />
        </div>
    );
};

SupportConsultations.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    analysts: PropTypes.object,
    // eslint-disable-next-line react/forbid-prop-types
    supportConsultations: PropTypes.object,
    // eslint-disable-next-line react/forbid-prop-types
    userItemsByDay: PropTypes.object,
    removeItem: PropTypes.func,
};

export default SupportConsultations;
