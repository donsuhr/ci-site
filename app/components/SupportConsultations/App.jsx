/* eslint global-require:0 */
import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { HashRouter, Route, Switch } from 'react-router-dom';

import ReduxDevToolsLoader from '../redux/ReduxDevToolsLoader';
import SupportConsultations from './SupportConsultations.container';
import ChooseAnalyst from '../ChooseAnalyst';
import ChooseAnalystTime from '../ChooseAnalystTime';
import VerifyRegistration from '../VerifyRegistration';

const App = ({ store, ...props }) => (
    <Provider store={store}>
        <div>
            <HashRouter>
                <Switch>
                    <Route exact path="/" component={SupportConsultations} />
                    <Route
                        path="/verify-registration"
                        component={VerifyRegistration}
                    />
                    <Route path="/choose-analyst" component={ChooseAnalyst} />
                    <Route
                        path="/choose-analyst-time"
                        component={ChooseAnalystTime}
                    />
                </Switch>
            </HashRouter>
            <ReduxDevToolsLoader />
        </div>
    </Provider>
);

App.propTypes = {
    /* eslint-disable react/forbid-prop-types */
    store: PropTypes.object.isRequired,
};

export default App;
