/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { SupportConsultationsContainer } from './SupportConsultations.container';

describe('SupportConsultations SupportConsultations.container', () => {
    it('renders', () => {
        const wrapper = shallow(
            <SupportConsultationsContainer
                user={{
                    initting: false,
                    isAuth: true,
                }}
            />
        );
        expect(wrapper.find('SupportConsultations')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading data', () => {
        const wrapper = shallow(
            <SupportConsultationsContainer user={{ initting: true }} />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
    });

    it('renders sign in', () => {
        const wrapper = shallow(
            <SupportConsultationsContainer
                user={{
                    initting: false,
                    isAuth: false,
                }}
            />
        );
        expect(wrapper.find('button').text()).toContain('Sign In');
    });
});
