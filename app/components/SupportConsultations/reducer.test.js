/* global it,expect,describe */
import reducer from './reducer';
import * as actions from './actions';

describe('SupportConsultations reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            confirmedProfile: false,
            chosenAnalystId: null,
        });
    });

    it('should handle CONFIRMED_PROFILE', () => {
        expect(
            reducer(
                {
                    confirmedProfile: false,
                    chosenAnalystId: null,
                },
                {
                    type: actions.CONFIRMED_PROFILE,
                }
            )
        ).toEqual({
            confirmedProfile: true,
            chosenAnalystId: null,
        });
    });
    it('should handle CHOOSE_ANALYST', () => {
        expect(
            reducer(
                {
                    confirmedProfile: false,
                    chosenAnalystId: null,
                },
                {
                    type: actions.CHOOSE_ANALYST,
                    id: 22,
                }
            )
        ).toEqual({
            confirmedProfile: false,
            chosenAnalystId: 22,
        });
    });

    it('should handle reset with SELECTION_COMPLETE', () => {
        expect(
            reducer(
                {
                    confirmedProfile: true,
                    chosenAnalystId: 22,
                },
                {
                    type: actions.SELECTION_COMPLETE,
                }
            )
        ).toEqual({
            confirmedProfile: false,
            chosenAnalystId: null,
        });
    });
});
