import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchAnalysts } from './actions';
import { chooseAnalyst } from '../SupportConsultations/actions';
import ChooseAnalyst from './ChooseAnalyst';

export class ChooseAnalystContainer extends React.Component {
    static propTypes = {
        // eslint-disable-next-line react/forbid-prop-types
        analysts: PropTypes.object,
        // eslint-disable-next-line react/forbid-prop-types
        user: PropTypes.object,
        chooseAnalyst: PropTypes.func,
        fetchAnalysts: PropTypes.func,
        confirmedProfile: PropTypes.bool,
        // eslint-disable-next-line react/forbid-prop-types
        history: PropTypes.object,
    };

    componentWillMount() {
        if (!this.props.confirmedProfile) {
            this.props.history.replace('/verify-registration');
        }
    }

    componentDidMount() {
        if (
            this.props.analysts.hasEverLoaded === false &&
            this.props.analysts.fetching === false
        ) {
            this.props.fetchAnalysts();
        }
        setTimeout(() => {
            if (!this.props.user.isAuth) {
                this.props.history.replace('/');
            }
        }, 1000);
    }

    chooseAnalyst = id => {
        this.props.chooseAnalyst(id);
        this.props.history.push('/choose-analyst-time');
    };

    render() {
        return (
            <div>
                <ChooseAnalyst
                    analysts={this.props.analysts}
                    chooseAnalyst={this.chooseAnalyst}
                />
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        analysts: state.analysts,
        user: state.user,
        confirmedProfile: state.ui.confirmedProfile,
        history: ownProps.history.isRequired,
    };
}

const ChooseAnalystConnected = connect(mapStateToProps, {
    fetchAnalysts,
    chooseAnalyst,
})(ChooseAnalystContainer);

export default ChooseAnalystConnected;
