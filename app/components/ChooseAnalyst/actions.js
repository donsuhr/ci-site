import config from '../../../config';
import fetchItems from '../util/fetch-util';

const endpoint = `${config.domains.api}/campusInsight/ci/analysts/`;

export const REQUEST_ANALYSTS = 'REQUEST_ANALYSTS';

export function requestAnalysts() {
    return {
        type: REQUEST_ANALYSTS,
    };
}

export const RECEIVE_ANALYSTS = 'RECEIVE_ANALYSTS';

export function receiveAnalysts(items) {
    return {
        type: RECEIVE_ANALYSTS,
        items: items.data,
    };
}

export const REQUEST_ANALYSTS_ERROR = 'REQUEST_ANALYSTS_ERROR';

export function requestSessionError(error) {
    return {
        type: REQUEST_ANALYSTS_ERROR,
        error,
    };
}

function fetchAnalysts() {
    return (dispatch, getState) => {
        const state = getState();
        const hasYear =
            Object.hasOwnProperty.call(state, 'domAttributes') &&
            Object.hasOwnProperty.call(state.domAttributes, 'year');
        const year = hasYear ? `${state.domAttributes.year}/` : '';
        return fetchItems({
            dispatch,
            endpoint: `${endpoint}${year}`,
            getState,
            key: 'analysts',
            startAction: requestAnalysts,
            endAction: receiveAnalysts,
            errorAction: requestSessionError,
        });
    };
}

export { fetchAnalysts };
