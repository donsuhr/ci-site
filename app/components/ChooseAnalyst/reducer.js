import { combineReducers } from 'redux';
import sortBy from 'lodash/sortBy';
import isDate from 'lodash/isDate';
import * as actions from './actions';
import moment from '../util/load-moment-tz';

const defaultState = {
    savingStatus: '',
};

const item = (state = defaultState, action, currentItem) => {
    switch (action.type) {
        case actions.RECEIVE_ANALYSTS:
            return {
                ...state,
                ...currentItem,
                savingStatus: '',
            };

        default:
            return state;
    }
};

export const byId = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_ANALYSTS:
            return action.items.reduce((p, c) => {
                p[c._id] = item(state[c._id], action, c);
                return p;
            }, {});

        default:
            return state;
    }
};

export const byTopic = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_ANALYSTS:
            const items = action.items.reduce((acc, x) => {
                if (!Object.hasOwnProperty.call(acc, x.topic)) {
                    acc[x.topic] = {
                        analysts: [],
                        title: x.topic,
                    };
                }
                acc[x.topic].analysts.push(x);
                return acc;
            }, {});
            Object.keys(items).forEach(x => {
                items[x].analysts = sortBy(items[x].analysts, 'lastName');
            });
            return items;
        default:
            return state;
    }
};

export const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_ANALYSTS:
            return true;
        case actions.RECEIVE_ANALYSTS:
        case actions.REQUEST_ANALYSTS_ERROR:
            return false;
        default:
            return state;
    }
};

export const loadStatus = (state = '', action) => {
    switch (action.type) {
        case actions.REQUEST_ANALYSTS_ERROR:
            return action.error;
        case actions.RECEIVE_ANALYSTS:
            return 'success';
        default:
            return state;
    }
};

export const hasEverLoaded = (state = false, action) => {
    switch (action.type) {
        case actions.RECEIVE_ANALYSTS:
        case actions.REQUEST_ANALYSTS_ERROR:
            return true;
        default:
            return state;
    }
};

const analysts = combineReducers({
    hasEverLoaded,
    fetching,
    byId,
    byTopic,
    loadStatus,
});

const availabilityByDay = (state, analystId) => {
    if (!state || !state.byId || !analystId || !state.byId[analystId]) {
        return {};
    }
    const { availability } = state.byId[analystId];

    const sorted = sortBy(availability, x => {
        if (!isDate(x.start)) {
            return new Date(x.start);
        }
        return x.start;
    });

    return sorted.reduce((acc, x) => {
        const start = moment(x.start)
            .tz('America/New_York')
            .format('dddd');
        if (!Object.hasOwnProperty.call(acc, start)) {
            acc[start] = [];
        }
        acc[start].push(x);
        return acc;
    }, {});
};

const uniqueStartTimes = state => {
    if (!state || !state.byId) {
        return {};
    }
    const uniq = Object.keys(state.byId).reduce((acc, x) => {
        state.byId[x].availability.forEach(time => {
            if (!Object.hasOwnProperty.call(acc, time.start)) {
                acc[time.start] = {};
            }
        });
        return acc;
    }, {});
    return Object.keys(uniq).sort();
};

function getAppointmentsGroupedByTopic(analystState,
    supportConsultationsState) {
    return Object.keys(
        supportConsultationsState.usedAvailabilityId
    ).reduce((acc, availId) => {
        const analystId = Object.keys(analystState.byId).find(x =>
            analystState.byId[x].availability.some(y => y._id === availId)
        );

        if (analystId) {
            const { topic } = analystState.byId[analystId];
            if (!Object.hasOwnProperty.call(acc, topic)) {
                acc[topic] = [];
            }
            acc[topic].push(availId);
        }

        return acc;
    }, {});
}

function getAppointmentFromId(state, availabilityId) {
    let availabilityObj;
    const analystId = Object.keys(state.byId).find(x =>
        state.byId[x].availability.some(y => {
            const found = y._id === availabilityId;
            if (found) {
                availabilityObj = y;
            }
            return found;
        })
    );
    return { availabilityObj, analystObj: state.byId[analystId] };
}

function getAllAppointmentsGroupedByDay(analystState,
    supportConsultationsState) {
    return Object.keys(
        supportConsultationsState.usedAvailabilityId
    ).reduce((acc, availId) => {
        const { availabilityObj } = getAppointmentFromId(analystState, availId);

        if (availabilityObj) {
            const day = moment(availabilityObj.start)
                .tz('America/New_York')
                .format('YYYYMMDD');
            if (!Object.hasOwnProperty.call(acc, day)) {
                acc[day] = [];
            }
            acc[day].push({ [availId]: availabilityObj.start });
        }

        return acc;
    }, {});
}

function getAllAppointmentsGroupedByHour(analystState,
    supportConsultationsState) {
    return Object.keys(analystState.byId).reduce((acc, analystId) => {
        analystState.byId[analystId].availability.forEach(availabilityObj => {
            const hour = moment(availabilityObj.start)
                .tz('America/New_York')
                .format('YYYYMMDDHHmm');
            if (!Object.hasOwnProperty.call(acc, hour)) {
                acc[hour] = [];
            }
            if (
                Object.hasOwnProperty.call(
                    supportConsultationsState.usedAvailabilityId,
                    availabilityObj._id
                )
            ) {
                acc[hour].push(
                    supportConsultationsState.usedAvailabilityId[
                        availabilityObj._id
                    ]
                );
            }
        });
        return acc;
    }, {});
}

function getAnalystsBookedSlotsByDay(analystState, supportConsultationsState) {
    const booked = Object.keys(
        supportConsultationsState.usedAvailabilityId
    ).reduce((acc, availId) => {
        const { availabilityObj, analystObj } = getAppointmentFromId(
            analystState,
            availId
        );
        if (analystObj) {
            if (!Object.hasOwnProperty.call(acc, analystObj._id)) {
                acc[analystObj._id] = {};
            }
            const day = moment(availabilityObj.start)
                .tz('America/New_York')
                .format('YYYYMMDD');
            if (!Object.hasOwnProperty.call(acc[analystObj._id], day)) {
                acc[analystObj._id][day] = [];
            }
            acc[analystObj._id][day].push(availabilityObj);
        }
        return acc;
    }, {});

    return Object.keys(analystState.byId).reduce((acc, analystId) => {
        acc[analystId] = {
            ...analystState.byId[analystId],
            bookedByDay: booked[analystId],
        };
        return acc;
    }, {});
}

export {
    analysts as default,
    availabilityByDay,
    uniqueStartTimes,
    getAppointmentsGroupedByTopic,
    getAllAppointmentsGroupedByDay,
    getAnalystsBookedSlotsByDay,
    getAllAppointmentsGroupedByHour,
};
