import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ShowError from '../ShowError';

const ChooseAnalyst = ({ analysts, chooseAnalyst }) => {
    const showLoadError = (analysts.loadStatus && analysts.loadStatus !== 'success' && analysts.loadStatus !== '');
    return (
        <div className="support-consultations__select-analyst">
            <h2 className="cmc-article__intro-p">Select An Analyst</h2>
            <p>Now choose an analyst to meet with.</p>
            {showLoadError && (<ShowError error={analysts.loadStatus} />)}
            <ul className="select-analyst-list">
                {Object.keys(analysts.byId).map((key) => {
                    const analyst = analysts.byId[key];
                    return (
                        <li key={analyst._id} className="select-analyst-list-item">
                            <button
                                className="select-analyst-button"
                                type="button"
                                onClick={(event) => {
                                    event.preventDefault();
                                    chooseAnalyst(analyst._id);
                                }}
                            >
                                <div className="select-analyst-list-item__name">
                                    {analyst.firstName} {analyst.lastName}
                                </div>
                                <div className="select-analyst-list-item__topic">
                                    {analyst.topic}
                                </div>
                            </button>
                        </li>
                    );
                })}
            </ul>
            <Link
                className="cmc-article__link cmc-article__link--reversed"
                to="/verify-registration"
            >
                Back
            </Link>
        </div>
    );
};

ChooseAnalyst.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    analysts: PropTypes.object,
    chooseAnalyst: PropTypes.func,
};

export default ChooseAnalyst;
