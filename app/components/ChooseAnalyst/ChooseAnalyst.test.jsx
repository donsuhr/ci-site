/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ChooseAnalyst from './ChooseAnalyst';

describe('ChooseAnalyst ChooseAnalyst', () => {
    it('renders', () => {
        const wrapper = shallow(
            <ChooseAnalyst
                analysts={{
                    byId: {
                        123: {
                            _id: 123,
                        },
                        345: {
                            _id: 345,
                        },
                    },
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });
    it('calls chooseAnalyst', () => {
        const chooseAnalyst = jest.fn();
        const wrapper = shallow(
            <ChooseAnalyst
                chooseAnalyst={chooseAnalyst}
                analysts={{
                    byId: {
                        123: {
                            _id: 123,
                        },
                        345: {
                            _id: 345,
                        },
                    },
                }}
            />
        );
        expect(chooseAnalyst).not.toHaveBeenCalled();
        wrapper
            .find('button')
            .first()
            .simulate('click', {
                preventDefault() {},
            });
        expect(chooseAnalyst).toHaveBeenCalled();
    });
});
