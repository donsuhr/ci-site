/* global it,expect,describe */
import { hasEverLoaded, byTopic, byId, loadStatus, fetching } from './reducer';
import * as actions from './actions';

describe('ChooseAnalyst reducer', () => {
    describe('byId', () => {
        it('should return the initial state', () => {
            expect(byId(undefined, {})).toEqual({});
        });

        it('should handle RECEIVE_ANALYSTS', () => {
            const state = { 123: { foo: 'bar' } };
            const action = {
                type: actions.RECEIVE_ANALYSTS,
                items: [{ _id: '456' }],
            };
            expect(byId(state, action)).toEqual({
                456: {
                    _id: '456',
                    savingStatus: '',
                },
            });
        });
    });

    describe('byTopic', () => {
        it('should return the initial state', () => {
            expect(byTopic(undefined, {})).toEqual({});
        });
        it('should handle RECEIVE_ANALYSTS', () => {
            const state = {};
            const action = {
                type: actions.RECEIVE_ANALYSTS,
                items: [{ topic: 'topic1' }],
            };
            expect(byTopic(state, action)).toEqual({
                topic1: {
                    title: 'topic1',
                    analysts: [{ topic: 'topic1' }],
                },
            });
        });
    });

    describe('fetching', () => {
        it('should return the initial state', () => {
            expect(fetching(undefined, {})).toEqual(false);
        });
        it('should handle REQUEST_ANALYSTS', () => {
            expect(
                fetching(false, {
                    type: actions.REQUEST_ANALYSTS,
                })
            ).toEqual(true);
        });
        it('should handle RECEIVE_ANALYSTS', () => {
            expect(
                fetching(true, {
                    type: actions.RECEIVE_ANALYSTS,
                })
            ).toEqual(false);
        });
        it('should handle REQUEST_ANALYSTS_ERROR', () => {
            expect(
                fetching(true, {
                    type: actions.REQUEST_ANALYSTS_ERROR,
                })
            ).toEqual(false);
        });
    });
    describe('loadStatus', () => {
        it('should return the initial state', () => {
            expect(loadStatus(undefined, {})).toEqual('');
        });
        it('should handle REQUEST_ANALYSTS_ERROR', () => {
            const action = {
                type: actions.REQUEST_ANALYSTS_ERROR,
                error: new Error('message'),
            };
            expect(loadStatus('', action)).toEqual(action.error);
        });
        it('should handle RECEIVE_ANALYSTS', () => {
            const action = {
                type: actions.RECEIVE_ANALYSTS,
            };
            expect(loadStatus('', action)).toEqual('success');
        });
    });

    describe('hasEverLoaded', () => {
        it('should return the initial state', () => {
            expect(hasEverLoaded(undefined, {})).toEqual(false);
        });
        it('should handle RECEIVE_ANALYSTS', () => {
            expect(
                hasEverLoaded(false, {
                    type: actions.RECEIVE_ANALYSTS,
                })
            ).toEqual(true);
        });
        it('should handle REQUEST_ANALYSTS_ERROR', () => {
            expect(
                hasEverLoaded(false, {
                    type: actions.REQUEST_ANALYSTS_ERROR,
                })
            ).toEqual(true);
        });
    });
});
