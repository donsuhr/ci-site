/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { ChooseAnalystContainer } from './ChooseAnalyst.container';

describe('ChooseAnalystTime ChooseAnalystContainer', () => {
    it('renders', () => {
        const history = {
            replace: jest.fn(),
        };
        const wrapper = shallow(<ChooseAnalystContainer history={history} />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
