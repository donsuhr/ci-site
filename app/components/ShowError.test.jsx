/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ShowError from './ShowError';

describe('Loading', () => {
    it('renders', () => {
        const error = { message: 'Error Message' };
        const wrapper = shallow(<ShowError error={error} />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
