import React from 'react';
import SummaryStatistics from './components/SummaryStatistics.container';
import EvaluationList from './components/EvaluationList.container';

const Report = () => (
    <div>
        <SummaryStatistics />
        <EvaluationList />
    </div>
);

export default Report;
