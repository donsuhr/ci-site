import config from '../../../config';
import fetchItems from '../util/fetch-util';

const endpoint = `${config.domains.api}/session-evaluation/?per_page=999`;
// const endpoint = `https://api-3.campusmanagement.com/session-evaluation/`;

export const REQUEST_EVALUATIONS = 'REQUEST_EVALUATIONS';

export function requestEvaluations() {
    return {
        type: REQUEST_EVALUATIONS,
    };
}

export const RECEIVE_EVALUATIONS = 'RECEIVE_EVALUATIONS';

export function receiveEvaluations(items) {
    return {
        type: RECEIVE_EVALUATIONS,
        items: items.data,
    };
}

export const REQUEST_EVALUATION_ERROR = 'REQUEST_EVALUATION_ERROR';

export function requestEvaluationError(error) {
    return {
        type: REQUEST_EVALUATION_ERROR,
        error,
    };
}

export function fetchEvaluations() {
    return (dispatch, getState) => {
        const state = getState();
        const hasYear =
            Object.hasOwnProperty.call(state, 'domAttributes') &&
            Object.hasOwnProperty.call(state.domAttributes, 'year');
        const year = hasYear ? `&year=${state.domAttributes.year}` : '';
        return fetchItems({
            dispatch,
            endpoint: `${endpoint}${year}`,
            startAction: requestEvaluations,
            endAction: receiveEvaluations,
            errorAction: requestEvaluationError,
            getState,
            key: 'evaluations',
            credentials: 'include',
        });
    };
}

