/* global it,expect,describe,afterEach */
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import * as actions from './evaluation.actions';
import config from '../../../config';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('EvaluationReport actions', () => {
    it('should create an action to request evaluations ', () => {
        const expectedAction = {
            type: actions.REQUEST_EVALUATIONS,
        };
        expect(actions.requestEvaluations()).toEqual(expectedAction);
    });

    it('should create an action to receive evaluations', () => {
        const items = { data: [] };
        const expectedAction = {
            type: actions.RECEIVE_EVALUATIONS,
            items: items.data,
        };
        expect(actions.receiveEvaluations(items)).toEqual(expectedAction);
    });

    it('should create an action for evaluation request error', () => {
        const error = {};
        const expectedAction = {
            type: actions.REQUEST_EVALUATION_ERROR,
            error,
        };
        expect(actions.requestEvaluationError(error)).toEqual(expectedAction);
    });

    describe('async actions', () => {
        afterEach(() => {
            nock.cleanAll();
        });

        it('should create REQUEST_EVALUATIONS action to fetch sessions', () => {
            const returnData = {
                data: [{ session: {} }],
            };
            nock.disableNetConnect();
            nock(`${config.domains.api}/`)
                .get('/session-evaluation/')
                .query(true)
                .reply(200, returnData);

            const expectedActions = [
                { type: actions.REQUEST_EVALUATIONS },
                { type: actions.RECEIVE_EVALUATIONS, items: returnData.data },
            ];
            const store = mockStore({ sessions: [] });
            return store.dispatch(actions.fetchEvaluations()).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });
    });
});
