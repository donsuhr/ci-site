/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { EvaluationReportView } from './View';

describe('EvaluationReport View', () => {
    it('renders', () => {
        const wrapper = shallow(
            <EvaluationReportView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                listenToAllUsers={jest.fn()}
                user={{
                    initting: false,
                    isAuth: true,
                    loadingRoot: false,
                    isAdmin: true,
                }}
                allUsers={{
                    hasEverLoaded: true,
                    fetching: false,
                }}
            />
        );
        expect(wrapper.find('Report')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading data', () => {
        const wrapper = shallow(
            <EvaluationReportView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                listenToAllUsers={jest.fn()}
                user={{
                    initting: true,
                    isAuth: true,
                    loadingRoot: false,
                    isAdmin: true,
                }}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
        expect(
            wrapper
                .find('Loading')
                .dive()
                .text()
        ).toContain('Loading');
    });

    it('renders authenticating', () => {
        const wrapper = shallow(
            <EvaluationReportView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                listenToAllUsers={jest.fn()}
                user={{
                    initting: false,
                    isAuth: true,
                    loadingRoot: true,
                    isAdmin: true,
                }}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
        expect(
            wrapper
                .find('Loading')
                .dive()
                .text()
        ).toContain('Authenticating');
    });

    it('renders sign in button', () => {
        const wrapper = shallow(
            <EvaluationReportView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                listenToAllUsers={jest.fn()}
                user={{
                    initting: false,
                    isAuth: false,
                    loadingRoot: false,
                    isAdmin: true,
                }}
            />
        );
        expect(wrapper.find('button').text()).toContain('Sign In');
    });

    it('renders auth required', () => {
        const wrapper = shallow(
            <EvaluationReportView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                listenToAllUsers={jest.fn()}
                user={{
                    initting: false,
                    isAuth: true,
                    loadingRoot: false,
                    isAdmin: false,
                }}
            />
        );
        expect(wrapper.find('p').text()).toContain('Admin rights required');
    });

    it('renders loading data', () => {
        const wrapper = shallow(
            <EvaluationReportView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                listenToAllUsers={jest.fn()}
                user={{
                    initting: false,
                    isAuth: true,
                    loadingRoot: false,
                    isAdmin: true,
                }}
                allUsers={{
                    hasEverLoaded: true,
                    fetching: true,
                }}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
        expect(
            wrapper
                .find('Loading')
                .dive()
                .text()
        ).toContain('Loading Data');
    });
});
