import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import ReduxDevToolsLoader from '../redux/ReduxDevToolsLoader';
import View from './View';

const App = ({ store, ...props }) => (
    <Provider store={store}>
        <div>
            <View {...props} />
            <ReduxDevToolsLoader />
        </div>
    </Provider>
);

App.propTypes = {
    /* eslint-disable react/forbid-prop-types */
    store: PropTypes.object.isRequired,
};

export default App;
