/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Report from './Report';

describe('EvaluationReport Report', () => {
    it('renders', () => {
        const wrapper = shallow(<Report />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
