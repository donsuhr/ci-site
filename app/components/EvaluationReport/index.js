/* eslint global-require:0 */
import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer as ReactHotLoaderContainer } from 'react-hot-loader';
import * as domActions from '../redux/domAttributes.actions';
import App from './App';

let configureStore;
if (process.env.NODE_ENV === 'production') {
    configureStore = require('../redux/configureStore.prod.js').default;
} else {
    configureStore = require('../redux/configureStore.dev.js').default;
}

let mountNode;
let domAttributes;

const store = configureStore(undefined, require('./reducers').default);

const render = Component => {
    ReactDOM.render(
        React.createElement(
            ReactHotLoaderContainer,
            {},
            React.createElement(App, { store, ...domAttributes })
        ),
        mountNode
    );
};

export default {
    config(attrs, el) {
        store.dispatch(domActions.initDomAttributes(attrs));
        domAttributes = attrs;
        mountNode = el;
        render();
    },
};


if (process.env.NODE_ENV !== 'production') {
    if (module.hot) {
        module.hot.accept('./App', () => {
            render(App);
        });
        module.hot.accept('./reducers', () => {
            store.replaceReducer(require('./reducers').default);
        });
    }
}
