import React from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

const linkFormatter = (cell, row) => (
    <a href={`#session-${row.id}`}>
        {cell}
    </a>
);

const EvaluationListSummary = ({ sessions, evaluationsBySession, getSessionCompScores }) => {
    const tableData = Object.keys(sessions).map((sessionId) => {
        const count = Object.hasOwnProperty.call(evaluationsBySession, sessionId) ?
            evaluationsBySession[sessionId].length : 0;
        const session = sessions[sessionId];
        const compScores = getSessionCompScores(session);
        return {
            id: session._id,
            title: session.title,
            composite: compScores.averages.composite.toFixed(1),
            count,
            attendance: session.attendance || 0,
            overall: compScores.averages.overall.toFixed(1),
            conveyed_info: compScores.averages.conveyed_info.toFixed(1),
            materials: compScores.averages.materials.toFixed(1),
            usefull_at_work: compScores.averages.useful_at_work.toFixed(1),
            repeat_next_year: compScores.averages.repeat_next_year.toFixed(1),
        };
    });
    return (
        <div className="comp-score-report-section">
            <h2 className="cmc-article__intro-p">Composite Scores</h2>
            <BootstrapTable data={tableData} striped hover containerClass="eval-summary-table">
                <TableHeaderColumn
                    isKey
                    dataField="title"
                    dataFormat={linkFormatter}
                    tdStyle={{ whiteSpace: 'normal' }}
                    dataSort
                >
                    Product Name
                </TableHeaderColumn>
                <TableHeaderColumn
                    dataSort
                    width="95px"
                    dataField="composite"
                >
                    Composite
                </TableHeaderColumn>
                <TableHeaderColumn
                    dataSort
                    width="65px"
                    dataField="count"
                >
                    Evals
                </TableHeaderColumn>
                <TableHeaderColumn
                    dataSort
                    width="90px"
                    dataField="attendance"
                >
                    Attendees
                </TableHeaderColumn>
                <TableHeaderColumn
                    dataSort
                    width="75px"
                    dataField="overall"
                >
                    Overall
                </TableHeaderColumn>
                <TableHeaderColumn
                    dataSort
                    width="90px"
                    dataField="conveyed_info"
                >
                    Presenter
                </TableHeaderColumn>
                <TableHeaderColumn
                    dataSort
                    width="85px"
                    dataField="materials"
                >
                    Materials
                </TableHeaderColumn>
                <TableHeaderColumn
                    dataSort
                    width="95px"
                    dataField="usefull_at_work"
                >
                    Usefulness
                </TableHeaderColumn>
                <TableHeaderColumn
                    dataSort
                    width="75px"
                    dataField="repeat_next_year"
                >
                    Repeat
                </TableHeaderColumn>
            </BootstrapTable>
        </div>
    );
};

EvaluationListSummary.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    sessions: PropTypes.object,
    // eslint-disable-next-line react/forbid-prop-types
    evaluationsBySession: PropTypes.object,
    getSessionCompScores: PropTypes.func.isRequired,
};

export default EvaluationListSummary;

