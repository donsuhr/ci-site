import React from 'react';
import PropTypes from 'prop-types';

const SessionDetailList = ({
    sessions,
    evaluationsBySession,
    getSessionCompScores,
    getSessionPercentile,
}) => (
    <div>
        <h2 className="cmc-article__intro-p">
            Individual Session Scorecards, Comments & Topic Suggestions:
        </h2>

        {Object.keys(sessions).map(sessionId => {
            const count = Object.hasOwnProperty.call(
                evaluationsBySession,
                sessionId
            )
                ? evaluationsBySession[sessionId].length
                : 0;
            const session = sessions[sessionId];
            const compScores = getSessionCompScores(session);
            const ile = getSessionPercentile(compScores);
            return (
                <div
                    key={sessionId}
                    className="panel panel-default session-detail"
                >
                    <div className="panel-heading">
                        <h3
                            id={`session-${session._id}`}
                            className="panel-title"
                        >
                            {session.title}
                        </h3>
                    </div>
                    <div className="panel-body">
                        <p className="presented-by">
                            <strong>Presented By:</strong>&nbsp;
                            {session.presenter.map(x => (
                                <span key={x._id}>
                                    <span className="label label-primary">
                                        {x.firstName} {x.lastName}
                                    </span>
                                    &nbsp;
                                </span>
                            ))}
                        </p>

                        <div className="row">
                            <div className="col-xs-1">
                                <div className="comp-score-box">
                                    <div className="comp-score">
                                        {compScores.averages.composite.toFixed(
                                            1
                                        )}
                                    </div>
                                    <div className="comp-score-box__ile">
                                        {(100 * ile).toFixed(2)}%ile
                                    </div>
                                    <div className="comp-score-box__label">
                                        Composite Score
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-11">
                                <div className="row">
                                    <div className="col-xs-8">
                                        <div className="row details-row">
                                            <div className="col-xs-3">
                                                <div className="details-row__item">
                                                    {compScores.averages.overall.toFixed(
                                                        0
                                                    )}
                                                </div>
                                                <div className="score-box-label">
                                                    Overall
                                                </div>
                                            </div>
                                            <div className="col-xs-3">
                                                <div className="details-row__item">
                                                    {compScores.averages.conveyed_info.toFixed(
                                                        0
                                                    )}
                                                </div>
                                                <div className="score-box-label">
                                                    Presenter
                                                </div>
                                            </div>
                                            <div className="col-xs-3">
                                                <div className="details-row__item">
                                                    {compScores.averages
                                                        .materials < 0
                                                        ? 'N/A'
                                                        : compScores.averages.materials.toFixed(0)
                                                    }
                                                </div>
                                                <div className="score-box-label">
                                                    Materials
                                                </div>
                                            </div>
                                            <div className="col-xs-3">
                                                <div className="details-row__item">
                                                    {compScores.averages.useful_at_work.toFixed(
                                                        0
                                                    )}
                                                </div>
                                                <div className="score-box-label">
                                                    Usefulness
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xs-4">
                                        <div className="row">
                                            <div className="col-xs-6">
                                                <div className="details-row--alt__item">
                                                    {session.attendance || 0}
                                                </div>
                                                <div className="score-box-label">
                                                    Attendees
                                                </div>
                                            </div>
                                            <div className="col-xs-6">
                                                <div className="details-row--alt__item">
                                                    {count}
                                                </div>
                                                <div className="score-box-label">
                                                    Evaluations
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-6 session-detail__topics">
                                {evaluationsBySession[sessionId] &&
                                    evaluationsBySession[sessionId].some(
                                        x => x.comments && x.comments.length > 0
                                    ) && (
                                        <div>
                                            <h4 className="score-box-label">
                                                Comments
                                            </h4>
                                            <ul className="list--normal">
                                                {evaluationsBySession[
                                                    sessionId
                                                ] &&
                                                    evaluationsBySession[
                                                        sessionId
                                                    ].map(x => {
                                                        if (x.comments) {
                                                            return (
                                                                <li
                                                                    key={`${x._id}-comment`}
                                                                >
                                                                    {x.comments}
                                                                </li>
                                                            );
                                                        }
                                                        return null;
                                                    })}
                                            </ul>
                                        </div>
                                    )}
                            </div>
                            <div className="col-xs-6 session-detail__topics">
                                {evaluationsBySession[sessionId] &&
                                    evaluationsBySession[sessionId].some(
                                        x =>
                                            x.topics_next_year &&
                                            x.topics_next_year.length > 0
                                    ) && (
                                        <div>
                                            <h4 className="score-box-label">
                                                Topic Suggestions
                                            </h4>
                                            <ul className="list--normal">
                                                {evaluationsBySession[
                                                    sessionId
                                                ].map(x => {
                                                    if (x.topics_next_year) {
                                                        return (
                                                            <li
                                                                key={`${x._id}-topic`}
                                                            >
                                                                {
                                                                    x.topics_next_year
                                                                }
                                                            </li>
                                                        );
                                                    }
                                                    return null;
                                                })}
                                            </ul>
                                        </div>
                                    )}
                            </div>
                        </div>

                        {evaluationsBySession[sessionId] && (
                            <table className="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Composite</th>
                                        <th>Overall</th>
                                        <th>Presenter</th>
                                        <th>Materials</th>
                                        <th>Usefulness</th>
                                        <th>Experience Level</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {evaluationsBySession[sessionId].map(x => (
                                        <tr key={`eval-row-${x._id}`}>
                                            <td>
                                                {x.scores.composite.toFixed(2)}
                                            </td>
                                            <td>
                                                {x.scores.overall} | {x.overall}
                                            </td>
                                            <td>
                                                {x.scores.conveyed_info} |{' '}
                                                {x.conveyed_info}
                                            </td>
                                            <td>
                                                {x.scores.materials} |{' '}
                                                {x.materials}
                                            </td>
                                            <td>
                                                {x.scores.useful_at_work} |{' '}
                                                {x.useful_at_work}
                                            </td>
                                            <td>{x.user_experience_level}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        )}
                    </div>
                </div>
            );
        })}
    </div>
);

SessionDetailList.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    sessions: PropTypes.object,
    // eslint-disable-next-line react/forbid-prop-types
    evaluationsBySession: PropTypes.object,
    getSessionCompScores: PropTypes.func.isRequired,
    getSessionPercentile: PropTypes.func.isRequired,
};

export default SessionDetailList;
