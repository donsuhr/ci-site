import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchEvaluations } from '../evaluation.actions';
import { fetchSessions } from '../../SessionBrowser/actions';
import { PropType as sessionPropType } from '../../SessionBrowser/reducer';
import {
    PropType as evaluationPropType,
    getSessionCompScores,
    getSummaryStatistics,
    getSessionPercentile,
} from '../evaluation.reducer';
import EvaluationListSummary from './EvaluationListSummary';
import SessionDetailList from './SessionDetailList';

import ShowError from '../../ShowError';

export class EvaluationListContainer extends React.Component {
    static propTypes = {
        fetchEvaluations: PropTypes.func.isRequired,
        fetchSessions: PropTypes.func.isRequired,
        boundGetSessionCompScores: PropTypes.func.isRequired,
        boundGetSessionPercentile: PropTypes.func.isRequired,
        evaluations: evaluationPropType,
        sessions: sessionPropType,
    };

    componentDidMount() {
        if (
            this.props.evaluations.hasEverLoaded === false &&
            this.props.evaluations.fetching === false
        ) {
            this.props.fetchEvaluations();
        }
        if (
            this.props.sessions.hasEverLoaded === false &&
            this.props.sessions.fetching === false
        ) {
            this.props.fetchSessions();
        }
    }

    render() {
        const {
            evaluations,
            sessions,
            boundGetSessionCompScores,
            boundGetSessionPercentile,
        } = this.props;
        const { loadStatus } = evaluations;
        const showLoadError =
            loadStatus && loadStatus !== 'success' && loadStatus !== '';
        if (
            showLoadError &&
            Object.hasOwnProperty.call(evaluations.loadStatus, 'message')
        ) {
            return (
                <p>
                    An active login to the{'\u00A0'}
                    <a
                        href="https://admin-3.campusmanagement.com"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        campus administration site
                    </a>
                    {'\u00A0'}is required to view session evaluation data.
                </p>
            );
        }
        return (
            <div>
                <EvaluationListSummary
                    sessions={sessions.byId}
                    evaluationsBySession={evaluations.bySessionId}
                    getSessionCompScores={boundGetSessionCompScores}
                />
                <SessionDetailList
                    sessions={sessions.byId}
                    evaluationsBySession={evaluations.bySessionId}
                    getSessionCompScores={boundGetSessionCompScores}
                    getSessionPercentile={boundGetSessionPercentile}
                />
                {showLoadError && <ShowError error={loadStatus} />}
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    const statistics = getSummaryStatistics(state.evaluations, state.sessions);
    return {
        evaluations: state.evaluations,
        sessions: state.sessions,
        boundGetSessionCompScores: getSessionCompScores.bind(
            null,
            state.evaluations
        ),
        boundGetSessionPercentile: getSessionPercentile.bind(null, statistics),
    };
}

const EvaluationListContaineConnected = connect(mapStateToProps, {
    fetchEvaluations,
    fetchSessions,
})(EvaluationListContainer);

export default EvaluationListContaineConnected;
