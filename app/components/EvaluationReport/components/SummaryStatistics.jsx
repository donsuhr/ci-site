import React from 'react';
import PropTypes from 'prop-types';
import Loading from '../../Loading';

const SummaryStatistics = ({
    statistics, sessions, evaluations, loading,
}) => {
    if (loading) {
        return (
            <div className="summary-section">
                <h2 className="cmc-article__intro-p">Summary Statistics</h2>
                <Loading>{loading}</Loading>
            </div>
        );
    }

    const sessionCount = Object.keys(sessions.byId).length;
    const evaluationCount = Object.keys(evaluations.byId).length;
    const sessionEvalPercent =
        100 * (statistics.sessionsEvaluated / sessionCount);
    return (
        <div className="summary-section">
            <h2 className="cmc-article__intro-p">Summary Statistics</h2>
            <table className="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Component</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody className="summary-statistics-totals-table">
                    <tr>
                        <td>Sessions</td>
                        <td>{sessionCount}</td>
                    </tr>
                    <tr>
                        <td>Sessions Evaluated</td>
                        <td>
                            {statistics.sessionsEvaluated}&nbsp; ({sessionEvalPercent.toFixed(1)}%)
                        </td>
                    </tr>
                    <tr>
                        <td>Evaluations</td>
                        <td>{evaluationCount}</td>
                    </tr>
                    <tr>
                        <td>Average Submissions per Evaluated Session</td>
                        <td>
                            {(evaluationCount / statistics.sessionsEvaluated
                            ).toFixed(0)}
                        </td>
                    </tr>
                </tbody>
            </table>

            <table className="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Component</th>
                        <th>Average</th>
                        <th>Standard Deviation</th>
                        <th>High</th>
                        <th>Low</th>
                    </tr>
                </thead>
                <tbody className="summary-statistics-scores-table">
                    <tr>
                        <td>Composite Score</td>
                        <td>
                            {statistics.metrics.composite.averages.toFixed(2)}
                        </td>
                        <td>
                            {statistics.metrics.composite.sigmas.toFixed(2)}
                        </td>
                        <td>
                            {statistics.metrics.composite.high.value.toFixed(2)}
                        </td>
                        <td>
                            {statistics.metrics.composite.low.value.toFixed(2)}
                        </td>
                    </tr>
                    <tr>
                        <td>Attendees</td>
                        <td>
                            {statistics.metrics.attendance.averages.toFixed(0)}
                        </td>
                        <td>
                            {statistics.metrics.attendance.sigmas.toFixed(3)}
                        </td>
                        <td>{statistics.metrics.attendance.high.value}</td>
                        <td>{statistics.metrics.attendance.low.value}</td>
                    </tr>
                    <tr>
                        <td>Overall</td>
                        <td>
                            {statistics.metrics.overall.averages.toFixed(3)}
                        </td>
                        <td>{statistics.metrics.overall.sigmas.toFixed(3)}</td>
                        <td>{statistics.metrics.overall.high.value}</td>
                        <td>
                            {statistics.metrics.overall.low.value.toFixed(3)}
                        </td>
                    </tr>
                    <tr>
                        <td>Presenter</td>
                        <td>
                            {statistics.metrics.conveyed_info.averages.toFixed(
                                3
                            )}
                        </td>
                        <td>
                            {statistics.metrics.conveyed_info.sigmas.toFixed(3)}
                        </td>
                        <td>{statistics.metrics.conveyed_info.high.value}</td>
                        <td>
                            {statistics.metrics.conveyed_info.low.value.toFixed(
                                3
                            )}
                        </td>
                    </tr>
                    <tr>
                        <td>Materials</td>
                        <td>
                            {statistics.metrics.materials.averages.toFixed(3)}
                        </td>
                        <td>
                            {statistics.metrics.materials.sigmas.toFixed(3)}
                        </td>
                        <td>{statistics.metrics.materials.high.value}</td>
                        <td>
                            {statistics.metrics.materials.low.value.toFixed(3)}
                        </td>
                    </tr>
                    <tr>
                        <td>Usefulness</td>
                        <td>
                            {statistics.metrics.useful_at_work.averages.toFixed(
                                3
                            )}
                        </td>
                        <td>
                            {statistics.metrics.useful_at_work.sigmas.toFixed(
                                3
                            )}
                        </td>
                        <td>{statistics.metrics.useful_at_work.high.value}</td>
                        <td>
                            {statistics.metrics.useful_at_work.low.value.toFixed(
                                3
                            )}
                        </td>
                    </tr>
                    <tr>
                        <td>Repeat</td>
                        <td>
                            {statistics.metrics.repeat_next_year.averages.toFixed(
                                3
                            )}
                        </td>
                        <td>
                            {statistics.metrics.repeat_next_year.sigmas.toFixed(
                                3
                            )}
                        </td>
                        <td>
                            {statistics.metrics.repeat_next_year.high.value}
                        </td>
                        <td>
                            {statistics.metrics.repeat_next_year.low.value.toFixed(
                                3
                            )}
                        </td>
                    </tr>
                    <tr>
                        <td>Submission Rate</td>
                        <td>
                            {(100 * statistics.metrics.submissionRate.averages
                            ).toFixed(2)}%
                        </td>
                        <td>
                            {(100 * statistics.metrics.submissionRate.sigmas
                            ).toFixed(2)}%
                        </td>
                        <td>
                            {(100 * statistics.metrics.submissionRate.high.value
                            ).toFixed(2)}%
                        </td>
                        <td>
                            {(100 * statistics.metrics.submissionRate.low.value
                            ).toFixed(2)}%
                        </td>
                    </tr>
                    <tr>
                        <td>Experience Level</td>
                        <td>
                            {statistics.metrics.user_experience_level.averages.toFixed(
                                1
                            )}
                        </td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
};

SummaryStatistics.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    sessions: PropTypes.object,
    // eslint-disable-next-line react/forbid-prop-types
    statistics: PropTypes.object,
    // eslint-disable-next-line react/forbid-prop-types
    evaluations: PropTypes.object,
    loading: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
    ]),
};

export default SummaryStatistics;
