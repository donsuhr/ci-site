/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SummaryStatistics from './SummaryStatistics';

describe('EvaluationReport SummaryStatistics ', () => {
    it('renders', () => {
        const wrapper = shallow(
            <SummaryStatistics
                statistics={{
                    metrics: {
                        composite: {
                            averages: 22,
                            sigmas: 22,
                            high: { value: 22 },
                            low: { value: 22 },
                        },
                        attendance: {
                            averages: 22,
                            sigmas: 22,
                            high: { value: 22 },
                            low: { value: 22 },
                        },
                        overall: {
                            averages: 22,
                            sigmas: 22,
                            high: { value: 22 },
                            low: { value: 22 },
                        },
                        conveyed_info: {
                            averages: 22,
                            sigmas: 22,
                            high: { value: 22 },
                            low: { value: 22 },
                        },
                        materials: {
                            averages: 22,
                            sigmas: 22,
                            high: { value: 22 },
                            low: { value: 22 },
                        },
                        useful_at_work: {
                            averages: 22,
                            sigmas: 22,
                            high: { value: 22 },
                            low: { value: 22 },
                        },
                        repeat_next_year: {
                            averages: 22,
                            sigmas: 22,
                            high: { value: 22 },
                            low: { value: 22 },
                        },
                        submissionRate: {
                            averages: 22,
                            sigmas: 22,
                            high: { value: 22 },
                            low: { value: 22 },
                        },
                        user_experience_level: {
                            averages: 22,
                        },
                    },
                }}
                sessions={{
                    byId: {},
                }}
                evaluations={{
                    byId: {},
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading data', () => {
        const wrapper = shallow(<SummaryStatistics loading="loading test" />);
        expect(wrapper.find('Loading')).toHaveLength(1);
        expect(
            wrapper
                .find('Loading')
                .dive()
                .text()
        ).toContain('loading test');
    });
});
