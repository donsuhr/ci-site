/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { SummaryStatisticsContainer } from './SummaryStatistics.container';

describe('EvaluationReport ReportContainer', () => {
    it('renders', () => {
        const wrapper = shallow(
            <SummaryStatisticsContainer
                fetchEvaluations={jest.fn()}
                fetchSessions={jest.fn()}
                evaluations={{
                    loadStatus: 'success',
                    hasEverLoaded: true,
                    fetching: false,
                }}
                sessions={{
                    hasEverLoaded: true,
                    fetching: false,
                }}
            />
        );
        expect(wrapper.find('SummaryStatistics')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading Evaluations', () => {
        const wrapper = shallow(
            <SummaryStatisticsContainer
                fetchEvaluations={jest.fn()}
                fetchSessions={jest.fn()}
                evaluations={{
                    loadStatus: 'success',
                    hasEverLoaded: false,
                    fetching: false,
                }}
                sessions={{
                    hasEverLoaded: true,
                    fetching: false,
                }}
            />
        );
        expect(wrapper.find('SummaryStatistics').props().loading).toContain(
            'Evaluations'
        );
    });
    it('renders loading Sessions', () => {
        const wrapper = shallow(
            <SummaryStatisticsContainer
                fetchEvaluations={jest.fn()}
                fetchSessions={jest.fn()}
                evaluations={{
                    loadStatus: 'success',
                    hasEverLoaded: true,
                    fetching: false,
                }}
                sessions={{
                    hasEverLoaded: false,
                    fetching: false,
                }}
            />
        );
        expect(wrapper.find('SummaryStatistics').props().loading).toContain(
            'Sessions'
        );
    });
    it('renders load error', () => {
        const wrapper = shallow(
            <SummaryStatisticsContainer
                fetchEvaluations={jest.fn()}
                fetchSessions={jest.fn()}
                evaluations={{
                    loadStatus: new Error('error'),
                    hasEverLoaded: true,
                    fetching: false,
                }}
                sessions={{
                    hasEverLoaded: false,
                    fetching: false,
                }}
            />
        );
        expect(wrapper.find('p').text()).toContain(
            'campus administration site'
        );
    });
});
