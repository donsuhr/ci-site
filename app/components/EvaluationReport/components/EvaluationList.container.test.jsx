/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { EvaluationListContainer } from './EvaluationList.container';

describe('Passport View', () => {
    it('renders', () => {
        const wrapper = shallow(
            <EvaluationListContainer
                fetchEvaluations={() => true}
                fetchSessions={() => true}
                boundGetSessionCompScores={() => true}
                boundGetSessionPercentile={() => true}
                evaluations={{
                    loadStatus: 'success',
                    byId: {},
                }}
                sessions={{
                    byId: {},
                }}
            />
        );
        expect(wrapper.find('EvaluationListSummary')).toHaveLength(1);
        expect(wrapper.find('SessionDetailList')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders error not auth on admin3', () => {
        const wrapper = shallow(
            <EvaluationListContainer
                fetchEvaluations={() => true}
                fetchSessions={() => true}
                boundGetSessionCompScores={() => true}
                boundGetSessionPercentile={() => true}
                evaluations={{
                    loadStatus: new Error('test error'),
                    byId: {},
                }}
            />
        );
        expect(wrapper.find('a').text()).toContain(
            'campus administration site'
        );
    });
});
