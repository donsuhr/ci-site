import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchEvaluations } from '../evaluation.actions';
import { fetchSessions } from '../../SessionBrowser/actions';
import {
    PropType as evaluationPropType,
    getSummaryStatistics,
} from '../evaluation.reducer';
import { PropType as sessionPropType } from '../../SessionBrowser/reducer';
import SummaryStatistics from './SummaryStatistics';

import ShowError from '../../ShowError';

export class SummaryStatisticsContainer extends React.Component {
    static propTypes = {
        fetchEvaluations: PropTypes.func.isRequired,
        fetchSessions: PropTypes.func.isRequired,
        evaluations: evaluationPropType,
        sessions: sessionPropType,
        // eslint-disable-next-line react/forbid-prop-types
        statistics: PropTypes.object,
    };

    componentDidMount() {
        if (
            this.props.evaluations.hasEverLoaded === false &&
            this.props.evaluations.fetching === false
        ) {
            this.props.fetchEvaluations();
        }
        if (
            this.props.sessions.hasEverLoaded === false &&
            this.props.sessions.fetching === false
        ) {
            this.props.fetchSessions();
        }
    }

    render() {
        const { evaluations, sessions, statistics } = this.props;
        const { loadStatus } = evaluations;
        let loading = false;
        if (!evaluations.hasEverLoaded || evaluations.fetching) {
            loading = 'Loading Evaluations...';
        }
        if (!sessions.hasEverLoaded || sessions.fetching) {
            loading = 'Loading Sessions...';
        }

        const showLoadError =
            loadStatus && loadStatus !== 'success' && loadStatus !== '';
        if (
            showLoadError &&
            Object.hasOwnProperty.call(evaluations.loadStatus, 'message')
        ) {
            return (
                <p>
                    An active login to the{'\u00A0'}
                    <a
                        href="https://admin-3.campusmanagement.com"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        campus administration site
                    </a>
                    {'\u00A0'}is required to view session evaluation data.
                </p>
            );
        }
        return (
            <div>
                <SummaryStatistics
                    statistics={statistics}
                    sessions={sessions}
                    evaluations={evaluations}
                    loading={loading}
                />
                {showLoadError && <ShowError error={loadStatus} />}
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        evaluations: state.evaluations,
        sessions: state.sessions,
        statistics: getSummaryStatistics(state.evaluations, state.sessions),
    };
}

const connected = connect(mapStateToProps, {
    fetchEvaluations,
    fetchSessions,
})(SummaryStatisticsContainer);

export default connected;
