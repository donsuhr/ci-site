import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    listenToAuth,
    requestPopupAuth,
    listenToUserRoot,
} from '../firebase/auth.actions';
import { listenToAllUsers } from '../firebase/allUsers.actions';
import { PropType as userPropType } from '../firebase/auth.reducer';
import { PropType as allUsersPropType } from '../firebase/allUsers.reducer';

import Report from './Report';
import Loading from '../Loading';

export class EvaluationReportView extends React.Component {
    static propTypes = {
        listenToAuth: PropTypes.func.isRequired,
        listenToUserRoot: PropTypes.func.isRequired,
        listenToAllUsers: PropTypes.func.isRequired,
        user: userPropType,
        allUsers: allUsersPropType,
        year: PropTypes.string,
    };

    componentDidMount() {
        this.props.listenToAuth().then(user => {
            this.props.listenToUserRoot(user.uid);
            this.props.listenToAllUsers();
        });
    }

    onSignInClick = event => {
        event.preventDefault();
        requestPopupAuth(this.props.year);
    };

    render() {
        if (this.props.user.initting) {
            return <Loading>Loading...</Loading>;
        }
        if (this.props.user.loadingRoot) {
            return <Loading>Authenticating...</Loading>;
        }
        if (!this.props.user.isAuth) {
            return (
                <button
                    type="button"
                    className="cmc-article__link"
                    onClick={this.onSignInClick}
                >
                    Sign In
                </button>
            );
        }
        if (!this.props.user.isAdmin) {
            return <p>Admin rights required.</p>;
        }
        if (
            this.props.allUsers.hasEverLoaded === false ||
            this.props.allUsers.fetching
        ) {
            return <Loading>Loading Data...</Loading>;
        }
        return <Report />;
    }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user,
        allUsers: state.allUsers,
        year: state.domAttributes.year,
    };
}

const EvaluationReportViewConnected = connect(mapStateToProps, {
    listenToAuth,
    listenToUserRoot,
    listenToAllUsers,
})(EvaluationReportView);

export default EvaluationReportViewConnected;
