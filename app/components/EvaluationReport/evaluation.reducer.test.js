/* global it,expect,describe */
import {
    hasEverLoaded,
    fetching,
    byId,
    loadStatus,
    bySessionId,
} from './evaluation.reducer';
import * as actions from './evaluation.actions';

describe('EvaluationReport reducer', () => {
    describe('hasEverLoaded', () => {
        it('should return the initial state', () => {
            expect(hasEverLoaded(undefined, {})).toEqual(false);
        });
        it('should handle RECEIVE_EVALUATIONS', () => {
            expect(
                hasEverLoaded(false, {
                    type: actions.RECEIVE_EVALUATIONS,
                })
            ).toEqual(true);
        });
        it('should handle REQUEST_EVALUATION_ERROR', () => {
            expect(
                hasEverLoaded(false, {
                    type: actions.REQUEST_EVALUATION_ERROR,
                })
            ).toEqual(true);
        });
    });

    describe('fetching', () => {
        it('should return the initial state', () => {
            expect(fetching(undefined, {})).toEqual(false);
        });
        it('should handle REQUEST_EVALUATIONS', () => {
            expect(
                fetching(false, {
                    type: actions.REQUEST_EVALUATIONS,
                })
            ).toEqual(true);
        });
        it('should handle RECEIVE_EVALUATIONS', () => {
            expect(
                fetching(true, {
                    type: actions.RECEIVE_EVALUATIONS,
                })
            ).toEqual(false);
        });
        it('should handle REQUEST_EVALUATION_ERROR', () => {
            expect(
                fetching(true, {
                    type: actions.REQUEST_EVALUATION_ERROR,
                })
            ).toEqual(false);
        });
    });
    describe('byId', () => {
        it('should return the initial state', () => {
            expect(byId(undefined, {})).toEqual({});
        });
        it('should handle RECEIVE_EVALUATIONS', () => {
            let state = { 123: { foo: 'bar' } };
            const action = {
                type: actions.RECEIVE_EVALUATIONS,
                items: [{ _id: '123' }],
            };
            expect(byId(state, action)).toEqual({
                123: {
                    _id: '123',
                    foo: 'bar',
                    savingStatus: '',
                    scores: {
                        composite: 15,
                        conveyed_info: 50,
                        materials: 50,
                        overall: 50,
                        repeat_next_year: 50,
                        useful_at_work: 50,
                        user_experience_level: 50,
                    },
                },
            });
            state = {};
            expect(byId(state, action)).toEqual({
                123: {
                    _id: '123',
                    savingStatus: '',
                    scores: {
                        composite: 15,
                        conveyed_info: 50,
                        materials: 50,
                        overall: 50,
                        repeat_next_year: 50,
                        useful_at_work: 50,
                        user_experience_level: 50,
                    },
                },
            });
        });
    });
    describe('loadStatus', () => {
        it('should return the initial state', () => {
            expect(loadStatus(undefined, {})).toEqual('');
        });
        it('should handle REQUEST_EVALUATION_ERROR', () => {
            const action = {
                type: actions.REQUEST_EVALUATION_ERROR,
                error: new Error('message'),
            };
            expect(loadStatus('', action)).toEqual(action.error);
        });
        it('should handle RECEIVE_EVALUATIONS', () => {
            const action = {
                type: actions.RECEIVE_EVALUATIONS,
            };
            expect(loadStatus('', action)).toEqual('success');
        });
    });
    describe('bySessionId', () => {
        it('should return the initial state', () => {
            expect(byId(undefined, {})).toEqual({});
        });
        it('should handle RECEIVE_EVALUATIONS', () => {
            let state = { 123: { foo: 'bar' } };
            const action = {
                type: actions.RECEIVE_EVALUATIONS,
                items: [{ session_id: '123' }],
            };
            expect(bySessionId(state, action)).toEqual({
                123: [
                    {
                        foo: 'bar',
                        savingStatus: '',
                        scores: {
                            composite: 15,
                            conveyed_info: 50,
                            materials: 50,
                            overall: 50,
                            repeat_next_year: 50,
                            useful_at_work: 50,
                            user_experience_level: 50,
                        },
                        session_id: '123',
                    },
                ],
            });
            state = {};
            expect(bySessionId(state, action)).toEqual({
                123: [
                    {
                        savingStatus: '',
                        scores: {
                            composite: 15,
                            conveyed_info: 50,
                            materials: 50,
                            overall: 50,
                            repeat_next_year: 50,
                            useful_at_work: 50,
                            user_experience_level: 50,
                        },
                        session_id: '123',
                    },
                ],
            });
        });
    });
});
