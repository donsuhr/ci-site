import PropTypes from 'prop-types';
import { combineReducers } from 'redux';
import * as actions from './evaluation.actions';

function getNumericScoreForValue(value = '') {
    const val = value.toLowerCase();
    const scoreValues = {
        excellent: 100,
        'strongly agree': 100,
        yes: 100,
        agree: 75,
        'above average': 75,
        neutral: 51,
        average: 52,
        'not applicable': 53,
        disagree: 25,
        'below average': 25,
        'strongly disagree': 0,
        poor: 0,
        no: 0,
        advanced: 100,
        intermediate: 50,
        beginner: 0,
    };
    if (value !== '' && !Object.hasOwnProperty.call(scoreValues, val)) {
        // eslint-disable-next-line no-console
        console.log('couldnt find value for score', val);
    }
    return Object.hasOwnProperty.call(scoreValues, val) ? scoreValues[val] : 50;
}

const item = (state = {}, action, currentItem) => {
    switch (action.type) {
        case actions.RECEIVE_EVALUATIONS:
            const scores = {
                overall: getNumericScoreForValue(currentItem.overall),
                conveyed_info: getNumericScoreForValue(currentItem.conveyed_info),
                repeat_next_year: getNumericScoreForValue(currentItem.repeat_next_year),
                user_experience_level: getNumericScoreForValue(currentItem.user_experience_level),
                materials: getNumericScoreForValue(currentItem.materials),
                useful_at_work: getNumericScoreForValue(currentItem.useful_at_work),
            };
            scores.composite = (10 * (scores.overall / 100)) +
                (5 * (scores.conveyed_info / 100)) +
                (5 * (scores.repeat_next_year / 100)) +
                (5 * (scores.materials / 100)) +
                (5 * (scores.useful_at_work / 100));

            return {
                ...state,
                ...currentItem,
                scores,
                savingStatus: '',
            };

        default:
            return state;
    }
};

export const byId = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_EVALUATIONS:
            return action.items.reduce((p, c) => {
                p[c._id] = item(state[c._id], action, c);
                return p;
            }, {});

        default:
            return state;
    }
};

export const bySessionId = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_EVALUATIONS:
            return action.items.reduce((p, c) => {
                if (!Object.hasOwnProperty.call(p, c.session_id)) {
                    p[c.session_id] = [];
                }
                p[c.session_id].push(item(state[c.session_id], action, c));
                return p;
            }, {});

        default:
            return state;
    }
};

export const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_EVALUATIONS:
            return true;
        case actions.RECEIVE_EVALUATIONS:
        case actions.REQUEST_EVALUATION_ERROR:
            return false;
        default:
            return state;
    }
};

export const loadStatus = (state = '', action) => {
    switch (action.type) {
        case actions.REQUEST_EVALUATION_ERROR:
            return action.error;
        case actions.RECEIVE_EVALUATIONS:
            return 'success';
        default:
            return state;
    }
};

export const hasEverLoaded = (state = false, action) => {
    switch (action.type) {
        case actions.RECEIVE_EVALUATIONS:
        case actions.REQUEST_EVALUATION_ERROR:
            return true;
        default:
            return state;
    }
};

const evaluations = combineReducers({
    hasEverLoaded,
    fetching,
    byId,
    bySessionId,
    loadStatus,
});

const PropType = PropTypes.shape({
    hasEverLoaded: PropTypes.bool,
    fetching: PropTypes.bool,
    loadStatus: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.instanceOf(Error),
    ]),
    byId: PropTypes.object,
    bySessionId: PropTypes.object,
});

function getSessionCompScores(state, session) {
    const evals = state.bySessionId[session._id];
    const attendees = session.attendance || 0;
    let sums = {
        overall: 0,
        conveyed_info: 0,
        repeat_next_year: 0,
        composite: 0,
        user_experience_level: 0,
        materials: 0,
        useful_at_work: 0,
    };

    let averages = {
        overall: 0,
        conveyed_info: 0,
        repeat_next_year: 0,
        composite: 0,
        user_experience_level: 0,
        materials: 0,
        useful_at_work: 0,
    };
    const numEvals = evals ? evals.length : 0;
    const submissionRate = attendees ? numEvals / attendees : 0;

    if (evals) {
        sums = evals.reduce((acc, x) => {
            acc.overall += x.scores.overall;
            acc.conveyed_info += x.scores.conveyed_info;
            acc.repeat_next_year += x.scores.repeat_next_year;
            acc.composite += x.scores.composite;
            acc.user_experience_level += x.scores.user_experience_level;
            acc.materials += x.scores.materials;
            acc.useful_at_work += x.scores.useful_at_work;
            return acc;
        }, sums);

        averages = {
            overall: sums.overall / numEvals,
            conveyed_info: sums.conveyed_info / numEvals,
            repeat_next_year: sums.repeat_next_year / numEvals,
            user_experience_level: sums.user_experience_level / numEvals,
            materials: sums.materials / numEvals,
            useful_at_work: sums.useful_at_work / numEvals,
            composite: (1 * (attendees / 30)) +
            (5 * (numEvals / attendees)) +
            (3 * (sums.composite / (0.0000000000000000001 + numEvals))),
        };
    }
    return {
        sums,
        averages,
        submissionRate,
    };
}

function getSummaryStatistics(state, sessionState) {
    if (!state.hasEverLoaded || state.fetching) {
        return {};
    }
    if (!sessionState.hasEverLoaded || sessionState.fetching) {
        return {};
    }
    const sessionsEvaluated = Object.keys(state.bySessionId).length || 0;
    const totalSessions = Object.keys(sessionState.byId).length || 0;

    const metrics = {
        overall: {
            sum: 0,
            standDev: 0,
            high: { value: 0, session: {} },
            low: { value: 100, session: {} },
        },
        conveyed_info: {
            sum: 0,
            standDev: 0,
            high: { value: 0, session: {} },
            low: { value: 100, session: {} },
        },
        repeat_next_year: {
            sum: 0,
            standDev: 0,
            high: { value: 0, session: {} },
            low: { value: 100, session: {} },
        },
        composite: {
            sum: 0,
            standDev: 0,
            high: { value: 0, session: {} },
            low: { value: 100, session: {} },
        },
        user_experience_level: {
            sum: 0,
            standDev: 0,
            high: { value: 0, session: {} },
            low: { value: 100, session: {} },
        },
        materials: {
            sum: 0,
            standDev: 0,
            high: { value: 0, session: {} },
            low: { value: 100, session: {} },
        },
        useful_at_work: {
            sum: 0,
            standDev: 0,
            high: { value: 0, session: {} },
            low: { value: 100, session: {} },
        },
        submissionRate: {
            sum: 0,
            standDev: 0,
            high: { value: 0, session: {} },
            low: { value: 100, session: {} },
        },
        attendance: {
            sum: 0,
            standDev: 0,
            high: { value: 0, session: {} },
            low: { value: 100, session: {} },
        },
    };

    Object.keys(state.bySessionId).forEach((sessionId) => {
        const session = sessionState.byId[sessionId];
        if (session) {
            const compScores = getSessionCompScores(state, session);

            const sumMinMaxKeys = [
                'overall',
                'conveyed_info',
                'repeat_next_year',
                'composite',
                'user_experience_level',
                'materials',
                'useful_at_work',
            ];
            sumMinMaxKeys.forEach((key) => {
                metrics[key].sum += compScores.averages[key];
                if (compScores.averages[key] > metrics[key].high.value) {
                    metrics[key].high.value = compScores.averages[key];
                    metrics[key].high.session = session;
                }
                if (compScores.averages[key] < metrics[key].low.value) {
                    metrics[key].low.value = compScores.averages[key];
                    metrics[key].low.session = session;
                }
            });
            metrics.submissionRate.sum += compScores.submissionRate;
            if (compScores.submissionRate > metrics.submissionRate.high.value) {
                metrics.submissionRate.high.value = compScores.submissionRate;
                metrics.submissionRate.high.session = session;
            }
            if (compScores.submissionRate < metrics.submissionRate.low.value) {
                metrics.submissionRate.low.value = compScores.submissionRate;
                metrics.submissionRate.low.session = session;
            }
        }
    });

    Object.keys(sessionState.byId).forEach((sessionId) => {
        // uses sessionState
        const session = sessionState.byId[sessionId];
        const sessionAttendance = (session && session.attendance) || 0;
        metrics.attendance.sum += sessionAttendance;
        if (sessionAttendance > metrics.attendance.high.value) {
            metrics.attendance.high.value = sessionAttendance;
            metrics.attendance.high.session = session;
        }
        if (sessionAttendance < metrics.attendance.low.value) {
            metrics.attendance.low.value = sessionAttendance;
            metrics.attendance.low.session = session;
        }
    });
    // compute averages
    // needs sums first
    metrics.overall.averages = metrics.overall.sum / sessionsEvaluated;
    metrics.conveyed_info.averages = metrics.conveyed_info.sum / sessionsEvaluated;
    metrics.repeat_next_year.averages = metrics.repeat_next_year.sum / sessionsEvaluated;
    metrics.composite.averages = metrics.composite.sum / sessionsEvaluated;
    metrics.user_experience_level.averages = metrics.user_experience_level.sum / sessionsEvaluated;
    metrics.materials.averages = metrics.materials.sum / sessionsEvaluated;
    metrics.useful_at_work.averages = metrics.useful_at_work.sum / sessionsEvaluated;
    metrics.attendance.averages = metrics.attendance.sum / totalSessions;
    metrics.submissionRate.averages = metrics.submissionRate.sum / totalSessions;

    Object.keys(state.bySessionId).forEach((sessionId) => {
        // set standard deviations
        // needs averages completed first
        const session = sessionState.byId[sessionId];
        if (session) {
            const compScores = getSessionCompScores(state, session);
            Object.keys(metrics).forEach((key) => {
                if (key !== 'attendance' && key !== 'submissionRate') {
                    metrics[key].standDev += (compScores.averages[key] - metrics[key].averages) *
                        (compScores.averages[key] - metrics[key].averages);
                }
            });

            const sessionAttendance = (session && session.attendance) || 0;
            metrics.attendance.standDev +=
                (sessionAttendance - metrics.attendance.averages) *
                (sessionAttendance - metrics.attendance.averages);

            metrics.submissionRate.standDev +=
                (compScores.submissionRate - metrics.submissionRate.averages) *
                (compScores.submissionRate - metrics.submissionRate.averages);
        }
    });

    // compute sigmas
    // needs standard deviations first
    metrics.attendance.sigmas = Math.sqrt(metrics.attendance.standDev / totalSessions);
    metrics.submissionRate.sigmas = Math.sqrt(metrics.submissionRate.standDev / totalSessions);
    metrics.overall.sigmas = Math.sqrt(metrics.overall.standDev / sessionsEvaluated);
    metrics.conveyed_info.sigmas = Math.sqrt(metrics.conveyed_info.standDev / sessionsEvaluated);
    metrics.composite.sigmas = Math.sqrt(metrics.composite.standDev / sessionsEvaluated);
    metrics.materials.sigmas = Math.sqrt(metrics.materials.standDev / sessionsEvaluated);
    metrics.useful_at_work.sigmas = Math.sqrt(metrics.useful_at_work.standDev / sessionsEvaluated);
    metrics.repeat_next_year.sigmas =
        Math.sqrt(metrics.repeat_next_year.standDev / sessionsEvaluated);

    return {
        metrics,
        sessionsEvaluated,
    };
}

function GetZPercent(z) {
    // z == number of standard deviations from the mean

    // if z is greater than 6.5 standard deviations from the mean
    // the number of significant digits will be outside of a reasonable
    // range
    if (z < -6.5) {
        return 0.0;
    }
    if (z > 6.5) {
        return 1.0;
    }
    let factK = 1;
    let sum = 0;
    let term = 1;
    let k = 0;
    const loopStop = Math.exp(-23);
    while (Math.abs(term) > loopStop) {
        // eslint-disable-next-line
        term = 0.3989422804 * Math.pow(-1, k) * Math.pow(z, k) / (2 * k + 1) / Math.pow(2, k) * Math.pow(z, k + 1) / factK;
        sum += term;
        k += 1;
        factK *= k;
    }
    sum += 0.5;

    return sum;
}

function getSessionPercentile(summaryStatistics, compScores) {
    try {
        const composite =
            (compScores.averages.composite - summaryStatistics.metrics.composite.averages)
            / summaryStatistics.metrics.composite.sigmas;
        return GetZPercent(composite);
    } catch (e) {
        return 0;
    }
}

export {
    evaluations as default,
    PropType,
    getSessionCompScores,
    getSummaryStatistics,
    getSessionPercentile,
};
