/* global it,expect,describe */

import React from 'react';
import { mount, shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Loading from './Loading';

Object.defineProperties(window.HTMLElement.prototype, {
    offsetLeft: {
        get() {
            return parseFloat(window.getComputedStyle(this).marginLeft) || 0;
        },
    },
    offsetTop: {
        get() {
            return parseFloat(window.getComputedStyle(this).marginTop) || 0;
        },
    },
    offsetHeight: {
        get() {
            return parseFloat(window.getComputedStyle(this).height) || 0;
        },
    },
    offsetWidth: {
        get() {
            return parseFloat(window.getComputedStyle(this).width) || 0;
        },
    },
});

describe('Loading', () => {
    it('renders', () => {
        const wrapper = shallow(<Loading>Loading...</Loading>);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const text = 'Loading Data...';
        const wrapper = shallow(
            <Loading>
                {text}
            </Loading>
        );
        expect(wrapper.text()).toContain(text);
    });

    it('renders sets x, y', () => {
        const wrapper = mount(
            <div style={{ width: 400, height: 300 }}>
                <Loading>Loading...</Loading>
            </div>
        );
        expect(wrapper.find('Loading').getNode().node.style.left).toBe('200px');
        expect(wrapper.find('Loading').getNode().node.style.top).toBe('150px');
    });
});
