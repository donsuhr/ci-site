import React from 'react';
import PropTypes from 'prop-types';
import moment from '../util/load-moment-tz';

const ScheduleConflict = ({ session, overlaps }) => (
    <div>
        <p>The session being added:</p>
        <p className="schedule-conflict-modal__session-wrapper">
            <span className="schedule-conflict-modal__session-title">
                {session.title}
            </span>
            <span className="schedule-conflict-modal__session-time">
                {moment(session.start)
                    .tz('America/New_York')
                    .format('h:mm a')}{' '}
                -&nbsp;
                {moment(session.end)
                    .tz('America/New_York')
                    .format('h:mm a')}
            </span>
        </p>
        <p>Conflicts with the following:</p>
        <ul className="schedule-conflict-modal__overlaps-list">
            {overlaps.map(x => (
                <li key={x.id}>
                    <span className="schedule-conflict-modal__session-title">
                        {x.title}
                    </span>
                    <span className="schedule-conflict-modal__session-time">
                        {moment(x.start)
                            .tz('America/New_York')
                            .format('h:mm a')}{' '}
                        -&nbsp;
                        {moment(x.end)
                            .tz('America/New_York')
                            .format('h:mm a')}
                    </span>
                </li>
            ))}
        </ul>
        <div className="cmc-form__submit-row">
            <button
                className="schedule-conflict-modal__cancel-btn"
                data-dismiss="modal"
            >
                Cancel
            </button>
            <button className="schedule-conflict-modal__replace-btn">
                Replace
            </button>
        </div>
    </div>
);

ScheduleConflict.propTypes = {
    session: PropTypes.shape({
        start: PropTypes.oneOfType([
            PropTypes.instanceOf(Date),
            PropTypes.string,
        ]),
        title: PropTypes.string,
    }),
    overlaps: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string,
            title: PropTypes.string,
            start: PropTypes.oneOfType([
                PropTypes.instanceOf(Date),
                PropTypes.string,
            ]),
        })
    ),
};

export default ScheduleConflict;
