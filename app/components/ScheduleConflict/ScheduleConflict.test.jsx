/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ScheduleConflict from './ScheduleConflict';

describe('ScheduleConflict', () => {
    it('renders', () => {
        const wrapper = shallow(
            <ScheduleConflict
                session={{
                    title: 'session title',
                    start: '2017-04-27T17:00:30.000Z',
                    end: '2017-04-27T17:01:30.000Z',
                }}
                overlaps={[
                    {
                        id: '123',
                        title: 'overlap title',
                        start: '2017-04-27T17:00:30.000Z',
                        end: '2017-04-27T17:01:30.000Z',
                    },
                ]}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
