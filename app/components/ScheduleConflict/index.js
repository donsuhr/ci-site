import React from 'react';
import ReactDOM from 'react-dom';
import ScheduleConflict from './ScheduleConflict';

let mountNode;
let domAttributes;

let render = () => {
    ReactDOM.render(
        React.createElement(ScheduleConflict, {
            ...domAttributes,
            mountNode,
        }),
        mountNode
    );
};

export default {
    config(attrs, el) {
        mountNode = el;
        domAttributes = attrs;
        render();
    },
    destroy(node) {
        ReactDOM.unmountComponentAtNode(node);
    },
};

if (process.env.NODE_ENV !== 'production') {
    if (module.hot) {
        // Development render functions
        const renderApp = render;
        const renderError = (error) => {
            // eslint-disable-next-line import/no-extraneous-dependencies, global-require
            const RedBox = require('redbox-react').default;

            ReactDOM.render(React.createElement(RedBox, { error }), mountNode);
        };

        // Wrap render in try/catch
        render = () => {
            try {
                renderApp();
            } catch (error) {
                renderError(error);
            }
        };
    }
}
