/* global it,expect,describe */
import {
    hasEverLoaded,
    fetching,
    byId,
    loadStatus,
    uniqueTracks,
    completedEvals,
} from './reducer';
import * as actions from './actions';

describe('SessionBrowser reducer', () => {
    describe('hasEverLoaded', () => {
        it('should return the initial state', () => {
            expect(hasEverLoaded(undefined, {})).toEqual(false);
        });
        it('should handle RECEIVE_SESSIONS', () => {
            expect(
                hasEverLoaded(false, {
                    type: actions.RECEIVE_SESSIONS,
                })
            ).toEqual(true);
        });
        it('should handle REQUEST_SESSIONS_ERROR', () => {
            expect(
                hasEverLoaded(false, {
                    type: actions.REQUEST_SESSIONS_ERROR,
                })
            ).toEqual(true);
        });
    });
    describe('fetching', () => {
        it('should return the initial state', () => {
            expect(fetching(undefined, {})).toEqual(false);
        });
        it('should handle REQUEST_SESSIONS', () => {
            expect(
                fetching(false, {
                    type: actions.REQUEST_SESSIONS,
                })
            ).toEqual(true);
        });
        it('should handle RECEIVE_SESSIONS', () => {
            expect(
                fetching(true, {
                    type: actions.RECEIVE_SESSIONS,
                })
            ).toEqual(false);
        });
        it('should handle REQUEST_SESSIONS_ERROR', () => {
            expect(
                fetching(true, {
                    type: actions.REQUEST_SESSIONS_ERROR,
                })
            ).toEqual(false);
        });
    });
    describe('byId', () => {
        it('should return the initial state', () => {
            expect(byId(undefined, {})).toEqual({});
        });
        it('should handle RECEIVE_SESSIONS', () => {
            let state = { 123: { foo: 'bar' } };
            const action = {
                type: actions.RECEIVE_SESSIONS,
                items: [{ _id: '123' }],
            };
            expect(byId(state, action)).toEqual({
                123: { _id: '123', savingStatus: '', foo: 'bar' },
            });
            state = {};
            expect(byId(state, action)).toEqual({
                123: { _id: '123', savingStatus: '' },
            });
        });
    });
    describe('loadStatus', () => {
        it('should return the initial state', () => {
            expect(loadStatus(undefined, {})).toEqual('');
        });
        it('should handle REQUEST_SESSIONS_ERROR', () => {
            const action = {
                type: actions.REQUEST_SESSIONS_ERROR,
                error: new Error('message'),
            };
            expect(loadStatus('', action)).toEqual(action.error);
        });
        it('should handle RECEIVE_SESSIONS', () => {
            const action = {
                type: actions.RECEIVE_SESSIONS,
            };
            expect(loadStatus('', action)).toEqual('success');
        });
    });
    describe('uniqueTracks', () => {
        it('should return the initial state', () => {
            expect(uniqueTracks(undefined, {})).toEqual({});
        });
        it('should handle RECEIVE_SESSIONS', () => {
            const action = {
                type: actions.RECEIVE_SESSIONS,
                items: [
                    {
                        track: [
                            {
                                title: 'title one',
                            },
                        ],
                    },
                    {
                        track: [
                            {
                                title: 'title one',
                            },
                        ],
                    },
                    { track: [{ title: 'title two' }] },
                ],
            };
            expect(uniqueTracks({}, action)).toEqual({
                'title one': { count: 2, title: 'title one' },
                'title two': { count: 1, title: 'title two' },
            });
        });
        describe('completedEvals', () => {
            it('should return the initial state', () => {
                expect(completedEvals(undefined, {})).toEqual({});
            });
        });
    });
});
