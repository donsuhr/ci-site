/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { ScheduleButtonContainer } from './ScheduleButton.container';

describe('SessionBrowser ScheduleButton.container', () => {
    it('renders', () => {
        const wrapper = shallow(
            <ScheduleButtonContainer
                getItemStatus={() => {}}
                isSessionInSchedule={() => {}}
            />
        );
        expect(wrapper.find('ScheduleButton')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
