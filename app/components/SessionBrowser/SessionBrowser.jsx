import React from 'react';
import PropTypes from 'prop-types';
import moment from '../util/load-moment-tz';
import Nav from './Nav';
import Loading from '../Loading';
import SessionRow from './SessionRow';

const SessionBrowser = ({
    tracks,
    currentTrack,
    fetching,
    sessionsByTrackByDay,
    select,
}) => (
    <div>
        <Nav tracks={tracks} filter={currentTrack} />
        {fetching && <Loading>Loading...</Loading>}
        {Object.keys(sessionsByTrackByDay).map(day => {
            const dayTitle = moment(sessionsByTrackByDay[day][0].start)
                .tz('America/New_York')
                .format('dddd, MMMM Do');
            return (
                <div key={day}>
                    <h2 className="session-browser__day-title">{dayTitle}</h2>
                    <ul>
                        {sessionsByTrackByDay[day].map(x => (
                            <SessionRow
                                key={x.id}
                                item={x}
                                select={select}
                                currentTrack={currentTrack}
                            />
                        ))}
                    </ul>
                </div>
            );
        })}
    </div>
);

SessionBrowser.propTypes = {
    tracks: PropTypes.object, // eslint-disable-line react/forbid-prop-types
    currentTrack: PropTypes.string,
    fetching: PropTypes.bool.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    sessionsByTrackByDay: PropTypes.object,
    select: PropTypes.string,
};

export default SessionBrowser;
