import { combineReducers } from 'redux';

import sessions from './reducer';
import schedule from '../firebase/schedule.reducer';
import user from '../firebase/auth.reducer';
import listeners from '../firebase/listeners.reducer';
import domAttributes from '../redux/domAttributes.reducers';

export default combineReducers({
    sessions,
    schedule,
    user,
    listeners,
    domAttributes,
});
