/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { EvalButtonContainer } from './EvalButton.container';

describe('SessionBrowser EvalButtonContainer.container', () => {
    it('renders', () => {
        const wrapper = shallow(<EvalButtonContainer />);
        expect(wrapper.find('EvalButton')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
