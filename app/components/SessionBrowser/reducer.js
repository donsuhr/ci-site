import PropTypes from 'prop-types';
import { combineReducers } from 'redux';
import sortBy from 'lodash/sortBy';
import Basil from 'basil.js';
import moment from '../util/load-moment-tz';
import * as actions from './actions';

const defaultState = {
    savingStatus: '',
};

const basil = new Basil();
const completedEvalsDefault = basil.get('evaluations') || {};

export const completedEvals = (state = completedEvalsDefault, action) => state;

const item = (state = defaultState, action, currentItem) => {
    switch (action.type) {
        case actions.RECEIVE_SESSIONS:
            return {
                ...state,
                ...currentItem,
                savingStatus: '',
            };

        default:
            return state;
    }
};

export const byId = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_SESSIONS:
            return action.items.reduce((p, c) => {
                p[c._id] = item(state[c._id], action, c);
                return p;
            }, {});

        default:
            return state;
    }
};

export const uniqueTracks = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_SESSIONS:
            return action.items.reduce((acc, session) => {
                session.track.forEach(track => {
                    if (!Object.hasOwnProperty.call(acc, track.title)) {
                        acc[track.title] = {
                            count: 0,
                            title: track.title,
                        };
                    }
                    acc[track.title].count += 1;
                });
                return acc;
            }, {});
        default:
            return state;
    }
};

export const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_SESSIONS:
            return true;
        case actions.RECEIVE_SESSIONS:
        case actions.REQUEST_SESSIONS_ERROR:
            return false;
        default:
            return state;
    }
};

export const loadStatus = (state = '', action) => {
    switch (action.type) {
        case actions.REQUEST_SESSIONS_ERROR:
            return action.error;
        case actions.RECEIVE_SESSIONS:
            return 'success';
        default:
            return state;
    }
};

export const hasEverLoaded = (state = false, action) => {
    switch (action.type) {
        case actions.RECEIVE_SESSIONS:
        case actions.REQUEST_SESSIONS_ERROR:
            return true;
        default:
            return state;
    }
};

const sessions = combineReducers({
    hasEverLoaded,
    fetching,
    byId,
    loadStatus,
    uniqueTracks,
    completedEvals,
});

function getSessionById(state, id) {
    return state.byId[id];
}

export function getSessionsByTrack(state, trackName) {
    if (!state || !trackName) {
        return [];
    }
    const items = Object.keys(state).reduce((acc, key) => {
        if (
            state[key].track.filter(track => track.title === trackName).length >
            0
        ) {
            acc.push(state[key]);
        }
        return acc;
    }, []);
    return sortBy(items, ['start', 'title']);
}

function getSessionsByTrackByDay(state, trackName) {
    const items = getSessionsByTrack(state, trackName);
    if (!items) {
        return {};
    }
    const containsCmcFilter = x =>
        x.institution &&
        x.institution.toLowerCase().indexOf('campus management') !== -1;
    const notContainsCmcFilter = x =>
        !x.institution ||
        (x.institution &&
            x.institution.toLowerCase().indexOf('campus management') === -1);
    return items.reduce((acc, x) => {
        const start = moment(x.start)
            .tz('America/New_York')
            .format('dddd');
        if (!Object.hasOwnProperty.call(acc, start)) {
            acc[start] = [];
        }
        const sorted = sortBy(x.presenter, ['institution', 'lastName']);
        const containsCmc = sorted.filter(containsCmcFilter);
        const notContainsCmc = sorted.filter(notContainsCmcFilter);
        x.presenter = [...notContainsCmc, ...containsCmc];
        acc[start].push(x);

        return acc;
    }, {});
}

function countSessionSignups(state, allUsersState) {
    const ret = {};
    Object.values(allUsersState.byId).forEach(user => {
        if (Object.hasOwnProperty.call(user, 'schedule')) {
            if (Object.hasOwnProperty.call(user.schedule, 'sessions')) {
                Object.values(user.schedule.sessions).forEach(scheduleItem => {
                    if (
                        Object.hasOwnProperty.call(
                            state.byId,
                            scheduleItem.sessionId
                        )
                    ) {
                        if (
                            !Object.hasOwnProperty.call(
                                ret,
                                scheduleItem.sessionId
                            )
                        ) {
                            ret[scheduleItem.sessionId] = {
                                title: state.byId[scheduleItem.sessionId].title,
                                count: 0,
                            };
                        }
                        ret[scheduleItem.sessionId].count += 1;
                    }
                });
            }
        }
    });
    return ret;
}

function countSessionTopicSignups(state, allUsersState) {
    const ret = {};
    Object.values(allUsersState.byId).forEach(user => {
        if (Object.hasOwnProperty.call(user, 'schedule')) {
            if (Object.hasOwnProperty.call(user.schedule, 'sessions')) {
                Object.values(user.schedule.sessions).forEach(scheduleItem => {
                    if (
                        Object.hasOwnProperty.call(
                            state.byId,
                            scheduleItem.sessionId
                        )
                    ) {
                        const { title } = state.byId[
                            scheduleItem.sessionId
                        ].track[0];
                        if (!Object.hasOwnProperty.call(ret, title)) {
                            ret[title] = {
                                title,
                                count: 0,
                            };
                        }
                        ret[title].count += 1;
                    }
                });
            }
        }
    });
    return ret;
}

const PropType = PropTypes.shape({
    hasEverLoaded: PropTypes.bool,
    fetching: PropTypes.bool,
    loadStatus: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object,
    ]),
    byId: PropTypes.object,
    uniqueTracks: PropTypes.object,
});

const sessionItemPropType = PropTypes.shape({
    id: PropTypes.string.isRequired,
    start: PropTypes.oneOfType([
        PropTypes.instanceOf(Date),
        PropTypes.string,
    ]),
    title: PropTypes.string,
    location: PropTypes.object,
});

export {
    sessions as default,
    getSessionsByTrackByDay,
    getSessionById,
    countSessionSignups,
    countSessionTopicSignups,
    PropType,
    sessionItemPropType,
};
