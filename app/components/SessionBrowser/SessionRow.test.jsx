/* global jest,it,expect,describe */

import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Provider } from 'react-redux';
import smoothScroll from 'cmc-site/app/components/smoothScroll';
import SessionRow from './SessionRow';

jest.mock('cmc-site/app/components/smoothScroll');

const store = {
    default: () => {},
    subscribe: () => {},
    dispatch: () => {},
    getState: () => ({
        sessions: {
            completedEvals: {},
        },
    }),
};

const item = {
    savingStatus: '',
    _id: '5830d8cb3aa27a048063d276',
    updatedAt: '2017-03-21T04:00:29.430Z',
    createdAt: '2016-11-19T22:57:15.849Z',
    title: 'New Offerings from Integration Services',
    description: 'The Integrations Services ...',
    start: '2017-04-28T21:00:00.000Z',
    end: '2017-04-28T21:45:00.000Z',
    status: 'Approved',
    active: true,
    attendance: 22,
    presenter: [],
    userType: ['Functional', 'Technical', 'Executive'],
    userLevel: ['Beginner', 'Intermediate', 'Expert'],
    track: [
        {
            _id: '5830d4d93aa27a048063d248',
            updatedAt: '2016-11-19T22:40:25.681Z',
            createdAt: '2016-11-19T22:40:25.681Z',
            title: 'Driving Excellence through Professional Services',
            id: '5830d4d93aa27a048063d248',
        },
    ],
    id: '5830d8cb3aa27a048063d276',
};

describe('SessionBrowser SessionRow', () => {
    it('renders', () => {
        const wrapper = shallow(
            <SessionRow item={item} currentTrack="current track" />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('has a special link for Roundtable', () => {
        const roundTableItem = {
            ...item,
            title: 'CampusNexus CRM Roundtable',
        };
        const wrapper = shallow(
            <SessionRow item={roundTableItem} currentTrack="current track" />
        );
        expect(wrapper.find('li a').last().text()).toContain('Sign up');
    });

    it('calls scrollto when selected', () => {
        jest.useFakeTimers();
        const roundTableItem = {
            ...item,
            title: 'CampusNexus CRM Roundtable',
        };
        const wrapper = mount(
            <Provider store={store}>
                <SessionRow
                    item={roundTableItem}
                    currentTrack="current track"
                    select={roundTableItem.id}
                />
            </Provider>
        );
        jest.runAllTimers();
        expect(smoothScroll.scrollTo).toBeCalled();
        expect(smoothScroll.scrollTo.mock.calls[0][0].get(0)).toBe(
            wrapper.find('SessionRow').node.sessionNode
        );
    });

    it('toggles expand on click', () => {
        const wrapper = shallow(
            <SessionRow item={item} currentTrack="current track" />
        );
        expect(wrapper.state().expanded).toBe(false);
        wrapper
            .find('button.session-browser__session-details-button')
            .simulate('click', { preventDefault() {} });
        expect(wrapper.state().expanded).toBe(true);
    });
});
