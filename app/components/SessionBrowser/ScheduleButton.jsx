import React from 'react';
import PropTypes from 'prop-types';
import {
    REQUEST_CHILD_ADD,
    REQUEST_CHILD_REMOVE,
} from '../firebase/schedule.actions';
import { PropType as userPropType } from '../firebase/auth.reducer';
import { sessionItemPropType } from './reducer';

const ScheduleButton = ({
    getItemStatus,
    addSessionToSchedule,
    removeSessionFromSchedule,
    isSessionInSchedule,
    item,
    user,
}) => {
    const scheduleItem = isSessionInSchedule(item.id);
    const status = getItemStatus(item.id);
    const addBtnTxt =
        status === REQUEST_CHILD_ADD ? 'Adding...' : 'Add to Schedule';
    const removeBtnTxt =
        status === REQUEST_CHILD_REMOVE
            ? 'Removing...'
            : 'Remove from Schedule';
    return scheduleItem ? (
        <button
            className="session-browser__session-add-link"
            onClick={event => {
                event.preventDefault();
                removeSessionFromSchedule(scheduleItem);
            }}
        >
            {removeBtnTxt}
        </button>
    ) : (
        <button
            className="session-browser__session-add-link"
            onClick={event => {
                event.preventDefault();
                addSessionToSchedule(user.info.uid, item.id);
            }}
        >
            {addBtnTxt}
        </button>
    );
};

ScheduleButton.propTypes = {
    getItemStatus: PropTypes.func.isRequired,
    addSessionToSchedule: PropTypes.func,
    removeSessionFromSchedule: PropTypes.func,
    isSessionInSchedule: PropTypes.func.isRequired,
    item: sessionItemPropType,
    user: userPropType,
};

export default ScheduleButton;
