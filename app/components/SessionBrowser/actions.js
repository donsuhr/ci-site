import config from '../../../config';
import fetchItems from '../util/fetch-util';

const endpoint = `${config.domains.api}/campusInsight/ci/sessions/`;

export const REQUEST_SESSIONS = 'REQUEST_SESSIONS';

function requestSessions() {
    return {
        type: REQUEST_SESSIONS,
    };
}

export const RECEIVE_SESSIONS = 'RECEIVE_SESSIONS';

function receiveSessions(items) {
    return {
        type: RECEIVE_SESSIONS,
        items: items.data,
    };
}

export const REQUEST_SESSIONS_ERROR = 'REQUEST_SESSIONS_ERROR';

function requestSessionError(error) {
    return {
        type: REQUEST_SESSIONS_ERROR,
        error,
    };
}

function fetchSessions() {
    return (dispatch, getState) => {
        const state = getState();
        const hasYear =
            Object.hasOwnProperty.call(state, 'domAttributes') &&
            Object.hasOwnProperty.call(state.domAttributes, 'year');
        const year = hasYear ? `${state.domAttributes.year}/` : '';
        const all = process.env.NETLIFY_ENV !== 'production' ? '?all=1' : '';
        return fetchItems({
            dispatch,
            endpoint: `${endpoint}${year}${all}`,
            startAction: requestSessions,
            endAction: receiveSessions,
            errorAction: requestSessionError,
            getState,
            key: 'sessions',
        });
    };
}

export { fetchSessions, requestSessionError, receiveSessions, requestSessions };
