/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Nav from './Nav';

const tracks = {
    'Value-Added Solutions': {
        count: 3,
    },
    'Finance & Human Resource Management': {
        count: 1,
    },
};

describe('SessionBrowser Nav', () => {
    it('renders', () => {
        const wrapper = shallow(
            <Nav filter="Value-Added Solutions" tracks={tracks} />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
