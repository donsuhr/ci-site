import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import $ from 'jquery';

const superFishOptions = {
    cssArrows: false,
    speed: 'fast',
    animation: { height: 'show' },
    animationOut: { height: 'hide' },
};

class Nav extends React.Component {
    static propTypes = {
        filter: PropTypes.string,
        tracks: PropTypes.object, // eslint-disable-line react/forbid-prop-types
    };

    componentDidMount() {
        this.rebootMenu();
    }

    componentDidUpdate() {
        this.rebootMenu();
    }

    rebootMenu() {
        if (this.menuEl) {
            $(this.menuEl)
                .superfish('destroy')
                .superfish(superFishOptions);
        }
    }

    render() {
        const { tracks, filter } = this.props;
        const selectedCount =
            tracks && tracks[filter] ? tracks[filter].count : 0;
        const tracksSorted = Object.keys(tracks).sort();
        return (
            <div className="horizontal-menu__wrapper">
                <ul
                    className="s3-doc-list__menu sf-menu horizontal-menu"
                    ref={node => {
                        this.menuEl = node;
                    }}
                >
                    <li className="menuItem">
                        <button tabIndex="0" className="sf-with-ul">
                            Select Track
                        </button>
                        <ul>
                            {tracksSorted.map(key => (
                                <li key={key}>
                                    <NavLink
                                        className="menu-item"
                                        activeClassName="menu-item--active-file"
                                        to={`/${key}`}
                                    >
                                        <span className="menu-item-text">
                                            {key}
                                        </span>
                                    </NavLink>
                                </li>
                            ))}
                        </ul>
                    </li>
                </ul>
                <span className="horizontal-menu__selected-filter-text">
                    {filter} ({selectedCount} items)
                </span>
            </div>
        );
    }
}

export default Nav;
