/* eslint global-require:0 */
import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import SessionBrowserConnected from './SessionBrowser.container';

let Router;

if (process.env.NODE_ENV === 'production') {
    Router = require('react-router-dom').BrowserRouter;
} else {
    Router = require('react-router-dom').HashRouter;
}

const RouteContainer = props => {
    const basename =
        process.env.NODE_ENV === 'production' ? props.basename : undefined;
    return (
        <Router basename={basename}>
            <Route
                path="/:filter?/:select?"
                render={({ match }) => (
                    <SessionBrowserConnected match={match} {...props} />
                )}
            />
        </Router>
    );
};

RouteContainer.propTypes = {
    basename: PropTypes.string,
};

export default RouteContainer;
