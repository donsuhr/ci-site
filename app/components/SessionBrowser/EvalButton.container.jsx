import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import EvalButton from './EvalButton';
import { PropType as userPropType } from '../firebase/auth.reducer';
import { sessionItemPropType } from './reducer';

export const EvalButtonContainer = ({
    user,
    item,
    shortText,
    returnTo,
    completedEvals,
}) => (
    <div>
        <EvalButton
            user={user}
            item={item}
            shortText={shortText}
            returnTo={returnTo}
            completedEvals={completedEvals}
        />
    </div>
);

EvalButtonContainer.propTypes = {
    user: userPropType,
    item: sessionItemPropType,
    shortText: PropTypes.string,
    returnTo: PropTypes.string,
    completedEvals: PropTypes.objectOf(PropTypes.bool),
};

function mapStateToProps(state, ownProps) {
    return {
        user: state.user,
        completedEvals: state.sessions.completedEvals,
        item: ownProps.item,
        shortText: ownProps.shortText,
        returnTo: ownProps.returnTo,
    };
}

const EvalButtonConnected = connect(mapStateToProps, {})(EvalButtonContainer);

export default EvalButtonConnected;
