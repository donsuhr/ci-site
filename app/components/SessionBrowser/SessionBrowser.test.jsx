/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SessionBrowser from './SessionBrowser';

const sessionsByTrackByDay = {
    Thursday: [
        {
            id: '58fe6ddbcb77715e22b476cc',
            start: '2017-04-27T18:00:00.000Z',
        },
        {
            id: '5830d7913aa27a048063d270',
            start: '2017-04-27T19:00:00.000Z',
        },
    ],
    Friday: [
        {
            id: '5830d8fd3aa27a048063d277',
            start: '2017-04-28T13:30:00.000Z',
        },
    ],
};
const currentTrack = 'Value-Added Solutions';
const tracks = {
    'Value-Added Solutions': {},
    'Finance & Human Resource Management': {},
};

describe('SessionBrowser', () => {
    it('renders', () => {
        const wrapper = shallow(
            <SessionBrowser
                sessionsByTrackByDay={sessionsByTrackByDay}
                currentTrack={currentTrack}
                fetching={false}
                tracks={tracks}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const wrapper = shallow(
            <SessionBrowser
                sessionsByTrackByDay={sessionsByTrackByDay}
                currentTrack={currentTrack}
                fetching
                tracks={tracks}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
    });

    it('renders two days', () => {
        const wrapper = shallow(
            <SessionBrowser
                sessionsByTrackByDay={sessionsByTrackByDay}
                currentTrack={currentTrack}
                fetching
                tracks={tracks}
            />
        );
        const days = wrapper
            .find('.session-browser__day-title')
            .map(x => x.text());
        expect(days).toEqual(['Thursday, April 27th', 'Friday, April 28th']);
    });
});
