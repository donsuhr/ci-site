import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchSessions } from './actions';
import {
    getSessionsByTrackByDay,
    PropType as sessionPropType,
} from './reducer';
import ShowError from '../ShowError';
import SessionBrowser from './SessionBrowser';
import { PropType as userPropType } from '../firebase/auth.reducer';

import { listenToScheduleSessions } from '../firebase/schedule.actions';
import {
    listenToAuth,
    signOut,
    requestPopupAuth,
    listenToUserRoot,
} from '../firebase/auth.actions';

export class SessionBrowserContainer extends React.Component {
    static propTypes = {
        fetchSessions: PropTypes.func.isRequired,
        loadStatus: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
        // eslint-disable-next-line react/forbid-prop-types
        tracks: PropTypes.object,
        currentTrack: PropTypes.string,
        listenToAuth: PropTypes.func,
        listenToUserRoot: PropTypes.func,
        listenToScheduleSessions: PropTypes.func,
        sessions: sessionPropType,
        user: userPropType,
        fetching: PropTypes.bool.isRequired,
        // eslint-disable-next-line react/forbid-prop-types
        sessionsByTrackByDay: PropTypes.object,
        select: PropTypes.string,
    };

    componentDidMount() {
        this.props.listenToAuth().then(user => {
            this.props.listenToUserRoot(user.uid);
        });
        if (
            this.props.sessions.hasEverLoaded === false &&
            this.props.sessions.fetching === false
        ) {
            this.props.fetchSessions();
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.user.isAuth !== prevProps.user.isAuth) {
            if (this.props.user.isAuth) {
                this.props.listenToScheduleSessions(this.props.user.info.uid);
            }
        }
    }

    render() {
        const {
            loadStatus,
            tracks,
            currentTrack,
            fetching,
            sessionsByTrackByDay,
            select,
        } = this.props;
        const showLoadError =
            loadStatus && loadStatus !== 'success' && loadStatus !== '';
        return (
            <div>
                <SessionBrowser
                    tracks={tracks}
                    currentTrack={currentTrack}
                    fetching={fetching}
                    sessionsByTrackByDay={sessionsByTrackByDay}
                    select={select}
                />
                {showLoadError && <ShowError error={loadStatus} />}
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    const filter =
         ownProps.match.params.filter ||
         Object.keys(state.sessions.uniqueTracks).sort()[0];
    return {
        currentTrack: filter,
        tracks: state.sessions.uniqueTracks,
        sessionsByTrackByDay: getSessionsByTrackByDay(
            state.sessions.byId,
            filter
        ),
        dataById: state.sessions.byId,
        sessions: state.sessions,
        fetching: state.sessions.fetching,
        loadStatus: state.sessions.loadStatus,
        schedule: state.schedule,
        user: state.user,
        requestPopupAuth,
        select: ownProps.match.params.select,
    };
}

const SessionBrowserConnected = connect(mapStateToProps, {
    fetchSessions,
    listenToAuth,
    listenToUserRoot,
    listenToScheduleSessions,
    signOut,
})(SessionBrowserContainer);

export default SessionBrowserConnected;
