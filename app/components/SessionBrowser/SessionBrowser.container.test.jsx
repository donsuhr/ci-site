/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { SessionBrowserContainer } from './SessionBrowser.container';

describe('SessionBrowser SessionBrowser.container', () => {
    it('renders', () => {
        const wrapper = shallow(
            <SessionBrowserContainer
                fetchSessions={jest.fn()}
                loadStatus="success"
                fetching={false}
            />
        );
        expect(wrapper.find('SessionBrowser')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders a load Error', () => {
        const wrapper = shallow(
            <SessionBrowserContainer
                fetchSessions={jest.fn()}
                loadStatus={{
                    message: 'error',
                }}
                fetching={false}
            />
        );
        expect(wrapper.find('ShowError')).toHaveLength(1);
    });
});
