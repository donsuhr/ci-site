import React from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';
import smoothScroll from 'cmc-site/app/components/smoothScroll';
import moment from '../util/load-moment-tz';

import ScheduleButton from './ScheduleButton.container';
import EvalButton from './EvalButton.container';
import { sessionItemPropType } from './reducer';

class SessionRow extends React.Component {
    static propTypes = {
        item: sessionItemPropType,
        select: PropTypes.string,
        currentTrack: PropTypes.string,
    };

    state = {
        expanded: false,
    };

    componentDidMount() {
        if (this.props.select === this.props.item.id) {
            this.toggleButton.click();
            setTimeout(() => {
                const $target = $(this.sessionNode);
                const offset = window.innerHeight - $target.outerHeight() - 26;
                smoothScroll.scrollTo($target, false, offset);
            }, 150);
        }
    }

    expand = event => {
        event.preventDefault();
        this.setState({ expanded: !this.state.expanded });
    };

    render() {
        const { item, currentTrack } = this.props;

        const evalReturnLink =
            process.env.NODE_ENV === 'production'
                ? encodeURIComponent(`/sessions/${currentTrack}/${item.id}`)
                : encodeURIComponent(`/sessions/#/${currentTrack}/${item.id}/`);

        return (
            <li
                className="session-browser__session"
                ref={node => {
                    this.sessionNode = node;
                }}
            >
                <h3 className="session-browser__session-title">{item.title}</h3>
                <div className="session-browser__more-info-wrapper">
                    <button
                        type="button"
                        className="session-browser__session-details-button"
                        onClick={this.expand}
                        ref={node => {
                            this.toggleButton = node;
                        }}
                    >
                        {this.state.expanded ? 'Close' : 'Details'}
                    </button>
                </div>
                <div className="session-browser__start-time">
                    <span className="session-browser__label">Time</span>
                    {moment(item.start)
                        .tz('America/New_York')
                        .format('h:mm a')}{' '}
                    -&nbsp;
                    {moment(item.end)
                        .tz('America/New_York')
                        .format('h:mm a')}
                </div>
                <div className="session-browser__location">
                    <span className="session-browser__label">Location</span>
                    {item.location && item.location.title
                        ? item.location.title
                        : ''}
                </div>
                <div className="session-browser__user-type">
                    <span className="session-browser__label">Type of User</span>
                    {item.userType.join(', ')}
                </div>
                <div className="session-browser__user-level">
                    <span className="session-browser__label">
                        Level of Expertise
                    </span>
                    {item.userLevel.join(', ')}
                </div>

                <div className="session-browser__session-details">
                    <div className="session-browser__description">
                        <span className="session-browser__label">
                            Description
                        </span>
                        <span
                            // eslint-disable-next-line react/no-danger
                            dangerouslySetInnerHTML={{
                                __html: item.description,
                            }}
                        />
                    </div>
                    <div className="session-browser__presenters">
                        <span className="session-browser__label">
                            Presenters
                        </span>
                        <ul className="session-browser__presenter-list">
                            {item.presenter.map(presenter => (
                                <li
                                    key={presenter._id}
                                    className="session-browser__presenter-list-item"
                                >
                                    <p className="session-browser__presenter-name-title-wrapper">
                                        <span className="session-browser__presenter-name">
                                            {presenter.firstName}{' '}
                                            {presenter.lastName}
                                        </span>
                                        <span className="session-browser__presenter-title">
                                            {presenter.title}
                                        </span>
                                        <span className="session-browser__presenter-institution">
                                            {presenter.institution}
                                        </span>
                                    </p>
                                    <span
                                        className="session-browser__presenter-list-bio"
                                        // eslint-disable-next-line react/no-danger
                                        dangerouslySetInnerHTML={{
                                            __html: presenter.bio,
                                        }}
                                    />
                                </li>
                            ))}
                        </ul>
                    </div>

                    <ul className="session-browser__session-links">
                        <EvalButton item={item} returnTo={evalReturnLink} />
                        <li>
                            <ScheduleButton item={item} />
                        </li>
                        {item.title === 'CampusNexus CRM Roundtable' && (
                            <li>
                                <a
                                    className="session-browser__session-add-link"
                                    href="/agenda/round-table-sign-up/"
                                >
                                    Sign up for this Session
                                </a>
                            </li>
                        )}
                    </ul>
                </div>
            </li>
        );
    }
}

export default SessionRow;
