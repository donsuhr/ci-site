import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    addSessionToSchedule,
    removeSessionFromSchedule,
} from '../firebase/schedule.actions';
import {
    isSessionInSchedule,
    getItemStatus,
} from '../firebase/schedule.reducer';
import ScheduleButton from './ScheduleButton';
import { PropType as userPropType } from '../firebase/auth.reducer';
import { sessionItemPropType } from './reducer';

export const ScheduleButtonContainer = ({ ...props }) => (
    <div>
        <ScheduleButton
            user={props.user}
            item={props.item}
            addSessionToSchedule={props.addSessionToSchedule}
            removeSessionFromSchedule={props.removeSessionFromSchedule}
            getItemStatus={props.getItemStatus}
            isSessionInSchedule={props.isSessionInSchedule}
        />
    </div>
);

ScheduleButtonContainer.propTypes = {
    getItemStatus: PropTypes.func,
    addSessionToSchedule: PropTypes.func,
    removeSessionFromSchedule: PropTypes.func,
    isSessionInSchedule: PropTypes.func,
    user: userPropType,
    item: sessionItemPropType,
};

function mapStateToProps(state, ownProps) {
    return {
        isSessionInSchedule: isSessionInSchedule.bind(null, state.schedule),
        getItemStatus: getItemStatus.bind(null, state.schedule),
        user: state.user,
        item: ownProps.item,
    };
}

const ScheduleButtonConnected = connect(mapStateToProps, {
    addSessionToSchedule,
    removeSessionFromSchedule,
})(ScheduleButtonContainer);

export default ScheduleButtonConnected;
