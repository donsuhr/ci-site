import React from 'react';
import PropTypes from 'prop-types';
import moment from '../util/load-moment-tz';
import { PropType as userPropType } from '../firebase/auth.reducer';
import { sessionItemPropType } from './reducer';

const EvalButton = ({
    item,
    user,
    completedEvals,
    shortText = false,
    returnTo,
}) => {
    if (item.title === 'CampusNexus CRM Roundtable') {
        return null;
    }
    if (moment().isAfter(moment(item.start)) || user.isAdmin) {
        if (Object.hasOwnProperty.call(completedEvals, item.id)) {
            return (
                <li>
                    <div
                        className="session-browser__session-eval-complete"
                        tabIndex="-1"
                    >
                        {shortText ? 'Eval Complete' : 'Evaluation Complete'}
                    </div>
                </li>
            );
        }
        return (
            <li>
                <a
                    className="session-browser__session-eval-link"
                    href={`/sessions/evaluation/?id=${item.id}&f=${returnTo}`}
                >
                    Evaluation
                </a>
            </li>
        );
    }
    return null;
};

EvalButton.propTypes = {
    item: sessionItemPropType,
    user: userPropType,
    // eslint-disable-next-line react/forbid-prop-types
    completedEvals: PropTypes.object,
    shortText: PropTypes.bool,
    returnTo: PropTypes.string,
};

export default EvalButton;
