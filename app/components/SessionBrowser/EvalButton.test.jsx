/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import EvalButton from './EvalButton';

const item = {
    id: '58fe6ddbcb77715e22b476cc',
    start: '2017-04-27T18:00:00.000Z',
    title: 'title',
};
const user = {
    isAdmin: true,
};

describe('SessionBrowser EvalButton', () => {
    it('renders complete', () => {
        const wrapper = shallow(
            <EvalButton
                item={item}
                user={user}
                completedEvals={{ [item.id]: true }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(
            wrapper.find('.session-browser__session-eval-complete')
        ).toHaveLength(1);
    });

    it('renders not complete', () => {
        const wrapper = shallow(
            <EvalButton
                item={item}
                user={user}
                completedEvals={{}}
                returnTo="foo"
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(
            wrapper.find('.session-browser__session-eval-link')
        ).toHaveLength(1);
    });

    it('renders nothing for crm roundtable', () => {
        const wrapper = shallow(
            <EvalButton
                item={{
                    id: '58fe6ddbcb77715e22b476cc',
                    start: '2017-04-27T18:00:00.000Z',
                    title: 'CampusNexus CRM Roundtable',
                }}
            />
        );
        expect(wrapper.html()).toBeNull();
    });
});
