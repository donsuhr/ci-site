/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ScheduleButton from './ScheduleButton';

const item = {
    id: '23',
};
const user = {
    info: {
        uid: 45,
    },
};
describe('SessionBrowser ScheduleButton', () => {
    it('renders', () => {
        const wrapper = shallow(
            <ScheduleButton
                getItemStatus={() => 'REQUEST_CHILD_ADD'}
                isSessionInSchedule={() => true}
                item={item}
                user={user}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });
    it('renders remove button', () => {
        const removeSessionFromSchedule = jest.fn();
        const wrapper = shallow(
            <ScheduleButton
                getItemStatus={() => ''}
                removeSessionFromSchedule={removeSessionFromSchedule}
                isSessionInSchedule={() => true}
                item={item}
            />
        );
        expect(removeSessionFromSchedule).not.toHaveBeenCalled();
        expect(wrapper.find('.session-browser__session-add-link').text()).toBe(
            'Remove from Schedule'
        );
        wrapper.find('.session-browser__session-add-link').simulate('click', {
            preventDefault() {},
        });
        expect(removeSessionFromSchedule).toHaveBeenCalled();
    });
    it('renders add button', () => {
        const addSessionToSchedule = jest.fn();
        const wrapper = shallow(
            <ScheduleButton
                getItemStatus={() => ''}
                addSessionToSchedule={addSessionToSchedule}
                isSessionInSchedule={() => false}
                item={item}
                user={user}
            />
        );
        expect(addSessionToSchedule).not.toHaveBeenCalled();
        expect(wrapper.find('.session-browser__session-add-link').text()).toBe(
            'Add to Schedule'
        );
        wrapper.find('.session-browser__session-add-link').simulate('click', {
            preventDefault() {},
        });
        expect(addSessionToSchedule).toHaveBeenCalled();
    });
});
