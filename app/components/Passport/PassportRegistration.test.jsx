/* global it,expect,describe */

import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import VerifyRegistration from './PassportRegistration';

function createNewStore() {
    return createStore(
        combineReducers({
            form: formReducer,
        })
    );
}

describe('Passport PassportRegistration', () => {
    it('renders edit mode', () => {
        const wrapper = shallow(
            <VerifyRegistration
                profile={{
                    fetching: false,
                    loadStatus: 'success',
                    info: {
                        firstName: '',
                        lastName: '',
                        email: '',
                    },
                }}
                user={{
                    isAuth: true,
                }}
            />
        );
        expect(wrapper.find('ReduxForm')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders edit mode with an error', () => {
        const wrapper = mount(
            <Provider store={createNewStore()}>
                <VerifyRegistration
                    profile={{
                        fetching: false,
                        loadStatus: { message: 'test error' },
                        info: {
                            firstName: '',
                            lastName: '',
                            email: '',
                        },
                    }}
                    user={{
                        isAuth: true,
                    }}
                />
            </Provider>
        );
        wrapper.find('PassportRegistrationView button').simulate('click', {
            preventDefault() {
            },
        });
        expect(wrapper.find('.show-error p').text()).toContain('test error');
    });

    it('renders loading data', () => {
        const wrapper = shallow(
            <VerifyRegistration
                profile={{
                    fetching: false,
                    loadStatus: 'success',
                    info: {},
                }}
                user={{
                    isAuth: false,
                }}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
    });

    it('renders a view mode', () => {
        const wrapper = shallow(
            <VerifyRegistration
                profile={{
                    fetching: false,
                    loadStatus: 'success',
                    info: {
                        firstName: 'firstName',
                        lastName: 'lastName',
                        email: 'email',
                    },
                }}
                user={{
                    isAuth: true,
                }}
            />
        );
        expect(wrapper.find('PassportRegistrationView')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
