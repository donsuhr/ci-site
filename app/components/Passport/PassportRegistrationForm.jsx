import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import renderField from '../util/redux-form-fields/renderField';
import Loading from '../Loading';

const validate = values => {
    const errors = {};
    if (!values.firstName) {
        errors.firstName = 'Please enter a first name.';
    }
    if (!values.lastName) {
        errors.lastName = 'Please enter a last name.';
    }
    if (!values.email) {
        errors.email = 'Please enter an email address.';
    }
    return errors;
};

class PassportRegistrationForm extends React.Component {
    static propTypes = {
        /* eslint-disable react/forbid-prop-types */
        error: PropTypes.string,
        submitting: PropTypes.bool,
        handleSubmit: PropTypes.func.isRequired,
    };

    componentDidMount() {
        this.firstField.focus();
    }

    render() {
        const { error, handleSubmit, submitting } = this.props;

        return (
            <form
                onSubmit={handleSubmit}
                className="cmc-form cmc-form--passport-registration-form"
            >
                <fieldset>
                    <ul>
                        <li>
                            <Field
                                inputRef={x => {
                                    this.firstField = x;
                                }}
                                name="firstName"
                                label="First Name"
                                component={renderField}
                                type="text"
                                id="firstName"
                                labelClassName="required"
                            />
                        </li>
                        <li>
                            <Field
                                name="lastName"
                                label="Last Name"
                                component={renderField}
                                type="text"
                                id="lastName"
                                labelClassName="required"
                            />
                        </li>
                        <li>
                            <Field
                                name="email"
                                label="Email"
                                component={renderField}
                                type="email"
                                id="email"
                                labelClassName="required"
                            />
                        </li>
                        <li className="cmc-form__submit-row">
                            <button
                                className="cmc-form__submit-button"
                                type="submit"
                                disabled={submitting}
                            >
                                Save
                            </button>
                        </li>
                    </ul>
                </fieldset>
                <p>{error && <strong>{error}</strong>}</p>
                {submitting && <Loading>Loading...</Loading>}
            </form>
        );
    }
}

const ReduxPassportRegistrationForm = reduxForm({
    form: 'PassportRegistrationForm',
    validate,
})(PassportRegistrationForm);

export default ReduxPassportRegistrationForm;
