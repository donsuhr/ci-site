import React from 'react';
import PropTypes from 'prop-types';

class PassportItemForm extends React.Component {
    static propTypes = {
        submitCorrectCode: PropTypes.func.isRequired,
        name: PropTypes.string.isRequired,
        /* eslint-disable react/forbid-prop-types */
        item: PropTypes.object,
        code: PropTypes.string,
    };

    state = {
        error: false,
    };

    handleSubmit = event => {
        event.preventDefault();
        const error = this.input.value !== this.props.code;
        this.setState({ error });
        if (!error) {
            this.props.submitCorrectCode(this.props.name, this.input.value);
        }
    };

    render() {
        if (this.props.item) {
            return null;
        }
        return (
            <form className="cmc-form" onSubmit={this.handleSubmit}>
                <label htmlFor={this.props.name}>Sponsor Code</label>
                <div className="input-wrapper">
                    <input
                        id={this.props.name}
                        type="number"
                        pattern="[0-9]*"
                        inputMode="numeric"
                        ref={node => {
                            this.input = node;
                        }}
                    />
                    <p
                        className="passport-list-item__input-error"
                        style={{ display: this.state.error ? 'block' : 'none' }}
                    >
                        The code entered is not correct. Please try again.
                    </p>
                </div>
                <div className="input-wrapper">
                    <button type="submit" className="cmc-form__submit-button">
                        Submit
                    </button>
                </div>
            </form>
        );
    }
}

export default PassportItemForm;
