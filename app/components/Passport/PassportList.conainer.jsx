import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PassportList from './PassportList';
import { listenToAuth } from '../firebase/auth.actions';
import { PropType as userProType } from '../firebase/auth.reducer';
import { fetchPassport, addPassportItem } from '../firebase/passport.actions';

export class PassportListContainer extends React.Component {
    static propTypes = {
        listenToAuth: PropTypes.func.isRequired,
        user: userProType,
        /* eslint-disable react/forbid-prop-types */
        passport: PropTypes.object,
        addPassportItem: PropTypes.func.isRequired,
        fetchPassport: PropTypes.func.isRequired,
    };

    componentDidMount() {
        this.props.listenToAuth().then(user => {
            this.props.fetchPassport(user.uid);
        });
    }

    submitCorrectCode = (name, code) => {
        this.props.addPassportItem(this.props.user.info.uid, name, code);
    };

    render() {
        let loading = false;
        if (this.props.user.initting) {
            loading = 'Authenticating...';
        }
        if (
            !this.props.passport.hasEverLoaded ||
            this.props.passport.fetching
        ) {
            loading = 'Loading Data...';
        }
        return (
            <PassportList
                passport={this.props.passport}
                submitCorrectCode={this.submitCorrectCode}
                loading={loading}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user,
        passport: state.passport,
    };
}

const PassportConnected = connect(mapStateToProps, {
    listenToAuth,
    fetchPassport,
    addPassportItem,
})(PassportListContainer);

export default PassportConnected;
