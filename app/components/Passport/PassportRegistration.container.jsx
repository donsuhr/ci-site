/* eslint-disable react/forbid-prop-types */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reset } from 'redux-form';
import VerifyRegistration from './PassportRegistration';
import { confirmProfile } from '../SupportConsultations/actions';
import { listenToAuth } from '../firebase/auth.actions';
import { updateProfile, listenToProfile } from '../firebase/profile.actions';
import { listenToScheduleSupport } from '../firebase/schedule.support.actions';

export class VerifyRegistrationContainer extends React.Component {
    static propTypes = {
        user: PropTypes.object,
        profile: PropTypes.object,
        listenToAuth: PropTypes.func,
        listenToProfile: PropTypes.func,
        listenToScheduleSupport: PropTypes.func,
        updateProfile: PropTypes.func,
        reset: PropTypes.func,
    };

    componentDidMount() {
        this.props.listenToAuth().then(user => {
            this.props.listenToScheduleSupport(user.uid);
            if (
                !this.props.profile.fetching &&
                this.props.profile.loadStatus !== 'success'
            ) {
                this.props.listenToProfile(user);
            }
        });
    }

    handleSubmit = data =>
        this.props.updateProfile(this.props.user.info.uid, data, 'update');

    handleSubmitSuccess = () => {
        this.props.reset('PassportRegistrationForm');
    };

    render() {
        return (
            <VerifyRegistration
                user={this.props.user}
                profile={this.props.profile}
                handleSubmit={this.handleSubmit}
                handleSubmitSuccess={this.handleSubmitSuccess}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        profile: state.profile,
        user: state.user,
        supportConsultations: state.supportConsultations,
    };
}

const VerifyRegistrationConnected = connect(mapStateToProps, {
    listenToAuth,
    listenToProfile,
    updateProfile,
    confirmProfile,
    listenToScheduleSupport,
    reset,
})(VerifyRegistrationContainer);

export default VerifyRegistrationConnected;
