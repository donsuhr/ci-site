import React from 'react';
import PropTypes from 'prop-types';
import Loading from '../Loading';

const PassportRegistrationView = ({ profile, onEditClicked }) => {
    const showLoadError =
        profile.loadStatus &&
        profile.loadStatus !== 'success' &&
        profile.loadStatus !== '';
    return (
        <div className="passport__profile-view">
            <h2 className="secondary-header--alt">My Registration Information</h2>
            <dl className="profile-view-list">
                <dt>First Name:</dt>
                <dd>{profile.info.firstName}</dd>

                <dt>Last Name:</dt>
                <dd>{profile.info.lastName}</dd>

                <dt>Email:</dt>
                <dd>{profile.info.email}</dd>

            </dl>
            <button
                className="passport__profile-view__edit-btn"
                onClick={onEditClicked}
            >
                Edit
            </button>
            {
                showLoadError &&
                <div className="show-error">
                    <p>
                        There was an error loading the profile. <br />
                        Error: {profile.loadStatus.message}
                    </p>
                </div>
            }
            {profile.fetching && <Loading>Loading...</Loading>}
        </div>
    );
};

PassportRegistrationView.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    profile: PropTypes.object,
    onEditClicked: PropTypes.func,
};

export default PassportRegistrationView;
