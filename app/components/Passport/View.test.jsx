/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { PassportView } from './View';

describe('Passport View', () => {
    it('renders', () => {
        const wrapper = shallow(
            <PassportView
                listenToAuth={jest.fn()}
                profile={{
                    fetching: false,
                    loadStatus: 'success',
                    info: {
                        firstName: 'firstName',
                        lastName: 'lastName',
                        email: 'email',
                    },
                }}
                user={{
                    initting: false,
                    isAuth: true,
                    loadingRoot: false,
                    isAdmin: true,
                }}
            />
        );
        expect(wrapper.find('Connect(PassportListContainer)')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading data', () => {
        const wrapper = shallow(
            <PassportView
                listenToAuth={jest.fn()}
                user={{
                    initting: true,
                }}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
        expect(
            wrapper
                .find('Loading')
                .dive()
                .text()
        ).toContain('Loading');
    });

    it('renders loading profile', () => {
        const wrapper = shallow(
            <PassportView
                listenToAuth={jest.fn()}
                user={{
                    initting: false,
                    isAuth: true,
                }}
                profile={{
                    fetching: true,
                }}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
        expect(
            wrapper
                .find('Loading')
                .dive()
                .text()
        ).toContain('Loading Profile');
    });

    it('renders sign in button', () => {
        const wrapper = shallow(
            <PassportView
                listenToAuth={jest.fn()}
                user={{
                    initting: false,
                    isAuth: false,
                }}
                profile={{}}
            />
        );
        expect(wrapper.find('button').text()).toContain('Sign In');
    });
});
