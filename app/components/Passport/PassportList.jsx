import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import $ from 'jquery';
import 'jquery-ui/ui/widgets/accordion';
import PassportItemForm from './PassportItemForm';
import Loading from '../Loading';

class PassportList extends React.Component {
    static propTypes = {
        passport: PropTypes.shape({
            /* eslint-disable react/forbid-prop-types */
            byId: PropTypes.object,
            sponsorDetails: PropTypes.shape({
                title: PropTypes.string,
                code: PropTypes.string,
            }),
        }),
        loading: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
        submitCorrectCode: PropTypes.func.isRequired,
    };

    state = { accordionInitted: false };

    componentDidUpdate(prevProps) {
        if (
            prevProps.loading !== false &&
            this.props.loading === false &&
            !this.state.accordionInitted
        ) {
            $('.passport-list').accordion({
                animate: 200,
                collapsible: true,
                heightStyle: 'content',
                activate: (event, $ui) => {
                    $ui.newPanel.find('input:first').trigger('focus');
                },
            });
            // eslint-disable-next-line react/no-did-update-set-state
            this.setState({ accordionInitted: true });
        }
    }

    componentWillUnmount() {
        if (this.state.accordionInitted) {
            $('.passport-list').accordion('destroy');
        }
    }

    render() {
        if (this.props.loading) {
            return (
                <div className="passport-list">
                    <Loading>{this.props.loading}</Loading>
                </div>
            );
        }
        return (
            <div className="passport-list">
                {Object.keys(this.props.passport.sponsorDetails)
                    .sort()
                    .map(x => {
                        const statusClassNames = classNames({
                            'passport-list__sponsor-title': true,
                            'passport-list__sponsor-title--checked': !!this
                                .props.passport.byId[x],
                        });
                        return [
                            <h2 className={statusClassNames}>
                                {this.props.passport.sponsorDetails[x].title}
                            </h2>,
                            <div className="passport-list__sponsor-info">
                                <PassportItemForm
                                    item={this.props.passport.byId[x]}
                                    name={x}
                                    code={
                                        this.props.passport.sponsorDetails[x]
                                            .code
                                    }
                                    submitCorrectCode={
                                        this.props.submitCorrectCode
                                    }
                                />
                            </div>,
                        ];
                    })}
            </div>
        );
    }
}

export default PassportList;
