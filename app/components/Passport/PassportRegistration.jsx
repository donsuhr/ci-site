/* eslint-disable react/forbid-prop-types */

import React from 'react';
import PropTypes from 'prop-types';
import PassportRegistrationForm from './PassportRegistrationForm';
import PassportRegistrationView from './PassportRegistrationView';
import Loading from '../Loading';

class VerifyRegistration extends React.Component {
    static propTypes = {
        user: PropTypes.object,
        profile: PropTypes.object,
        handleSubmit: PropTypes.func,
        handleSubmitSuccess: PropTypes.func,
    };

    state = {
        viewMode:
            this.props.profile.loadStatus === 'success' &&
            (this.props.profile.info.firstName === '' ||
                this.props.profile.info.lastName === '' ||
                this.props.profile.info.email === '')
                ? 'edit'
                : 'view',
    };

    onEditClicked = event => {
        event.preventDefault();
        this.setState({ viewMode: 'edit' });
    };

    onSubmitSuccess = () => {
        this.setState({ viewMode: 'view' });
        this.props.handleSubmitSuccess();
    };

    render() {
        const { user, profile, handleSubmit } = this.props;
        const showLoadError =
            profile.loadStatus &&
            profile.loadStatus !== 'success' &&
            profile.loadStatus !== '';
        const fetching = !user.isAuth || profile.fetching;

        if (fetching) {
            return <Loading>Loading...</Loading>;
        }

        if (this.state.viewMode === 'edit') {
            return (
                <div>
                    <h2 className="secondary-header--alt">
                        My Registration Information
                    </h2>
                    <p>
                        To begin, please verify or enter your contact
                        information.
                    </p>
                    <PassportRegistrationForm
                        initialValues={profile.info}
                        onSubmitSuccess={this.onSubmitSuccess}
                        onSubmit={handleSubmit}
                    />
                    {showLoadError && (
                        <div className="show-error">
                            <p>
                                There was an error loading the profile. <br />
                                Error: {this.props.profile.loadStatus.message}
                            </p>
                        </div>
                    )}
                </div>
            );
        }

        return (
            <PassportRegistrationView
                profile={profile}
                onEditClicked={this.onEditClicked}
            />
        );
    }
}

export default VerifyRegistration;
