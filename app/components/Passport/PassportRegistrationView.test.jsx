/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import PassportRegistrationView from './PassportRegistrationView';

describe('Passport PassportRegistrationView', () => {
    it('renders', () => {
        const wrapper = shallow(
            <PassportRegistrationView
                profile={{
                    fetching: false,
                    loadStatus: 'success',
                    info: {
                        firstName: 'firstName',
                        lastName: 'lastName',
                        email: 'email',
                    },
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders a load error', () => {
        const wrapper = shallow(
            <PassportRegistrationView
                profile={{
                    fetching: false,
                    loadStatus: { message: 'test message' },
                    info: {
                        firstName: 'firstName',
                        lastName: 'lastName',
                        email: 'email',
                    },
                }}
            />
        );
        expect(wrapper.find('.show-error p').text()).toContain('test message');
    });

    it('renders loading', () => {
        const wrapper = shallow(
            <PassportRegistrationView
                profile={{
                    fetching: true,
                    info: {},
                }}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
    });
});
