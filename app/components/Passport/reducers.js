import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import user from '../firebase/auth.reducer';
import listeners from '../firebase/listeners.reducer';
import passport from '../firebase/passport.reducer';
import profile from '../firebase/profile.reducer';
import domAttributes from '../redux/domAttributes.reducers';

export default combineReducers({
    user,
    listeners,
    passport,
    profile,
    form: formReducer,
    domAttributes,
});
