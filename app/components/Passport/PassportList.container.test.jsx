/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { PassportListContainer } from './PassportList.conainer';

describe('PassportReport PassportListContainer', () => {
    it('renders', () => {
        const wrapper = shallow(
            <PassportListContainer
                listenToAuth={() => true}
                addPassportItem={() => true}
                fetchPassport={() => true}
                passport={{
                    hasEverLoaded: true,
                    fetching: false,
                }}
                user={{ initting: false }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('PassportList')).toHaveLength(1);
    });
    it('renders loading', () => {
        const wrapper = shallow(
            <PassportListContainer
                listenToAuth={() => true}
                addPassportItem={() => true}
                fetchPassport={() => true}
                passport={{
                    hasEverLoaded: false,
                    fetching: false,
                }}
                user={{ initting: false }}
            />
        );
        expect(wrapper.find('PassportList').props().loading).toContain(
            'Loading'
        );
    });

    it('renders authenticating', () => {
        const wrapper = shallow(
            <PassportListContainer
                listenToAuth={() => true}
                addPassportItem={() => true}
                fetchPassport={() => true}
                passport={{
                    hasEverLoaded: true,
                    fetching: false,
                }}
                user={{ initting: true }}
            />
        );
        expect(wrapper.find('PassportList').props().loading).toContain(
            'Authenticating'
        );
    });
});
