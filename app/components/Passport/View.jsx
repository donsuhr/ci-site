import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { listenToAuth, requestPopupAuth } from '../firebase/auth.actions';
import { PropType as userPropType } from '../firebase/auth.reducer';
import { listenToProfile } from '../firebase/profile.actions';
import PassportList from './PassportList.conainer';
import Loading from '../Loading';
import PassportRegistration from './PassportRegistration.container';

export class PassportView extends React.Component {
    static propTypes = {
        /* eslint-disable react/forbid-prop-types */
        listenToAuth: PropTypes.func.isRequired,
        listenToProfile: PropTypes.func,
        user: userPropType,
        profile: PropTypes.object,
        year: PropTypes.string,
    };

    componentDidMount() {
        this.props.listenToAuth().then(user => {
            if (
                !this.props.profile.fetching &&
                this.props.profile.loadStatus !== 'success'
            ) {
                this.props.listenToProfile(user);
            }
        });
    }

    onSignInClick = event => {
        event.preventDefault();
        requestPopupAuth(this.props.year);
    };

    render() {
        if (this.props.user.initting) {
            return <Loading>Loading...</Loading>;
        }
        const hasProfile =
            this.props.profile.loadStatus === 'success' &&
            (this.props.profile.info.firstName !== '' &&
                this.props.profile.info.lastName !== '' &&
                this.props.profile.info.email !== '');

        if (!this.props.user.isAuth) {
            return (
                <button
                    type="button"
                    className="cmc-article__link"
                    onClick={this.onSignInClick}
                >
                    Sign In
                </button>
            );
        }
        if (this.props.profile.fetching) {
            return <Loading>Loading Profile...</Loading>;
        }
        return (
            <div>
                <PassportRegistration />
                {hasProfile && <PassportList />}
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user,
        profile: state.profile,
        year: state.domAttributes.year,
    };
}

const PassportViewConnected = connect(mapStateToProps, {
    listenToAuth,
    listenToProfile,
})(PassportView);

export default PassportViewConnected;
