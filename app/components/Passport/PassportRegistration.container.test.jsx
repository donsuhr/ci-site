/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { VerifyRegistrationContainer } from './PassportRegistration.container';

describe('PassportReport PassportRegistration container', () => {
    it('renders', () => {
        const wrapper = shallow(<VerifyRegistrationContainer />);
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('VerifyRegistration')).toHaveLength(1);
    });
});
