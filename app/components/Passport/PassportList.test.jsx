/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import PassportList from './PassportList';

describe('PassportReport PassportList', () => {
    it('renders', () => {
        const wrapper = shallow(
            <PassportList
                submitCorrectCode={() => true}
                passport={{
                    sponsorDetails: {
                        sponsor2: {
                            title: 'sponsor2',
                            code: '123',
                        },
                    },
                    byId: {
                        sponsor2: {},
                    },
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('PassportItemForm')).toHaveLength(1);
    });

    it('renders loading', () => {
        const wrapper = shallow(
            <PassportList
                loading="test loading"
                submitCorrectCode={() => true}
            />
        );
        expect(
            wrapper
                .find('Loading')
                .dive()
                .text()
        ).toContain('test loading');
    });
});
