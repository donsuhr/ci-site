/* global jest,it,expect,describe */

import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import PassportItemForm from './PassportItemForm';

describe('PassportReport PassportItemForm', () => {
    it('renders', () => {
        const wrapper = shallow(
            <PassportItemForm
                submitCorrectCode={() => true}
                name="sponsor2"
                code="123"
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders an error on wrong code', () => {
        const wrapper = mount(
            <PassportItemForm
                submitCorrectCode={() => true}
                name="sponsor2"
                code="123"
            />
        );
        expect(
            wrapper.find('.passport-list-item__input-error').node.style.display
        ).toBe('none');

        wrapper.find('form').simulate('submit', {
            preventDefault() {},
        });
        expect(
            wrapper.find('.passport-list-item__input-error').node.style.display
        ).toBe('block');
    });

    it('calls submitCorrectCode', () => {
        const submitCorrectCode = jest.fn();
        const wrapper = mount(
            <PassportItemForm
                submitCorrectCode={submitCorrectCode}
                name="sponsor2"
                code="123"
            />
        );
        wrapper.find('input').node.value = '123';
        expect(submitCorrectCode).not.toHaveBeenCalled();
        wrapper.find('form').simulate('submit', {
            preventDefault() {},
        });
        expect(submitCorrectCode).toHaveBeenCalled();
    });
});
