import { combineReducers } from 'redux';
import * as actions from './userProfiles.actions';

const defaultState = {
    loading: false,
};

export const byId = (state = defaultState, action) => {
    switch (action.type) {
        case actions.USER_PROFILE_RECEIVED:
            return {
                ...state,
                [action.data.ref.parent.key]: {
                    ref: action.data.ref,
                    key: action.data.key,
                    val: action.data.val(),
                    loading: false,
                },

            };
        case actions.REQUEST_USER_PROFILE:
            return {
                ...state,
                [action.userId]: {
                    loading: true,
                },
            };
        default:
            return state;
    }
};

const userProfiles = combineReducers({
    byId,
});

export { userProfiles as default };
