import { combineReducers } from 'redux';
import * as actions from './profile.actions';

const defaultState = {
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
    institution: '',
};

export const info = (state = defaultState, action) => {
    switch (action.type) {
        case actions.PROFILE_LOADED:
            return {
                ...state,
                ...action.data,
            };
        default:
            return state;
    }
};

export const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.PROFILE_REQUESTED:
            return true;
        case actions.REQUEST_PROFILE_ERROR:
        case actions.PROFILE_LOADED:
            return false;
        default:
            return state;
    }
};

export const loadStatus = (state = '', action) => {
    switch (action.type) {
        case actions.REQUEST_PROFILE_ERROR:
            return action.error;
        case actions.PROFILE_LOADED:
            return 'success';
        default:
            return state;
    }
};

const profile = combineReducers({
    info,
    fetching,
    loadStatus,
});

export default profile;
