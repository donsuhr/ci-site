/* global jest,it,expect,describe */

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from './profile.actions';
import * as listenerActions from './listeners.actions';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../scripts/firebase');
const { mockDatabase } = require('../../scripts/firebase');

describe('firebase Schedule actions', () => {
    it('should create an action for profile loaded', () => {
        const expectedAction = {
            type: actions.PROFILE_LOADED,
            data: {},
        };
        expect(actions.onProfileLoaded(expectedAction.data)).toEqual(
            expectedAction
        );
    });
    it('should create an action for request profile error', () => {
        const expectedAction = {
            type: actions.REQUEST_PROFILE_ERROR,
            error: {},
        };
        expect(actions.onProfileLoadError(expectedAction.error)).toEqual(
            expectedAction
        );
    });
    it('should create an action for request profile ', () => {
        const expectedAction = {
            type: actions.PROFILE_REQUESTED,
        };
        expect(actions.profileRequested()).toEqual(expectedAction);
    });
    it('should create an action for request profile update', () => {
        const expectedAction = {
            type: actions.REQUEST_PROFILE_UPDATE,
        };
        expect(actions.requestProfileUpdate()).toEqual(expectedAction);
    });
    it('should create an action for request profile saved', () => {
        const expectedAction = {
            type: actions.PROFILE_SAVED,
        };
        expect(actions.onProfileSaved()).toEqual(expectedAction);
    });
    it('should create an action for request profile saved error', () => {
        const expectedAction = {
            type: actions.PROFILE_SAVE_ERROR,
            error: {},
        };
        expect(actions.onProfileSaveError(expectedAction.error)).toEqual(
            expectedAction
        );
    });
    describe('firebase actions', () => {
        it('listenToProfile', () => {
            const store = mockStore({ domAttributes: { year: '2018' } });
            store.dispatch(
                actions.listenToProfile({ uid: 123, providerData: [{}] })
            );
            const ref = mockDatabase.child('users/123/profile');

            expect(store.getActions()[0].type).toEqual(
                actions.PROFILE_REQUESTED
            );
            expect(store.getActions()[1].type).toEqual(
                listenerActions.LISTENER_ADDED
            );
            ref.set({ foo: 'bar' });
            mockDatabase.flush();
            expect(store.getActions()[2].type).toEqual(actions.PROFILE_LOADED);
        });

        it('updateProfile', () => {
            const store = mockStore({});
            const promise = store.dispatch(
                actions.updateProfile({ userId: 123, data: {} })
            );
            expect(store.getActions()[0].type).toEqual(
                actions.REQUEST_PROFILE_UPDATE
            );
            promise.then(() => {
                expect(store.getActions()[1].type).toEqual(
                    actions.PROFILE_SAVED
                );
            });
        });
    });
});
