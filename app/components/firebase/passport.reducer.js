import { combineReducers } from 'redux';
import * as actions from './passport.actions';
import * as authActions from './auth.actions';

const sponsorDetails = () => ({
    // PLATINUM
    ACI: {
        code: '1224',
        title: 'ACI Worldwide',
    },
    Campus: {
        code: '1642',
        title: 'Campus Management',
    },
    // GOLD
    Ambassador: {
        code: '2262',
        title: 'Ambassador Education Solutions',
    },
    // Silver
    iData: {
        code: '3432',
        title: 'iData',
    },
    Onehop: {
        code: '3663',
        title: 'Onehop',
    },
    Socket: {
        code: '3762',
        title: 'Socket Labs',
    },
    // BRONZE
    AspirEDU: {
        code: '4277',
        title: 'AspirEDU',
    },
    DocuSign: {
        code: '4362',
        title: 'DocuSign',
    },
    Illumira: {
        code: '4455',
        title: 'Illumira',
    },
    SchoolDocs: {
        code: '4724',
        title: 'SchoolDocs',
    },
    WealthEngine: {
        code: '4932',
        title: 'WealthEngine',
    },
    Burning: {
        code: '4287',
        title: 'Burning Glass',
    },
    DubLabs: {
        code: '4382',
        title: 'DubLabs',
    },
    NelNet: {
        code: '4635',
        title: 'NelNet',
    },
    Softdocs: {
        code: '4736',
        title: 'Softdocs',
    },
    Peak: {
        code: '4732',
        title: 'Peak Performance Technologies',
    },
    Constituo: {
        code: '4266',
        title: 'Constituo',
    },
    EPX: {
        code: '4379',
        title: 'EPX',
    },
    Runner: {
        code: '4786',
        title: 'Runner',
    },
    TouchNet: {
        code: '4868',
        title: 'TouchNet',
    },
});

export const byId = (state = {}, action) => {
    switch (action.type) {
        case actions.PASSPORT_CHILD_ADDED:
            return {
                ...state,
                [action.data.key]: {
                    ref: action.data.ref,
                    key: action.data.key,
                    val: action.data.val(),
                },
            };
        case actions.PASSPORT_CHILD_REMOVED:
            const removedState = { ...state };
            delete removedState[action.data.key];
            return removedState;

        case actions.PASSPORT_CHILD_CHANGED:
            const changedState = { ...state };
            changedState[action.data.key] = {
                ref: action.data.ref,
                key: action.data.key,
                val: action.data.val(),
            };
            return changedState;
        case authActions.SIGN_OUT_SUCCESS:
        case authActions.PASSIVE_SIGN_OUT:
            return {};
        default:
            return state;
    }
};

export const hasEverLoaded = (state = false, action) => {
    switch (action.type) {
        case actions.PASSPORT_RECEIVED:
            return true;
        default:
            return state;
    }
};

export const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_PASSPORT:
            return true;
        case actions.PASSPORT_RECEIVED:
            return false;
        default:
            return state;
    }
};

const passport = combineReducers({
    hasEverLoaded,
    fetching,
    byId,
    sponsorDetails,
});

export default passport;
