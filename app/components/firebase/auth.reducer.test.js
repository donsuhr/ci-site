/* global it,expect,describe */
import { info, isAuth, isAdmin, initting, loadingRoot } from './auth.reducer';
import * as actions from './auth.actions';

describe('firebase auth Reducer', () => {
    describe('info', () => {
        it('should return the initial state', () => {
            expect(info(undefined, {})).toEqual({});
        });
        it('should handle AUTH_INIT', () => {
            const action = {
                type: actions.AUTH_INIT,
                user: {
                    displayName: 'displayName',
                    email: 'email',
                    uid: 'uid',
                },
            };
            expect(info({}, action)).toEqual(action.user);
        });
        it('should handle SIGN_OUT_SUCCESS', () => {
            const action = {
                type: actions.SIGN_OUT_SUCCESS,
            };
            expect(
                info(
                    {
                        displayName: 'displayName',
                        email: 'email',
                        uid: 'uid',
                    },
                    action
                )
            ).toEqual({});
        });
        it('should handle PASSIVE_SIGN_OUT', () => {
            const action = {
                type: actions.PASSIVE_SIGN_OUT,
            };
            expect(
                info(
                    {
                        displayName: 'displayName',
                        email: 'email',
                        uid: 'uid',
                    },
                    action
                )
            ).toEqual({});
        });
    });
    describe('isAdmin', () => {
        it('should return the initial state', () => {
            expect(isAdmin(undefined, {})).toEqual(false);
        });
        it('should handle USER_ROOT_LOADED admin true', () => {
            const action = {
                type: actions.USER_ROOT_LOADED,
                data: {
                    val() {
                        return { admin: true };
                    },
                },
            };
            expect(isAdmin(false, action)).toEqual(true);
        });
        it('should handle USER_ROOT_LOADED admin false', () => {
            const action = {
                type: actions.USER_ROOT_LOADED,
                data: {
                    val() {
                        return { admin: false };
                    },
                },
            };
            expect(isAdmin(false, action)).toEqual(false);
        });
        it('should handle USER_ROOT_LOADED admin null', () => {
            const action = {
                type: actions.USER_ROOT_LOADED,
                data: {},
            };
            expect(isAdmin(false, action)).toEqual(false);
        });
        it('should handle REQUEST_USER_ROOT_ERROR', () => {
            const action = {
                type: actions.REQUEST_USER_ROOT_ERROR,
            };
            expect(isAdmin(false, action)).toEqual(false);
        });
        it('should handle SIGN_OUT_SUCCESS', () => {
            const action = {
                type: actions.SIGN_OUT_SUCCESS,
            };
            expect(isAdmin(false, action)).toEqual(false);
        });
        it('should handle PASSIVE_SIGN_OUT', () => {
            const action = {
                type: actions.PASSIVE_SIGN_OUT,
            };
            expect(isAdmin(false, action)).toEqual(false);
        });
    });
    describe('initting', () => {
        it('should return the initial state', () => {
            expect(initting(undefined, {})).toEqual(false);
        });
        it('should handle AUTH_INIT_REQUESTED', () => {
            const action = {
                type: actions.AUTH_INIT_REQUESTED,
            };
            expect(initting(false, action)).toEqual(true);
        });
        it('should handle AUTH_INIT', () => {
            const action = {
                type: actions.AUTH_INIT,
            };
            expect(initting(false, action)).toEqual(false);
        });
        it('should handle REQUEST_USER_ROOT_ERROR', () => {
            const action = {
                type: actions.REQUEST_USER_ROOT_ERROR,
            };
            expect(initting(false, action)).toEqual(false);
        });
        it('should handle SIGN_OUT_SUCCESS', () => {
            const action = {
                type: actions.SIGN_OUT_SUCCESS,
            };
            expect(initting(false, action)).toEqual(false);
        });
        it('should handle PASSIVE_SIGN_OUT', () => {
            const action = {
                type: actions.PASSIVE_SIGN_OUT,
            };
            expect(initting(false, action)).toEqual(false);
        });
    });
    describe('isAuth', () => {
        it('should return the initial state', () => {
            expect(isAuth(undefined, {})).toEqual(false);
        });
        it('should handle AUTH_INIT', () => {
            const action = {
                type: actions.AUTH_INIT,
            };
            expect(isAuth(false, action)).toEqual(true);
        });
        it('should handle SIGN_OUT_SUCCESS', () => {
            const action = {
                type: actions.SIGN_OUT_SUCCESS,
            };
            expect(isAuth(false, action)).toEqual(false);
        });
        it('should handle PASSIVE_SIGN_OUT', () => {
            const action = {
                type: actions.PASSIVE_SIGN_OUT,
            };
            expect(isAuth(false, action)).toEqual(false);
        });
    });
    describe('loadingRoot', () => {
        it('should return the initial state', () => {
            expect(loadingRoot(undefined, {})).toEqual(false);
        });
        it('should handle USER_ROOT_REQUESTED', () => {
            const action = {
                type: actions.USER_ROOT_REQUESTED,
            };
            expect(loadingRoot(false, action)).toEqual(true);
        });
        it('should handle USER_ROOT_LOADED', () => {
            const action = {
                type: actions.USER_ROOT_LOADED,
            };
            expect(loadingRoot(false, action)).toEqual(false);
        });
        it('should handle REQUEST_USER_ROOT_ERROR', () => {
            const action = {
                type: actions.REQUEST_USER_ROOT_ERROR,
            };
            expect(loadingRoot(false, action)).toEqual(false);
        });
    });
});
