/* global it,expect,describe */
import { info, fetching, loadStatus } from './profile.reducer';
import * as actions from './profile.actions';

describe('firebase profile reducer', () => {
    describe('info', () => {
        it('should return the initial state', () => {
            expect(info(undefined, {})).toEqual({
                firstName: '',
                lastName: '',
                phone: '',
                email: '',
                institution: '',
            });
        });

        it('should handle PROFILE_LOADED', () => {
            const state = {};
            const action = {
                type: actions.PROFILE_LOADED,
                data: {
                    foo: 'bar',
                },
            };
            expect(info(state, action)).toEqual({
                foo: 'bar',
            });
        });
    });
    describe('fetching', () => {
        it('should return the initial state', () => {
            expect(fetching(undefined, {})).toEqual(false);
        });
        it('should handle PROFILE_REQUESTED', () => {
            const action = {
                type: actions.PROFILE_REQUESTED,
            };
            expect(fetching({}, action)).toEqual(true);
        });
        it('should handle REQUEST_PROFILE_ERROR', () => {
            const action = {
                type: actions.REQUEST_PROFILE_ERROR,
            };
            expect(fetching({}, action)).toEqual(false);
        });
        it('should handle PROFILE_LOADED', () => {
            const action = {
                type: actions.PROFILE_LOADED,
            };
            expect(fetching({}, action)).toEqual(false);
        });
    });
    describe('loadStatus', () => {
        it('should return the initial state', () => {
            expect(loadStatus(undefined, {})).toEqual('');
        });
        it('should handle REQUEST_PROFILE_ERROR', () => {
            const action = {
                type: actions.REQUEST_PROFILE_ERROR,
                error: 'error',
            };
            expect(loadStatus({}, action)).toEqual(action.error);
        });
        it('should handle PROFILE_LOADED', () => {
            const action = {
                type: actions.PROFILE_LOADED,
            };
            expect(loadStatus({}, action)).toEqual('success');
        });
    });
});
