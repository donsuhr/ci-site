import createFirebaseApp from '../../scripts/firebase';
import { addListener } from './listeners.actions';
import { getListenerByRef } from './listeners.reducer';

export const ALL_USERS_REQUESTED = 'ALL_USERS_REQUESTED';
export function allUsersRequested() {
    return {
        type: ALL_USERS_REQUESTED,
    };
}

export const ALL_USERS_LOADED = 'ALL_USERS_LOADED';
export function onAllUsersLoaded(data) {
    return {
        type: ALL_USERS_LOADED,
        data,
    };
}

export const ALL_USERS_ERROR = 'ALL_USERS_ERROR';
export function onAllUsersError(error) {
    return {
        type: ALL_USERS_ERROR,
        error,
    };
}

export const ALL_USERS_CHILD_ADDED = 'ALL_USERS_CHILD_ADDED';
export function onAllUsersChildAdded(data) {
    return {
        type: ALL_USERS_CHILD_ADDED,
        data,
    };
}

export const ALL_USERS_CHILD_REMOVED = 'ALL_USERS_CHILD_REMOVED';
export function onAllUsersChildRemoved(data) {
    return {
        type: ALL_USERS_CHILD_REMOVED,
        data,
    };
}

export const ALL_USERS_CHILD_CHANGED = 'ALL_USERS_CHILD_CHANGED';
export function onAllUsersChildChanged(data) {
    return {
        type: ALL_USERS_CHILD_CHANGED,
        data,
    };
}

export function listenToAllUsers() {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        const ref = firebaseDb.ref('users');
        if (!getListenerByRef(getState(), ref)) {
            dispatch(allUsersRequested());
            ref.on(
                'value',
                data => {
                    dispatch(onAllUsersLoaded(data));
                },
                error => {
                    dispatch(onAllUsersError(error));
                }
            );
            ref.on('child_added', data => {
                dispatch(onAllUsersChildAdded(data));
            });
            ref.on('child_removed', data => {
                dispatch(onAllUsersChildRemoved(data));
            });
            ref.on('child_changed', data => {
                dispatch(onAllUsersChildChanged(data));
            });

            dispatch(addListener(ref));
        }
    };
}
