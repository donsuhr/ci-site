import { combineReducers } from 'redux';
import * as actions from './easybook.actions';

export const byAvailabilityId = (state = {}, action) => {
    switch (action.type) {
        case actions.EASYBOOK_CHILD_ADDED:
            return {
                ...state,
                [action.data.key]: {
                    ref: action.data.ref,
                    key: action.data.key,
                    val: action.data.val(),
                },
            };
        case actions.EASYBOOK_CHILD_REMOVED:
            const copy = { ...state };
            delete copy[action.data.key];
            return copy;
        default:
            return state;
    }
};

export const hasEverLoaded = (state = false, action) => {
    switch (action.type) {
        case actions.EASYBOOK_LOADED:
            return true;
        default:
            return state;
    }
};

export const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.EASYBOOK_REQUESTED:
            return true;
        case actions.EASYBOOK_LOADED:
            return false;
        default:
            return state;
    }
};

const easybook = combineReducers({
    byAvailabilityId,
    hasEverLoaded,
    fetching,
});

export { easybook as default };
