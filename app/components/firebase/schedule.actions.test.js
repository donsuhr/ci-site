/* global jest,it,expect,describe */

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from './schedule.actions';
import * as listenerActions from './listeners.actions';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../scripts/firebase');
const { mockDatabase } = require('../../scripts/firebase');

describe('firebase Schedule actions', () => {
    it('should create an action to request child add', () => {
        const expectedAction = {
            type: actions.REQUEST_CHILD_ADD,
            id: '123',
        };
        expect(actions.requestChildAdd(expectedAction.id)).toEqual(
            expectedAction
        );
    });

    it('should create an action to request child remove', () => {
        const expectedAction = {
            type: actions.REQUEST_CHILD_REMOVE,
            id: 123,
        };
        expect(actions.requestChildRemove(expectedAction.id)).toEqual(
            expectedAction
        );
    });

    it('should create an action for session child added', () => {
        const expectedAction = {
            type: actions.SCHEDULE_SESSION_CHILD_ADDED,
            data: {},
        };
        expect(
            actions.onScheduleSessionChildAdded(expectedAction.data)
        ).toEqual(expectedAction);
    });

    it('should create an action for session child removed', () => {
        const expectedAction = {
            type: actions.SCHEDULE_SESSION_CHILD_REMOVED,
            data: {},
        };
        expect(
            actions.onScheduleSessionChildRemoved(expectedAction.data)
        ).toEqual(expectedAction);
    });

    it('should create an action for pct child removed', () => {
        const expectedAction = {
            type: actions.SCHEDULE_PCT_CHILD_REMOVED,
            data: {},
        };
        expect(actions.onSchedulePctChildRemoved(expectedAction.data)).toEqual(
            expectedAction
        );
    });

    it('should create an action for pct child added', () => {
        const expectedAction = {
            type: actions.SCHEDULE_PCT_CHILD_ADDED,
            data: {},
        };
        expect(actions.onSchedulePctChildAdded(expectedAction.data)).toEqual(
            expectedAction
        );
    });

    it('should create an action for sessions loaded', () => {
        const expectedAction = {
            type: actions.SCHEDULE_SESSIONS_LOADED,
            data: {},
        };
        expect(actions.onScheduleSessionsLoaded(expectedAction.data)).toEqual(
            expectedAction
        );
    });

    it('should create an action for pcts loaded', () => {
        const expectedAction = {
            type: actions.SCHEDULE_PCTS_LOADED,
            data: {},
        };
        expect(actions.onSchedulePctsLoaded(expectedAction.data)).toEqual(
            expectedAction
        );
    });

    describe('firebase actions', () => {
        it('listenToScheduleSessions', () => {
            const store = mockStore({ domAttributes: { year: '2018' } });
            store.dispatch(actions.listenToScheduleSessions('123'));
            const ref = mockDatabase.child('users/123/schedule/sessions');

            expect(store.getActions()[0].type).toEqual(
                listenerActions.LISTENER_ADDED
            );
            ref.set({ foo: 'bar' });
            mockDatabase.flush();
            expect(store.getActions()[1].type).toEqual(
                actions.SCHEDULE_SESSIONS_LOADED
            );
            const child = ref.push({ foo: 'bar' });
            mockDatabase.flush();
            expect(store.getActions()[2].type).toEqual(
                actions.SCHEDULE_SESSION_CHILD_ADDED
            );
            child.remove();
            mockDatabase.flush();
            expect(
                store.getActions()[store.getActions().length - 1].type
            ).toEqual(actions.SCHEDULE_SESSION_CHILD_REMOVED);
        });

        it('listenToSchedulePct', () => {
            const store = mockStore({ domAttributes: { year: '2018' } });
            store.dispatch(actions.listenToSchedulePct('123'));
            const ref = mockDatabase.child('users/123/schedule/pct');

            expect(store.getActions()[0].type).toEqual(
                listenerActions.LISTENER_ADDED
            );
            ref.set({ foo: 'bar' });
            mockDatabase.flush();
            expect(store.getActions()[1].type).toEqual(
                actions.SCHEDULE_PCTS_LOADED
            );
            const child = ref.push({ foo: 'bar' });
            mockDatabase.flush();
            expect(store.getActions()[2].type).toEqual(
                actions.SCHEDULE_PCT_CHILD_ADDED
            );
            child.remove();
            mockDatabase.flush();
            expect(
                store.getActions()[store.getActions().length - 1].type
            ).toEqual(actions.SCHEDULE_PCT_CHILD_REMOVED);
        });
    });

    describe('async actions', () => {
        it('should create an action to remove session from schedule', () => {
            const removeFn = jest.fn();
            const scheduleItem = {
                sessionId: 123,
                ref: {
                    remove: removeFn,
                },
            };
            const store = mockStore({});
            store.dispatch(actions.removeSessionFromSchedule(scheduleItem));
            const expectedAction = {
                type: actions.REQUEST_CHILD_REMOVE,
                id: 123,
            };
            expect(store.getActions()[0]).toEqual(expectedAction);
            expect(removeFn).toHaveBeenCalled();
        });

        it('should create an action to remove pct from schedule', () => {
            const removeFn = jest.fn();
            const scheduleItem = {
                pctId: 123,
                ref: {
                    remove: removeFn,
                },
            };
            const store = mockStore({});
            store.dispatch(actions.removePctFromSchedule(scheduleItem));
            const expectedAction = {
                type: actions.REQUEST_CHILD_REMOVE,
                id: 123,
            };
            expect(store.getActions()[0]).toEqual(expectedAction);
            expect(removeFn).toHaveBeenCalled();
        });
    });
});
