/* global jest,it,expect,describe */

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from './easybook.actions';
import * as listenerActions from './listeners.actions';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../scripts/firebase');
const { mockDatabase } = require('../../scripts/firebase');

describe('firebase easybook actions', () => {
    it('should create an action for schedule support requested save', () => {
        const expectedAction = {
            type: actions.SCHEDULE_SUPPORT_REQUESTED_EASY_BOOK_SAVE,
        };
        expect(actions.onScheduleSupportRequestedEasyBookSave()).toEqual(
            expectedAction
        );
    });
    it('should create an action for schedule support saved', () => {
        const refPromise = Promise.resolve();
        const expectedAction = {
            type: actions.SCHEDULE_SUPPORT_ITEM_EASY_BOOK_SAVED,
            refPromise,
        };
        expect(actions.onScheduleSupportEasyBookItemSaved(refPromise)).toEqual(
            expectedAction
        );
    });
    it('should create an action easybook requested', () => {
        const expectedAction = {
            type: actions.EASYBOOK_REQUESTED,
        };
        expect(actions.onEasyBookRequested()).toEqual(expectedAction);
    });
    it('should create an action easybook loaded', () => {
        const expectedAction = {
            type: actions.EASYBOOK_LOADED,
        };
        expect(actions.onEasyBookLoaded()).toEqual(expectedAction);
    });
    it('should create an action easybook child removed', () => {
        const expectedAction = {
            type: actions.EASYBOOK_CHILD_REMOVED,
            data: {},
        };
        expect(actions.onEasyBookChildRemoved(expectedAction.data)).toEqual(
            expectedAction
        );
    });
    it('should create an action easybook child added', () => {
        const expectedAction = {
            type: actions.EASYBOOK_CHILD_ADDED,
            data: {},
        };
        expect(actions.onEasyBookChildAdded(expectedAction.data)).toEqual(
            expectedAction
        );
    });
    it('easyBook', () => {
        const store = mockStore({ domAttributes: { year: '2018' } });
        const infoData = {
            firstName: 'fname',
            lastName: 'lname',
            institution: 'institution',
        };
        const refPromise = store.dispatch(
            actions.easyBook({
                availabilityId: 'avail123',
                ...infoData,
            })
        );
        mockDatabase.flush();
        const easyBookRef = mockDatabase.child('easybook/avail123');
        const usedAvailRef = mockDatabase.child('usedAvail/easybook/avail123');

        expect(store.getActions()[0].type).toEqual(
            actions.SCHEDULE_SUPPORT_REQUESTED_EASY_BOOK_SAVE
        );
        expect(store.getActions()[1].type).toEqual(
            actions.SCHEDULE_SUPPORT_ITEM_EASY_BOOK_SAVED
        );
        expect(refPromise).toEqual(store.getActions()[1].refPromise);
        expect(easyBookRef.data).toEqual({ info: { ...infoData } });
        expect(usedAvailRef.data).toEqual(true);
    });
    it('easybook close button', () => {
        const store = mockStore({ domAttributes: { year: '2018' } });
        store.dispatch(
            actions.easyBook({
                availabilityId: 'avail123',
                firstName: 'closed',
                lastName: 'closed',
            })
        );
        mockDatabase.flush();
        const easyBookRef = mockDatabase.child('easybook/avail123');
        expect(easyBookRef.data).toEqual({ info: 'closed' });
    });
    it('listenToEasyBook', () => {
        const store = mockStore({ domAttributes: { year: '2018' } });
        store.dispatch(actions.listenToEasyBook());
        const ref = mockDatabase.child('easybook');

        expect(store.getActions()[0].type).toEqual(actions.EASYBOOK_REQUESTED);
        expect(store.getActions()[1].type).toEqual(
            listenerActions.LISTENER_ADDED
        );
        ref.set({ foo: 'bar' });
        mockDatabase.flush();
        expect(store.getActions()[2].type).toEqual(actions.EASYBOOK_LOADED);

        const child = ref.push({ foo: 'bar' });
        mockDatabase.flush();
        expect(store.getActions()[3].type).toEqual(
            actions.EASYBOOK_CHILD_ADDED
        );
        child.remove();
        mockDatabase.flush();
        expect(store.getActions()[store.getActions().length - 1].type).toEqual(
            actions.EASYBOOK_CHILD_REMOVED
        );
    });
    it('removeEasyBook', () => {
        const remove = jest.fn();
        const store = mockStore({ domAttributes: { year: '2018' } });
        store.dispatch(
            actions.removeEasyBook({ key: 'avail123', ref: { remove } })
        );
        const ref = mockDatabase.child('usedAvail/easybook/avail123');
        mockDatabase.flush();
        expect(ref.data).toBeNull();
        expect(remove).toHaveBeenCalled();
    });

    it('removeRegularBook', () => {
        const store = mockStore({ domAttributes: { year: '2018' } });
        store.dispatch(actions.removeRegularBook('regId', 'userId', 'availId'));
        const usedAvailRef = mockDatabase.child('usedAvail/userId/avail123');
        const supportCouncilRef = mockDatabase.child(
            'users/userId/schedule/supportCouncil/regId'
        );
        expect(usedAvailRef.data).toBeNull();
        expect(supportCouncilRef.data).toBeNull();
    });
});
