import { SubmissionError } from 'redux-form';
import { addListener } from './listeners.actions';
import { getListenerByRef } from './listeners.reducer';
import createFirebaseApp from '../../scripts/firebase';
import { gleanDefaultProfile } from './profile.util';

export const PROFILE_LOADED = 'PROFILE_LOADED';
export function onProfileLoaded(data) {
    return {
        type: PROFILE_LOADED,
        data,
    };
}

export const REQUEST_PROFILE_ERROR = 'REQUEST_PROFILE_ERROR';
export function onProfileLoadError(error) {
    return {
        type: REQUEST_PROFILE_ERROR,
        error,
    };
}

export const PROFILE_REQUESTED = 'PROFILE_REQUESTED';
export function profileRequested() {
    return {
        type: PROFILE_REQUESTED,
    };
}

export const REQUEST_PROFILE_UPDATE = 'REQUEST_PROFILE_UPDATE';
export function requestProfileUpdate() {
    return {
        type: REQUEST_PROFILE_UPDATE,
    };
}

export const PROFILE_SAVED = 'PROFILE_SAVED';
export function onProfileSaved() {
    return {
        type: PROFILE_SAVED,
    };
}

export const PROFILE_SAVE_ERROR = 'PROFILE_SAVE_ERROR';
export function onProfileSaveError(error) {
    return {
        type: PROFILE_SAVE_ERROR,
        error,
    };
}

export function listenToProfile(user) {
    return (dispatch, getState) => {
        dispatch(profileRequested());
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        const userRef = firebaseDb.ref(`users/${user.uid}/profile`);
        if (!getListenerByRef(getState(), userRef)) {
            userRef.on(
                'value',
                data => {
                    const profileData = gleanDefaultProfile(user, data.val());
                    dispatch(onProfileLoaded(profileData));
                },
                error => {
                    dispatch(onProfileLoadError(error));
                }
            );
            dispatch(addListener(userRef));
        }
    };
}

export function updateProfile(userId, data, mode = 'set') {
    return (dispatch, getState) => {
        dispatch(requestProfileUpdate());
        return new Promise((resolve, reject) => {
            const { year } = getState().domAttributes;
            const firebaseDb = createFirebaseApp(year).db;
            const userRef = firebaseDb.ref(`users/${userId}/profile`);
            Promise.resolve()
                .then(() => userRef[mode](data))
                .then(() => {
                    dispatch(onProfileSaved());
                    resolve();
                })
                .catch(error => {
                    const submitError = new SubmissionError({
                        _error: error.message,
                    });
                    dispatch(onProfileSaveError(submitError));
                    reject(submitError);
                });
        });
    };
}
