/* global jest,it,expect,describe */

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from './auth.actions';
import * as listenerActions from './listeners.actions';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../scripts/firebase');
const { mockDatabase } = require('../../scripts/firebase');

describe('firebase auth actions', () => {
    it('should create an action for auth init', () => {
        const expectedAction = {
            type: actions.AUTH_INIT,
            user: {},
        };
        expect(actions.onAuthInit(expectedAction.user)).toEqual(expectedAction);
    });
    it('should create an action for auth init requested', () => {
        const expectedAction = {
            type: actions.AUTH_INIT_REQUESTED,
        };
        expect(actions.onAuthInitRequested()).toEqual(expectedAction);
    });
    it('should create an action for sign in error', () => {
        const expectedAction = {
            type: actions.SIGN_IN_ERROR,
            payload: {},
        };
        expect(actions.signInError(expectedAction.payload)).toEqual(
            expectedAction
        );
    });
    it('should create an action for sign in success', () => {
        const user = {};
        const result = { user };
        const expectedAction = {
            type: actions.SIGN_IN_SUCCESS,
            payload: user,
        };
        expect(actions.signInSuccess(result)).toEqual(expectedAction);
    });
    it('should create an action for sign out request', () => {
        const expectedAction = {
            type: actions.SIGN_OUT_SUCCESS,
        };
        expect(actions.signOutSuccess()).toEqual(expectedAction);
    });
    it('should create an action for passive sign out', () => {
        const expectedAction = {
            type: actions.PASSIVE_SIGN_OUT,
        };
        expect(actions.passiveSignOut()).toEqual(expectedAction);
    });
    it('should create an action for root user loaded', () => {
        const expectedAction = {
            type: actions.USER_ROOT_LOADED,
            data: {},
        };
        expect(actions.onUserRootLoaded(expectedAction.data)).toEqual(
            expectedAction
        );
    });
    it('should create an action user root requested', () => {
        const expectedAction = {
            type: actions.USER_ROOT_REQUESTED,
            userId: 123,
        };
        expect(actions.userRootRequested(expectedAction.userId)).toEqual(
            expectedAction
        );
    });
    it('should create an action user root requested', () => {
        const expectedAction = {
            type: actions.REQUEST_USER_ROOT_ERROR,
            error: {},
        };
        expect(actions.onUserRootLoadError(expectedAction.error)).toEqual(
            expectedAction
        );
    });
    describe('firebase actions', () => {
        it('listenToAuth', () => {
            const store = mockStore({ domAttributes: { year: '2018' } });
            const listener = store.dispatch(actions.listenToAuth());
            expect(store.getActions()[0].type).toEqual(
                actions.AUTH_INIT_REQUESTED
            );
            mockDatabase.changeAuthState({
                uid: 'testUid',
                provider: 'custom',
                token: 'authToken',
                auth: {
                    isAdmin: true,
                },
            });
            mockDatabase.flush();
            return listener.then(user => {
                expect(store.getActions()[1].type).toEqual(actions.AUTH_INIT);
            });
        });
        it('listenToAuth', () => {
            const store = mockStore({ domAttributes: { year: '2018' } });
            store.dispatch(actions.listenToUserRoot('123'));
            const ref = mockDatabase.child('users/123');

            expect(store.getActions()[0].type).toEqual(
                actions.USER_ROOT_REQUESTED
            );
            expect(store.getActions()[1].type).toEqual(
                listenerActions.LISTENER_ADDED
            );
            ref.set({ foo: 'bar' });
            mockDatabase.flush();
            expect(store.getActions()[2].type).toEqual(
                actions.USER_ROOT_LOADED
            );
        });
        it('signOut', () => {
            const store = mockStore({ domAttributes: { year: '2018' } });
            const signout = store.dispatch(actions.signOut());
            mockDatabase.flush();
            return signout.then(() => {
                expect(store.getActions()[0].type).toEqual(
                    actions.SIGN_OUT_SUCCESS
                );
            });
        });
    });
});
