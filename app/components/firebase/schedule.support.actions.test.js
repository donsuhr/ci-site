/* global jest,it,expect,describe,afterEach */

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import * as actions from './schedule.support.actions';
import * as listenerActions from './listeners.actions';

import config from '../../../config';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../scripts/firebase');
const { mockDatabase } = require('../../scripts/firebase');

function getLastAction(store) {
    const storeActions = store.getActions();
    return storeActions[storeActions.length - 1];
}

describe('firebase schedule.support actions', () => {
    it('should create an action for schedule support loaded', () => {
        const expectedAction = {
            type: actions.SCHEDULE_SUPPORT_LOADED,
            data: {},
        };
        expect(actions.onScheduleSupportLoaded(expectedAction.data)).toEqual(
            expectedAction
        );
    });

    it('should create an action to request user profile', () => {
        const expectedAction = {
            type: actions.SCHEDULE_SUPPORT_REQUESTED,
        };
        expect(actions.onScheduleSupportRequested()).toEqual(expectedAction);
    });
    it('should create an action to request save', () => {
        const expectedAction = {
            type: actions.SCHEDULE_SUPPORT_REQUESTED_SAVE,
        };
        expect(actions.onScheduleSupportRequestedSave()).toEqual(
            expectedAction
        );
    });
    it('should create an action to remove child', () => {
        const expectedAction = {
            type: actions.SCHEDULE_SUPPORT_CHILD_REMOVED,
        };
        expect(actions.onScheduleSupportChildRemoved()).toEqual(expectedAction);
    });
    it('should create an action to add child', () => {
        const expectedAction = {
            type: actions.SCHEDULE_SUPPORT_CHILD_ADDED,
        };
        expect(actions.onScheduleSupportChildAdded()).toEqual(expectedAction);
    });
    it('should create an action for used availability loaded', () => {
        const expectedAction = {
            type: actions.SCHEDULE_USED_AVAILABILITY_LOADED,
            data: {},
        };
        expect(
            actions.onScheduleUsedAvailabilityLoaded(expectedAction.data)
        ).toEqual(expectedAction);
    });

    it('should create an action for used availability changed', () => {
        const expectedAction = {
            type: actions.SCHEDULE_USED_AVAILABILITY_CHANGED,
            data: {},
        };
        expect(
            actions.onScheduleUsedAvailabilityChanged(expectedAction.data)
        ).toEqual(expectedAction);
    });
    it('should create an action for used availability requested', () => {
        const expectedAction = {
            type: actions.SCHEDULE_USED_AVAILABILITY_REQUESTED,
        };
        expect(actions.onScheduleUsedAvailabilityRequested()).toEqual(
            expectedAction
        );
    });

    describe('async firebase actions', () => {
        it('removeSupportFromSchedule', () => {
            const removeFn = jest.fn();
            const scheduleItem = {
                val: {
                    availabilityId: 'avail1',
                },
                ref: {
                    remove: removeFn,
                },
            };
            mockDatabase.child('usedAvail/123/avail1').set({ foo: 'bar' });
            mockDatabase.flush();
            const store = mockStore({ user: { info: { uid: '123' } }, domAttributes: { year: '2018' } });
            store.dispatch(actions.removeSupportFromSchedule(scheduleItem));

            expect(removeFn).toHaveBeenCalled();
            mockDatabase.flush();
            expect(mockDatabase.child('usedAvail/123/avail1').data).toBeNull();
        });

        it('addSupportCouncilToSchedule', () => {
            nock.disableNetConnect();
            nock(`${config.domains.api}/`)
                .post('/campusInsight/ci/web-hook--schedule-support-item/', {})
                .query(true)
                .reply(200, 'created');
            const expectedActions = [
                { type: actions.SCHEDULE_SUPPORT_REQUESTED_SAVE },
                { type: actions.SCHEDULE_SUPPORT_ITEM_SAVED },
            ];

            const store = mockStore({
                ui: { chosenAnalystId: '123' },
                user: { info: { uid: '234' } },
                profile: { info: {} },
                domAttributes: { year: '2018' },
            });
            const result = store.dispatch(
                actions.addSupportCouncilToSchedule({
                    analyst: {},
                    availabilityId: '456',
                })
            );
            expect(store.getActions()[0]).toEqual(expectedActions[0]);
            expect(store.getActions()[1].type).toEqual(expectedActions[1].type);
            expect(store.getActions()[1].refPromise).toBe(result);
            nock.cleanAll();
        });

        it('listenToScheduleSupport', () => {
            const store = mockStore({ domAttributes: { year: '2018' } });
            store.dispatch(actions.listenToScheduleSupport('123'));
            const ref = mockDatabase.child('users/123/schedule/supportCouncil');

            expect(store.getActions()[0]).toEqual({
                type: actions.SCHEDULE_SUPPORT_REQUESTED,
            });
            expect(store.getActions()[1].type).toEqual(
                listenerActions.LISTENER_ADDED
            );
            ref.set({ foo: 'bar' });
            mockDatabase.flush();
            expect(store.getActions()[2].type).toEqual(
                actions.SCHEDULE_SUPPORT_LOADED
            );
            const child = ref.push({ foo: 'bar' });
            mockDatabase.flush();
            expect(store.getActions()[3].type).toEqual(
                actions.SCHEDULE_SUPPORT_CHILD_ADDED
            );
            child.remove();
            mockDatabase.flush();
            expect(
                store.getActions()[store.getActions().length - 1].type
            ).toEqual(actions.SCHEDULE_SUPPORT_CHILD_REMOVED);
        });

        it('listenToUsedAvailability', () => {
            const store = mockStore({ domAttributes: { year: '2018' } });
            store.dispatch(actions.listenToUsedAvailability());
            const ref = mockDatabase.child('usedAvail');
            expect(store.getActions()[0].type).toEqual(
                actions.SCHEDULE_USED_AVAILABILITY_REQUESTED
            );
            expect(store.getActions()[1].type).toEqual(
                listenerActions.LISTENER_ADDED
            );
            // set
            ref.set({ foo: 'bar' });
            mockDatabase.flush();
            expect(store.getActions()[2].type).toEqual(
                actions.SCHEDULE_USED_AVAILABILITY_LOADED
            );
            // change
            ref.set({ foo: 'baz' });
            mockDatabase.flush();
            expect(getLastAction(store).type).toEqual(
                actions.SCHEDULE_USED_AVAILABILITY_CHANGED
            );
            // push
            const added = ref.push({ foo: 'baz' });
            mockDatabase.flush();
            expect(getLastAction(store).type).toEqual(
                actions.SCHEDULE_USED_AVAILABILITY_CHANGED
            );
            // remove
            added.remove();
            mockDatabase.flush();

            expect(getLastAction(store).type).toEqual(
                actions.SCHEDULE_USED_AVAILABILITY_CHANGED
            );
        });
    });

    describe('async nock actions', () => {
        afterEach(() => {
            nock.cleanAll();
        });
        it('postInfoToWebhook', () => {
            nock.disableNetConnect();
            const scope = nock(`${config.domains.api}/`)
                .post('/campusInsight/ci/web-hook--schedule-support-item/', {
                    analystFirstName: 'foo',
                    userEmail: 'email',
                })
                .query(true)
                .reply(200, 'created');
            actions.postInfoToWebhook({
                analyst: { firstName: 'foo' },
                profile: { info: { email: 'email' } },
            });
            expect(scope.done.bind(nock)).not.toThrow();
        });
    });
});
