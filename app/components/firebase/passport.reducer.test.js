/* global it,expect,describe */
import { hasEverLoaded, fetching, byId } from './passport.reducer';
import * as actions from './passport.actions';
import * as authActions from './auth.actions';

describe('firebase Passport Reducer', () => {
    describe('hasEverLoaded', () => {
        it('should return the initial state', () => {
            expect(hasEverLoaded(undefined, {})).toEqual(false);
        });
        it('should handle PASSPORT_RECEIVED', () => {
            expect(
                hasEverLoaded(false, {
                    type: actions.PASSPORT_RECEIVED,
                })
            ).toEqual(true);
        });
    });
    describe('fetching', () => {
        it('should return the initial state', () => {
            expect(fetching(undefined, {})).toEqual(false);
        });
        it('should handle REQUEST_PASSPORT', () => {
            expect(
                fetching(false, {
                    type: actions.REQUEST_PASSPORT,
                })
            ).toEqual(true);
        });
        it('should handle PASSPORT_RECEIVED', () => {
            expect(
                fetching(true, {
                    type: actions.PASSPORT_RECEIVED,
                })
            ).toEqual(false);
        });
    });
    describe('byId', () => {
        it('should return the initial state', () => {
            expect(byId(undefined, {})).toEqual({});
        });
        it('should handle PASSPORT_CHILD_ADDED', () => {
            const state = [];
            const action = {
                type: actions.PASSPORT_CHILD_ADDED,
                data: {
                    ref: 'ref',
                    key: 'key',
                    val: () => ({ first: 'name' }),
                },
            };
            expect(byId(state, action)).toEqual({
                key: {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
            });
        });
        it('should handle PASSPORT_CHILD_REMOVED', () => {
            const state = {
                key: {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
            };
            const action = {
                type: actions.PASSPORT_CHILD_REMOVED,
                data: {
                    key: 'key',
                },
            };
            expect(byId(state, action)).toEqual({});
        });
        it('should handle PASSPORT_CHILD_CHANGED', () => {
            const state = {
                key: {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
            };
            const action = {
                type: actions.PASSPORT_CHILD_CHANGED,
                data: {
                    ref: 'ref',
                    key: 'key',
                    val: () => ({ first: 'changed' }),
                },
            };
            expect(byId(state, action)).toEqual({
                key: {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'changed' },
                },
            });
        });
        it('should handle SIGN_OUT_SUCCESS', () => {
            const state = {
                key: {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
            };
            const action = {
                type: authActions.SIGN_OUT_SUCCESS,
            };
            expect(byId(state, action)).toEqual({});
        });
        it('should handle PASSIVE_SIGN_OUT', () => {
            const state = {
                key: {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
            };
            const action = {
                type: authActions.PASSIVE_SIGN_OUT,
            };
            expect(byId(state, action)).toEqual({});
        });
    });
});
