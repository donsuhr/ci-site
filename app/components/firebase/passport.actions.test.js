/* global jest,it,expect,describe */

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from './passport.actions';
import * as listenerActions from './listeners.actions';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../scripts/firebase');
const { mockDatabase } = require('../../scripts/firebase');

describe('firebase passport actions', () => {
    it('should create an action for passport received', () => {
        const expectedAction = {
            type: actions.PASSPORT_RECEIVED,
            data: {},
        };
        expect(actions.onReceivePassport(expectedAction.data)).toEqual(
            expectedAction
        );
    });
    it('should create an action for passport child added', () => {
        const expectedAction = {
            type: actions.PASSPORT_CHILD_ADDED,
            data: {},
        };
        expect(actions.onPassportChildAdded(expectedAction.data)).toEqual(
            expectedAction
        );
    });
    it('should create an action for passport child removed', () => {
        const expectedAction = {
            type: actions.PASSPORT_CHILD_REMOVED,
            data: {},
        };
        expect(actions.onPassportChildRemoved(expectedAction.data)).toEqual(
            expectedAction
        );
    });
    it('should create an action for passport child changed', () => {
        const expectedAction = {
            type: actions.PASSPORT_CHILD_CHANGED,
            data: {},
        };
        expect(actions.onPassportChildChanged(expectedAction.data)).toEqual(
            expectedAction
        );
    });
    it('should create an action for requesting a passport', () => {
        const expectedAction = {
            type: actions.REQUEST_PASSPORT,
        };
        expect(actions.onRequestPassport()).toEqual(expectedAction);
    });

    it('should create an action for requesting a passport child add', () => {
        const expectedAction = {
            type: actions.PASSPORT_REQUEST_ADD_CHILD,
            userId: '123',
            name: 'name',
        };
        expect(
            actions.requestAddPassportItem(
                expectedAction.userId,
                expectedAction.name
            )
        ).toEqual(expectedAction);
    });

    describe('firebase actions', () => {
        it('fetchPassport', () => {
            const store = mockStore({ domAttributes: { year: '2018' } });
            store.dispatch(actions.fetchPassport('123'));
            const ref = mockDatabase.child('users/123/passport');

            expect(store.getActions()[0].type).toEqual(
                actions.REQUEST_PASSPORT
            );
            expect(store.getActions()[1].type).toEqual(
                listenerActions.LISTENER_ADDED
            );
            ref.set({ foo: 'bar' });
            mockDatabase.flush();
            expect(store.getActions()[2].type).toEqual(
                actions.PASSPORT_RECEIVED
            );
            const child = ref.push({ foo: 'bar' });
            mockDatabase.flush();
            expect(store.getActions()[3].type).toEqual(
                actions.PASSPORT_CHILD_ADDED
            );
            child.set({ foo: 'baz' });
            mockDatabase.flush();
            expect(
                store.getActions()[store.getActions().length - 1].type
            ).toEqual(actions.PASSPORT_CHILD_CHANGED);
            child.remove();
            mockDatabase.flush();
            expect(
                store.getActions()[store.getActions().length - 1].type
            ).toEqual(actions.PASSPORT_CHILD_REMOVED);
        });

        it('addPassportItem', () => {
            const store = mockStore({ domAttributes: { year: '2018' } });
            const ref = store.dispatch(
                actions.addPassportItem('123', 'name', 'value')
            );
            const dispatchedAction = store.getActions()[0];
            expect(dispatchedAction.type).toEqual(
                actions.PASSPORT_REQUEST_ADD_CHILD
            );
            expect(dispatchedAction.userId).toEqual('123');
            expect(dispatchedAction.name).toEqual('name');
            mockDatabase.flush();
            return ref.then(result => {
                expect(result.code).toEqual('value');
            });
        });
    });
});
