import $ from 'jquery';
import createFirebaseApp from '../../scripts/firebase';
import { addListener } from './listeners.actions';
import { getListenerByRef } from './listeners.reducer';

export const AUTH_INIT = 'AUTH_INIT';
export function onAuthInit(user) {
    return {
        type: AUTH_INIT,
        user,
    };
}

export const AUTH_INIT_REQUESTED = 'AUTH_INIT_REQUESTED';
export function onAuthInitRequested() {
    return {
        type: AUTH_INIT_REQUESTED,
    };
}

export const SIGN_IN_ERROR = 'SIGN_IN_ERROR';
export function signInError(error) {
    return {
        type: SIGN_IN_ERROR,
        payload: error,
    };
}

export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
export function signInSuccess(result) {
    return {
        type: SIGN_IN_SUCCESS,
        payload: result.user,
    };
}

export const SIGN_OUT_SUCCESS = 'SIGN_OUT_SUCCESS';
export function signOutSuccess() {
    return {
        type: SIGN_OUT_SUCCESS,
    };
}

export const PASSIVE_SIGN_OUT = 'PASSIVE_SIGN_OUT';
export function passiveSignOut() {
    return {
        type: PASSIVE_SIGN_OUT,
    };
}

export const USER_ROOT_LOADED = 'USER_ROOT_LOADED';
export function onUserRootLoaded(data) {
    return {
        type: USER_ROOT_LOADED,
        data,
    };
}

export const USER_ROOT_REQUESTED = 'USER_ROOT_REQUESTED';
export function userRootRequested(userId) {
    return {
        type: USER_ROOT_REQUESTED,
        userId,
    };
}

export const REQUEST_USER_ROOT_ERROR = 'REQUEST_USER_ROOT_ERROR';
export function onUserRootLoadError(error) {
    return {
        type: REQUEST_USER_ROOT_ERROR,
        error,
    };
}

export function listenToAuth() {
    return (dispatch, getState) => {
        if (listenToAuth.cached) {
            return listenToAuth.cached;
        }
        listenToAuth.cached = new Promise((resolve, reject) => {
            dispatch(onAuthInitRequested());
            const { year } = getState().domAttributes;
            const firebaseAuth = createFirebaseApp(year).auth;
            firebaseAuth.onAuthStateChanged(user => {
                if (user) {
                    dispatch(onAuthInit(user));
                    resolve(user);
                } else {
                    dispatch(passiveSignOut());
                    delete listenToAuth.cached;
                }
            });
        });
        return listenToAuth.cached;
    };
}

export function listenToUserRoot(userId) {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        const userRef = firebaseDb.ref(`users/${userId}`);
        if (!getListenerByRef(getState(), userRef)) {
            dispatch(userRootRequested(userId));
            userRef.on(
                'value',
                data => {
                    dispatch(onUserRootLoaded(data));
                },
                error => {
                    dispatch(onUserRootLoadError(error));
                }
            );
            dispatch(addListener(userRef));
        }
    };
}

export function signOut(year) {
    const fb = createFirebaseApp(year);
    return dispatch => fb.auth.signOut().then(() => dispatch(signOutSuccess()));
}

export function requestPopupAuth(year) {
    return new Promise((resolve, reject) => {
        const $AccountModal = $(`#AccountModal-${year}`);
        const onModalHide = function onModalHide(event) {
            $AccountModal.off('hide.bs.modal', onModalHide);
            $AccountModal.data('inpromise', '');
            // eslint-disable-next-line prefer-promise-reject-errors
            reject('didnt log in');
        };
        $AccountModal.data('inpromise', true);
        $AccountModal.on('hide.bs.modal', onModalHide).modal('show');
        const fb = createFirebaseApp(year);
        const off = fb.auth.onAuthStateChanged(user => {
            if (user) {
                off();
                $AccountModal.off('hide.bs.modal', onModalHide);
                $AccountModal.modal('hide');
                $AccountModal.data('inpromise', '');
                resolve(user);
            }
        });
    });
}
