function gleanDefaultProfile(user, data = {}) {
    const defaults = {
        firstName: '',
        lastName: '',
        phone: '',
        email: '',
        institution: '',
    };

    if (data === null) {
        data = {};
    }
    switch (user.providerData[0].providerId) {
        case 'google.com':
        case 'facebook.com':
            let fName;
            let lName;
            if (user.providerData[0].displayName) {
                if (user.providerData[0].displayName.indexOf(' ') !== -1) {
                    [fName, lName] = user.providerData[0].displayName.split(
                        ' '
                    );
                }
            }
            defaults.firstName = data.firstName || fName || '';
            defaults.lastName = data.lastName || lName || '';
            defaults.email = data.email || user.providerData[0].email || '';
            break;
        default:
        case 'password':
            defaults.email = data.email || user.providerData[0].email || '';
            break;
    }

    return {
        ...defaults,
        ...data,
    };
}

// eslint-disable-next-line import/prefer-default-export
export { gleanDefaultProfile };
