import createFirebaseApp from '../../scripts/firebase';
import { addListener } from './listeners.actions';
import { getListenerByRef } from './listeners.reducer';

export const PASSPORT_RECEIVED = 'PASSPORT_RECEIVED';
export function onReceivePassport(data) {
    return {
        type: PASSPORT_RECEIVED,
        data,
    };
}

export const PASSPORT_CHILD_ADDED = 'PASSPORT_CHILD_ADDED';
export function onPassportChildAdded(data) {
    return {
        type: PASSPORT_CHILD_ADDED,
        data,
    };
}
export const PASSPORT_CHILD_REMOVED = 'PASSPORT_CHILD_REMOVED';
export function onPassportChildRemoved(data) {
    return {
        type: PASSPORT_CHILD_REMOVED,
        data,
    };
}

export const PASSPORT_CHILD_CHANGED = 'PASSPORT_CHILD_CHANGED';
export function onPassportChildChanged(data) {
    return {
        type: PASSPORT_CHILD_CHANGED,
        data,
    };
}

export const REQUEST_PASSPORT = 'REQUEST_PASSPORT';
export function onRequestPassport() {
    return {
        type: REQUEST_PASSPORT,
    };
}

export const PASSPORT_REQUEST_ADD_CHILD = 'PASSPORT_REQUEST_ADD_CHILD';
export function requestAddPassportItem(userId, name) {
    return {
        type: PASSPORT_REQUEST_ADD_CHILD,
        userId,
        name,
    };
}

export function fetchPassport(userId) {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        const scheduleRef = firebaseDb.ref(`users/${userId}/passport`);
        if (!getListenerByRef(getState(), scheduleRef)) {
            dispatch(onRequestPassport());

            scheduleRef.once('value', (data) => {
                dispatch(onReceivePassport(data));
            });
            scheduleRef.on('child_added', (data) => {
                dispatch(onPassportChildAdded(data));
            });
            scheduleRef.on('child_removed', (data) => {
                dispatch(onPassportChildRemoved(data));
            });
            scheduleRef.on('child_changed', (data) => {
                dispatch(onPassportChildChanged(data));
            });

            dispatch(addListener(scheduleRef));
        }
    };
}

export function addPassportItem(userId, name, value) {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        dispatch(requestAddPassportItem(userId, name));
        return firebaseDb.ref(`users/${userId}/passport/${name}`).set({ code: value });
    };
}
