import PropTypes from 'prop-types';
import { combineReducers } from 'redux';
import * as actions from './allUsers.actions';

export const item = (state = {}, action) => {
    switch (action.type) {
        case actions.ALL_USERS_LOADED:
            return {
                ...state,
                ...action.item,
            };
        default:
            return state;
    }
};

export const byId = (state = {}, action) => {
    switch (action.type) {
        case actions.ALL_USERS_LOADED:
            const val = action.data.val();
            return Object.keys(val).reduce((acc, key) => {
                acc[key] = item(state[key], {
                    type: action.type,
                    item: val[key],
                });
                return acc;
            }, {});

        default:
            return state;
    }
};

export const hasEverLoaded = (state = false, action) => {
    switch (action.type) {
        case actions.ALL_USERS_LOADED:
            return true;
        default:
            return state;
    }
};

export const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.ALL_USERS_REQUESTED:
            return true;
        case actions.ALL_USERS_LOADED:
        case actions.ALL_USERS_ERROR:
            return false;
        default:
            return state;
    }
};

const allUsers = combineReducers({
    hasEverLoaded,
    fetching,
    byId,
});

function withPassport(state) {
    if (!state.byId) {
        return {};
    }
    return Object.keys(state.byId).reduce((acc, c) => {
        const currentItem = state.byId[c];
        if (Object.hasOwnProperty.call(currentItem, 'passport')) {
            acc[c] = {
                ...currentItem,
            };
        }
        return acc;
    }, {});
}

function getUserEmail(value, id) {
    if (Object.hasOwnProperty.call(value, 'profile')
        && Object.hasOwnProperty.call(value.profile, 'email')
        && value.profile.email !== '') {
        return value.profile.email;
    }
    if (Object.hasOwnProperty.call(value, 'providerData')
        && Object.hasOwnProperty.call(value.providerData, 'email')
        && value.providerData.email !== '') {
        return value.providerData.email;
    }
    return id;
}

function countUserActivity(state) {
    return Object.keys(state.byId).map((x) => {
        const value = state.byId[x];
        const id = getUserEmail(value, x);
        const hasSchedule = Object.hasOwnProperty.call(value, 'schedule');
        const hasSessions = hasSchedule && Object.hasOwnProperty.call(value.schedule, 'sessions');
        const hasPct = hasSchedule && Object.hasOwnProperty.call(value.schedule, 'pct');
        const sessions = hasSessions ? Object.keys(value.schedule.sessions).length : 0;
        const pct = hasPct ? Object.keys(value.schedule.pct).length : 0;
        const total = sessions + pct;
        return {
            id,
            sessions,
            pct,
            total,
        };
    });
}

const PropType = PropTypes.shape({
    hasEverLoaded: PropTypes.bool,
    fetching: PropTypes.bool,
    byId: PropTypes.object,
});

function getDisplayName(user) {
    if (Object.hasOwnProperty.call(user, 'profile')
        && Object.hasOwnProperty.call(user.profile, 'firstName')
        && user.profile.firstName !== '') {
        return `${user.profile.firstName} ${user.profile.lastName}`;
    }
    if (Object.hasOwnProperty.call(user, 'providerData')
        && Object.hasOwnProperty.call(user.providerData, 'displayName')
        && user.providerData.displayName !== '') {
        return user.providerData.displayName;
    }
    return user.id;
}

export {
    allUsers as default,
    withPassport,
    countUserActivity,
    PropType,
    getDisplayName,
};
