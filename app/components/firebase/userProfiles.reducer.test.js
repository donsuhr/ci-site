/* global it,expect,describe */
import { byId } from './userProfiles.reducer';
import * as actions from './userProfiles.actions';

describe('firebase UserProfile reducer', () => {
    describe('byId', () => {
        it('should return the initial state', () => {
            expect(byId(undefined, {})).toEqual({ loading: false });
        });

        it('should handle USER_PROFILE_RECEIVED', () => {
            const state = {};
            const action = {
                type: actions.USER_PROFILE_RECEIVED,
                data: {
                    ref: { parent: { key: 'foo' } },
                    key: 'key',
                    val: () => true,
                },
            };
            expect(byId(state, action)).toEqual({
                foo: {
                    ...action.data,
                    loading: false,
                    val: true,
                },
            });
        });

        it('should handle REQUEST_USER_PROFILE', () => {
            const state = {};
            const action = {
                type: actions.REQUEST_USER_PROFILE,
                userId: '123',
            };
            expect(byId(state, action)).toEqual({
                123: {
                    loading: true,
                },
            });
        });
    });
});
