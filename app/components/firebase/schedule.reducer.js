import { combineReducers } from 'redux';
import sortBy from 'lodash/sortBy';
import isDate from 'lodash/isDate';
import moment from '../util/load-moment-tz';
import * as actions from './schedule.actions';
import * as authActions from './auth.actions';
import { getAppointmentsByDay } from '../firebase/schedule.support.reducer';

export const sessions = (state = [], action) => {
    switch (action.type) {
        case actions.SCHEDULE_SESSION_CHILD_ADDED:
            return [
                ...state,
                {
                    ref: action.data.ref,
                    key: action.data.key,
                    sessionId: action.data.val().sessionId,
                },
            ];
        case actions.SCHEDULE_SESSION_CHILD_REMOVED:
            const index = state.findIndex(x => x.key === action.data.key);
            if (index >= 0) {
                return [...state.slice(0, index), ...state.slice(index + 1)];
            }
            return state;
        case authActions.SIGN_OUT_SUCCESS:
        case authActions.PASSIVE_SIGN_OUT:
            return [];
        default:
            return state;
    }
};

export const pct = (state = [], action) => {
    switch (action.type) {
        case actions.SCHEDULE_PCT_CHILD_ADDED:
            return [
                ...state,
                {
                    ref: action.data.ref,
                    key: action.data.key,
                    pctId: action.data.val().pctId,
                },
            ];
        case actions.SCHEDULE_PCT_CHILD_REMOVED:
            const index = state.findIndex(x => x.key === action.data.key);
            if (index >= 0) {
                return [...state.slice(0, index), ...state.slice(index + 1)];
            }
            return state;
        case authActions.SIGN_OUT_SUCCESS:
        case authActions.PASSIVE_SIGN_OUT:
            return [];
        default:
            return state;
    }
};

export const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_CHILD_ADD:
            return true;
        case actions.SCHEDULE_SESSION_CHILD_ADDED:
        case actions.SCHEDULE_SESSION_CHILD_REMOVED:
            return false;
        default:
            return state;
    }
};

export const childStatus = (state = {}, action) => {
    switch (action.type) {
        case actions.REQUEST_CHILD_ADD:
        case actions.REQUEST_CHILD_REMOVE:
            return {
                ...state,
                [action.id]: { state: action.type },
            };
        case actions.SCHEDULE_SESSION_CHILD_ADDED:
        case actions.SCHEDULE_SESSION_CHILD_REMOVED:
            const copy1 = { ...state };
            delete copy1[action.data.val().sessionId];
            return copy1;
        case actions.SCHEDULE_PCT_CHILD_ADDED:
        case actions.SCHEDULE_PCT_CHILD_REMOVED:
            const copy2 = { ...state };
            delete copy2[action.data.val().pctId];
            return copy2;
        default:
            return state;
    }
};

export const hasEverLoadedSessions = (state = false, action) => {
    switch (action.type) {
        case actions.SCHEDULE_SESSIONS_LOADED:
            return true;
        default:
            return state;
    }
};

export const hasEverLoadedPcts = (state = false, action) => {
    switch (action.type) {
        case actions.SCHEDULE_PCTS_LOADED:
            return true;
        default:
            return state;
    }
};

const schedule = combineReducers({
    hasEverLoadedSessions,
    hasEverLoadedPcts,
    fetching,
    pct,
    sessions,
    childStatus,
});

export default schedule;

export function isSessionInSchedule(state, sessionId) {
    if (!state || !state.sessions) {
        return false;
    }
    const filtered = state.sessions.filter(x => x.sessionId === sessionId);
    if (filtered.length === 0) {
        return false;
    }
    return filtered[0];
}

export function isPctInSchedule(state, pctId) {
    if (!state || !state.pct) {
        return false;
    }
    const filtered = state.pct.filter(x => x.pctId === pctId);
    if (filtered.length === 0) {
        return false;
    }
    return filtered[0];
}

export function getItemStatus(state, id) {
    if (!state || !state.childStatus) {
        return null;
    }
    return state.childStatus[id] ? state.childStatus[id].state : null;
}

export function getSessionItemById(state, sessionId) {
    if (!state || !state.sessions) {
        return null;
    }
    const filtered = state.sessions.filter(x => x.sessionId === sessionId);
    if (filtered.length < 1) {
        return null;
    }
    return filtered[0];
}

export function getPctItemById(state, sessionId) {
    if (!state || !state.pct) {
        return null;
    }
    const filtered = state.pct.filter(x => x.pctId === sessionId);
    if (filtered.length < 1) {
        return null;
    }
    return filtered[0];
}

export function getScheduledSessions(
    scheduleState,
    sessionsState,
    pctState,
    presetState,
    analystState,
    supportConsultationsState
) {
    if (!scheduleState) {
        return [];
    }
    if (!sessionsState || !sessionsState.byId) {
        return [];
    }

    const items = {};
    Object.keys(presetState).reduce((acc, presetKey) => {
        const item = presetState[presetKey];
        const retKey = moment(item.start)
            .tz('America/New_York')
            .format('YYYYMMDD');
        if (!{}.hasOwnProperty.call(acc, retKey)) {
            acc[retKey] = [];
        }
        acc[retKey].push(item);
        return acc;
    }, items);

    Object.keys(sessionsState.byId).reduce((acc, sessionKey) => {
        const item = sessionsState.byId[sessionKey];
        if (isSessionInSchedule(scheduleState, sessionKey)) {
            const retKey = moment(item.start)
                .tz('America/New_York')
                .format('YYYYMMDD');
            if (!{}.hasOwnProperty.call(acc, retKey)) {
                acc[retKey] = [];
            }
            item.type = 'session';
            acc[retKey].push(item);
        }
        return acc;
    }, items);

    Object.keys(pctState.byId).reduce((acc, sessionKey) => {
        const item = pctState.byId[sessionKey];
        if (isPctInSchedule(scheduleState, sessionKey)) {
            const retKey = moment(item.start)
                .tz('America/New_York')
                .format('YYYYMMDD');
            if (!{}.hasOwnProperty.call(acc, retKey)) {
                acc[retKey] = [];
            }
            item.type = 'pct';
            acc[retKey].push(item);
        }
        return acc;
    }, items);

    const supportConsultations = getAppointmentsByDay(
        supportConsultationsState,
        analystState
    );
    Object.keys(supportConsultations).reduce((acc, key) => {
        if (!{}.hasOwnProperty.call(acc, key)) {
            acc[key] = [];
        }
        acc[key] = [...acc[key], ...supportConsultations[key]];
        return acc;
    }, items);

    Object.keys(items).forEach(key => {
        items[key] = sortBy(items[key], x => {
            if (!isDate(x.start)) {
                return new Date(x.start);
            }
            return x.start;
        });
    });

    return items;
}

export function getSupportConsultationConflicts(
    scheduledItems,
    scStart,
    scEnd
) {
    const key = moment(scStart).tz('America/New_York').format('YYYYMMDD');
    if (Object.hasOwnProperty.call(scheduledItems, key)) {
        return scheduledItems[key].filter(x => {
            const scStartDate = new Date(scStart);
            const scEndDate = new Date(scEnd);
            const itemStart = new Date(x.start);
            const itemEnd = new Date(x.end);
            return scStartDate < itemEnd && scEndDate > itemStart;
        });
    }
    return [];
}
