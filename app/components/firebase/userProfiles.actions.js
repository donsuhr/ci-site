import createFirebaseApp from '../../scripts/firebase';

export const USER_PROFILE_RECEIVED = 'USER_PROFILE_RECEIVED';
export function onReceiveProfile(data) {
    return {
        type: USER_PROFILE_RECEIVED,
        data,
    };
}

export const REQUEST_USER_PROFILE = 'REQUEST_USER_PROFILE';
export function onRequestUserProfile(userId) {
    return {
        type: REQUEST_USER_PROFILE,
        userId,
    };
}

function fetchUserProfile(userId) {
    return (dispatch, getState) => {
        const currently = getState().userProfiles.byId[userId];
        if (currently && currently.loading) {
            return;
        }
        dispatch(onRequestUserProfile(userId));
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        const ref = firebaseDb.ref(`users/${userId}/profile`);
        ref.once('value', (data) => {
            dispatch(onReceiveProfile(data));
        })
            .catch((error) => {
                // eslint-disable-next-line no-console
                console.log('caught error from value', error);
            });
    };
}

export { fetchUserProfile };
