import $ from 'jquery';
import createFirebaseApp from '../../scripts/firebase';
import { requestPopupAuth } from './auth.actions';
import { addListener } from './listeners.actions';
import { getSessionById } from '../SessionBrowser/reducer';
import { getPctById } from '../PctBrowser/reducer';
import { getListenerByRef } from './listeners.reducer';
import ScheduleConflict from '../ScheduleConflict';

export const REQUEST_CHILD_ADD = 'REQUEST_CHILD_ADD';
export function requestChildAdd(id) {
    return {
        type: REQUEST_CHILD_ADD,
        id,
    };
}

export const REQUEST_CHILD_REMOVE = 'REQUEST_CHILD_REMOVE';
export function requestChildRemove(id) {
    return {
        type: REQUEST_CHILD_REMOVE,
        id,
    };
}

export const SCHEDULE_SESSION_CHILD_ADDED = 'SCHEDULE_SESSION_CHILD_ADDED';
export function onScheduleSessionChildAdded(data) {
    return {
        type: SCHEDULE_SESSION_CHILD_ADDED,
        data,
    };
}

export const SCHEDULE_SESSION_CHILD_REMOVED = 'SCHEDULE_SESSION_CHILD_REMOVED';
export function onScheduleSessionChildRemoved(data) {
    return {
        type: SCHEDULE_SESSION_CHILD_REMOVED,
        data,
    };
}

export const SCHEDULE_PCT_CHILD_REMOVED = 'SCHEDULE_PCT_CHILD_REMOVED';
export function onSchedulePctChildRemoved(data) {
    return {
        type: SCHEDULE_PCT_CHILD_REMOVED,
        data,
    };
}

export const SCHEDULE_PCT_CHILD_ADDED = 'SCHEDULE_PCT_CHILD_ADDED';
export function onSchedulePctChildAdded(data) {
    return {
        type: SCHEDULE_PCT_CHILD_ADDED,
        data,
    };
}

export const SCHEDULE_SESSIONS_LOADED = 'SCHEDULE_SESSIONS_LOADED';
export function onScheduleSessionsLoaded(data) {
    return {
        type: SCHEDULE_SESSIONS_LOADED,
        data,
    };
}

export const SCHEDULE_PCTS_LOADED = 'SCHEDULE_PCTS_LOADED';
export function onSchedulePctsLoaded(data) {
    return {
        type: SCHEDULE_PCTS_LOADED,
        data,
    };
}

export function removeSessionFromSchedule(scheduleItem) {
    return dispatch => {
        dispatch(requestChildRemove(scheduleItem.sessionId));
        return scheduleItem.ref.remove();
    };
}

export function removePctFromSchedule(scheduleItem) {
    return dispatch => {
        dispatch(requestChildRemove(scheduleItem.pctId));
        return scheduleItem.ref.remove();
    };
}

export function listenToScheduleSessions(userId) {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        const scheduleRef = firebaseDb.ref(`users/${userId}/schedule/sessions`);
        if (!getListenerByRef(getState(), scheduleRef)) {
            scheduleRef.once('value', data => {
                dispatch(onScheduleSessionsLoaded(data));
            });
            scheduleRef.on('child_added', data => {
                dispatch(onScheduleSessionChildAdded(data));
            });
            scheduleRef.on('child_removed', data => {
                dispatch(onScheduleSessionChildRemoved(data));
            });

            dispatch(addListener(scheduleRef));
        }
    };
}

export function listenToSchedulePct(userId) {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        const scheduleRef = firebaseDb.ref(`users/${userId}/schedule/pct`);
        if (!getListenerByRef(getState(), scheduleRef)) {
            scheduleRef.once('value', data => {
                dispatch(onSchedulePctsLoaded(data));
            });
            scheduleRef.on('child_added', data => {
                dispatch(onSchedulePctChildAdded(data));
            });
            scheduleRef.on('child_removed', data => {
                dispatch(onSchedulePctChildRemoved(data));
            });

            dispatch(addListener(scheduleRef));
        }
    };
}

function confirmOverlaps(session, overlaps) {
    return new Promise((resolve, reject) => {
        const $ConfirmModal = $('#ConfirmModal');
        const mountNode = document.getElementById('ScheduleConflictMountPoint');

        function removeListeners() {
            // eslint-disable-next-line no-use-before-define
            $ConfirmModal.off('hide.bs.modal', onModalHide);
            $ConfirmModal.off(
                'click',
                '.schedule-conflict-modal__replace-btn,.schedule-conflict-modal__add-anyway-btn',
                // eslint-disable-next-line no-use-before-define
                onButtonClick
            );
        }

        const onModalHide = function onModalHide(event) {
            removeListeners();
            // eslint-disable-next-line prefer-promise-reject-errors
            reject('canceled confirm dialog');
        };

        const onButtonClick = function onButtonClick(event) {
            event.preventDefault();
            const $target = $(event.target);
            let action = '';
            if ($target.is('.schedule-conflict-modal__replace-btn')) {
                action = 'replace';
            }
            if ($target.is('.schedule-conflict-modal__add-anyway-btn')) {
                action = 'addAnyway';
            }
            ScheduleConflict.destroy(mountNode);
            removeListeners();
            $ConfirmModal.modal('hide');
            resolve({ action, overlaps });
        };

        $ConfirmModal.on(
            'click',
            '.schedule-conflict-modal__replace-btn,.schedule-conflict-modal__add-anyway-btn',
            onButtonClick
        );

        ScheduleConflict.config(
            {
                session,
                overlaps,
            },
            mountNode
        );

        $ConfirmModal.on('hide.bs.modal', onModalHide).modal('show');
    });
}

function gatherConflicts({
    user,
    ref,
    itemId,
    dispatch,
    getById,
    state,
    idKey,
    removeAction,
}) {
    return new Promise((resolve, reject) => {
        ref
            .once('value')
            .then(dataSnapshot => {
                const scheduleSessions = [];
                const scheduleSessionIds = [];
                dataSnapshot.forEach(x => {
                    scheduleSessions.push(x);
                    scheduleSessionIds.push(x.val()[idKey]);
                });
                if (scheduleSessionIds.includes(itemId)) {
                    // eslint-disable-next-line prefer-promise-reject-errors
                    reject('already in schedule');
                    throw new Error('already in schedule');
                } else {
                    return scheduleSessions;
                }
            })
            .then(schedule => {
                const session = getById(state, itemId);
                const overlaps = schedule.reduce((acc, x) => {
                    // http://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
                    // (StartA <= EndB) and (EndA >= StartB)
                    const schedSession = getById(state, x.val()[idKey]);
                    if (typeof schedSession !== 'undefined') {
                        if (
                            session.start < schedSession.end &&
                            session.end > schedSession.start
                        ) {
                            acc.push({
                                ref: x.ref,
                                key: x.key,
                                ...schedSession,
                            });
                        }
                    }
                    return acc;
                }, []);
                if (overlaps.length > 0) {
                    return confirmOverlaps(session, overlaps);
                }
                return { action: 'addAnyway', overlaps };
            })
            .then(({ action, overlaps }) => {
                if (action === 'replace') {
                    overlaps.forEach(x => {
                        dispatch(removeAction(x));
                    });
                }
                resolve(user);
            })
            .catch(err => {
                reject(err);
            });
    });
}

function requestWarningPopup(id, storageKey) {
    return new Promise((resolve, reject) => {
        const $modal = $(id);
        const removeListeners = function removeListeners() {
            // eslint-disable-next-line no-use-before-define
            $modal.off('hide.bs.modal', onModalHide);
            $modal.off(
                'click',
                '.schedule-warning-modal__continue-btn',
                // eslint-disable-next-line no-use-before-define
                onContinueClick
            );
        };
        const onContinueClick = function onContinueClick(event) {
            event.preventDefault();
            removeListeners();
            localStorage.setItem(storageKey, 'true');
            $modal.modal('hide');
            resolve(true);
        };
        const onModalHide = function onModalHide() {
            removeListeners();
            // eslint-disable-next-line prefer-promise-reject-errors
            reject('didnt confirm');
        };
        $modal.on('hide.bs.modal', onModalHide);
        $modal.on(
            'click',
            '.schedule-warning-modal__continue-btn',
            onContinueClick
        );
        $modal.modal('show');
    });
}

export function addSessionToSchedule(userId, sessionId) {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        let auth = Promise.resolve({ uid: userId });
        if (userId === undefined) {
            auth = requestPopupAuth(getState().domAttributes.year);
        }
        let authUser;
        return auth
            .then(user => {
                authUser = user;
                try {
                    const { title } = getState().sessions.byId[sessionId].title;
                    if (title === 'CampusNexus CRM Round Table') {
                        const seenWarning = localStorage.getItem(
                            'seenRoundTableWarning'
                        );
                        if (seenWarning === 'true') {
                            return true;
                        }
                        return requestWarningPopup(
                            '#RoundTableWarningModal',
                            'seenRoundTableWarning'
                        );
                    }
                    return true;
                } catch (e) {
                    return true;
                }
            })
            .then(() =>
                gatherConflicts({
                    user: authUser,
                    ref: firebaseDb.ref(
                        `users/${authUser.uid}/schedule/sessions`
                    ),
                    itemId: sessionId,
                    dispatch,
                    getById: getSessionById,
                    state: getState().sessions,
                    idKey: 'sessionId',
                    removeAction: removeSessionFromSchedule,
                })
            )
            .then(user => {
                dispatch(requestChildAdd(sessionId));
                return firebaseDb
                    .ref(`users/${user.uid}/schedule/sessions`)
                    .push({
                        sessionId,
                    });
            })
            .catch(e => {
                // eslint-disable-next-line no-console
                console.log('popup error:', e);
            });
    };
}

export function addPctToSchedule(userId, itemId) {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        let auth = Promise.resolve({ uid: userId });
        if (userId === undefined) {
            auth = requestPopupAuth(getState().domAttributes.year);
        }
        let authUser;
        return auth
            .then(user => {
                authUser = user;
                try {
                    const seenWarning = localStorage.getItem('seenWarning');
                    if (seenWarning === 'true') {
                        return true;
                    }
                    return requestWarningPopup('#WarningModal', 'seenWarning');
                } catch (e) {
                    return true;
                }
            })
            .then(() =>
                gatherConflicts({
                    user: authUser,
                    ref: firebaseDb.ref(`users/${authUser.uid}/schedule/pct`),
                    itemId,
                    dispatch,
                    getById: getPctById,
                    state: getState().pct,
                    idKey: 'pctId',
                    removeAction: removePctFromSchedule,
                })
            )
            .then(user => {
                dispatch(requestChildAdd(itemId));
                return firebaseDb.ref(`users/${user.uid}/schedule/pct`).push({
                    pctId: itemId,
                });
            })
            .catch(e => {
                // eslint-disable-next-line no-console
                console.log('popup error:', e);
            });
    };
}
