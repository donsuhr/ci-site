/* global it,expect,describe */
import { item, byId, fetching, hasEverLoaded } from './allUsers.reducer';
import * as actions from './allUsers.actions';

describe('firebase allUsers Reducer', () => {
    describe('item', () => {
        it('should return the initial state', () => {
            expect(item(undefined, {})).toEqual({});
        });
        it('should handle ALL_USERS_LOADED', () => {
            const actionItem = { foo: 'bar' };
            const action = {
                type: actions.ALL_USERS_LOADED,
                item: actionItem,
            };
            expect(item({}, action)).toEqual({ ...actionItem });
        });
    });
    describe('byId', () => {
        it('should return the initial state', () => {
            expect(byId(undefined, {})).toEqual({});
        });
        it('should handle ALL_USERS_LOADED', () => {
            const state = {};
            const action = {
                type: actions.ALL_USERS_LOADED,
                data: {
                    ref: 'ref',
                    key: 'key',
                    val: () => ({ first: { prop1: 'val' } }),
                },
            };
            expect(byId(state, action)).toEqual({
                first: {
                    prop1: 'val',
                },
            });
        });
    });
    describe('hasEverLoaded', () => {
        it('should return the initial state', () => {
            expect(hasEverLoaded(undefined, {})).toEqual(false);
        });
        it('should handle ALL_USERS_LOADED', () => {
            expect(
                hasEverLoaded(false, {
                    type: actions.ALL_USERS_LOADED,
                })
            ).toEqual(true);
        });
    });
    describe('fetching', () => {
        it('should return the initial state', () => {
            expect(fetching(undefined, {})).toEqual(false);
        });
        it('should handle ALL_USERS_REQUESTED', () => {
            expect(
                fetching(false, {
                    type: actions.ALL_USERS_REQUESTED,
                })
            ).toEqual(true);
        });
        it('should handle ALL_USERS_LOADED', () => {
            expect(
                fetching(true, {
                    type: actions.ALL_USERS_LOADED,
                })
            ).toEqual(false);
        });
        it('should handle ALL_USERS_ERROR', () => {
            expect(
                fetching(true, {
                    type: actions.ALL_USERS_ERROR,
                })
            ).toEqual(false);
        });
    });
});
