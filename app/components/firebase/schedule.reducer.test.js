/* global it,expect,describe */
import {
    sessions,
    pct,
    fetching,
    childStatus,
    hasEverLoadedSessions,
    hasEverLoadedPcts,
} from './schedule.reducer';
import * as actions from './schedule.actions';
import * as authActions from './auth.actions';

describe('firebase Schedule reducer', () => {
    describe('sessions', () => {
        it('should return the initial state', () => {
            expect(sessions(undefined, {})).toEqual([]);
        });

        it('should handle SCHEDULE_SESSION_CHILD_ADDED', () => {
            const state = {};
            const action = {
                type: actions.SCHEDULE_SESSION_CHILD_ADDED,
                data: {
                    ref: { parent: { key: 'foo' } },
                    key: 'key',
                    val: () => ({ sessionId: 123 }),
                },
            };
            expect(sessions(state, action)).toEqual([
                {
                    ref: { parent: { key: 'foo' } },
                    key: 'key',
                    sessionId: 123,
                },
            ]);
        });
        it('should handle SCHEDULE_SESSION_CHILD_REMOVED', () => {
            const state = [
                {
                    ref: { parent: { key: 'foo' } },
                    key: 'key',
                    sessionId: 123,
                },
                {
                    ref: { parent: { key: 'foo' } },
                    key: 'key2',
                    sessionId: 234,
                },
            ];
            const action = {
                type: actions.SCHEDULE_SESSION_CHILD_REMOVED,
                data: {
                    key: 'key',
                },
            };
            expect(sessions(state, action)).toEqual([
                {
                    ref: { parent: { key: 'foo' } },
                    key: 'key2',
                    sessionId: 234,
                },
            ]);
        });
        it('should handle SIGN_OUT_SUCCESS', () => {
            const state = [
                {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
            ];
            const action = {
                type: authActions.SIGN_OUT_SUCCESS,
            };
            expect(sessions(state, action)).toEqual([]);
        });
        it('should handle PASSIVE_SIGN_OUT', () => {
            const state = [
                {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
            ];
            const action = {
                type: authActions.PASSIVE_SIGN_OUT,
            };
            expect(sessions(state, action)).toEqual([]);
        });
    });
    describe('pct', () => {
        it('should return the initial state', () => {
            expect(pct(undefined, {})).toEqual([]);
        });

        it('should handle SCHEDULE_PCT_CHILD_ADDED', () => {
            const state = {};
            const action = {
                type: actions.SCHEDULE_PCT_CHILD_ADDED,
                data: {
                    ref: { parent: { key: 'foo' } },
                    key: 'key',
                    val: () => ({ pctId: 123 }),
                },
            };
            expect(pct(state, action)).toEqual([
                {
                    ref: { parent: { key: 'foo' } },
                    key: 'key',
                    pctId: 123,
                },
            ]);
        });
        it('should handle SCHEDULE_PCT_CHILD_REMOVED', () => {
            const state = [
                {
                    ref: { parent: { key: 'foo' } },
                    key: 'key',
                    pctId: 123,
                },
                {
                    ref: { parent: { key: 'foo' } },
                    key: 'key2',
                    pctId: 234,
                },
            ];
            const action = {
                type: actions.SCHEDULE_PCT_CHILD_REMOVED,
                data: {
                    key: 'key',
                },
            };
            expect(pct(state, action)).toEqual([
                {
                    ref: { parent: { key: 'foo' } },
                    key: 'key2',
                    pctId: 234,
                },
            ]);
        });
        it('should handle SIGN_OUT_SUCCESS', () => {
            const state = [
                {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
            ];
            const action = {
                type: authActions.SIGN_OUT_SUCCESS,
            };
            expect(pct(state, action)).toEqual([]);
        });
        it('should handle PASSIVE_SIGN_OUT', () => {
            const state = [
                {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
            ];
            const action = {
                type: authActions.PASSIVE_SIGN_OUT,
            };
            expect(pct(state, action)).toEqual([]);
        });
    });
    describe('fetching', () => {
        it('should return the initial state', () => {
            expect(fetching(undefined, {})).toEqual(false);
        });

        it('should handle REQUEST_CHILD_ADD', () => {
            const state = {};
            const action = {
                type: actions.REQUEST_CHILD_ADD,
            };
            expect(fetching(state, action)).toEqual(true);
        });
        it('should handle SCHEDULE_SESSION_CHILD_ADDED', () => {
            const state = {};
            const action = {
                type: actions.SCHEDULE_SESSION_CHILD_ADDED,
            };
            expect(fetching(state, action)).toEqual(false);
        });
        it('should handle SCHEDULE_SESSION_CHILD_REMOVED', () => {
            const state = {};
            const action = {
                type: actions.SCHEDULE_SESSION_CHILD_REMOVED,
            };
            expect(fetching(state, action)).toEqual(false);
        });
    });
    describe('childStatus', () => {
        it('should return the initial state', () => {
            expect(childStatus(undefined, {})).toEqual({});
        });
        it('should handle REQUEST_CHILD_ADD', () => {
            const state = {};
            const action = {
                type: actions.REQUEST_CHILD_ADD,
                id: '123',
            };
            expect(childStatus(state, action)).toEqual({
                123: { state: actions.REQUEST_CHILD_ADD },
            });
        });
        it('should handle REQUEST_CHILD_REMOVE', () => {
            const state = {};
            const action = {
                type: actions.REQUEST_CHILD_REMOVE,
                id: '123',
            };
            expect(childStatus(state, action)).toEqual({
                123: { state: actions.REQUEST_CHILD_REMOVE },
            });
        });
        it('should handle SCHEDULE_SESSION_CHILD_ADDED', () => {
            const state = {
                123: { state: actions.REQUEST_CHILD_REMOVE },
            };
            const action = {
                type: actions.SCHEDULE_SESSION_CHILD_ADDED,
                data: { val: () => ({ sessionId: '123' }) },
            };
            expect(childStatus(state, action)).toEqual({});
        });
        it('should handle SCHEDULE_SESSION_CHILD_REMOVED', () => {
            const state = {
                123: { state: actions.REQUEST_CHILD_REMOVE },
            };
            const action = {
                type: actions.SCHEDULE_SESSION_CHILD_REMOVED,
                data: { val: () => ({ sessionId: '123' }) },
            };
            expect(childStatus(state, action)).toEqual({});
        });
        it('should handle SCHEDULE_PCT_CHILD_ADDED', () => {
            const state = {
                123: { state: actions.SCHEDULE_PCT_CHILD_ADDED },
            };
            const action = {
                type: actions.SCHEDULE_PCT_CHILD_ADDED,
                data: { val: () => ({ pctId: '123' }) },
            };
            expect(childStatus(state, action)).toEqual({});
        });
        it('should handle SCHEDULE_PCT_CHILD_REMOVED', () => {
            const state = {
                123: { state: actions.SCHEDULE_PCT_CHILD_ADDED },
            };
            const action = {
                type: actions.SCHEDULE_PCT_CHILD_REMOVED,
                data: { val: () => ({ pctId: '123' }) },
            };
            expect(childStatus(state, action)).toEqual({});
        });
    });
    describe('hasEverLoadedSessions', () => {
        it('should return the initial state', () => {
            expect(hasEverLoadedSessions(undefined, {})).toEqual(false);
        });

        it('should handle SCHEDULE_SESSIONS_LOADED', () => {
            const state = {};
            const action = {
                type: actions.SCHEDULE_SESSIONS_LOADED,
            };
            expect(hasEverLoadedSessions(state, action)).toEqual(true);
        });
    });
    describe('hasEverLoadedPcts', () => {
        it('should return the initial state', () => {
            expect(hasEverLoadedPcts(undefined, {})).toEqual(false);
        });

        it('should handle SCHEDULE_PCTS_LOADED', () => {
            const state = {};
            const action = {
                type: actions.SCHEDULE_PCTS_LOADED,
            };
            expect(hasEverLoadedPcts(state, action)).toEqual(true);
        });
    });
});
