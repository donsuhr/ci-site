/* global jest,it,expect,describe */

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from './allUsers.actions';
import * as listenerActions from './listeners.actions';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../scripts/firebase');
const { mockDatabase } = require('../../scripts/firebase');

describe('firebase allUsers actions', () => {
    it('should create an action for all users requested', () => {
        const expectedAction = {
            type: actions.ALL_USERS_REQUESTED,
        };
        expect(actions.allUsersRequested()).toEqual(expectedAction);
    });
    it('should create an action for all users loaded', () => {
        const expectedAction = {
            type: actions.ALL_USERS_LOADED,
            data: {},
        };
        expect(actions.onAllUsersLoaded(expectedAction.data)).toEqual(
            expectedAction
        );
    });
    it('should create an action for all users load error', () => {
        const expectedAction = {
            type: actions.ALL_USERS_ERROR,
            error: {},
        };
        expect(actions.onAllUsersError(expectedAction.error)).toEqual(
            expectedAction
        );
    });
    it('should create an action for all users child added', () => {
        const expectedAction = {
            type: actions.ALL_USERS_CHILD_ADDED,
            data: {},
        };
        expect(actions.onAllUsersChildAdded(expectedAction.data)).toEqual(
            expectedAction
        );
    });
    it('should create an action for all users child removed', () => {
        const expectedAction = {
            type: actions.ALL_USERS_CHILD_REMOVED,
            data: {},
        };
        expect(actions.onAllUsersChildRemoved(expectedAction.data)).toEqual(
            expectedAction
        );
    });
    it('should create an action for all users child changed', () => {
        const expectedAction = {
            type: actions.ALL_USERS_CHILD_CHANGED,
            data: {},
        };
        expect(actions.onAllUsersChildChanged(expectedAction.data)).toEqual(
            expectedAction
        );
    });
    describe('firebase actions', () => {
        it('listenToAllUsers', () => {
            const store = mockStore({ domAttributes: { year: '2018' } });
            store.dispatch(actions.listenToAllUsers());
            const ref = mockDatabase.child('users');
            expect(store.getActions()[0].type).toEqual(
                actions.ALL_USERS_REQUESTED
            );
            expect(store.getActions()[1].type).toEqual(
                listenerActions.LISTENER_ADDED
            );
            ref.set({ foo: 'bar' });
            mockDatabase.flush();
            expect(
                store.getActions()[store.getActions().length - 1].type
            ).toEqual(actions.ALL_USERS_LOADED);
            const child = ref.push({ foo: 'bar' });
            /**
             * value is using `on` not `once` and is called every flush
             * using  length - 2 to get add/remove/change
             */
            mockDatabase.flush();
            expect(
                store.getActions()[store.getActions().length - 2].type
            ).toEqual(actions.ALL_USERS_CHILD_ADDED);
            child.set({ foo: 'bar1' });
            mockDatabase.flush();
            expect(
                store.getActions()[store.getActions().length - 2].type
            ).toEqual(actions.ALL_USERS_CHILD_CHANGED);
            child.remove();
            mockDatabase.flush();
            expect(
                store.getActions()[store.getActions().length - 2].type
            ).toEqual(actions.ALL_USERS_CHILD_REMOVED);
        });
    });
});
