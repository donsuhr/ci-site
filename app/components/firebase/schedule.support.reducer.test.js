/* global it,expect,describe */
import {
    hasEverLoaded,
    hasEverLoadedUsedAvailability,
    fetching,
    userItems,
    usedAvailabilityId,
} from './schedule.support.reducer';
import * as actions from './schedule.support.actions';
import * as authActions from './auth.actions';

describe('firebase Schedule Support reducer', () => {
    describe('hasEverLoaded', () => {
        it('should return the initial state', () => {
            expect(hasEverLoaded(undefined, {})).toEqual(false);
        });

        it('should handle SCHEDULE_SUPPORT_LOADED', () => {
            const state = {};
            const action = {
                type: actions.SCHEDULE_SUPPORT_LOADED,
            };
            expect(hasEverLoaded(state, action)).toEqual(true);
        });
    });

    describe('fetching', () => {
        it('should return the initial state', () => {
            expect(fetching(undefined, {})).toEqual(false);
        });

        it('should handle SCHEDULE_SUPPORT_REQUESTED', () => {
            const state = {};
            const action = {
                type: actions.SCHEDULE_SUPPORT_REQUESTED,
            };
            expect(fetching(state, action)).toEqual(true);
        });
        it('should handle SCHEDULE_SUPPORT_REQUESTED_SAVE', () => {
            const state = {};
            const action = {
                type: actions.SCHEDULE_SUPPORT_REQUESTED_SAVE,
            };
            expect(fetching(state, action)).toEqual(true);
        });
        it('should handle SCHEDULE_SUPPORT_LOADED', () => {
            const state = {};
            const action = {
                type: actions.SCHEDULE_SUPPORT_LOADED,
            };
            expect(fetching(state, action)).toEqual(false);
        });
        it('should handle SCHEDULE_SUPPORT_ITEM_SAVED', () => {
            const state = {};
            const action = {
                type: actions.SCHEDULE_SUPPORT_ITEM_SAVED,
            };
            expect(fetching(state, action)).toEqual(false);
        });
    });
    describe('hasEverLoadedUsedAvailability', () => {
        it('should return the initial state', () => {
            expect(hasEverLoadedUsedAvailability(undefined, {})).toEqual(false);
        });

        it('should handle SCHEDULE_USED_AVAILABILITY_LOADED', () => {
            const state = {};
            const action = {
                type: actions.SCHEDULE_USED_AVAILABILITY_LOADED,
            };
            expect(hasEverLoadedUsedAvailability(state, action)).toEqual(true);
        });
    });

    describe('usedAvailabilityId', () => {
        it('should return the initial state', () => {
            expect(usedAvailabilityId(undefined, {})).toEqual({});
        });

        it('should handle SCHEDULE_USED_AVAILABILITY_CHANGED', () => {
            const state = {};
            const action = {
                type: actions.SCHEDULE_USED_AVAILABILITY_CHANGED,
                data: {
                    val: () => ({
                        user1: {
                            avail1: 'scheduleId1',
                            avail2: 'scheduleId2',
                        },
                        user2: { avail3: 'scheduleId3' },
                    }),
                },
            };
            expect(usedAvailabilityId(state, action)).toEqual({
                avail1: { userId: 'user1', scheduleId: 'scheduleId1' },
                avail2: { userId: 'user1', scheduleId: 'scheduleId2' },
                avail3: { userId: 'user2', scheduleId: 'scheduleId3' },
            });
        });
    });

    describe('userItems', () => {
        it('should return the initial state', () => {
            expect(userItems(undefined, {})).toEqual([]);
        });

        it('should handle SCHEDULE_SUPPORT_CHILD_ADDED', () => {
            const state = [];
            const action = {
                type: actions.SCHEDULE_SUPPORT_CHILD_ADDED,
                data: {
                    ref: 'ref',
                    key: 'key',
                    val: () => ({ first: 'name' }),
                },
            };
            expect(userItems(state, action)).toEqual([
                {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
            ]);
        });
        it('should handle SCHEDULE_SUPPORT_CHILD_REMOVED', () => {
            const state = [
                {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
                {
                    ref: 'ref',
                    key: 'key2',
                    val: { first: 'name2' },
                },
            ];
            const action = {
                type: actions.SCHEDULE_SUPPORT_CHILD_REMOVED,
                data: {
                    key: 'key',
                },
            };
            expect(userItems(state, action)).toEqual([
                {
                    ref: 'ref',
                    key: 'key2',
                    val: { first: 'name2' },
                },
            ]);
        });
        it('should handle SIGN_OUT_SUCCESS', () => {
            const state = [
                {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
            ];
            const action = {
                type: authActions.SIGN_OUT_SUCCESS,
            };
            expect(userItems(state, action)).toEqual([]);
        });
        it('should handle PASSIVE_SIGN_OUT', () => {
            const state = [
                {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
            ];
            const action = {
                type: authActions.PASSIVE_SIGN_OUT,
            };
            expect(userItems(state, action)).toEqual([]);
        });
    });
});
