/* global jest, it,expect,describe */
import listeners, { getListenerByRef } from './listeners.reducer';
import * as actions from './listeners.actions';
import * as authActions from './auth.actions';

describe('firebase listeners Reducer', () => {
    describe('listeners', () => {
        it('should return the initial state', () => {
            expect(listeners(undefined, {})).toEqual([]);
        });
        it('should handle LISTENER_ADDED', () => {
            const ref = {};
            expect(
                listeners([], {
                    type: actions.LISTENER_ADDED,
                    ref,
                })
            ).toEqual([ref]);
        });
        it('should handle SIGN_OUT_SUCCESS', () => {
            const off = jest.fn();
            const ref = { off };
            expect(
                listeners([ref], {
                    type: authActions.SIGN_OUT_SUCCESS,
                })
            ).toEqual([]);
            expect(off).toHaveBeenCalled();
        });
        it('should handle PASSIVE_SIGN_OUT', () => {
            const off = jest.fn();
            const ref = { off };
            expect(
                listeners([ref], {
                    type: authActions.PASSIVE_SIGN_OUT,
                })
            ).toEqual([]);
            expect(off).toHaveBeenCalled();
        });
    });
    describe('getListenerByRef', () => {
        it('exits on null state', () => {
            expect(getListenerByRef({})).toBeNull();
        });

        it('returns the ref', () => {
            const ref = {
                toString() {
                    return 'users';
                },
            };
            expect(getListenerByRef({ listeners: [ref] }, 'users')).toBe(ref);
        });
    });
});
