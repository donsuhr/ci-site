/* global it,expect,describe */

import * as actions from './listeners.actions';

describe('firebase listeners actions', () => {
    it('should create an action for listener added', () => {
        const expectedAction = {
            type: actions.LISTENER_ADDED,
            ref: {},
        };
        expect(actions.addListener(expectedAction.ref)).toEqual(expectedAction);
    });
});
