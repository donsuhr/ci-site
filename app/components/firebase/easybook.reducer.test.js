/* global it,expect,describe */
import { byAvailabilityId, hasEverLoaded, fetching } from './easybook.reducer';
import * as actions from './easybook.actions';

describe('firebase easybook Reducer', () => {
    describe('byAvailabilityId', () => {
        it('should return the initial state', () => {
            expect(byAvailabilityId(undefined, {})).toEqual({});
        });
        it('should handle EASYBOOK_CHILD_ADDED', () => {
            const action = {
                type: actions.EASYBOOK_CHILD_ADDED,
                data: {
                    ref: 'ref',
                    key: 'key',
                    val: () => ({ first: 'name' }),
                },
            };
            expect(byAvailabilityId({}, action)).toEqual({
                key: {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
            });
        });
        it('should handle EASYBOOK_CHILD_REMOVED', () => {
            const state = {
                key: {
                    ref: 'ref',
                    key: 'key',
                    val: { first: 'name' },
                },
            };
            const action = {
                type: actions.EASYBOOK_CHILD_REMOVED,
                data: {
                    key: 'key',
                },
            };
            expect(byAvailabilityId(state, action)).toEqual({});
        });
    });
    describe('hasEverLoaded', () => {
        it('should return the initial state', () => {
            expect(hasEverLoaded(undefined, {})).toEqual(false);
        });
        it('should handle EASYBOOK_LOADED', () => {
            expect(
                hasEverLoaded(false, {
                    type: actions.EASYBOOK_LOADED,
                })
            ).toEqual(true);
        });
    });
    describe('fetching', () => {
        it('should return the initial state', () => {
            expect(fetching(undefined, {})).toEqual(false);
        });
        it('should handle EASYBOOK_REQUESTED', () => {
            expect(
                fetching(false, {
                    type: actions.EASYBOOK_REQUESTED,
                })
            ).toEqual(true);
        });
        it('should handle EASYBOOK_LOADED', () => {
            expect(
                fetching(true, {
                    type: actions.EASYBOOK_LOADED,
                })
            ).toEqual(false);
        });
    });
});
