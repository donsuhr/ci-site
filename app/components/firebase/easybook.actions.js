import createFirebaseApp from '../../scripts/firebase';
import { addListener } from './listeners.actions';
import { getListenerByRef } from './listeners.reducer';

export const SCHEDULE_SUPPORT_REQUESTED_EASY_BOOK_SAVE =
    'SCHEDULE_SUPPORT_REQUESTED_EASY_BOOK_SAVE';
export function onScheduleSupportRequestedEasyBookSave() {
    return {
        type: SCHEDULE_SUPPORT_REQUESTED_EASY_BOOK_SAVE,
    };
}

export const SCHEDULE_SUPPORT_ITEM_EASY_BOOK_SAVED =
    'SCHEDULE_SUPPORT_ITEM_EASY_BOOK_SAVED';
export function onScheduleSupportEasyBookItemSaved(refPromise) {
    return {
        type: SCHEDULE_SUPPORT_ITEM_EASY_BOOK_SAVED,
        refPromise,
    };
}

export const EASYBOOK_REQUESTED = 'EASYBOOK_REQUESTED';
export function onEasyBookRequested() {
    return {
        type: EASYBOOK_REQUESTED,
    };
}

export const EASYBOOK_LOADED = 'EASYBOOK_LOADED';
export function onEasyBookLoaded(data) {
    return {
        type: EASYBOOK_LOADED,
        data,
    };
}

export const EASYBOOK_CHILD_REMOVED = 'EASYBOOK_CHILD_REMOVED';
export function onEasyBookChildRemoved(data) {
    return {
        type: EASYBOOK_CHILD_REMOVED,
        data,
    };
}

export const EASYBOOK_CHILD_ADDED = 'EASYBOOK_CHILD_ADDED';
export function onEasyBookChildAdded(data) {
    return {
        type: EASYBOOK_CHILD_ADDED,
        data,
    };
}

function easyBook(options) {
    const {
        availabilityId, firstName, lastName, institution,
    } = options;
    return (dispatch, getState) => {
        dispatch(onScheduleSupportRequestedEasyBookSave());
        const val = {
            info: {
                firstName,
                lastName,
                institution,
            },
        };
        if (firstName === 'closed' && lastName === 'closed') {
            val.info = 'closed';
        }
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        const refPromise = firebaseDb
            .ref(`easybook/${availabilityId}`)
            .set(val)
            .catch(error => {
                // eslint-disable-next-line no-console
                console.log('easy book error', error);
            });
        firebaseDb
            .ref(`usedAvail/easybook/${availabilityId}`)
            .set(true)
            .catch(error => {
                // eslint-disable-next-line no-console
                console.log('easy book error 2s', error);
            });
        dispatch(onScheduleSupportEasyBookItemSaved(refPromise));
        return refPromise;
    };
}

function listenToEasyBook() {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        const ref = firebaseDb.ref('easybook');
        if (!getListenerByRef(getState(), ref)) {
            dispatch(onEasyBookRequested());
            ref.once('value', data => {
                dispatch(onEasyBookLoaded(data));
            });
            ref.on('child_added', data => {
                dispatch(onEasyBookChildAdded(data));
            });
            ref.on('child_removed', data => {
                dispatch(onEasyBookChildRemoved(data));
            });

            dispatch(addListener(ref));
        }
    };
}

function removeEasyBook(scheduleItem) {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        const availabilityId = scheduleItem.key;
        firebaseDb.ref(`usedAvail/easybook/${availabilityId}`).set(null);
        return scheduleItem.ref.remove();
    };
}

function removeRegularBook({ regularBookId, userId, availabilityId }) {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        firebaseDb.ref(`usedAvail/${userId}/${availabilityId}`).set(null);
        firebaseDb
            .ref(`users/${userId}/schedule/supportCouncil/${regularBookId}`)
            .set(null);
    };
}

export { easyBook, listenToEasyBook, removeEasyBook, removeRegularBook };
