import config from '../../../config';
import createFirebaseApp from '../../scripts/firebase';
import { addListener } from './listeners.actions';
import { getListenerByRef } from './listeners.reducer';

export const SCHEDULE_SUPPORT_LOADED = 'SCHEDULE_SUPPORT_LOADED';
export function onScheduleSupportLoaded(data) {
    return {
        type: SCHEDULE_SUPPORT_LOADED,
        data,
    };
}

export const SCHEDULE_SUPPORT_REQUESTED = 'SCHEDULE_SUPPORT_REQUESTED';
export function onScheduleSupportRequested() {
    return {
        type: SCHEDULE_SUPPORT_REQUESTED,
    };
}

export const SCHEDULE_SUPPORT_REQUESTED_SAVE =
    'SCHEDULE_SUPPORT_REQUESTED_SAVE';
export function onScheduleSupportRequestedSave() {
    return {
        type: SCHEDULE_SUPPORT_REQUESTED_SAVE,
    };
}

export const SCHEDULE_SUPPORT_ITEM_SAVED = 'SCHEDULE_SUPPORT_ITEM_SAVED';
function onScheduleSupportItemSaved(refPromise) {
    return {
        type: SCHEDULE_SUPPORT_ITEM_SAVED,
        refPromise,
    };
}

export const SCHEDULE_SUPPORT_CHILD_REMOVED = 'SCHEDULE_SUPPORT_CHILD_REMOVED';
export function onScheduleSupportChildRemoved(data) {
    return {
        type: SCHEDULE_SUPPORT_CHILD_REMOVED,
        data,
    };
}

export const SCHEDULE_SUPPORT_CHILD_ADDED = 'SCHEDULE_SUPPORT_CHILD_ADDED';
export function onScheduleSupportChildAdded(data) {
    return {
        type: SCHEDULE_SUPPORT_CHILD_ADDED,
        data,
    };
}

export const SCHEDULE_USED_AVAILABILITY_LOADED =
    'SCHEDULE_USED_AVAILABILITY_LOADED';
export function onScheduleUsedAvailabilityLoaded(data) {
    return {
        type: SCHEDULE_USED_AVAILABILITY_LOADED,
        data,
    };
}

export const SCHEDULE_USED_AVAILABILITY_CHANGED =
    'SCHEDULE_USED_AVAILABILITY_CHANGED';
export function onScheduleUsedAvailabilityChanged(data) {
    return {
        type: SCHEDULE_USED_AVAILABILITY_CHANGED,
        data,
    };
}

export const SCHEDULE_USED_AVAILABILITY_REQUESTED =
    'SCHEDULE_USED_AVAILABILITY_REQUESTED';
export function onScheduleUsedAvailabilityRequested() {
    return {
        type: SCHEDULE_USED_AVAILABILITY_REQUESTED,
    };
}

export function removeSupportFromSchedule(scheduleItem) {
    return (dispatch, getState) => {
        const userId = getState().user.info.uid;
        const { availabilityId } = scheduleItem.val;
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        firebaseDb.ref(`usedAvail/${userId}/${availabilityId}`).set(null);
        return scheduleItem.ref.remove();
    };
}

export function postInfoToWebhook({
    analyst, profile, start, end,
}) {
    const endpoint = `${config.domains
        .api}/campusInsight/ci/web-hook--schedule-support-item/?site=ci&group=en-us`;
    const data = {
        analystFirstName: analyst.firstName,
        analystLastName: analyst.lastName,
        analystTopic: analyst.topic,
        userFirstName: profile.info.firstName,
        userLastName: profile.info.lastName,
        userInstitution: profile.info.institution,
        userEmail: profile.info.email,
        userPhone: profile.info.phone,
        start,
        end,
    };
    fetch(endpoint, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    });
}

export function addSupportCouncilToSchedule({
    availabilityId,
    analyst,
    start,
    end,
}) {
    return (dispatch, getState) => {
        dispatch(onScheduleSupportRequestedSave());
        const analystId = getState().ui.chosenAnalystId;
        const userId = getState().user.info.uid;
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        const refPromise = firebaseDb
            .ref(`users/${userId}/schedule/supportCouncil`)
            .push({
                analystId,
                availabilityId,
                // add start and end to save from having to load analysts
                // when displaying the schedule to the user
                start,
                end,
            });
        firebaseDb
            .ref(`usedAvail/${userId}/${availabilityId}`)
            .set(refPromise.key);
        const { profile } = getState();
        postInfoToWebhook({
            analyst,
            profile,
            start,
            end,
        });
        dispatch(onScheduleSupportItemSaved(refPromise));
        return refPromise;
    };
}

export function listenToScheduleSupport(userId) {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        const scheduleRef = firebaseDb.ref(
            `users/${userId}/schedule/supportCouncil`
        );
        if (!getListenerByRef(getState(), scheduleRef)) {
            dispatch(onScheduleSupportRequested());
            scheduleRef.once('value', data => {
                dispatch(onScheduleSupportLoaded(data));
            });
            scheduleRef.on('child_added', data => {
                dispatch(onScheduleSupportChildAdded(data));
            });
            scheduleRef.on('child_removed', data => {
                dispatch(onScheduleSupportChildRemoved(data));
            });

            dispatch(addListener(scheduleRef));
        }
    };
}

export function listenToUsedAvailability() {
    return (dispatch, getState) => {
        const { year } = getState().domAttributes;
        const firebaseDb = createFirebaseApp(year).db;
        const scheduleRef = firebaseDb.ref('usedAvail');
        if (!getListenerByRef(getState(), scheduleRef)) {
            dispatch(onScheduleUsedAvailabilityRequested());
            scheduleRef.once('value', data => {
                dispatch(onScheduleUsedAvailabilityLoaded(data));
            });
            scheduleRef.on('child_changed', data => {
                scheduleRef.once('value', allData => {
                    dispatch(onScheduleUsedAvailabilityChanged(allData));
                });
            });
            scheduleRef.on('child_added', data => {
                scheduleRef.once('value', allData => {
                    dispatch(onScheduleUsedAvailabilityChanged(allData));
                });
            });
            scheduleRef.on('child_removed', data => {
                scheduleRef.once('value', allData => {
                    dispatch(onScheduleUsedAvailabilityChanged(allData));
                });
            });
            dispatch(addListener(scheduleRef));
        }
    };
}
