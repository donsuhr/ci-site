/* global jest,it,expect,describe */

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from './userProfiles.actions';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../scripts/firebase');
const { mockDatabase } = require('../../scripts/firebase');

describe('firebase UserProfile actions', () => {
    it('should create an action to recieve profile', () => {
        const expectedAction = {
            type: actions.USER_PROFILE_RECEIVED,
            data: {},
        };
        expect(actions.onReceiveProfile(expectedAction.data)).toEqual(
            expectedAction
        );
    });

    it('should create an action to request user profile', () => {
        const expectedAction = {
            type: actions.REQUEST_USER_PROFILE,
            userId: 123,
        };
        expect(actions.onRequestUserProfile(expectedAction.userId)).toEqual(
            expectedAction
        );
    });

    describe('async actions', () => {
        it('should create REQUEST_EVALUATIONS action to fetch sessions', () => {
            const returnData = {};

            const expectedActions = [
                { type: actions.REQUEST_USER_PROFILE, userId: 123 },
                { type: actions.USER_PROFILE_RECEIVED },
            ];
            mockDatabase.child('users/123/profile').set(returnData);

            const store = mockStore({ userProfiles: { byId: {} }, domAttributes: { year: '2018' } });
            store.dispatch(actions.fetchUserProfile(123));

            mockDatabase.flush();
            expect(store.getActions()[0]).toEqual(expectedActions[0]);
            expect(store.getActions()[1].type).toEqual(expectedActions[1].type);
        });
    });
});
