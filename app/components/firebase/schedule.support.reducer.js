import { combineReducers } from 'redux';
import sortBy from 'lodash/sortBy';
import isDate from 'lodash/isDate';
import moment from '../util/load-moment-tz';
import * as actions from './schedule.support.actions';
import * as authActions from './auth.actions';

export const userItems = (state = [], action) => {
    switch (action.type) {
        case actions.SCHEDULE_SUPPORT_CHILD_ADDED:
            return [
                ...state,
                {
                    ref: action.data.ref,
                    key: action.data.key,
                    val: action.data.val(),
                },
            ];
        case actions.SCHEDULE_SUPPORT_CHILD_REMOVED:
            const index = state.findIndex(x => x.key === action.data.key);
            if (index >= 0) {
                return [
                    ...state.slice(0, index),
                    ...state.slice(index + 1),
                ];
            }
            return state;
        case authActions.SIGN_OUT_SUCCESS:
        case authActions.PASSIVE_SIGN_OUT:
            return [];
        default:
            return state;
    }
};

export const usedAvailabilityId = (state = {}, action) => {
    switch (action.type) {
        case actions.SCHEDULE_USED_AVAILABILITY_CHANGED:
            const val = action.data.val();
            return Object.keys(val).reduce((acc, userId) => {
                const availabilityIds = val[userId];
                Object.keys(availabilityIds).forEach((id) => {
                    acc[id] = {
                        userId,
                        scheduleId: availabilityIds[id],
                    };
                });
                return acc;
            }, {});
        default:
            return state;
    }
};

export const hasEverLoadedUsedAvailability = (state = false, action) => {
    switch (action.type) {
        case actions.SCHEDULE_USED_AVAILABILITY_LOADED:
            return true;
        default:
            return state;
    }
};

export const hasEverLoaded = (state = false, action) => {
    switch (action.type) {
        case actions.SCHEDULE_SUPPORT_LOADED:
            return true;
        default:
            return state;
    }
};

export const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.SCHEDULE_SUPPORT_REQUESTED:
        case actions.SCHEDULE_SUPPORT_REQUESTED_SAVE:
            return true;
        case actions.SCHEDULE_SUPPORT_LOADED:
        case actions.SCHEDULE_SUPPORT_ITEM_SAVED:
            return false;
        default:
            return state;
    }
};

const getAppointmentsByDay = (state, analystState) => {
    if (!state || !state.userItems) {
        return {};
    }
    if (!analystState || analystState.loadStatus !== 'success') {
        return {};
    }
    const items = state.userItems.reduce((acc, x) => {
        const item = x.val;
        item.type = 'support';
        const analyst = analystState.byId[item.analystId];
        item.title = `${analyst.firstName} ${analyst.lastName} / ${analyst.topic} \u2014 Support Consultation`;
        item.location = { title: 'TBD' };
        const retKey = moment(item.start).tz('America/New_York').format('YYYYMMDD');
        if (!{}.hasOwnProperty.call(acc, retKey)) {
            acc[retKey] = [];
        }
        acc[retKey].push(item);
        return acc;
    }, {});

    Object.keys(items).forEach((key) => {
        items[key] = sortBy(items[key], (x) => {
            if (!isDate(x.start)) {
                return (new Date(x.start));
            }
            return x.start;
        });
    });

    return items;
};

export function getScheduleItemById(state, analystId, availabilityId) {
    if (!state || !state.userItems) {
        return null;
    }
    const filtered = state.userItems.filter(x =>
        x.val.analystId === analystId && x.val.availabilityId === availabilityId
    );
    if (filtered.length < 1) {
        return null;
    }
    return filtered[0];
}

export function getScheduleItemIsAvailable(state, availabilityId) {
    return !Object.keys(state.usedAvailabilityId).includes(availabilityId);
}

const supportConsultations = combineReducers({
    hasEverLoaded,
    hasEverLoadedUsedAvailability,
    fetching,
    userItems,
    usedAvailabilityId,
});

function getRegularBookedByAvailabilityId(state, availabilityId) {
    const item = state.usedAvailabilityId[availabilityId];
    if (item === true) {
        return null;
    }
    return item;
}

export {
    supportConsultations as default,
    getAppointmentsByDay,
    getRegularBookedByAvailabilityId,
};
