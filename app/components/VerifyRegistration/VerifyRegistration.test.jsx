/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import VerifyRegistration from './VerifyRegistration';

describe('VerifyRegistration', () => {
    it('renders', () => {
        const user = {
            isAuth: true,
        };
        const profile = {
            fetching: false,
            info: {
                firstName: 'firstName',
            },
        };
        const wrapper = shallow(
            <VerifyRegistration profile={profile} user={user} />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const user = {
            isAuth: false,
        };
        const profile = {
            fetching: true,
        };
        const wrapper = shallow(
            <VerifyRegistration profile={profile} user={user} />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
    });

    it('renders load error', () => {
        const user = {
            isAuth: true,
        };
        const profile = {
            fetching: false,
            loadStatus: {
                message: 'load error',
            },
        };
        const wrapper = shallow(
            <VerifyRegistration profile={profile} user={user} />
        );
        expect(wrapper.find('.show-error').text()).toContain(
            'There was an error loading the profile'
        );
    });

    it('renders data', () => {
        const user = {
            isAuth: true,
        };
        const profile = {
            fetching: false,
            info: {
                firstName: 'firstName',
            },
        };
        const wrapper = shallow(
            <VerifyRegistration profile={profile} user={user} />
        );
        expect(wrapper.find('ReduxForm')).toHaveLength(1);
    });
});
