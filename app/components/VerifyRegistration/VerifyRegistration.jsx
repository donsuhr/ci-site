/* eslint-disable react/forbid-prop-types */

import React from 'react';
import PropTypes from 'prop-types';
import ProfileForm from '../ProfileForm';
import Loading from '../Loading';

const VerifyRegistration = ({
    user,
    profile,
    handleSubmit,
    handleSubmitSuccess,
}) => {
    const showLoadError =
        profile.loadStatus &&
        profile.loadStatus !== 'success' &&
        profile.loadStatus !== '';
    const fetching = !user.isAuth || profile.fetching;

    if (fetching) {
        return <Loading>Loading...</Loading>;
    }
    return (
        <div>
            <h2 className="cmc-article__intro-p">Contact Information</h2>
            <p>To begin, please verify or enter your contact information.</p>
            <ProfileForm
                initialValues={profile.info}
                onSubmitSuccess={handleSubmitSuccess}
                onSubmit={handleSubmit}
            />
            {showLoadError && (
                <div className="show-error">
                    <p>
                        There was an error loading the profile. <br />
                        Error: {profile.loadStatus.message}
                    </p>
                </div>
            )}
        </div>
    );
};

VerifyRegistration.propTypes = {
    user: PropTypes.object,
    profile: PropTypes.object,
    handleSubmit: PropTypes.func,
    handleSubmitSuccess: PropTypes.func,
};

export default VerifyRegistration;
