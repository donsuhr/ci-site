/* eslint-disable react/forbid-prop-types */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { reset } from 'redux-form';
import queryString from 'query-string';
import VerifyRegistration from './VerifyRegistration';
import { confirmProfile } from '../SupportConsultations/actions';
import { listenToAuth } from '../firebase/auth.actions';
import { updateProfile, listenToProfile } from '../firebase/profile.actions';
import { listenToScheduleSupport } from '../firebase/schedule.support.actions';

export class VerifyRegistrationContainer extends React.Component {
    static propTypes = {
        user: PropTypes.object,
        profile: PropTypes.object,
        supportConsultations: PropTypes.object,
        listenToAuth: PropTypes.func,
        listenToProfile: PropTypes.func,
        listenToScheduleSupport: PropTypes.func,
        updateProfile: PropTypes.func,
        confirmProfile: PropTypes.func,
        reset: PropTypes.func,
        queryAllow: PropTypes.string,
        history: PropTypes.object,
    };

    componentDidMount() {
        this.props.listenToAuth().then(user => {
            this.props.listenToScheduleSupport(user.uid);
            if (
                !this.props.profile.fetching &&
                this.props.profile.loadStatus !== 'success'
            ) {
                this.props.listenToProfile(user);
            }
        });
        setTimeout(() => {
            if (!this.props.user.isAuth) {
                this.props.history.replace('/');
            }
        }, 1000);
    }

    handleSubmit = data => this.props.updateProfile(this.props.user.info.uid, data);

    handleSubmitSuccess = () => {
        this.props.reset('RegistrationForm');
        this.props.confirmProfile();
        this.props.history.push('/choose-analyst');
    };

    render() {
        if (
            this.props.supportConsultations.userItems.length > 0 &&
            this.props.queryAllow !== '1233'
        ) {
            return (
                <div>
                    <p>
                        You may only sign-up for one appointment at this time.
                        To sign up for more, please visit theSupport
                        Consultation Desk.
                    </p>
                    <Link
                        className="cmc-article__link cmc-article__link--reversed"
                        to="/"
                    >
                        Back
                    </Link>
                </div>
            );
        }
        return (
            <VerifyRegistration
                user={this.props.user}
                profile={this.props.profile}
                handleSubmit={this.handleSubmit}
                handleSubmitSuccess={this.handleSubmitSuccess}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    const parsed = queryString.parse(window.location.search);
    return {
        profile: state.profile,
        user: state.user,
        supportConsultations: state.supportConsultations,
        queryAllow: parsed.allow,
        history: ownProps.history,
    };
}

const VerifyRegistrationConnected = connect(mapStateToProps, {
    listenToAuth,
    listenToProfile,
    updateProfile,
    confirmProfile,
    listenToScheduleSupport,
    reset,
})(VerifyRegistrationContainer);

export default VerifyRegistrationConnected;
