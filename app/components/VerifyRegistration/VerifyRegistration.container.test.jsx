/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { VerifyRegistrationContainer } from './VerifyRegistration.container';

describe('VerifyRegistration.container', () => {
    it('renders', () => {
        const supportConsultations = {
            userItems: {
                length: 2,
            },
        };
        const wrapper = shallow(
            <VerifyRegistrationContainer
                supportConsultations={supportConsultations}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders error', () => {
        const supportConsultations = {
            userItems: {
                length: 2,
            },
        };
        const wrapper = shallow(
            <VerifyRegistrationContainer
                supportConsultations={supportConsultations}
            />
        );
        expect(wrapper.find('VerifyRegistration')).toHaveLength(0);
    });

    it('renders data', () => {
        const supportConsultations = {
            userItems: {
                length: 0,
            },
        };
        const wrapper = shallow(
            <VerifyRegistrationContainer
                supportConsultations={supportConsultations}
            />
        );
        expect(wrapper.find('VerifyRegistration')).toHaveLength(1);
    });
});
