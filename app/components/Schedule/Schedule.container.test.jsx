/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { ScheduleContainer } from './Schedule.container';

const defaultProps = {
    sessions: {
        hasEverLoaded: true,
        fetching: false,
        loadStatus: 'success',
    },
    pct: {
        hasEverLoaded: true,
        fetching: false,
        loadStatus: 'success',
    },
    schedule: {
        hasEverLoaded: true,
        fetching: false,
        hasEverLoadedSessions: true,
        hasEverLoadedPcts: true,
    },
    analysts: {
        hasEverLoaded: true,
        fetching: false,
    },
    supportConsultations: {
        hasEverLoadedUsedAvailability: true,
        hasEverLoaded: true,
    },
    user: {
        initting: false,
        isAuth: true,
    },
};

function createEl(alterations) {
    /* eslint-disable react/prop-types */
    const props = {
        ...defaultProps,
        ...alterations,
    };
    return (
        <ScheduleContainer
            sessions={props.sessions}
            analysts={props.analysts}
            schedule={props.schedule}
            pct={props.pct}
            supportConsultations={props.supportConsultations}
            user={props.user}
            fetchSessions={jest.fn()}
            listenToAuth={jest.fn()}
            listenToUserRoot={jest.fn()}
            fetchPcts={jest.fn()}
            listenToScheduleSessions={jest.fn()}
            getSessionItemById={jest.fn()}
            getPctItemById={jest.fn()}
            removeSessionFromSchedule={jest.fn()}
            removeSupportFromSchedule={jest.fn()}
            removePctFromSchedule={jest.fn()}
            listenToSchedulePct={jest.fn()}
        />
    );
    /* eslint-enable react/prop-types */
}

describe('Schedule Schedule.container', () => {
    it('renders Authenticating', () => {
        const wrapper = shallow(createEl({ user: { initting: true } }));
        expect(wrapper.find('Loading').dive().text()).toContain(
            'Authenticating'
        );
    });

    it('renders sign in', () => {
        const wrapper = shallow(
            createEl({ user: { initting: false, isAuth: false } })
        );
        expect(wrapper.find('button').text()).toContain('Sign In');
    });

    it('renders fetching schedule', () => {
        const wrapper = shallow(
            createEl({ schedule: { hasEverLoadedPcts: false } })
        );
        expect(wrapper.find('Loading').dive().text()).toContain('Schedule');
    });
    it('renders Loading 0 Percent', () => {
        const wrapper = shallow(
            createEl({
                sessions: { fetching: true },
                analysts: { fetching: true },
                pct: { fetching: true },
            })
        );
        expect(wrapper.find('Loading').dive().text()).toContain('Loading 0%');
    });
    it('renders Loading 33 Percent', () => {
        const wrapper = shallow(
            createEl({ analysts: { fetching: true }, pct: { fetching: true } })
        );
        expect(wrapper.find('Loading').dive().text()).toContain('Loading 33%');
    });
    it('renders Loading 66 Percent', () => {
        const wrapper = shallow(createEl({ pct: { fetching: true } }));
        expect(wrapper.find('Loading').dive().text()).toContain('Loading 66%');
    });

    it('renders Load Errors', () => {
        const sessions = {
            ...defaultProps.sessions,
            loadStatus: new Error('Foo Error'),
        };
        const wrapper = shallow(createEl({ sessions }));
        expect(wrapper.find('ShowError').dive().text()).toContain('Foo Error');
    });
    it('renders ', () => {
        const wrapper = shallow(createEl());
        expect(wrapper.find('Schedule')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
