/* eslint-disable react/forbid-prop-types */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Basil from 'basil.js';
import queryString from 'query-string';
import Loading from '../Loading';
import ShowError from '../ShowError';
import Schedule from './Schedule';
import { fetchSessions } from '../SessionBrowser/actions';
import { fetchPcts } from '../PctBrowser/actions';
import { fetchAnalysts } from '../ChooseAnalyst/actions';
import {
    listenToAuth,
    requestPopupAuth,
    listenToUserRoot,
} from '../firebase/auth.actions';
import { PropType as userPropType } from '../firebase/auth.reducer';

import {
    listenToScheduleSessions,
    listenToSchedulePct,
    removeSessionFromSchedule,
    removePctFromSchedule,
} from '../firebase/schedule.actions';
import {
    getScheduledSessions,
    getSessionItemById,
    getPctItemById,
} from '../firebase/schedule.reducer';
import {
    listenToScheduleSupport,
    removeSupportFromSchedule,
} from '../firebase/schedule.support.actions';
import { getScheduleItemById } from '../firebase/schedule.support.reducer';

const basil = new Basil();
const parsed = queryString.parse(window.location.search);
const selectSessionId = parsed.s;

export class ScheduleContainer extends React.Component {
    static propTypes = {
        fetchSessions: PropTypes.func.isRequired,
        listenToAuth: PropTypes.func.isRequired,
        listenToUserRoot: PropTypes.func.isRequired,
        fetchPcts: PropTypes.func.isRequired,
        listenToScheduleSessions: PropTypes.func.isRequired,
        getSessionItemById: PropTypes.func.isRequired,
        getPctItemById: PropTypes.func.isRequired,
        removeSessionFromSchedule: PropTypes.func.isRequired,
        removeSupportFromSchedule: PropTypes.func.isRequired,
        removePctFromSchedule: PropTypes.func.isRequired,
        listenToSchedulePct: PropTypes.func.isRequired,
        sessions: PropTypes.object,
        schedule: PropTypes.object,
        analysts: PropTypes.object,
        pct: PropTypes.object,
        scheduledSessions: PropTypes.object,
        user: userPropType,
        supportConsultations: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
        }),
        fetchAnalysts: PropTypes.func,
        listenToScheduleSupport: PropTypes.func,
        getScheduleItemById: PropTypes.func,
        select: PropTypes.string,
        // eslint-disable-next-line react/forbid-prop-types
        completedEvals: PropTypes.object,
        year: PropTypes.string,
    };

    componentDidMount() {
        this.props.listenToAuth().then(user => {
            this.props.listenToUserRoot(user.uid);
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user.isAuth) {
            if (
                nextProps.sessions.hasEverLoaded === false &&
                nextProps.sessions.fetching === false
            ) {
                this.props.fetchSessions();
            }
            if (
                nextProps.pct.hasEverLoaded === false &&
                nextProps.pct.fetching === false
            ) {
                this.props.fetchPcts();
            }
            if (
                nextProps.analysts.hasEverLoaded === false &&
                nextProps.analysts.fetching === false
            ) {
                this.props.fetchAnalysts();
            }
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.user.isAuth !== prevProps.user.isAuth) {
            if (this.props.user.isAuth) {
                this.props.listenToScheduleSessions(this.props.user.info.uid);
                this.props.listenToSchedulePct(this.props.user.info.uid);
                this.props.listenToScheduleSupport(this.props.user.info.uid);
            }
        }
    }

    onSignInClick = event => {
        event.preventDefault();
        requestPopupAuth(this.props.year);
    };

    removeItem = item => {
        switch (item.type) {
            case 'session':
                const session = this.props.getSessionItemById(item.id);
                this.props.removeSessionFromSchedule(session);
                break;
            case 'pct':
                const pct = this.props.getPctItemById(item.id);
                this.props.removePctFromSchedule(pct);
                break;
            case 'support':
                const scheduleItem = this.props.getScheduleItemById(
                    item.analystId,
                    item.availabilityId
                );
                this.props.removeSupportFromSchedule(scheduleItem);
                break;
            default:
                // eslint-disable-next-line no-console
                console.log('remove item from schedule', item.type, item.id);
                break;
        }
    };

    render() {
        const fetchingData =
            this.props.sessions.fetching ||
            this.props.analysts.fetching ||
            this.props.pct.fetching;
        const fetchingSchedule =
            !this.props.schedule.hasEverLoadedSessions ||
            !this.props.schedule.hasEverLoadedPcts ||
            !this.props.supportConsultations.hasEverLoaded;
        const { loadStatus } = this.props.sessions;
        const showLoadError =
            loadStatus && loadStatus !== 'success' && loadStatus !== '';

        if (this.props.user.initting) {
            return <Loading>Authenticating...</Loading>;
        }
        if (!this.props.user.isAuth) {
            return (
                <button
                    type="button"
                    className="cmc-article__link"
                    onClick={this.onSignInClick}
                >
                    Sign In
                </button>
            );
        }
        if (fetchingSchedule) {
            return <Loading>Loading Schedule...</Loading>;
        }
        if (fetchingData) {
            const loading = [
                this.props.sessions.fetching,
                this.props.analysts.fetching,
                this.props.pct.fetching,
            ];
            const percent = Math.floor(
                (loading.filter(x => !x).length / loading.length) * 100
            );
            return <Loading>{`Loading ${percent}%`}</Loading>;
        }
        if (showLoadError) {
            return <ShowError error={loadStatus} />;
        }

        return (
            <Schedule
                removeItem={this.removeItem}
                scheduledSessions={this.props.scheduledSessions}
                user={this.props.user}
                select={this.props.select}
                completedEvals={this.props.completedEvals}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        sessions: state.sessions,
        schedule: state.schedule,
        supportConsultations: state.supportConsultations,
        pct: state.pct,
        analysts: state.analysts,
        user: state.user,
        scheduledSessions: getScheduledSessions(
            state.schedule,
            state.sessions,
            state.pct,
            state.preset,
            state.analysts,
            state.supportConsultations
        ),
        getSessionItemById: getSessionItemById.bind(null, state.schedule),
        getPctItemById: getPctItemById.bind(null, state.schedule),
        getScheduleItemById: getScheduleItemById.bind(
            null,
            state.supportConsultations
        ),
        select: selectSessionId,
        completedEvals: basil.get('evaluations') || {},
        year: state.domAttributes.year,
    };
}

const ScheduleConnected = connect(mapStateToProps, {
    fetchSessions,
    fetchPcts,
    fetchAnalysts,
    listenToScheduleSessions,
    listenToSchedulePct,
    listenToScheduleSupport,
    listenToAuth,
    listenToUserRoot,
    removeSessionFromSchedule,
    removeSupportFromSchedule,
    removePctFromSchedule,
})(ScheduleContainer);

export default ScheduleConnected;
