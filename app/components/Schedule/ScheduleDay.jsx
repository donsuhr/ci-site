import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from '../util/load-moment-tz';
import DayRow from './DayRow';

const ScheduleDay = ({
    dayItems,
    removeItem,
    children,
    completedEvals,
    user,
    select,
}) => {
    const first = dayItems[0];
    const day = moment(first.start)
        .tz('America/New_York')
        .format('dddd, MMMM Do');
    const thisMorning = moment.tz('America/New_York').startOf('day');
    const isToday = moment(first.start)
        .tz('America/New_York')
        .hour(1)
        .isAfter(thisMorning);
    const containsSelect = dayItems.find(x => x._id === select);
    const isSevenDaysAfter = thisMorning.isAfter(
        moment(first.start)
            .tz('America/New_York')
            .add(7, 'days')
    );
    const show = isToday || containsSelect || isSevenDaysAfter;
    const headerClassNames = classNames({
        'event-schedule-table__day-heading': true,
        show,
    });
    const tableClassNames = classNames(
        'event-schedule-table',
        'event-schedule-table--collapsible',
        'event-schedule-table--schedule'
    );
    return (
        <div>
            <h2 className={headerClassNames}>{day}</h2>
            <div
                className="event-schedule-table--schedule-wrapper"
                style={{ display: show ? 'block' : 'none' }}
            >
                <table className={tableClassNames}>
                    <thead>
                        <tr>
                            <th className="event-schedule-table__time-cell">
                                Time
                            </th>
                            <th>Event</th>
                            <th>Location</th>
                            <th>
                                <div className="event-schedule-table__button-placeholder">
                                    {'\u00A0'}
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {dayItems.map(item => {
                            const key = item._id || item.availabilityId;
                            return (
                                <DayRow
                                    key={key}
                                    item={item}
                                    removeItem={removeItem}
                                    user={user}
                                    completedEvals={completedEvals}
                                    select={select}
                                />
                            );
                        })}
                    </tbody>
                </table>
                {children}
            </div>
        </div>
    );
};

ScheduleDay.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    dayItems: PropTypes.array,
    // eslint-disable-next-line react/forbid-prop-types
    user: PropTypes.object,
    // eslint-disable-next-line react/forbid-prop-types
    completedEvals: PropTypes.object,
    removeItem: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
    select: PropTypes.string,
};

export default ScheduleDay;
