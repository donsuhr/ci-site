import React from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';
import smoothScroll from 'cmc-site/app/components/smoothScroll';
import moment from '../util/load-moment-tz';
import DayRowButtons from './DayRowButtons';

class DayRow extends React.Component {
    static propTypes = {
        // eslint-disable-next-line react/forbid-prop-types
        user: PropTypes.object,
        // eslint-disable-next-line react/forbid-prop-types
        completedEvals: PropTypes.object,
        removeItem: PropTypes.func,
        item: PropTypes.shape({
            _id: PropTypes.string,
            start: PropTypes.oneOfType([
                PropTypes.instanceOf(Date),
                PropTypes.string,
            ]),
            end: PropTypes.oneOfType([
                PropTypes.instanceOf(Date),
                PropTypes.string,
            ]),
            title: PropTypes.string,
            location: PropTypes.object,
        }),
        select: PropTypes.string,
    };

    componentDidMount() {
        if (this.props.select && this.props.select === this.props.item._id) {
            setTimeout(() => {
                const $target = $(this.sessionRow);
                const offset = window.innerHeight - $target.outerHeight() - 26;
                smoothScroll.scrollTo($target, false, offset);
                setTimeout(() => {
                    $(this.sessionRow)
                        .find(
                            '.session-browser__session-eval-complete, .session-browser__session-eval-link'
                        )
                        .first()
                        .trigger('focus');
                }, 610);
            }, 10);
        }
    }

    render() {
        const {
            item, user, removeItem, completedEvals,
        } = this.props;
        const start = moment(item.start)
            .tz('America/New_York')
            .format('h:mm a');
        const end = moment(item.end)
            .tz('America/New_York')
            .format('h:mm a');
        const location =
            item.location && item.location.title
                ? item.location.title
                : '\u00A0';
        return (
            <tr
                ref={node => {
                    this.sessionRow = node;
                }}
            >
                <td className="event-schedule-table__time-cell">
                    {start} – {end}
                </td>
                <td>{item.title}</td>
                <td>{location}</td>
                <td>
                    <DayRowButtons
                        item={item}
                        removeItem={removeItem}
                        user={user}
                        completedEvals={completedEvals}
                    />
                </td>
            </tr>
        );
    }
}

export default DayRow;
