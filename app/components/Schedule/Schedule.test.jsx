/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Schedule from './Schedule';

describe('Schedule Schedule', () => {
    it('renders', () => {
        const wrapper = shallow(
            <Schedule
                scheduledSessions={{
                    20170310: [
                        {
                            analystId: '5860926299ab5cb274586cf5',
                            availabilityId: '589e04cb3414ba604118d257',
                            end: '2017-03-10T15:22:00.000Z',
                            start: '2017-03-10T15:21:00.000Z',
                            type: 'support',
                            title:
                                'Corrine123 Snoddy / Academics — Support Consultation',
                            location: { title: 'TBD' },
                        },
                    ],
                    20170424: [
                        {
                            _id: '20170424bb',
                            title: 'Breakfast Buffet',
                            start: '2017-04-24T11:30:00.000Z',
                            end: '2017-04-24T12:45:00.000Z',
                            type: 'preset',
                        },
                        {
                            _id: '20170424lb',
                            title: 'Lunch Buffet',
                            start: '2017-04-24T16:30:00.000Z',
                            end: '2017-04-24T17:30:00.000Z',
                            type: 'preset',
                        },
                    ],
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });
    it('renders an add button for pct', () => {
        const wrapper = shallow(
            <Schedule
                scheduledSessions={{
                    20170310: [
                        {
                            start: '2017-03-10T15:21:00.000Z',
                        },
                    ],
                }}
            />
        );
        expect(wrapper.find('a').node.props.href).toContain(
            'pre-conference-training'
        );
    });
    it('renders an add button for sessions', () => {
        const wrapper = shallow(
            <Schedule
                scheduledSessions={{
                    20170428: [
                        {
                            start: '2017-04-28T15:21:00.000Z',
                        },
                    ],
                }}
            />
        );
        expect(wrapper.find('a').node.props.href).toContain('sessions');
    });
});
