/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import DayRowButtons from './DayRowButtons';

describe('Schedule DayRowButtons', () => {
    it('renders', () => {
        const wrapper = shallow(
            <DayRowButtons
                item={{
                    id: '123',
                    type: 'session',
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders remove and eval button for session', () => {
        const wrapper = shallow(
            <DayRowButtons
                item={{
                    id: '123',
                    type: 'session',
                }}
            />
        );
        expect(wrapper.find('button').text()).toContain('Remove');
        expect(wrapper.find('EvalButton')).toHaveLength(1);
    });
    it('renders does not render eval for pct', () => {
        const wrapper = shallow(
            <DayRowButtons
                item={{
                    id: '123',
                    type: 'pct',
                }}
            />
        );
        expect(wrapper.find('button').text()).toContain('Remove');
        expect(wrapper.find('EvalButton')).toHaveLength(0);
    });
    it('renders a placeholder for other types', () => {
        const wrapper = shallow(
            <DayRowButtons
                item={{
                    id: '123',
                    type: 'things',
                }}
            />
        );
        expect(wrapper.find('button')).toHaveLength(0);
        expect(wrapper.find('EvalButton')).toHaveLength(0);
        expect(
            wrapper.find('.event-schedule-table__button-placeholder')
        ).toHaveLength(1);
    });

    it('calls remove', () => {
        const removeItem = jest.fn();
        const item = {
            id: '123',
            type: 'session',
        };
        const wrapper = shallow(
            <DayRowButtons
                item={{
                    id: '123',
                    type: 'session',
                }}
                removeItem={removeItem}
            />
        );
        wrapper.find('.event-schedule-table__remove-link').simulate('click', {
            preventDefault() {},
        });
        expect(removeItem).toHaveBeenCalledWith(item);
    });
});
