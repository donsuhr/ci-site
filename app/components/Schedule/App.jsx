import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import ReduxDevToolsLoader from '../redux/ReduxDevToolsLoader';
import ScheduleConnected from './Schedule.container';


const App = ({ store, ...props }) => (
    <Provider store={store}>
        <div>
            <ScheduleConnected {...props} />
            <ReduxDevToolsLoader />
        </div>
    </Provider>
);

App.propTypes = {
    /* eslint-disable react/forbid-prop-types */
    store: PropTypes.object.isRequired,
};

export default App;
