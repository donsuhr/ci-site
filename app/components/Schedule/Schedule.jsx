import React from 'react';
import PropTypes from 'prop-types';
import ScheduleDay from './ScheduleDay';

const Schedule = ({
    scheduledSessions,
    removeItem,
    completedEvals,
    user,
    select,
}) => {
    const orderedDays = Object.keys(scheduledSessions).sort();
    return (
        <div>
            {orderedDays.map(x => {
                const firstStart = new Date(scheduledSessions[x][0].start);
                return (
                    <ScheduleDay
                        key={x}
                        dayItems={scheduledSessions[x]}
                        removeItem={removeItem}
                        completedEvals={completedEvals}
                        user={user}
                        select={select}
                    >
                        {firstStart < new Date('2017-04-27T00:00:00-05:00') ? (
                            <a
                                className="schedule-browse-link"
                                href="/pre-conference-training/"
                            >
                                Add To Schedule
                            </a>
                        ) : (
                            <a
                                className="schedule-browse-link"
                                href="/sessions/"
                            >
                                Add To Schedule
                            </a>
                        )}
                    </ScheduleDay>
                );
            })}
        </div>
    );
};

Schedule.propTypes = {
    removeItem: PropTypes.func,
    // eslint-disable-next-line react/forbid-prop-types
    scheduledSessions: PropTypes.object,
    // eslint-disable-next-line react/forbid-prop-types
    completedEvals: PropTypes.object,
    // eslint-disable-next-line react/forbid-prop-types
    user: PropTypes.object,
    select: PropTypes.string,
};

export default Schedule;
