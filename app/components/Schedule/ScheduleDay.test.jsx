/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ScheduleDay from './ScheduleDay';
import moment from '../util/load-moment-tz';


describe('Schedule ScheduleDay', () => {
    it('renders', () => {
        const wrapper = shallow(
            <ScheduleDay
                dayItems={[
                    {
                        _id: '20170426bb',
                        title: 'Breakfast Buffet',
                        start: '2017-04-26T11:30:00.000Z',
                        end: '2017-04-26T12:45:00.000Z',
                        type: 'preset',
                    },
                    {
                        _id: '20170426lb',
                        title: 'Lunch Buffet',
                        start: '2017-04-26T16:30:00.000Z',
                        end: '2017-04-26T17:30:00.000Z',
                        type: 'preset',
                    },
                    {
                        _id: '20170426wr',
                        title: 'Welcome Reception',
                        start: '2017-04-26T21:00:00.000Z',
                        end: '2017-04-26T22:30:00.000Z',
                        type: 'preset',
                    },
                ]}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders children', () => {
        const wrapper = shallow(
            <ScheduleDay
                dayItems={[
                    {
                        _id: '20170424bb',
                        title: 'Breakfast Buffet',
                        start: '2017-04-24T11:30:00.000Z',
                        end: '2017-04-24T12:45:00.000Z',
                        type: 'preset',
                    },
                ]}
            >
                <div id="target">children</div>
            </ScheduleDay>
        );
        expect(wrapper.find('#target').text()).toBe('children');
    });

    it('hides show toggle for yesterday', () => {
        const yesterday = moment()
            .add(-1, 'days')
            .tz('America/New_York')
            .toISOString();
        const wrapper = shallow(
            <ScheduleDay
                dayItems={[
                    {
                        _id: '20170424bb',
                        title: 'Breakfast Buffet',
                        start: yesterday,
                        end: yesterday,
                        type: 'preset',
                    },
                ]}
            />
        );
        expect(wrapper.find('h2').node.props.className).not.toContain('show');
        expect(
            wrapper.find('.event-schedule-table--schedule-wrapper').node.props
                .style.display
        ).toBe('none');
    });

    it('shows toggle for today', () => {
        const today = moment().tz('America/New_York').toISOString();
        const wrapper = shallow(
            <ScheduleDay
                dayItems={[
                    {
                        _id: '20170424bb',
                        title: 'Breakfast Buffet',
                        start: today,
                        end: today,
                        type: 'preset',
                    },
                ]}
            />
        );
        expect(wrapper.find('h2').node.props.className).toContain('show');
        expect(
            wrapper.find('.event-schedule-table--schedule-wrapper').node.props
                .style.display
        ).toBe('block');
    });

    it('shows toggle for selected', () => {
        const yesterday = moment()
            .add(-1, 'days')
            .tz('America/New_York')
            .toISOString();
        const wrapper = shallow(
            <ScheduleDay
                select="20170424bb"
                dayItems={[
                    {
                        _id: '20170424bb',
                        title: 'Breakfast Buffet',
                        start: yesterday,
                        end: yesterday,
                        type: 'preset',
                    },
                ]}
            />
        );
        expect(wrapper.find('h2').node.props.className).toContain('show');
        expect(
            wrapper.find('.event-schedule-table--schedule-wrapper').node.props
                .style.display
        ).toBe('block');
    });

    it('collapses past days', () => {
        const mockDate = new Date('2017-04-27T12:00:00+05:00').getTime();
        Date.now = jest.fn(() => mockDate);
        const wrapper = shallow(
            <ScheduleDay
                dayItems={[
                    {
                        _id: '20170426bb',
                        title: 'Breakfast Buffet',
                        start: '2017-04-26T11:30:00.000Z',
                        end: '2017-04-26T12:45:00.000Z',
                        type: 'preset',
                    },
                ]}
            />
        );
        expect(
            wrapper
                .find('.event-schedule-table--schedule-wrapper')
                .prop('style')
        ).toMatchObject({ display: 'none' });
    });

    it('shows current and future days', () => {
        const mockDate = new Date('2017-04-26T12:00:00+05:00').getTime();
        Date.now = jest.fn(() => mockDate);
        const wrapper = shallow(
            <ScheduleDay
                dayItems={[
                    {
                        _id: '20170426bb',
                        title: 'Breakfast Buffet',
                        start: '2017-04-26T11:30:00.000Z',
                        end: '2017-04-26T12:45:00.000Z',
                        type: 'preset',
                    },
                ]}
            />
        );
        expect(
            wrapper
                .find('.event-schedule-table--schedule-wrapper')
                .prop('style')
        ).toMatchObject({ display: 'block' });
    });

    it('shows days 7 days after day', () => {
        const mockDate = new Date('2017-05-04T14:00:00+05:00').getTime();
        Date.now = jest.fn(() => mockDate);
        const wrapper = shallow(
            <ScheduleDay
                dayItems={[
                    {
                        _id: '20170426bb',
                        title: 'Breakfast Buffet',
                        start: '2017-04-26T11:30:00.000Z',
                        end: '2017-04-26T12:45:00.000Z',
                        type: 'preset',
                    },
                ]}
            />
        );
        expect(
            wrapper
                .find('.event-schedule-table--schedule-wrapper')
                .prop('style')
        ).toMatchObject({ display: 'block' });
    });
});
