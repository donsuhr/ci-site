/* global jest,it,expect,describe */

import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import DayRow from './DayRow';

describe('Schedule DayRow', () => {
    it('renders', () => {
        const wrapper = shallow(
            <DayRow
                item={{
                    _id: '20170426wr',
                    title: 'Welcome Reception',
                    start: '2017-04-26T21:00:00.000Z',
                    end: '2017-04-26T22:30:00.000Z',
                    type: 'preset',
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('focus the selected item', () => {
        jest.useFakeTimers();
        const wrapper = mount(
            <table>
                <tbody>
                    <DayRow
                        item={{
                            _id: '58fe6ddbcb77715e22b476cc',
                            title: 'Welcome Reception',
                            start: '2017-04-26T21:00:00.000Z',
                            end: '2017-04-26T22:30:00.000Z',
                            type: 'session',
                            id: '58fe6ddbcb77715e22b476cc',
                        }}
                        completedEvals={{
                            '58fe6ddbcb77715e22b476cc': {},
                        }}
                        select="58fe6ddbcb77715e22b476cc"
                    />
                </tbody>
            </table>
        );
        jest.runAllTimers();
        const focusedElement = document.activeElement;
        expect(
            wrapper
                .find('.session-browser__session-eval-complete')
                .matchesElement(focusedElement)
        ).toBe(true);
    });
});
