import React from 'react';
import PropTypes from 'prop-types';
import EvalButton from '../SessionBrowser/EvalButton';

const DayRowButtons = ({
    item, user, removeItem, completedEvals,
}) => {
    if (
        item.type === 'session' ||
        item.type === 'pct' ||
        item.type === 'support'
    ) {
        const removeBtn = (
            <button
                className="event-schedule-table__remove-link"
                onClick={event => {
                    event.preventDefault();
                    removeItem(item);
                }}
            >
                Remove
            </button>
        );
        if (item.type === 'session') {
            return (
                <ul className="event-schedule-table__button-list">
                    <li>{removeBtn}</li>
                    <EvalButton
                        user={user}
                        item={item}
                        completedEvals={completedEvals}
                        shortText
                        returnTo={encodeURIComponent(
                            `/my-conference/schedule/?s=${item._id}`
                        )}
                    />
                </ul>
            );
        }
        return (
            <ul className="event-schedule-table__button-list">
                <li>{removeBtn}</li>
            </ul>
        );
    }
    return (
        <ul className="event-schedule-table__button-list">
            <li>
                <div className="event-schedule-table__button-placeholder">
                    {'\u00A0'}
                </div>
            </li>
        </ul>
    );
};

DayRowButtons.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    item: PropTypes.shape({
        _id: PropTypes.string,
    }),
    // eslint-disable-next-line react/forbid-prop-types
    user: PropTypes.object,
    // eslint-disable-next-line react/forbid-prop-types
    completedEvals: PropTypes.object,
    removeItem: PropTypes.func,
};

export default DayRowButtons;
