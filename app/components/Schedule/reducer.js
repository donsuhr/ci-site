import moment from '../util/load-moment-tz';

const presetItems = [
    // 20170424:
    {
        _id: '20170424bb',
        title: 'Breakfast Buffet',
        start: moment.tz('2017-04-24T07:30', 'America/New_York').toDate(),
        end: moment.tz('2017-04-24T08:45', 'America/New_York').toDate(),
        type: 'preset',
    },
    {
        _id: '20170424lb',
        title: 'Lunch Buffet',
        start: moment.tz('2017-04-24T12:30', 'America/New_York').toDate(),
        end: moment.tz('2017-04-24T13:30', 'America/New_York').toDate(),
        type: 'preset',
    },
    // 20170425:
    {
        _id: '20170425bb',
        title: 'Breakfast Buffet',
        start: moment.tz('2017-04-25T07:30', 'America/New_York').toDate(),
        end: moment.tz('2017-04-25T08:45', 'America/New_York').toDate(),
        type: 'preset',
    },
    {
        _id: '20170425lb',
        title: 'Lunch Buffet',
        start: moment.tz('2017-04-25T12:30', 'America/New_York').toDate(),
        end: moment.tz('2017-04-25T13:30', 'America/New_York').toDate(),
        type: 'preset',
    },
    // 20170426:
    {
        _id: '20170426bb',
        title: 'Breakfast Buffet',
        start: moment.tz('2017-04-26T07:30', 'America/New_York').toDate(),
        end: moment.tz('2017-04-26T08:45', 'America/New_York').toDate(),
        type: 'preset',
    },
    {
        _id: '20170426lb',
        title: 'Lunch Buffet',
        start: moment.tz('2017-04-26T12:30', 'America/New_York').toDate(),
        end: moment.tz('2017-04-26T13:30', 'America/New_York').toDate(),
        type: 'preset',
    },
    {
        _id: '20170426wr',
        title: 'Welcome Reception',
        start: moment.tz('2017-04-26T17:00', 'America/New_York').toDate(),
        end: moment.tz('2017-04-26T18:30', 'America/New_York').toDate(),
        type: 'preset',
    },
    // 20170427:
    {
        _id: '20170427bb',
        title: 'Breakfast Buffet',
        start: moment.tz('2017-04-27T07:30', 'America/New_York').toDate(),
        end: moment.tz('2017-04-27T08:45', 'America/New_York').toDate(),
        type: 'preset',
    },
    {
        _id: '20170427lb',
        title: 'Lunch Buffet',
        start: moment.tz('2017-04-27T12:00', 'America/New_York').toDate(),
        end: moment.tz('2017-04-27T13:00', 'America/New_York').toDate(),
        type: 'preset',
    },
    {
        _id: '20170427ee',
        title: 'Evening Event',
        start: moment.tz('2017-04-27T19:00', 'America/New_York').toDate(),
        end: moment.tz('2017-04-27T23:00', 'America/New_York').toDate(),
        type: 'preset',
    },
    // 20170428:
    {
        _id: '20170428bb',
        title: 'Breakfast Buffet',
        start: moment.tz('2017-04-28T07:30', 'America/New_York').toDate(),
        end: moment.tz('2017-04-28T08:45', 'America/New_York').toDate(),
        type: 'preset',
    },
    {
        _id: '20170428lb',
        title: 'Lunch Buffet',
        start: moment.tz('2017-04-28T12:00', 'America/New_York').toDate(),
        end: moment.tz('2017-04-28T13:00', 'America/New_York').toDate(),
        type: 'preset',
    },
];

const preset = (state = presetItems, action) => state;

export default preset;
