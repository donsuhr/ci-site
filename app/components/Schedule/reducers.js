import { combineReducers } from 'redux';

import sessions from '../SessionBrowser/reducer';
import pct from '../PctBrowser/reducer';
import schedule from '../firebase/schedule.reducer';
import user from '../firebase/auth.reducer';
import preset from './reducer';
import analysts from '../ChooseAnalyst/reducer';
import supportConsultations from '../firebase/schedule.support.reducer';
import domAttributes from '../redux/domAttributes.reducers';

export default combineReducers({
    sessions,
    pct,
    schedule,
    user,
    preset,
    analysts,
    supportConsultations,
    domAttributes,
});
