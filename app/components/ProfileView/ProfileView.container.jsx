import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { listenToAuth } from '../firebase/auth.actions';
import { listenToProfile } from '../firebase/profile.actions';

import ProfileView from './ProfileView';

export class ProfileViewContainer extends React.Component {
    static propTypes = {
        profile: PropTypes.shape({
            info: PropTypes.shape({
                firstName: PropTypes.string,
                lastName: PropTypes.string,
            }),
            loadStatus: PropTypes.string,
        }),
        listenToAuth: PropTypes.func,
        listenToProfile: PropTypes.func,
    };

    componentDidMount() {
        this.props.listenToAuth().then(user => {
            if (this.props.profile.loadStatus !== 'success') {
                this.props.listenToProfile(user);
            }
        });
    }

    render() {
        const showProfile =
            this.props.profile.info.firstName &&
            this.props.profile.info.lastName;
        if (showProfile) {
            return <ProfileView profile={this.props.profile} />;
        }
        return null;
    }
}

function mapStateToProps(state, ownProps) {
    return {
        profile: state.profile,
    };
}

const ProfileViewConnected = connect(mapStateToProps, {
    listenToAuth,
    listenToProfile,
})(ProfileViewContainer);

export default ProfileViewConnected;
