/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { ProfileViewContainer } from './ProfileView.container';

describe('ProfileView ProfileViewContainer', () => {
    it('renders', () => {
        const wrapper = shallow(
            <ProfileViewContainer
                profile={{
                    info: {
                        firstName: 'firstName',
                        lastName: 'lastName',
                    },
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('ProfileView')).toHaveLength(1);
    });

    it('renders null if there is no profile', () => {
        const wrapper = shallow(
            <ProfileViewContainer
                profile={{
                    info: {},
                }}
            />
        );
        expect(wrapper.node).toBeNull();
    });
});
