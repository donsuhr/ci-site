/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ProfileView from './ProfileView';

describe('ProfileView ProfileView', () => {
    it('renders', () => {
        const wrapper = shallow(
            <ProfileView
                profile={{
                    loadStatus: 'success',
                    info: {
                        firstName: 'firstName',
                        lastName: 'lastName',
                        email: 'email',
                    },
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders an error', () => {
        const wrapper = shallow(
            <ProfileView
                profile={{
                    loadStatus: {
                        message: 'error message',
                    },
                    info: {
                        firstName: 'firstName',
                        lastName: 'lastName',
                        email: 'email',
                    },
                }}
            />
        );
        expect(wrapper.find('.show-error').text()).toContain('error message');
    });

    it('renders loading', () => {
        const wrapper = shallow(
            <ProfileView
                profile={{
                    fetching: true,
                    info: {},
                }}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
    });
});
