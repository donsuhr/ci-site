import React from 'react';
import PropTypes from 'prop-types';

const ShowError = ({ error: { message } }) => (
    <div className="show-error">
        <p>There was an error loading the sessions. <br /> Error: {message}</p>
    </div>
);

ShowError.propTypes = {
    error: PropTypes.shape({
        message: PropTypes.string.isRequired,
    }),
};

export default ShowError;
