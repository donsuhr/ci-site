import { combineReducers } from 'redux';

import user from '../firebase/auth.reducer';
import allUsers from '../firebase/allUsers.reducer';
import listeners from '../firebase/listeners.reducer';
import domAttributes from '../redux/domAttributes.reducers';

export default combineReducers({
    allUsers,
    user,
    listeners,
    domAttributes,
});
