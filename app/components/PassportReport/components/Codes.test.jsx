/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Codes from './Codes';

describe('PassportReport charts Codes', () => {
    it('renders', () => {
        const wrapper = shallow(
            <Codes
                data={{
                    sponsor1: '123',
                    sponsor2: '234',
                    sponsor3: '456',
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
