import React from 'react';
import PropTypes from 'prop-types';

const Profile = ({ data }) => {
    if (!data) {
        return (
            <div className="passport-report-item__profile">
                <div className="passport-report-item__title secondary-header--alt">Profile</div>
                <p>
                    No profile data set. User did not sign up for Support
                    Consultation. Find user email in firebase.
                </p>
            </div>
        );
    }
    return (
        <div className="passport-report-item__profile">
            <div
                className="passport-report-item__profile__title secondary-header--alt"
            >
                Profile
            </div>
            <dl>
                <dt>Name</dt>
                <dd>{data.firstName} {data.lastName}</dd>
                <dt>Institution</dt>
                <dd>{data.institution}</dd>
                <dt>Email</dt>
                <dd>{data.email}</dd>
                <dt>Phone</dt>
                <dd>{data.phone}</dd>
            </dl>
        </div>
    );
};

Profile.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    data: PropTypes.object,
};

export default Profile;
