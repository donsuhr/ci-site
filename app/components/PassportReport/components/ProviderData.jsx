import React from 'react';
import PropTypes from 'prop-types';

const ProviderData = ({ data }) => {
    if (!data) {
        return (
            <div className="passport-report-item__provider-data">
                <div className="passport-report-item__title secondary-header--alt">
                    Provider Data
                </div>
                <p>
                    No provider data set. User has not connected since the
                    beginning of providerData collection. Find user email in
                    firebase.
                </p>
            </div>
        );
    }
    return (
        <div className="passport-report-item__provider-data">
            <div className="passport-report-item__profile__title secondary-header--alt">
                Provider Data
            </div>
            <dl>
                {Object.keys(data).map(key => [
                    <dt>{key}</dt>,
                    <dd>{data[key]}</dd>,
                ])}
                {Object.keys(data).includes('photoURL') && (
                    <div>
                        <dt>Photo</dt>
                        <dd>
                            <img
                                src={data.photoURL}
                                className="passport-report-item__provider-data-photo"
                                alt=""
                            />
                        </dd>
                    </div>
                )}
            </dl>
        </div>
    );
};

ProviderData.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    data: PropTypes.object,
};

export default ProviderData;
