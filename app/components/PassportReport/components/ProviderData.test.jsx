/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ProviderData from './ProviderData';

describe('PassportReport charts SponsorChart', () => {
    it('renders', () => {
        const wrapper = shallow(
            <ProviderData
                data={{
                    foo: 'bar',
                    photoURL: 'url',
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders message when no data provided', () => {
        const wrapper = shallow(<ProviderData />);
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('p').text()).toContain('No provider data set');
    });
});
