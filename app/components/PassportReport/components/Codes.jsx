import React from 'react';
import PropTypes from 'prop-types';

const Codes = ({ data }) => (
    <div className="passport-report-item__codes">
        <div className="passport-report-item__title secondary-header--alt">Codes</div>
        <dl>
            {Object.keys(data).map(sponsor => ([
                <dt>{sponsor}</dt>,
                <dd>{data[sponsor].code}</dd>,
            ]))}
        </dl>
    </div>
);

Codes.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    data: PropTypes.object,
};

export default Codes;
