/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SponsorChart from './SponsorChart';

describe('PassportReport charts SponsorChart', () => {
    it('renders', () => {
        const wrapper = shallow(<SponsorChart chartData={[]} />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
