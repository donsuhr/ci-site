import React from 'react';
import PropTypes from 'prop-types';
import { Pie, PieChart, Legend, Cell, Tooltip } from 'recharts';
import randomColor from 'randomcolor';

const SponsorChart = ({ chartData }) => (
    <div>
        <p className="cmc-article__intro-p">Completion by Sponsor</p>
        <div className="sponsor-chart">
            <PieChart width={400} height={250}>
                <Pie
                    cx={150}
                    data={chartData}
                    label
                >
                    {
                        chartData.map((entry, index) => (
                            <Cell key={`cell-${entry.name}`} fill={randomColor()} />
                        ))
                    }
                </Pie>
                <Legend layout="vertical" align="right" />
                <Tooltip />
            </PieChart>
        </div>
    </div>
);

SponsorChart.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    chartData: PropTypes.array,
};

export default SponsorChart;
