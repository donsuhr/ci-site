/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Profile from './Profile';

describe('PassportReport charts Profile', () => {
    it('renders', () => {
        const wrapper = shallow(
            <Profile
                data={{
                    firstName: 'firstName',
                    lastName: 'lastName',
                    institution: 'institution',
                    email: 'email',
                    phone: 'phone',
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders message when no data provided', () => {
        const wrapper = shallow(<Profile />);
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('p').text()).toContain('No profile data set');
    });
});
