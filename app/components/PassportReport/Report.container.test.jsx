/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { PassportReportContainer } from './Report.container';

describe('PassportReport PassportReportContainer', () => {
    it('renders', () => {
        const wrapper = shallow(
            <PassportReportContainer
                allUsers={{
                    hasEverLoaded: true,
                    fetching: false,
                }}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('PassportReport')).toHaveLength(1);
    });
    it('renders loading', () => {
        const wrapper = shallow(
            <PassportReportContainer
                allUsers={{
                    hasEverLoaded: true,
                    fetching: true,
                }}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
    });
});
