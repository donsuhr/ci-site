import React from 'react';
import PropTypes from 'prop-types';
import sortBy from 'lodash/sortBy';
import $ from 'jquery';
import 'jquery-ui/ui/widgets/accordion';

import Codes from './components/Codes';
import Profile from './components/Profile';
import ProviderData from './components/ProviderData';
import SponsorChart from './components/SponsorChart';

class PassportReport extends React.Component {
    static propTypes = {
        // eslint-disable-next-line react/forbid-prop-types
        users: PropTypes.object,
        totalUsers: PropTypes.number,
        getDisplayName: PropTypes.func,
    };

    static destoryAccordian() {
        $('.passport-list').accordion('destroy');
    }

    static initAccordian() {
        $('.passport-list').accordion({
            animate: 200,
            collapsible: true,
            heightStyle: 'content',
            active: false,
        });
    }

    componentDidMount() {
        PassportReport.initAccordian();
    }

    componentDidUpdate(prevProps) {
        if (
            Object.keys(this.props.users).length !==
            Object.keys(prevProps.users).length
        ) {
            PassportReport.destoryAccordian();
            PassportReport.initAccordian();
        }
    }

    componentWillUnmount() {
        PassportReport.destoryAccordian();
    }

    render() {
        const { users, totalUsers, getDisplayName } = this.props;
        const asArray = Object.keys(users).map(x => ({ id: x, ...users[x] }));
        const sorted = sortBy(asArray, x => -Object.keys(x.passport).length);
        const sponsorCounts = Object.keys(users).reduce((acc, userId) => {
            const { passport } = users[userId];
            Object.keys(passport).forEach(sponsor => {
                if (!Object.hasOwnProperty.call(acc, sponsor)) {
                    acc[sponsor] = 0;
                }
                acc[sponsor] += 1;
            });
            return acc;
        }, {});
        const chartData = Object.keys(sponsorCounts).map(sponsor => ({
            name: sponsor,
            value: sponsorCounts[sponsor],
        }));
        return (
            <div>
                <p className="cmc-article__intro-p">
                    Passport Users: {asArray.length} of {totalUsers}
                </p>
                <div className="passport-list">
                    {sorted.map(item => {
                        const name = getDisplayName(item);
                        const total = Object.keys(item.passport).length;

                        return [
                            <h2
                                className="passport-list__sponsor-title"
                                key={item.id}
                            >
                                {name} - {total}
                            </h2>,
                            <div className="passport-list__sponsor-info">
                                <div className="passport-report-item__info-container">
                                    <Profile data={item.profile} />
                                    <ProviderData data={item.providerData} />
                                </div>
                                <Codes data={item.passport} />
                            </div>,
                        ];
                    })}
                </div>
                <SponsorChart chartData={chartData} />
                <p>/app/components/firebase/passport.reducer.js</p>
            </div>
        );
    }
}

export default PassportReport;
