import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    listenToAuth,
    requestPopupAuth,
    listenToUserRoot,
} from '../firebase/auth.actions';
import PassportReportConnected from './Report.container';
import Loading from '../Loading';

export class PassportReportView extends React.Component {
    static propTypes = {
        listenToAuth: PropTypes.func.isRequired,
        listenToUserRoot: PropTypes.func.isRequired,
        user: PropTypes.shape({
            isAuth: PropTypes.bool,
            isAdmin: PropTypes.bool,
            initting: PropTypes.bool,
            loadingRoot: PropTypes.bool,
        }),
        year: PropTypes.string,
    };

    componentDidMount() {
        this.props.listenToAuth().then(user => {
            this.props.listenToUserRoot(user.uid);
        });
    }

    onSignInClick = event => {
        event.preventDefault();
        requestPopupAuth(this.props.year);
    };

    render() {
        if (this.props.user.initting) {
            return <Loading>Loading...</Loading>;
        }
        if (this.props.user.loadingRoot) {
            return <Loading>Authenticating...</Loading>;
        }
        if (!this.props.user.isAuth) {
            return (
                <button
                    type="button"
                    className="cmc-article__link"
                    onClick={this.onSignInClick}
                >
                    Sign In
                </button>
            );
        }
        if (!this.props.user.isAdmin) {
            return <p>Admin rights required.</p>;
        }
        return <PassportReportConnected />;
    }
}

function mapStateToProps(state, ownProps) {
    return {
        user: state.user,
        year: state.domAttributes.year,
    };
}

const PassportReportViewConnected = connect(mapStateToProps, {
    listenToAuth,
    listenToUserRoot,
})(PassportReportView);

export default PassportReportViewConnected;
