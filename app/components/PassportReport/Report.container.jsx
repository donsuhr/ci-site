import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PassportReport from './Report';
import { listenToAllUsers } from '../firebase/allUsers.actions';
import { withPassport, getDisplayName } from '../firebase/allUsers.reducer';

import Loading from '../Loading';

export class PassportReportContainer extends React.Component {
    static propTypes = {
        allUsers: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        // eslint-disable-next-line react/forbid-prop-types
        usersWithPassport: PropTypes.object,
        listenToAllUsers: PropTypes.func,
        allUsersCount: PropTypes.number,
    };

    componentDidMount() {
        if (
            this.props.allUsers.hasEverLoaded === false &&
            this.props.allUsers.fetching === false
        ) {
            this.props.listenToAllUsers();
        }
    }

    render() {
        if (this.props.allUsers.fetching) {
            return <Loading>Loading Users...</Loading>;
        }
        return (
            <PassportReport
                totalUsers={this.props.allUsersCount}
                users={this.props.usersWithPassport}
                getDisplayName={getDisplayName}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        allUsers: state.allUsers,
        usersWithPassport: withPassport(state.allUsers),
        allUsersCount: Object.keys(state.allUsers.byId).length,
    };
}

const PassportReportConnected = connect(mapStateToProps, {
    listenToAllUsers,
})(PassportReportContainer);

export default PassportReportConnected;
