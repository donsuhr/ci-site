/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { PassportReportView } from './View';

describe('PassportReport View', () => {
    it('renders', () => {
        const wrapper = shallow(
            <PassportReportView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    initting: false,
                    isAuth: true,
                    loadingRoot: false,
                    isAdmin: true,
                }}
            />
        );
        expect(wrapper.find('Connect(PassportReportContainer)')).toHaveLength(
            1
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading data', () => {
        const wrapper = shallow(
            <PassportReportView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    initting: true,
                }}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
        expect(
            wrapper
                .find('Loading')
                .dive()
                .text()
        ).toContain('Loading');
    });

    it('renders authenticating', () => {
        const wrapper = shallow(
            <PassportReportView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    initting: false,
                    loadingRoot: true,
                }}
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
        expect(
            wrapper
                .find('Loading')
                .dive()
                .text()
        ).toContain('Authenticating');
    });

    it('renders sign in button', () => {
        const wrapper = shallow(
            <PassportReportView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    initting: false,
                    loadingRoot: false,
                    isAuth: false,
                }}
            />
        );
        expect(wrapper.find('button').text()).toContain('Sign In');
    });

    it('renders auth required', () => {
        const wrapper = shallow(
            <PassportReportView
                listenToAuth={jest.fn()}
                listenToUserRoot={jest.fn()}
                user={{
                    initting: false,
                    loadingRoot: false,
                    isAuth: true,
                }}
            />
        );
        expect(wrapper.find('p').text()).toContain('Admin rights required');
    });
});
