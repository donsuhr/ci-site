/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import PassportReport from './Report';

const users = {
    Rjuz6asLFiRaYlFwVMSvVWN7jd63: {
        passport: { ACI: { code: '1224' } },
        profile: {
            email: 'stevesuhr@donsuhr.com',
            firstName: 'Steve',
            institution: 'adsf',
            lastName: 'Smith',
            phone: 'adsf',
        },
        providerData: {
            displayName: 'Steve Smith',
            email: 'stevesuhr@donsuhr.com',
            photoURL:
                'https://scontent.xx.fbcdn.net/v/t1.0-1/s100x100/1379841_10150004552801901_469209496895221757_n.jpg?oh=b8d1ac9d0b972e309c5372d60cd28f83&oe=5963E797',
            providerId: 'facebook.com',
            uid: '365710833794177',
        },
    },
    V8JHFtYzFMUosKpABNyPxw06qNs2: {
        admin: true,
        passport: {
            ACI: { code: '123' },
            Ambassador: { code: '234' },
            AspirEDU: { code: '234' },
            Burning: { code: '4287' },
            Constituo: { code: '4266' },
            EPX: { code: '234' },
            SchoolDocs: { code: '234' },
            Socket: { code: '234' },
            TouchNet: { code: '234' },
            WealthEngine: { code: '234' },
            iData: { code: '234' },
        },
        profile: {
            email: 'jbozman@campusmgmt.com',
            firstName: 'Jim',
            institution: 'CMC test',
            lastName: 'Test',
            phone: '1112223333',
        },
        providerData: {
            email: 'jbozman@campusmgmt.com',
            providerId: 'password',
            uid: 'jbozman@campusmgmt.com',
        },
        schedule: {
            pct: {
                '-KZReg9jJI8-bSM_Qt1k': { pctId: '584ecfa54b0330538e482ef0' },
                '-KZRexWVX_h0l-KlVZ59': { pctId: '5850046404358711eec835d5' },
                '-KZb1aE2yrYg2UdjKSNE': { pctId: '584ffb5904358711eec835cf' },
            },
            sessions: {
                '-K_ej4VZPaaRjBzpuQ0K': {
                    sessionId: '5862a15904358711eec83793',
                },
                '-K_ej84680C9U3SbYtxR': {
                    sessionId: '5862a87d04358711eec837ab',
                },
                '-K_ejBVcvY_bMcBVGW4f': {
                    sessionId: '5862ad1704358711eec837ba',
                },
                '-K_ejDfyJpIJykeKgsHy': {
                    sessionId: '5862af0b04358711eec837bf',
                },
                '-KbzHNluzT3SjeidfuPq': {
                    sessionId: '58928e3acce8ce5b5b5efe4c',
                },
                '-KdQdfxk8Efyfo8LDXBc': {
                    sessionId: '585d572904358711eec83745',
                },
                '-KdQdixjoiyKWpf-STXp': {
                    sessionId: '5861414004358711eec83766',
                },
                '-KdQdqMQYetDe6TvIKni': {
                    sessionId: '5861541204358711eec83784',
                },
            },
        },
    },
};

describe('PassportReport Report', () => {
    it('renders', () => {
        const wrapper = shallow(
            <PassportReport
                users={users}
                totalUsers={222}
                getDisplayName={() => 'display name'}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('.passport-list__sponsor-title')).toHaveLength(
            Object.keys(users).length
        );
    });
});
