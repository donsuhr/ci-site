/* global jest,it,expect,describe */

import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router';
import { createStore, combineReducers } from 'redux';
import { reducer as formReducer, SubmissionError } from 'redux-form';

import ProfileForm from './ProfileForm';

function createNewStore() {
    return createStore(
        combineReducers({
            form: formReducer,
        })
    );
}

describe('ProfileForm ProfileViewContainer', () => {
    it('renders', () => {
        const wrapper = shallow(
            <Provider store={createNewStore()}>
                <MemoryRouter>
                    <ProfileForm handleSubmit={jest.fn()} />
                </MemoryRouter>
            </Provider>
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('shows a validation error', () => {
        const wrapper = mount(
            <Provider store={createNewStore()}>
                <MemoryRouter>
                    <ProfileForm onSubmit={jest.fn()} />
                </MemoryRouter>
            </Provider>
        );
        const firstName = wrapper.findWhere(
            n => n.name() === 'Field' && n.prop('name') === 'firstName'
        );

        wrapper.find('form').simulate('submit');
        const errorSpan = firstName.find('span');
        expect(errorSpan.text()).toContain('Please enter a first name');
    });

    it('removes the validation error', () => {
        const wrapper = mount(
            <Provider store={createNewStore()}>
                <MemoryRouter>
                    <ProfileForm onSubmit={jest.fn()} />
                </MemoryRouter>
            </Provider>
        );
        const firstName = wrapper.findWhere(
            n => n.name() === 'Field' && n.prop('name') === 'firstName'
        );

        const firstNameInput = wrapper.find('input#firstName');
        firstNameInput.node.value = 'bo chen**&&))';
        firstNameInput.simulate('change', firstNameInput);

        wrapper.find('form').simulate('submit');
        expect(firstName.find('span')).toHaveLength(0);
    });

    it('shows submitting loading', () => {
        const wrapper = mount(
            <Provider store={createNewStore()}>
                <MemoryRouter>
                    <ProfileForm
                        onSubmit={jest.fn(() => new Promise((resolve, reject) => {}))}
                        initialValues={{
                            firstName: 'fname',
                            lastName: 'lname',
                            phone: 'phone',
                            email: 'email@x.com',
                            institution: 'institution',
                        }}
                    />
                </MemoryRouter>
            </Provider>
        );
        wrapper.find('form').simulate('submit');
        expect(wrapper.find('Loading')).toHaveLength(1);
    });

    it('shows an error', done => {
        const wrapper = mount(
            <Provider store={createNewStore()}>
                <MemoryRouter>
                    <ProfileForm
                        onSubmit={jest.fn(() => {
                            const submitError = new SubmissionError({
                                _error: 'test error',
                            });
                            return Promise.reject(submitError);
                        })}
                        initialValues={{
                            firstName: 'fname',
                            lastName: 'lname',
                            phone: 'phone',
                            email: 'email@x.com',
                            institution: 'institution',
                        }}
                    />
                </MemoryRouter>
            </Provider>
        );
        wrapper.find('form').simulate('submit');
        setTimeout(() => {
            expect(wrapper.find('strong').text()).toContain('test error');
            done();
        }, 100);
    });
});
