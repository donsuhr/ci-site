/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import TopicChart from '../../charts/TopicChart';

const data = [
    {
        topic: 'topic 1',
        A: 1,
    },
    {
        topic: 'topic 2',
        A: 2,
    },
];

describe('TimeChart', () => {
    it('renders', () => {
        const wrapper = shallow(<TopicChart data={data} />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const text = 'Loading Data...';
        const wrapper = shallow(<TopicChart loading={text} />);
        expect(wrapper.find('Loading').dive().text()).toContain(text);
    });

    it('renders data', () => {
        const wrapper = shallow(<TopicChart loading={false} data={data} />);
        expect(wrapper.find('ResponsiveContainer')).toHaveLength(1);
    });
});
