/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import TimeChart from '../../charts/TimeChart';

const data = [
    {
        name: '01:30 AM',
        Monday: 2,
        Tuesday: 3,
        Wednesday: 0,
    },
];
const days = new Set('Monday', 'Tuesday', 'Wednesday');

describe('TimeChart', () => {
    it('renders', () => {
        const wrapper = shallow(<TimeChart data={data} days={days} />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const text = 'Loading Data...';
        const wrapper = shallow(<TimeChart loading={text} />);
        expect(wrapper.find('Loading').dive().text()).toContain(text);
    });

    it('renders data', () => {
        const wrapper = shallow(
            <TimeChart loading={false} data={data} days={days} />
        );
        expect(wrapper.find('ResponsiveContainer')).toHaveLength(1);
    });
});
