/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import DayChart from '../../charts/DayChart';

const data = [
    {
        name: 'YYYYMMDD',
        value: 2,
    },
];

describe('DayChart', () => {
    it('renders', () => {
        const wrapper = shallow(<DayChart data={data} />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const text = 'Loading Data...';
        const wrapper = shallow(<DayChart loading={text} />);
        expect(wrapper.find('Loading').dive().text()).toContain(text);
    });

    it('renders data', () => {
        const wrapper = shallow(<DayChart loading={false} data={data} />);
        expect(wrapper.find('ResponsiveContainer')).toHaveLength(1);
    });
});
