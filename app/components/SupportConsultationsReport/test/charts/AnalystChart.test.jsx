/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import AnalystChart from '../../charts/AnalystChart';

const data = [
    {
        name: 'name one',
        d1: 2,
        d2: 3,
    },
];
describe('AnalystChart', () => {
    it('renders', () => {
        const wrapper = shallow(<AnalystChart data={data} />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const text = 'Loading Data...';
        const wrapper = shallow(<AnalystChart loading={text} />);
        expect(wrapper.find('Loading').dive().text()).toContain(text);
    });

    it('renders data', () => {
        const wrapper = shallow(<AnalystChart loading={false} data={data} />);
        expect(wrapper.find('ResponsiveContainer')).toHaveLength(1);
    });
});
