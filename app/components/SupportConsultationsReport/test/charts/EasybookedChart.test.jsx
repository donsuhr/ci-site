/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import EasybookedChart from '../../charts/EasybookedChart';

const data = [
    {
        name: 'EasyBooked',
        value: 2,
    },
    {
        name: 'Regular',
        value: 3,
    },
];

describe('EasybookedChart', () => {
    it('renders', () => {
        const wrapper = shallow(<EasybookedChart data={data} />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const text = 'Loading Data...';
        const wrapper = shallow(<EasybookedChart loading={text} />);
        expect(wrapper.find('Loading').dive().text()).toContain(text);
    });

    it('renders data', () => {
        const wrapper = shallow(
            <EasybookedChart loading={false} data={data} />
        );
        expect(wrapper.find('ResponsiveContainer')).toHaveLength(1);
    });
});
