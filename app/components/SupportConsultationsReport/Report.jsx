import React from 'react';
import TopicChart from './charts/TopicChart.container';
import DayChart from './charts/DayChart.container';
import EasybookedChart from './charts/EasybookedChart.container';
import AnalystChart from './charts/AnalystChart.container';
import TimeChart from './charts/TimeChart.container';

const Report = () => (
    <div>
        <DayChart />
        <EasybookedChart />
        <TopicChart />
        <AnalystChart />
        <TimeChart />
    </div>
);

export default Report;
