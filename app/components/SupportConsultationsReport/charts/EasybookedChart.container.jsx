import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import EasybookedChart from './EasybookedChart';
import { listenToAuth } from '../../firebase/auth.actions';
import { listenToUsedAvailability } from '../../firebase/schedule.support.actions';

class Container extends React.Component {
    static propTypes = {
        supportConsultations: PropTypes.shape({
            usedAvailabilityId: PropTypes.object,
            hasEverLoadedUsedAvailability: PropTypes.bool,
        }),
        listenToUsedAvailability: PropTypes.func,
        listenToAuth: PropTypes.func,
    };

    componentDidMount() {
        this.props.listenToAuth().then(user => {
            this.props.listenToUsedAvailability();
        });
    }

    render() {
        let loading = false;
        if (!this.props.supportConsultations.hasEverLoadedUsedAvailability) {
            loading = 'Loading Data...';
        }
        const counter = loading
            ? { easybooked: 0, regular: 0 }
            : Object.keys(
                this.props.supportConsultations.usedAvailabilityId
            ).reduce(
                (acc, x) => {
                    const avalibilityObj = this.props.supportConsultations
                        .usedAvailabilityId[x];
                    if (avalibilityObj.userId === 'easybook') {
                        acc.easybooked += 1;
                    } else {
                        acc.regular += 1;
                    }
                    return acc;
                },
                { easybooked: 0, regular: 0 }
            );

        const data = loading
            ? []
            : [
                { name: 'EasyBooked', value: counter.easybooked },
                { name: 'Regular', value: counter.regular },
            ];

        return <EasybookedChart data={data} loading={loading} />;
    }
}

function mapStateToProps(state, ownProps) {
    return {
        supportConsultations: state.supportConsultations,
    };
}

const DayChartConnected = connect(mapStateToProps, {
    listenToAuth,
    listenToUsedAvailability,
})(Container);

export default DayChartConnected;
