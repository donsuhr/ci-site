import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from '../../util/load-moment-tz';
import TimeChart from './TimeChart';
import { fetchAnalysts } from '../../ChooseAnalyst/actions';
import { getAllAppointmentsGroupedByHour } from '../../ChooseAnalyst/reducer';
import { listenToAuth } from '../../firebase/auth.actions';
import { listenToUsedAvailability } from '../../firebase/schedule.support.actions';

class Container extends React.Component {
    static propTypes = {
        analysts: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        // eslint-disable-next-line react/forbid-prop-types
        appointmentsGroupedByHour: PropTypes.object,
        supportConsultations: PropTypes.shape({
            hasEverLoadedUsedAvailability: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        fetchAnalysts: PropTypes.func,
        listenToUsedAvailability: PropTypes.func,
        listenToAuth: PropTypes.func,
    };

    componentDidMount() {
        if (
            this.props.analysts.hasEverLoaded === false &&
            this.props.analysts.fetching === false
        ) {
            this.props.fetchAnalysts();
        }
        this.props.listenToAuth().then(user => {
            this.props.listenToUsedAvailability();
        });
    }

    render() {
        const days = new Set();
        let minDayTime = 9999;
        let maxDayTime = 0;

        Object.keys(this.props.appointmentsGroupedByHour)
            .sort()
            .forEach(dayTime => {
                const mTime = moment.tz(
                    dayTime,
                    'YYYYMMDDHHmm',
                    'America/New_York'
                );
                days.add(mTime.format('YYYYMMDD'));
                const time = mTime.format('HHmm');
                minDayTime = Math.min(minDayTime, parseInt(time, 10));
                maxDayTime = Math.max(maxDayTime, parseInt(time, 10));
            });
        minDayTime = minDayTime.toString();
        if (minDayTime.length === 3) {
            minDayTime = `0${minDayTime}`;
        }
        maxDayTime = maxDayTime.toString();
        if (maxDayTime.length === 3) {
            maxDayTime = `0${maxDayTime}`;
        }
        const start = moment()
            .hour(minDayTime.substr(0, 2))
            .minute(minDayTime.substr(2, 2));
        const end = moment()
            .hour(maxDayTime.substr(0, 2))
            .minute(maxDayTime.substr(2, 2));

        const data = [];
        const dayNames = new Set();
        for (
            const m = start;
            m.diff(end, 'minutes') <= 0;
            m.add(30, 'minutes')
        ) {
            const obj = { name: m.format('hh:mm A') };
            days.forEach(day => {
                const dayname = moment
                    .tz(day, 'YYYYMMDD', 'America/New_York')
                    .format('dddd');
                dayNames.add(dayname);
                const key = `${day}${m.format('HHmm')}`;
                obj[dayname] = this.props.appointmentsGroupedByHour[key]
                    ? this.props.appointmentsGroupedByHour[key].length
                    : 0;
            });
            data.push(obj);
        }

        let loading = false;
        if (!this.props.supportConsultations.hasEverLoadedUsedAvailability) {
            loading = 'Loading Data...';
        }
        if (
            this.props.analysts.hasEverLoaded === false ||
            this.props.analysts.fetching
        ) {
            loading = 'Loading Analysts...';
        }
        return <TimeChart data={data} days={dayNames} loading={loading} />;
    }
}

function mapStateToProps(state, ownProps) {
    return {
        analysts: state.analysts,
        supportConsultations: state.supportConsultations,
        appointmentsGroupedByHour: getAllAppointmentsGroupedByHour(
            state.analysts,
            state.supportConsultations
        ),
    };
}

const TopicChartConnected = connect(mapStateToProps, {
    fetchAnalysts,
    listenToAuth,
    listenToUsedAvailability,
})(Container);

export default TopicChartConnected;
