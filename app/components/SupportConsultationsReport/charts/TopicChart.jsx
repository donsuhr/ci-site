import React from 'react';
import PropTypes from 'prop-types';
import {
    ResponsiveContainer,
    Radar,
    RadarChart,
    PolarGrid,
    PolarAngleAxis,
    PolarRadiusAxis,
    Tooltip,
} from 'recharts';
import Loading from '../../Loading';

const TopicChart = ({ data, loading }) => {
    if (loading) {
        return (
            <div className="topics-chart">
                <Loading>{loading}</Loading>
            </div>
        );
    }
    return (
        <div className="topics-chart">
            <ResponsiveContainer>
                <RadarChart data={data}>
                    <Radar
                        name="Topic"
                        dataKey="A"
                        stroke="#78A22F"
                        fill="#78A22F"
                        fillOpacity={0.6}
                    />
                    <PolarGrid />
                    <PolarAngleAxis dataKey="topic" />
                    <PolarRadiusAxis />
                    <Tooltip />
                </RadarChart>
            </ResponsiveContainer>
        </div>
    );
};

TopicChart.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    data: PropTypes.array,
    loading: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
    ]),
};

export default TopicChart;
