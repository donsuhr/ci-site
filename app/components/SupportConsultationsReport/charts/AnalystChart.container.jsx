import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import AnalystChart from './AnalystChart';
import { fetchAnalysts } from '../../ChooseAnalyst/actions';
import { getAnalystsBookedSlotsByDay } from '../../ChooseAnalyst/reducer';
import { listenToAuth } from '../../firebase/auth.actions';
import { listenToUsedAvailability } from '../../firebase/schedule.support.actions';
import { listenToEasyBook } from '../../firebase/easybook.actions';

class Container extends React.Component {
    static propTypes = {
        analysts: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        // eslint-disable-next-line react/forbid-prop-types
        analystsBookedSlotsByDay: PropTypes.object,
        supportConsultations: PropTypes.shape({
            hasEverLoadedUsedAvailability: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        easybook: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        fetchAnalysts: PropTypes.func,
        listenToUsedAvailability: PropTypes.func,
        listenToAuth: PropTypes.func,
        listenToEasyBook: PropTypes.func,
    };

    componentDidMount() {
        if (
            this.props.analysts.hasEverLoaded === false &&
            this.props.analysts.fetching === false
        ) {
            this.props.fetchAnalysts();
        }
        this.props.listenToAuth().then(user => {
            this.props.listenToUsedAvailability();
            this.props.listenToEasyBook();
        });
    }

    render() {
        let loading = false;
        if (
            !this.props.supportConsultations.hasEverLoadedUsedAvailability ||
            !this.props.easybook.hasEverLoaded
        ) {
            loading = 'Loading Data...';
        }
        if (
            this.props.analysts.hasEverLoaded === false ||
            this.props.analysts.fetching
        ) {
            loading = 'Loading Analysts...';
        }
        const data = loading
            ? []
            : Object.keys(
                this.props.analystsBookedSlotsByDay
            ).reduce((acc, analystId) => {
                const analyst = this.props.analystsBookedSlotsByDay[
                    analystId
                ];
                const d1 =
                      analyst.bookedByDay &&
                      Object.hasOwnProperty.call(
                          analyst.bookedByDay,
                          '20170427'
                      )
                          ? analyst.bookedByDay['20170427'].length
                          : 0;
                const d2 =
                      analyst.bookedByDay &&
                      Object.hasOwnProperty.call(
                          analyst.bookedByDay,
                          '20170428'
                      )
                          ? analyst.bookedByDay['20170428'].length
                          : 0;
                acc.push({
                    name: `${analyst.firstName} ${analyst.lastName}`,
                    d1,
                    d2,
                });
                return acc;
            }, []);

        return <AnalystChart data={data} loading={loading} />;
    }
}

function mapStateToProps(state, ownProps) {
    return {
        analysts: state.analysts,
        easybook: state.easybook,
        supportConsultations: state.supportConsultations,
        analystsBookedSlotsByDay: getAnalystsBookedSlotsByDay(
            state.analysts,
            state.supportConsultations
        ),
    };
}

const AnalystChartConnected = connect(mapStateToProps, {
    fetchAnalysts,
    listenToAuth,
    listenToUsedAvailability,
    listenToEasyBook,
})(Container);

export default AnalystChartConnected;
