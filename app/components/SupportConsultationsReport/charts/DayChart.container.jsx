import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from '../../util/load-moment-tz';
import DayChart from './DayChart';
import { fetchAnalysts } from '../../ChooseAnalyst/actions';
import { getAllAppointmentsGroupedByDay } from '../../ChooseAnalyst/reducer';
import { listenToAuth } from '../../firebase/auth.actions';
import { listenToUsedAvailability } from '../../firebase/schedule.support.actions';
import { listenToEasyBook } from '../../firebase/easybook.actions';

class Container extends React.Component {
    static propTypes = {
        analysts: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        // eslint-disable-next-line react/forbid-prop-types
        appointmentsGroupedByDay: PropTypes.object,
        fetchAnalysts: PropTypes.func,
        listenToUsedAvailability: PropTypes.func,
        listenToAuth: PropTypes.func,
        listenToEasyBook: PropTypes.func,
        supportConsultations: PropTypes.shape({
            hasEverLoadedUsedAvailability: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        easybook: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
    };

    componentDidMount() {
        if (
            this.props.analysts.hasEverLoaded === false &&
            this.props.analysts.fetching === false
        ) {
            this.props.fetchAnalysts();
        }
        this.props.listenToAuth().then(user => {
            this.props.listenToUsedAvailability();
            this.props.listenToEasyBook();
        });
    }

    render() {
        let loading = false;
        if (
            !this.props.supportConsultations.hasEverLoadedUsedAvailability ||
            !this.props.easybook.hasEverLoaded
        ) {
            loading = 'Loading Data...';
        }
        if (
            this.props.analysts.hasEverLoaded === false ||
            this.props.analysts.fetching
        ) {
            loading = 'Loading Analysts...';
        }

        const data = loading
            ? []
            : Object.keys(this.props.appointmentsGroupedByDay)
                .sort()
                .reduce((acc, day) => {
                    const name = moment
                        .tz(day, 'YYYYMMDD', 'America/New_York')
                        .format('dddd');
                    acc.push({
                        name,
                        value: this.props.appointmentsGroupedByDay[day]
                            .length,
                    });
                    return acc;
                }, []);
        return <DayChart data={data} loading={loading} />;
    }
}

function mapStateToProps(state, ownProps) {
    return {
        analysts: state.analysts,
        easybook: state.easybook,
        supportConsultations: state.supportConsultations,
        appointmentsGroupedByDay: getAllAppointmentsGroupedByDay(
            state.analysts,
            state.supportConsultations
        ),
    };
}

const DayChartConnected = connect(mapStateToProps, {
    fetchAnalysts,
    listenToAuth,
    listenToUsedAvailability,
    listenToEasyBook,
})(Container);

export default DayChartConnected;
