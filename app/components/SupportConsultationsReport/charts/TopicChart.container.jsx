import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TopicChart from './TopicChart';
import { fetchAnalysts } from '../../ChooseAnalyst/actions';
import { getAppointmentsGroupedByTopic } from '../../ChooseAnalyst/reducer';
import { listenToAuth } from '../../firebase/auth.actions';
import { listenToUsedAvailability } from '../../firebase/schedule.support.actions';
import { listenToEasyBook } from '../../firebase/easybook.actions';

class Container extends React.Component {
    static propTypes = {
        analysts: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        // eslint-disable-next-line react/forbid-prop-types
        appointmentsGroupedByTopic: PropTypes.object,
        supportConsultations: PropTypes.shape({
            hasEverLoadedUsedAvailability: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        easybook: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        fetchAnalysts: PropTypes.func,
        listenToUsedAvailability: PropTypes.func,
        listenToAuth: PropTypes.func,
        listenToEasyBook: PropTypes.func,
    };

    componentDidMount() {
        if (
            this.props.analysts.hasEverLoaded === false &&
            this.props.analysts.fetching === false
        ) {
            this.props.fetchAnalysts();
        }
        this.props.listenToAuth().then(user => {
            this.props.listenToUsedAvailability();
            this.props.listenToEasyBook();
        });
    }

    render() {
        let loading = false;
        if (
            !this.props.supportConsultations.hasEverLoadedUsedAvailability ||
            !this.props.easybook.hasEverLoaded
        ) {
            loading = 'Loading Data...';
        }
        if (
            this.props.analysts.hasEverLoaded === false ||
            this.props.analysts.fetching
        ) {
            loading = 'Loading Analysts...';
        }
        const data = loading
            ? []
            : Object.keys(
                this.props.appointmentsGroupedByTopic
            ).reduce((acc, topic) => {
                acc.push({
                    topic,
                    A: this.props.appointmentsGroupedByTopic[topic].length,
                });
                return acc;
            }, []);
        return <TopicChart data={data} loading={loading} />;
    }
}

function mapStateToProps(state, ownProps) {
    return {
        analysts: state.analysts,
        easybook: state.easybook,
        supportConsultations: state.supportConsultations,
        appointmentsGroupedByTopic: getAppointmentsGroupedByTopic(
            state.analysts,
            state.supportConsultations
        ),
    };
}

const TopicChartConnected = connect(mapStateToProps, {
    fetchAnalysts,
    listenToAuth,
    listenToUsedAvailability,
    listenToEasyBook,
})(Container);

export default TopicChartConnected;
