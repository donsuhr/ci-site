import React from 'react';
import PropTypes from 'prop-types';
import {
    ResponsiveContainer,
    Bar,
    BarChart,
    Legend,
    XAxis,
    YAxis,
    Tooltip,
    CartesianGrid,
} from 'recharts';
import Loading from '../../Loading';

const AnalystChart = ({ data, loading }) => {
    if (loading) {
        return (
            <div className="analysts-chart">
                <Loading>{loading}</Loading>
            </div>
        );
    }
    return (
        <div className="analysts-chart">
            <ResponsiveContainer>
                <BarChart data={data} layout="vertical">
                    <XAxis type="number" />
                    <YAxis
                        dataKey="name"
                        type="category"
                        interval={0}
                        width={140}
                    />
                    <CartesianGrid strokeDasharray="3 3" />
                    <Tooltip />
                    <Legend />
                    <Bar
                        dataKey="d1"
                        stackId="a"
                        fill="#78A22F"
                        name="Thursday"
                        maxBarSize={30}
                    />
                    <Bar
                        dataKey="d2"
                        stackId="a"
                        fill="#00468A"
                        name="Friday"
                        maxBarSize={30}
                    />
                </BarChart>
            </ResponsiveContainer>
        </div>
    );
};

AnalystChart.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    data: PropTypes.array,
    loading: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
    ]),
};

export default AnalystChart;
