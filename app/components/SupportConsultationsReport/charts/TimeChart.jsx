import React from 'react';
import PropTypes from 'prop-types';
import {
    ResponsiveContainer,
    Line,
    LineChart,
    XAxis,
    YAxis,
    Tooltip,
    CartesianGrid,
    Legend,
} from 'recharts';
import Loading from '../../Loading';

const colors = ['#78A22F', '#00468A'];

const TimeChart = ({ data, days, loading }) => {
    if (loading) {
        return (
            <div className="analysts-chart">
                <Loading>{loading}</Loading>
            </div>
        );
    }
    return (
        <div className="time-chart">
            <ResponsiveContainer>
                <LineChart data={data}>
                    <XAxis dataKey="name" />
                    <YAxis />
                    <CartesianGrid strokeDasharray="3 3" />
                    <Tooltip />
                    <Legend />
                    {[...days].map((day, index) => (
                        <Line
                            key={day}
                            type="monotone"
                            dataKey={day}
                            stroke={colors[index]}
                        />
                    ))}
                </LineChart>
            </ResponsiveContainer>
        </div>
    );
};

TimeChart.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    data: PropTypes.array,
    // eslint-disable-next-line react/forbid-prop-types
    days: PropTypes.object,
    loading: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
    ]),
};

export default TimeChart;
