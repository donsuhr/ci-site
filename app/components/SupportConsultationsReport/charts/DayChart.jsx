import React from 'react';
import PropTypes from 'prop-types';
import {
    ResponsiveContainer,
    Pie,
    PieChart,
    Legend,
    Cell,
    Tooltip,
} from 'recharts';
import Loading from '../../Loading';

const colors = ['#78A22F', '#00468A'];

const DayChart = ({ data, loading }) => {
    if (loading) {
        return (
            <div className="day-chart">
                <Loading>{loading}</Loading>
            </div>
        );
    }
    return (
        <div className="day-chart">
            <ResponsiveContainer>
                <PieChart>
                    <Pie data={data} fill="#78A22F" label>
                        {data.map((entry, index) => (
                            <Cell
                                key={`cell-${entry.name}`}
                                fill={colors[index]}
                            />
                        ))}
                    </Pie>
                    <Legend />
                    <Tooltip />
                </PieChart>
            </ResponsiveContainer>
        </div>
    );
};

DayChart.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    data: PropTypes.array,
    loading: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
    ]),
};
export default DayChart;
