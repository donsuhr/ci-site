/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ResetPasswordForm from './ResetPasswordForm';

describe('EmailSignin ResetPasswordForm', () => {
    it('renders', () => {
        const wrapper = shallow(<ResetPasswordForm />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('calls set mode', () => {
        const setMode = jest.fn();
        const wrapper = shallow(<ResetPasswordForm setMode={setMode} />);

        expect(setMode).not.toHaveBeenCalled();
        wrapper.find('.cmc-form__submit-button--back').simulate('click', {
            preventDefault() {},
        });
        expect(setMode).toHaveBeenCalled();
    });
});
