import $ from 'jquery';
import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'formsy-react';
import MyInput from './Input';
import Loading from '../Loading';
import createFirebaseApp from '../../scripts/firebase';

class LoginForm extends React.Component {
    static propTypes = {
        destroy: PropTypes.func,
        setMode: PropTypes.func,
        year: PropTypes.string,
    };

    state = {
        canSubmit: false,
        signinError: '',
        loading: false,
    };

    componentDidMount() {
        $('.account-modal .modal-title').text('Login');
        $('.account-modal [name="email"]').trigger('focus');
    }

    submit = data => {
        this.setState({
            loading: true,
        });
        const firebaseAuth = createFirebaseApp(this.props.year).auth;
        firebaseAuth
            .signInWithEmailAndPassword(data.email, data.password)
            .then(() => {
                this.setState({
                    loading: false,
                });
                const $AccountModal = $(`#AccountModal-${this.props.year}`);
                if ($AccountModal.data('inpromise') !== true) {
                    this.props.setMode('loginsuccess');
                    window.location.href = '/my-conference/';
                } else {
                    this.props.destroy();
                }
            })
            .catch(error => {
                const translate = {
                    'auth/user-not-found': 'Incorrect username or password',
                    'auth/user-disabled': 'Account disabled',
                    'auth/wrong-password': 'Incorrect username or password',
                    'auth/invalid-email': 'Invalid Email address',
                };
                this.setState({
                    signinError: translate[error.code] || 'Unknown Error',
                    loading: false,
                });
            });
    };

    enableButton = () => {
        this.setState({ canSubmit: true });
    };

    disableButton = () => {
        this.setState({ canSubmit: false });
    };

    render() {
        const { setMode, destroy } = this.props;
        return (
            <Form
                className="cmc-form cmc-form--centered cmc-form--login"
                onSubmit={this.submit}
                onValid={this.enableButton}
                onInvalid={this.disableButton}
            >
                <p>Log in or sign up with your Email.</p>
                <p className="signin-error">{this.state.signinError}</p>
                {this.state.loading && <Loading>Loading...</Loading>}
                <fieldset>
                    <ul>
                        <li>
                            <MyInput
                                name="email"
                                title="Email"
                                validations="isEmail"
                                validationError="This is not a valid email"
                                required
                            />
                        </li>
                        <li>
                            <MyInput
                                name="password"
                                title="Password"
                                type="password"
                                required
                            />
                        </li>
                        <li className="cmc-form__submit-row cmc-form__submit-row--login">
                            <button
                                className="cmc-form__submit-button cmc-form__submit-button--login"
                                type="submit"
                                disabled={!this.state.canSubmit}
                            >
                                Login
                            </button>
                            <button
                                type="button"
                                className="cmc-form__submit-button cmc-form__create-account-link"
                                onClick={event => {
                                    event.preventDefault();
                                    setMode('signup');
                                }}
                            >
                                Create Account
                            </button>
                        </li>
                        <li>
                            <button
                                type="button"
                                className="cmc-form__back-link login-form__back-btn"
                                onClick={destroy}
                            >
                                Back
                            </button>
                            <button
                                type="button"
                                className="cmc-form__forgot-password-link"
                                onClick={event => {
                                    event.preventDefault();
                                    setMode('forgot');
                                }}
                            >
                                Forgot Password
                            </button>
                        </li>
                    </ul>
                </fieldset>
            </Form>
        );
    }
}

export default LoginForm;
