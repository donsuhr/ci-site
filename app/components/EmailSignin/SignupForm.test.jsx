/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SignupForm from './SignupForm';

describe('EmailSignin SignupForm', () => {
    it('renders', () => {
        const wrapper = shallow(<SignupForm />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('calls set mode', () => {
        const setMode = jest.fn();
        const wrapper = shallow(<SignupForm setMode={setMode} />);

        expect(setMode).not.toHaveBeenCalled();
        wrapper.find('.cmc-form__submit-button--back').simulate('click', {
            preventDefault() {},
        });
        expect(setMode).toHaveBeenCalled();
    });
});
