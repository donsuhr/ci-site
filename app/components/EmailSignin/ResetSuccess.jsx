import React from 'react';
import PropTypes from 'prop-types';

const ResetSuccess = ({ setMode }) => (
    <div className="cmc-form cmc-form--centered cmc-form--login">
        <p>Email Sent. After clicking the link in the email, click continue to log in.</p>
        <fieldset>
            <ul>
                <li className="cmc-form__submit-row cmc-form__submit-row--signup">
                    <button
                        type="button"
                        className="cmc-form__submit-button"
                        onClick={(event) => {
                            event.preventDefault();
                            setMode('login');
                        }}
                    >
                        Continue
                    </button>
                </li>
            </ul>
        </fieldset>
    </div>
);

ResetSuccess.propTypes = {
    setMode: PropTypes.func,
};

export default ResetSuccess;
