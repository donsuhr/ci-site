import React from 'react';
import Loading from '../Loading';

const LoginSuccess = () => (
    <div className="cmc-form cmc-form--centered cmc-form--login">
        <Loading>Loading...</Loading>
    </div>
);

export default LoginSuccess;
