/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import LoginForm from './LoginForm';

describe('EmailSignin ResetPasswordForm', () => {
    it('renders', () => {
        const wrapper = shallow(<LoginForm />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('calls destroy', () => {
        const destroy = jest.fn();
        const wrapper = shallow(<LoginForm destroy={destroy} />);

        expect(destroy).not.toHaveBeenCalled();
        wrapper.find('.login-form__back-btn').simulate('click', {
            preventDefault() {
            },
        });
        expect(destroy).toHaveBeenCalled();
    });

    it('calls set mode', () => {
        const setMode = jest.fn();
        const wrapper = shallow(<LoginForm setMode={setMode} />);

        expect(setMode).not.toHaveBeenCalled();
        wrapper.find('.cmc-form__forgot-password-link').simulate('click', {
            preventDefault() {
            },
        });
        expect(setMode).toHaveBeenCalled();
    });
});
