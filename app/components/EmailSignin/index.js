import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer as ReactHotLoaderContainer } from 'react-hot-loader';
import App from './App';

let mountNode;
let domAttributes;

const render = Component => {
    ReactDOM.render(
        React.createElement(
            ReactHotLoaderContainer,
            {},
            React.createElement(App, { mountNode, ...domAttributes })
        ),
        mountNode
    );
};

export default {
    config(attrs, el) {
        mountNode = el;
        domAttributes = attrs;
        render(App);
    },
};

if (process.env.NODE_ENV !== 'production') {
    if (module.hot) {
        module.hot.accept('./App', () => {
            render(App);
        });
    }
}

