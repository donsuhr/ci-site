import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import LoginForm from './LoginForm';
import SignupForm from './SignupForm';
import ForgotPasswordForm from './ForgotPasswordForm';
import ResetSuccess from './ResetSuccess';
import LoginSuccess from './LoginSuccess';

class App extends React.Component {
    static propTypes = {
        mountNode: PropTypes.object, // eslint-disable-line react/forbid-prop-types
        year: PropTypes.string,
    };

    state = {
        mode: 'login',
    };

    setMode = mode => {
        this.setState({ mode });
    };

    destroy = event => {
        if (event) {
            event.preventDefault();
        }
        setTimeout(() => {
            ReactDOM.unmountComponentAtNode(this.props.mountNode);
        });
    };

    render() {
        const { mode } = this.state;
        return (
            <div>
                {mode === 'login' ? (
                    <LoginForm
                        year={this.props.year}
                        setMode={this.setMode}
                        destroy={this.destroy}
                    />
                ) : (
                    ''
                )}
                {mode === 'signup' ? (
                    <SignupForm
                        year={this.props.year}
                        setMode={this.setMode}
                        destroy={this.destroy}
                    />
                ) : (
                    ''
                )}
                {mode === 'forgot' ? (
                    <ForgotPasswordForm
                        year={this.props.year}
                        setMode={this.setMode}
                    />
                ) : (
                    ''
                )}
                {mode === 'resetSuccess' ? (
                    <ResetSuccess setMode={this.setMode} />
                ) : (
                    ''
                )}
                {mode === 'loginsuccess' ? <LoginSuccess /> : ''}
            </div>
        );
    }
}

export default App;
