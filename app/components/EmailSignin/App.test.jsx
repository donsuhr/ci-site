/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import App from './App';

describe('EmailSignin App', () => {
    it('renders login', () => {
        const wrapper = shallow(<App year="2018" />);
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('LoginForm')).toHaveLength(1);
    });

    it('renders signup', () => {
        const wrapper = shallow(<App year="2018" />);
        wrapper.instance().setMode('signup');
        wrapper.update();
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('SignupForm')).toHaveLength(1);
    });

    it('renders forgot', () => {
        const wrapper = shallow(<App year="2018" />);
        wrapper.instance().setMode('forgot');
        wrapper.update();
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('ForgotPasswordForm')).toHaveLength(1);
    });

    it('renders resetSuccess', () => {
        const wrapper = shallow(<App year="2018" />);
        wrapper.instance().setMode('resetSuccess');
        wrapper.update();
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('ResetSuccess')).toHaveLength(1);
    });

    it('renders loginsuccess', () => {
        const wrapper = shallow(<App year="2018" />);
        wrapper.instance().setMode('loginsuccess');
        wrapper.update();
        expect(toJson(wrapper)).toMatchSnapshot();
        expect(wrapper.find('LoginSuccess')).toHaveLength(1);
    });
});
