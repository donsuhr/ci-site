/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ResetSuccess from './ResetSuccess';

describe('EmailSignin ResetPasswordForm', () => {
    it('renders', () => {
        const wrapper = shallow(<ResetSuccess />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('calls set mode', () => {
        const setMode = jest.fn();
        const wrapper = shallow(<ResetSuccess setMode={setMode} />);
        expect(setMode).not.toHaveBeenCalled();
        wrapper.find('.cmc-form__submit-button').simulate('click', {
            preventDefault() {},
        });
        expect(setMode).toHaveBeenCalled();
    });
});
