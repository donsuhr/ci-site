/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import LoginSuccess from './LoginSuccess';

describe('EmailSignin LoginSuccess', () => {
    it('renders', () => {
        const wrapper = shallow(<LoginSuccess />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
