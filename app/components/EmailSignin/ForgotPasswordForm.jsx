import React from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';
import { Form } from 'formsy-react';
import MyInput from './Input';
import Loading from '../Loading';
import createFirebaseApp from '../../scripts/firebase';

class ForgotPasswordForm extends React.Component {
    static propTypes = {
        setMode: PropTypes.func,
        year: PropTypes.string,
    };

    state = {
        canSubmit: false,
        signinError: '',
        loading: false,
    };

    componentDidMount() {
        $('.account-modal .modal-title').text('Forgot Password');
        $('.account-modal [name="email"]').trigger('focus');
    }

    submit = data => {
        this.setState({
            loading: true,
        });
        const firebaseAuth = createFirebaseApp(this.props.year).auth;
        firebaseAuth
            .sendPasswordResetEmail(data.email)
            .then(() => {
                this.setState({
                    loading: false,
                });
                this.props.setMode('resetSuccess');
            })
            .catch(error => {
                this.setState({
                    signinError: error.message,
                    loading: false,
                });
            });
    };

    enableButton = () => {
        this.setState({ canSubmit: true });
    };

    disableButton = () => {
        this.setState({ canSubmit: false });
    };

    render() {
        const { setMode } = this.props;
        return (
            <Form
                className="cmc-form cmc-form--centered cmc-form--login"
                onSubmit={this.submit}
                onValid={this.enableButton}
                onInvalid={this.disableButton}
            >
                <p>
                    Complete the form below to receive password reset
                    instructions.
                </p>
                <p className="signin-error">{this.state.signinError}</p>
                {this.state.loading && <Loading>Loading...</Loading>}
                <fieldset>
                    <ul>
                        <li>
                            <MyInput
                                name="email"
                                title="Email"
                                validations="isEmail"
                                validationError="This is not a valid email"
                                required
                            />
                        </li>
                        <li className="cmc-form__submit-row cmc-form__submit-row--signup">
                            <button
                                className="cmc-form__submit-button "
                                type="submit"
                                disabled={!this.state.canSubmit}
                            >
                                Send Reset Email
                            </button>
                            <button
                                type="button"
                                className="cmc-form__submit-button cmc-form__submit-button--back"
                                onClick={event => {
                                    event.preventDefault();
                                    setMode('login');
                                }}
                            >
                                Back
                            </button>
                        </li>
                    </ul>
                </fieldset>
            </Form>
        );
    }
}

export default ForgotPasswordForm;
