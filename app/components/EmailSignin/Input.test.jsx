/* global it,expect,describe */

import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Form } from 'formsy-react';
import MyInput from './Input';

describe('EmailSignin MyInput', () => {
    it('renders', () => {
        const wrapper = mount(
            <Form>
                <MyInput
                    className="classname"
                    title="title"
                    type="email"
                    name="name"
                />
            </Form>
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
