import PropTypes from 'prop-types';
import { combineReducers } from 'redux';
import sortBy from 'lodash/sortBy';
import moment from '../util/load-moment-tz';
import * as actions from './actions';

const defaultState = {
    savingStatus: '',
};

const item = (state = defaultState, action, currentItem) => {
    switch (action.type) {
        case actions.RECEIVE_PCTS:
            return {
                ...state,
                ...currentItem,
                savingStatus: '',
            };

        default:
            return state;
    }
};

export const byId = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_PCTS:
            return action.items.reduce((p, c) => {
                p[c._id] = item(state[c._id], action, c);
                return p;
            }, {});

        default:
            return state;
    }
};

export const uniqueTracks = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_PCTS:
            return action.items.reduce((acc, session) => {
                if (Object.hasOwnProperty.call(session, 'track')) {
                    const { track } = session;
                    if (!Object.hasOwnProperty.call(acc, track.title)) {
                        acc[track.title] = {
                            count: 0,
                            title: track.title,
                        };
                    }
                    acc[track.title].count += 1;
                }
                return acc;
            }, {});
        default:
            return state;
    }
};

export const fetching = (state = false, action) => {
    switch (action.type) {
        case actions.REQUEST_PCTS:
            return true;
        case actions.RECEIVE_PCTS:
        case actions.REQUEST_PCTS_ERROR:
            return false;
        default:
            return state;
    }
};

export const loadStatus = (state = '', action) => {
    switch (action.type) {
        case actions.REQUEST_PCTS_ERROR:
            return action.error;
        case actions.RECEIVE_PCTS:
            return 'success';
        default:
            return state;
    }
};

export const hasEverLoaded = (state = false, action) => {
    switch (action.type) {
        case actions.RECEIVE_PCTS:
        case actions.REQUEST_PCTS_ERROR:
            return true;
        default:
            return state;
    }
};

const pct = combineReducers({
    fetching,
    byId,
    loadStatus,
    uniqueTracks,
    hasEverLoaded,
});

export default pct;

export function getPctById(state, id) {
    return state.byId[id];
}

export function groupPctByDay(state) {
    if (!state) {
        return {};
    }
    return Object.keys(state).reduce((acc, key) => {
        const x = state[key];
        const start = moment(x.start)
            .tz('America/New_York')
            .format('YYYYMMDD');
        if (!Object.hasOwnProperty.call(acc, start)) {
            acc[start] = [];
        }
        acc[start].push(x);
        return acc;
    }, {});
}

export function getPctByTrack(state, trackName) {
    if (!state || !trackName) {
        return [];
    }
    const items = Object.keys(state).reduce((acc, key) => {
        if (state[key].track.title === trackName) {
            acc.push(state[key]);
        }
        return acc;
    }, []);
    return sortBy(items, x => x.start);
}

export function getPctByTrackByDay(state, trackName) {
    const items = getPctByTrack(state, trackName);
    if (!items) {
        return {};
    }
    return items.reduce((acc, x) => {
        const start = moment(x.start)
            .tz('America/New_York')
            .format('dddd');
        if (!Object.hasOwnProperty.call(acc, start)) {
            acc[start] = [];
        }
        acc[start].push(x);
        return acc;
    }, {});
}

export function countPctSignups(state, allUsersState) {
    const ret = {};
    Object.values(allUsersState.byId).forEach(user => {
        if (Object.hasOwnProperty.call(user, 'schedule')) {
            if (Object.hasOwnProperty.call(user.schedule, 'pct')) {
                Object.values(user.schedule.pct).forEach(scheduleItem => {
                    if (
                        Object.hasOwnProperty.call(
                            state.byId,
                            scheduleItem.pctId
                        )
                    ) {
                        if (
                            !Object.hasOwnProperty.call(ret, scheduleItem.pctId)
                        ) {
                            ret[scheduleItem.pctId] = {
                                title: state.byId[scheduleItem.pctId].title,
                                count: 0,
                            };
                        }
                        ret[scheduleItem.pctId].count += 1;
                    }
                });
            }
        }
    });
    return ret;
}

export function countPctTopicSignups(state, allUsersState) {
    const ret = {};
    Object.values(allUsersState.byId).forEach(user => {
        if (Object.hasOwnProperty.call(user, 'schedule')) {
            if (Object.hasOwnProperty.call(user.schedule, 'pct')) {
                Object.values(user.schedule.pct).forEach(scheduleItem => {
                    if (
                        Object.hasOwnProperty.call(
                            state.byId,
                            scheduleItem.pctId
                        )
                    ) {
                        const { title } = state.byId[scheduleItem.pctId].track;
                        if (!Object.hasOwnProperty.call(ret, title)) {
                            ret[title] = {
                                title: state.byId[scheduleItem.pctId].title,
                                count: 0,
                            };
                        }
                        ret[title].count += 1;
                    }
                });
            }
        }
    });
    return ret;
}

export const PropType = PropTypes.shape({
    hasEverLoaded: PropTypes.bool,
    fetching: PropTypes.bool,
    loadStatus: PropTypes.string,
    byId: PropTypes.object,
    uniqueTracks: PropTypes.object,
});
