import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { fetchPcts } from './actions';
import { groupPctByDay } from './reducer';

import ShowError from '../ShowError';
import PctBrowser from './PctBrowser';

import {
    listenToSchedulePct,
    addPctToSchedule,
    removePctFromSchedule,
} from '../firebase/schedule.actions';
import {
    listenToAuth,
    signOut,
    requestPopupAuth,
} from '../firebase/auth.actions';
import { isPctInSchedule, getItemStatus } from '../firebase/schedule.reducer';

export class PctBrowserContainer extends React.Component {
    static propTypes = {
        fetchPcts: PropTypes.func.isRequired,
        loadStatus: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
        listenToAuth: PropTypes.func,
        listenToSchedulePct: PropTypes.func,
        pct: PropTypes.shape({
            hasEverLoaded: PropTypes.bool,
            fetching: PropTypes.bool,
        }),
        user: PropTypes.shape({
            isAuth: PropTypes.bool,
            info: PropTypes.object,
        }),
    };

    componentDidMount() {
        this.props.listenToAuth();

        if (
            this.props.pct.hasEverLoaded === false &&
            this.props.pct.fetching === false
        ) {
            this.props.fetchPcts();
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.user.isAuth !== prevProps.user.isAuth) {
            if (this.props.user.isAuth) {
                this.props.listenToSchedulePct(this.props.user.info.uid);
            }
        }
    }

    render() {
        const { loadStatus } = this.props;
        const showLoadError =
            loadStatus && loadStatus !== 'success' && loadStatus !== '';
        return (
            <div>
                <PctBrowser {...this.props} />
                {showLoadError && <ShowError error={loadStatus} />}
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        sessionsGroupedByDay: groupPctByDay(state.pct.byId),
        dataById: state.pct.byId,
        fetching: state.pct.fetching,
        loadStatus: state.pct.loadStatus,
        requestPopupAuth,
        pct: state.pct,
        isPctInSchedule: isPctInSchedule.bind(null, state.schedule),
        getItemStatus: getItemStatus.bind(null, state.schedule),
        user: state.user,
    };
}

const PctBrowserConnected = connect(mapStateToProps, {
    fetchPcts,
    listenToAuth,
    addPctToSchedule,
    removePctFromSchedule,
    listenToSchedulePct,
    signOut,
})(PctBrowserContainer);

export default PctBrowserConnected;
