import React from 'react';
import PropTypes from 'prop-types';
import Loading from '../Loading';
import Day from './Day';

const PctBrowser = ({ fetching, sessionsGroupedByDay, ...props }) => {
    const daysOrdered = Object.keys(sessionsGroupedByDay).sort();
    return (
        <div>
            {fetching && <Loading>Loading...</Loading>}

            {daysOrdered.map((x, i) => (
                <Day key={x} daySessions={sessionsGroupedByDay[x]} {...props} />
            ))}
        </div>
    );
};

PctBrowser.propTypes = {
    fetching: PropTypes.bool.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    sessionsGroupedByDay: PropTypes.object,
};

export default PctBrowser;
