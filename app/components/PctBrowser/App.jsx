import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import ReduxDevToolsLoader from '../redux/ReduxDevToolsLoader';
import PctBrowserConnected from './PctBrowser.container';

const App = ({ store, ...props }) => (
    <Provider store={store}>
        <div>
            <PctBrowserConnected {...props} />
            <ReduxDevToolsLoader />
        </div>
    </Provider>
);

App.propTypes = {
    /* eslint-disable react/forbid-prop-types */
    store: PropTypes.object.isRequired,
};

export default App;
