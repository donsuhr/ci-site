/* global it,expect,describe,afterEach */
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import * as actions from './actions';
import config from '../../../config';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('PctBrowser actions', () => {
    it('should create an action to request sessions ', () => {
        const expectedAction = {
            type: actions.REQUEST_PCTS,
        };
        expect(actions.requestPcts()).toEqual(expectedAction);
    });

    it('should create an action to receive sessions', () => {
        const items = { data: [] };
        const expectedAction = {
            type: actions.RECEIVE_PCTS,
            items: items.data,
        };
        expect(actions.receivePcts(items)).toEqual(expectedAction);
    });

    it('should create an action for session request error', () => {
        const error = {};
        const expectedAction = {
            type: actions.REQUEST_PCTS_ERROR,
            error,
        };
        expect(actions.requestPctsError(error)).toEqual(expectedAction);
    });
    describe('async actions', () => {
        afterEach(() => {
            nock.cleanAll();
        });

        it('should create REQUEST_PCTS action to fetch sessions', () => {
            const returnData = {
                data: [{ session: {} }],
            };
            nock.disableNetConnect();
            nock(`${config.domains.api}/`)
                .get('/campusInsight/ci/pct/')
                .query(true)
                .reply(200, returnData);

            const expectedActions = [
                { type: actions.REQUEST_PCTS },
                { type: actions.RECEIVE_PCTS, items: returnData.data },
            ];
            const store = mockStore({ sessions: [] });
            return store.dispatch(actions.fetchPcts()).then(() => {
                // return of async actions
                expect(store.getActions()).toEqual(expectedActions);
            });
        });

        it(('should change the endpoint if there is a year'), () => {
            const returnData = {
                data: [{ session: {} }],
            };
            const scope = nock(`${config.domains.api}/`)
                .get('/campusInsight/ci/pct/2017/')
                .query(true)
                .reply(200, returnData);

            const store = mockStore({ sessions: [], domAttributes: { year: '2017' } });
            return store.dispatch(actions.fetchPcts()).then(() => {
                // return of async actions
                expect(scope.isDone()).toBe(true);
            });
        });
    });
});
