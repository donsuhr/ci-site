/* global it,expect,describe */
import {
    hasEverLoaded,
    fetching,
    byId,
    loadStatus,
    uniqueTracks,
} from './reducer';
import * as actions from './actions';

describe('PctBrowser reducer', () => {
    describe('hasEverLoaded', () => {
        it('should return the initial state', () => {
            expect(hasEverLoaded(undefined, {})).toEqual(false);
        });
        it('should handle RECEIVE_PCTS', () => {
            expect(
                hasEverLoaded(false, {
                    type: actions.RECEIVE_PCTS,
                })
            ).toEqual(true);
        });
        it('should handle REQUEST_PCTS_ERROR', () => {
            expect(
                hasEverLoaded(false, {
                    type: actions.REQUEST_PCTS_ERROR,
                })
            ).toEqual(true);
        });
    });
    describe('fetching', () => {
        it('should return the initial state', () => {
            expect(fetching(undefined, {})).toEqual(false);
        });
        it('should handle REQUEST_PCTS', () => {
            expect(
                fetching(false, {
                    type: actions.REQUEST_PCTS,
                })
            ).toEqual(true);
        });
        it('should handle RECEIVE_PCTS', () => {
            expect(
                fetching(true, {
                    type: actions.RECEIVE_PCTS,
                })
            ).toEqual(false);
        });
        it('should handle REQUEST_PCTS_ERROR', () => {
            expect(
                fetching(true, {
                    type: actions.REQUEST_PCTS_ERROR,
                })
            ).toEqual(false);
        });
    });
    describe('byId', () => {
        it('should return the initial state', () => {
            expect(byId(undefined, {})).toEqual({});
        });
        it('should handle RECEIVE_PCTS', () => {
            let state = { 123: { foo: 'bar' } };
            const action = {
                type: actions.RECEIVE_PCTS,
                items: [{ _id: '123' }],
            };
            expect(byId(state, action)).toEqual({
                123: { _id: '123', savingStatus: '', foo: 'bar' },
            });
            state = {};
            expect(byId(state, action)).toEqual({
                123: { _id: '123', savingStatus: '' },
            });
        });
    });
    describe('loadStatus', () => {
        it('should return the initial state', () => {
            expect(loadStatus(undefined, {})).toEqual('');
        });
        it('should handle REQUEST_PCTS_ERROR', () => {
            const action = {
                type: actions.REQUEST_PCTS_ERROR,
                error: new Error('message'),
            };
            expect(loadStatus('', action)).toEqual(action.error);
        });
        it('should handle RECEIVE_PCTS', () => {
            const action = {
                type: actions.RECEIVE_PCTS,
            };
            expect(loadStatus('', action)).toEqual('success');
        });
    });
    describe('uniqueTracks', () => {
        it('should return the initial state', () => {
            expect(uniqueTracks(undefined, {})).toEqual({});
        });
        it('should handle RECEIVE_PCTS', () => {
            const action = {
                type: actions.RECEIVE_PCTS,
                items: [
                    {
                        track: {
                            title: 'title one',
                        },
                    },
                    {
                        track: {
                            title: 'title one',
                        },
                    },
                    { track: { title: 'title two' } },
                ],
            };
            expect(uniqueTracks({}, action)).toEqual({
                'title one': { count: 2, title: 'title one' },
                'title two': { count: 1, title: 'title two' },
            });
        });
    });
});
