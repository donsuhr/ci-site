import { combineReducers } from 'redux';
import pct from './reducer';
import schedule from '../firebase/schedule.reducer';
import user from '../firebase/auth.reducer';
import listeners from '../firebase/listeners.reducer';
import domAttributes from '../redux/domAttributes.reducers';

export default combineReducers({
    pct,
    schedule,
    user,
    listeners,
    domAttributes,
});
