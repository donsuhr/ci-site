import React from 'react';
import PropTypes from 'prop-types';
import sortBy from 'lodash/sortBy';
import moment from '../util/load-moment-tz';
import Session from './Session';

const Day = ({ daySessions, ...props }) => {
    const sorted = sortBy(daySessions, ['start', 'end', 'title']);
    return (
        <div>
            <h2 className="pct-browser__day-title">
                {moment(sorted[0].start)
                    .tz('America/New_York')
                    .format('dddd, MMMM Do')}
            </h2>
            <ul>
                {sorted.map(x => <Session key={x.id} session={x} {...props} />)}
            </ul>
        </div>
    );
};

Day.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    daySessions: PropTypes.array,
};

export default Day;
