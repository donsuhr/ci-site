/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Day from './Day';

const daySessions = [
    {
        id: '58fe6ddbcb77715e22b476cc',
        start: '2017-04-27T18:00:00.000Z',
        end: '2017-04-27T18:00:00.000Z',
        title: 'title 1',
    },
    {
        id: '5830d7913aa27a048063d270',
        end: '2017-04-27T19:00:00.000Z',
        start: '2017-04-27T19:00:00.000Z',
        title: 'title 2',
    },
];

describe('PctBrowser Day', () => {
    it('renders', () => {
        const wrapper = shallow(<Day daySessions={daySessions} />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders a title', () => {
        const wrapper = shallow(<Day daySessions={daySessions} />);
        expect(wrapper.find('.pct-browser__day-title').text()).toContain(
            'Thursday, April 27th'
        );
    });
    it('renders two sessions', () => {
        const wrapper = shallow(<Day daySessions={daySessions} />);
        expect(wrapper.find('Session')).toHaveLength(2);
    });
});
