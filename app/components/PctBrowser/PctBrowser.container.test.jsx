/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { PctBrowserContainer } from './PctBrowser.container';

describe('PctBrowser PctBrowser.container', () => {
    it('renders', () => {
        const wrapper = shallow(
            <PctBrowserContainer
                fetchPcts={jest.fn()}
                loadStatus="success"
                fetching={false}
            />
        );
        expect(wrapper.find('PctBrowser')).toHaveLength(1);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders a load Error', () => {
        const wrapper = shallow(
            <PctBrowserContainer
                fetchPcts={jest.fn()}
                loadStatus={{
                    message: 'error',
                }}
                fetching={false}
            />
        );
        expect(wrapper.find('ShowError')).toHaveLength(1);
    });
});
