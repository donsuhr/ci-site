import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from '../util/load-moment-tz';
import {
    REQUEST_CHILD_ADD,
    REQUEST_CHILD_REMOVE,
} from '../firebase/schedule.actions';

class Session extends React.Component {
    static propTypes = {
        session: PropTypes.shape({
            description: PropTypes.string,
            title: PropTypes.string,
            flagAsNew: PropTypes.bool,
        }),
        getItemStatus: PropTypes.func,
        isPctInSchedule: PropTypes.func,
        removePctFromSchedule: PropTypes.func,
        addPctToSchedule: PropTypes.func,
        user: PropTypes.shape({
            isAuth: PropTypes.bool,
            info: PropTypes.object,
        }),
    };

    state = {
        expanded: false,
    };

    expand = event => {
        event.preventDefault();
        this.setState({ expanded: !this.state.expanded });
    };

    render() {
        const item = this.props.session;
        const scheduleItem = this.props.isPctInSchedule(item.id);
        const status = this.props.getItemStatus(item.id);
        const addBtnTxt =
            status === REQUEST_CHILD_ADD ? 'Adding...' : 'Add to Schedule';
        const removeBtnTxt =
            status === REQUEST_CHILD_REMOVE
                ? 'Removing...'
                : 'Remove from Schedule';
        const ulButtonWrapperClassNames = classNames(
            'pct-browser__session-links',
            'cmc-article__link-list',
            'cmc-article__link-list--centered'
        );
        return (
            <li className="pct-browser__session">
                <h3 className="pct-browser__session-title">
                    {item.flagAsNew && <span>New!</span>}
                    {item.flagAsNew && ' \u2014 '}
                    {item.title}
                </h3>
                <div className="pct-browser__more-info-wrapper">
                    <button
                        type="button"
                        className="pct-browser__session-details-button"
                        onClick={this.expand}
                    >
                        {this.state.expanded ? 'Close' : 'Details'}
                    </button>
                </div>
                <div className="pct-browser__start-time">
                    <span className="pct-browser__label">Time</span>
                    {moment(item.start)
                        .tz('America/New_York')
                        .format('h:mm a')}&nbsp;-&nbsp;
                    {moment(item.end)
                        .tz('America/New_York')
                        .format('h:mm a')}
                </div>
                <div className="pct-browser__status">
                    <span className="pct-browser__label">Status</span>
                    {item.status ? item.status : ''}
                </div>
                <div className="pct-browser__track">
                    <span className="pct-browser__label">Track</span>
                    {item.track && item.track.title ? item.track.title : ''}
                </div>
                <div className="pct-browser__product">
                    <span className="pct-browser__label">Product</span>
                    {item.product && item.product.title
                        ? item.product.title
                        : ''}
                </div>

                <div className="pct-browser__level">
                    <span className="pct-browser__label">Level</span>
                    {item.level ? item.level : ''}
                </div>
                <div className="pct-browser__type">
                    <span className="pct-browser__label">Type</span>
                    {item.type ? item.type : ''}
                </div>
                <div className="pct-browser__session-details">
                    <div className="pct-browser__price">
                        <span className="pct-browser__label">Price</span>
                        {item.price ? `$${item.price}` : ''}
                    </div>
                    <div className="pct-browser__location">
                        <span className="pct-browser__label">Location</span>
                        {item.location && item.location.title
                            ? item.location.title
                            : ''}
                    </div>
                    <div className="pct-browser__description">
                        <span className="pct-browser__label">Description</span>
                        {
                            <span
                                // eslint-disable-next-line react/no-danger
                                dangerouslySetInnerHTML={{
                                    __html: item.description,
                                }}
                            />
                        }
                    </div>
                    <div className="pct-browser__objectives">
                        <span className="pct-browser__label">
                            Learning Objectives
                        </span>
                        {
                            <span
                                // eslint-disable-next-line react/no-danger
                                dangerouslySetInnerHTML={{
                                    __html: item.learningObjectives,
                                }}
                            />
                        }
                    </div>
                    <div className="pct-browser__target-audience">
                        <span className="pct-browser__label">
                            Target Audience
                        </span>
                        {
                            <span
                                // eslint-disable-next-line react/no-danger
                                dangerouslySetInnerHTML={{
                                    __html: item.targetAudience,
                                }}
                            />
                        }
                    </div>
                    <div className="pct-browser__prerequisites">
                        <span className="pct-browser__label">
                            Course Prerequisites
                        </span>
                        {
                            <span
                                // eslint-disable-next-line react/no-danger
                                dangerouslySetInnerHTML={{
                                    __html: item.prerequisites,
                                }}
                            />
                        }
                    </div>
                    <div className="pct-browser__related-courses">
                        <span className="pct-browser__label">
                            Related Courses
                        </span>
                        {
                            <span
                                // eslint-disable-next-line react/no-danger
                                dangerouslySetInnerHTML={{
                                    __html: item.relatedCourses,
                                }}
                            />
                        }
                    </div>

                    <ul className={ulButtonWrapperClassNames}>
                        <li className="cmc-article__link-list-item">
                            {scheduleItem ? (
                                <button
                                    className="cmc-article__link pct-browser__session-add-link"
                                    onClick={event => {
                                        event.preventDefault();
                                        this.props.removePctFromSchedule(
                                            scheduleItem
                                        );
                                    }}
                                >
                                    {removeBtnTxt}
                                </button>
                            ) : (
                                <button
                                    className="cmc-article__link pct-browser__session-add-link"
                                    onClick={event => {
                                        event.preventDefault();
                                        this.props.addPctToSchedule(
                                            this.props.user.info.uid,
                                            item.id
                                        );
                                    }}
                                >
                                    {addBtnTxt}
                                </button>
                            )}
                        </li>
                        <li className="cmc-article__link-list-item">
                            <a
                                className="cmc-article__link"
                                href="https://s4.goeshow.com/campusmgmt/insight/2017/registration_form.cfm"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Register
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        );
    }
}

export default Session;
