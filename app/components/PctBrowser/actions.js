import config from '../../../config';
import fetchItems from '../util/fetch-util';

const endpoint = `${config.domains.api}/campusInsight/ci/pct/`;

export const REQUEST_PCTS = 'REQUEST_PCTS';
export function requestPcts() {
    return {
        type: REQUEST_PCTS,
    };
}

export const RECEIVE_PCTS = 'RECEIVE_PCTS';
export function receivePcts(items) {
    return {
        type: RECEIVE_PCTS,
        items: items.data,
    };
}

export const REQUEST_PCTS_ERROR = 'REQUEST_PCTS_ERROR';
export function requestPctsError(error) {
    return {
        type: REQUEST_PCTS_ERROR,
        error,
    };
}

export function fetchPcts() {
    return (dispatch, getState) => {
        const state = getState();
        const hasYear =
            Object.hasOwnProperty.call(state, 'domAttributes') &&
            Object.hasOwnProperty.call(state.domAttributes, 'year');
        const year = hasYear ? `${state.domAttributes.year}/` : '';
        const all = process.env.NETLIFY_ENV !== 'production' ? '?all=1' : '';
        return fetchItems({
            dispatch,
            endpoint: `${endpoint}${year}${all}`,
            startAction: requestPcts,
            endAction: receivePcts,
            errorAction: requestPctsError,
            getState,
            key: 'pct',
        });
    };
}
