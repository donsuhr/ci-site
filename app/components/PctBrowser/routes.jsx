/* eslint global-require:0 */
import React from 'react';
import { HashRouter, Route } from 'react-router-dom';
import PctBrowserConnected from './PctBrowser.container';

const RouteContainer = props => (
    <HashRouter>
        <Route
            path="/:filter/:select?"
            render={({ match }) => (
                <PctBrowserConnected match={match} {...props} />
            )}
        />
    </HashRouter>
);

export default RouteContainer;
