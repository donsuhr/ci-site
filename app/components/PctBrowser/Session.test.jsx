/* global jest,it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Session from './Session';

jest.mock('cmc-site/app/components/smoothScroll');

const item = {
    savingStatus: '',
    flagAsNew: true,
    _id: '5830d8cb3aa27a048063d276',
    updatedAt: '2017-03-21T04:00:29.430Z',
    createdAt: '2016-11-19T22:57:15.849Z',
    title: 'New Offerings from Integration Services',
    description: 'The Integrations Services ...',
    start: '2017-04-28T21:00:00.000Z',
    end: '2017-04-28T21:45:00.000Z',
    status: 'Approved',
    active: true,
    attendance: 22,
    presenter: [],
    userType: ['Functional', 'Technical', 'Executive'],
    userLevel: ['Beginner', 'Intermediate', 'Expert'],
    track: {
        title: 'Driving Excellence through Professional Services',
    },
    product: {
        title: 'Product',
    },
    level: 'Advanced',
    type: 'Dev',
    price: 222,
    location: {
        title: 'location',
    },
    learningObjectives: 'learningObjectives....',
    targetAudience: 'targetAudience....',
    prerequisites: 'prerequisites....',
    relatedCourses: 'relatedCourses....',
    id: '5830d8cb3aa27a048063d276',
};

const user = {
    info: {
        uid: 123,
    },
};

describe('PctBrowser Session', () => {
    it('renders', () => {
        const wrapper = shallow(
            <Session
                session={item}
                getItemStatus={() => 'REQUEST_CHILD_ADD'}
                isPctInSchedule={jest.fn(() => true)}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('toggles expand on click', () => {
        const wrapper = shallow(
            <Session
                session={item}
                getItemStatus={() => 'REQUEST_CHILD_ADD'}
                isPctInSchedule={jest.fn(() => true)}
            />
        );
        expect(wrapper.state().expanded).toBe(false);
        wrapper
            .find('button.pct-browser__session-details-button')
            .simulate('click', {
                preventDefault() {
                },
            });
        expect(wrapper.state().expanded).toBe(true);
    });

    it('renders remove button', () => {
        const removePctFromSchedule = jest.fn();
        const wrapper = shallow(
            <Session
                session={item}
                getItemStatus={() => ''}
                isPctInSchedule={() => true}
                removePctFromSchedule={removePctFromSchedule}
                user={user}
            />
        );
        expect(removePctFromSchedule).not.toHaveBeenCalled();
        expect(wrapper.find('.pct-browser__session-add-link').text()).toBe(
            'Remove from Schedule'
        );
        wrapper.find('.pct-browser__session-add-link').simulate('click', {
            preventDefault() {
            },
        });
        expect(removePctFromSchedule).toHaveBeenCalled();
    });
    it('renders add button', () => {
        const addPctToSchedule = jest.fn();
        const wrapper = shallow(
            <Session
                session={item}
                getItemStatus={() => ''}
                isPctInSchedule={() => false}
                addPctToSchedule={addPctToSchedule}
                user={user}
            />
        );
        expect(addPctToSchedule).not.toHaveBeenCalled();
        expect(wrapper.find('.pct-browser__session-add-link').text()).toBe(
            'Add to Schedule'
        );
        wrapper.find('.pct-browser__session-add-link').simulate('click', {
            preventDefault() {
            },
        });
        expect(addPctToSchedule).toHaveBeenCalled();
    });
});
