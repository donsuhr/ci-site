/* global it,expect,describe */

import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import PctBrowser from './PctBrowser';

const sessionsByTrackByDay = {
    Thursday: [
        {
            id: '58fe6ddbcb77715e22b476cc',
            start: '2017-04-27T18:00:00.000Z',
        },
        {
            id: '5830d7913aa27a048063d270',
            start: '2017-04-27T19:00:00.000Z',
        },
    ],
    Friday: [
        {
            id: '5830d8fd3aa27a048063d277',
            start: '2017-04-28T13:30:00.000Z',
        },
    ],
};

describe('PctBrowser', () => {
    it('renders', () => {
        const wrapper = shallow(
            <PctBrowser
                sessionsGroupedByDay={sessionsByTrackByDay}
                fetching={false}
            />
        );
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('renders loading', () => {
        const wrapper = shallow(
            <PctBrowser
                sessionsGroupedByDay={sessionsByTrackByDay}
                fetching
            />
        );
        expect(wrapper.find('Loading')).toHaveLength(1);
    });
    it('renders two days', () => {
        const wrapper = shallow(
            <PctBrowser
                sessionsGroupedByDay={sessionsByTrackByDay}
                fetching={false}
            />
        );
        expect(wrapper
            .find('Day')).toHaveLength(2);
    });
});
