// eslint-disable-next-line max-len
/* eslint comma-dangle: ["error", {"arrays": "always-multiline", "objects": "always-multiline", "imports": "always-multiline", "exports": "always-multiline", "functions": "never"}] */

const webpack = require('webpack');
const path = require('path');
const parentDllEntry = require('cmc-site/webpack.dll.config').entry.dll;

const dll = parentDllEntry.concat([
    'classnames',
    'firebase',
    'formsy-react',
    'moment-timezone',
    'randomcolor',
    'react',
    'react-transition-group',
    'react-bootstrap',
    'react-bootstrap-table',
    'react-dom',
    'react-redux',
    'react-router',
    'react-tooltip',
    'recharts',
    'redux',
    'redux-form',
    'redux-thunk',
    'slick-carousel',
    'redux-devtools',
    'redux-devtools-dock-monitor',
    'redux-devtools-log-monitor',
]);

module.exports = {
    entry: {
        dll,
    },

    output: {
        filename: '[name].bundle.js',
        path: __dirname,
        // The name of the global variable which the library's
        // require() function will be assigned to
        library: '[name]_lib',
    },
    resolve: {
        modules: ['node_modules'],
        alias: {
            underscore: path.join(
                __dirname,
                'node_modules/underscore/underscore'
            ),
            handlebars: 'handlebars/runtime',
        },
    },

    plugins: [
        new webpack.ProvidePlugin({
            'window.jQuery': 'jquery', // garlic, headroom, bootstrap
            jQuery: 'jquery', // bootstrap parsley superfish
        }),
        new webpack.DllPlugin({
            // The path to the manifest file which maps between
            // modules included in a bundle and the internal IDs
            // within that bundle
            path: '[name]-manifest.json',
            // The name of the global variable which the library's
            // require function has been assigned to. This must match the
            // output.library option above
            name: '[name]_lib',
        }),
    ],
};
