'use strict';

module.exports = function config(api) {
    api.cache(true);

    let presets = [
        [
            '@babel/preset-env',
            {
                modules: false,
                loose: true,
                useBuiltIns: 'usage',
                corejs: { version: 3, proposals: true },
            },
        ],
        '@babel/preset-react',
    ];
    let plugins = [
        '@babel/plugin-syntax-dynamic-import',
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-transform-spread',
    ];

    if (process.env.NODE_ENV === 'test') {
        plugins = [
            '@babel/plugin-syntax-dynamic-import',
            'istanbul',
            [
                'module-resolver',
                {
                    alias: {
                        config: './config.js',
                    },
                },
            ],
        ];
    }

    if (process.env.NODE_ENV === 'production') {
        presets.push('react-optimize');
    }
    return {
        presets,
        plugins,
    };
};
