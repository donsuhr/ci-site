const timeGrunt = require('time-grunt');
const loadGruntConfig = require('load-grunt-config');
const path = require('path');
const loadDotEnv = require('cmc-load-dot-env');

if (!{}.hasOwnProperty.call(process.env, 'AWS_ACCESS_KEY_ID__CMC_DEMOCENTER_DL')) {
    // eslint-disable-next-line no-console
    console.log('process.env.AWS_ACCESS_KEY_ID__CMC_DEMOCENTER_DL not found, loading .env');
    loadDotEnv();
}

const modulesDir = path.resolve(require.resolve('cmc-load-dot-env'), '../..');

module.exports = function gruntfile(grunt) {
    const config = {
        app: 'app',
        dist: 'dist',
        sassOutput: '.tmp/sass-out/',
        livereloadPort: 35729,
        testLiveReload: 35730,
        modulesDir,
    };

    timeGrunt(grunt);

    loadGruntConfig(grunt, {
        configPath: path.join(modulesDir, 'cmc-site/build/grunt'),
        overridePath: path.join(process.cwd(), '/build/grunt'),
        data: config,
        jitGrunt: {
            staticMappings: {
                sprite: 'grunt-spritesmith',
                scsslint: 'grunt-scss-lint',
            },
        },
    });

    grunt.registerTask('serve', (target) => {
        if (target === 'dist') {
            grunt.task.run([
                'build',
                'connect:dist:keepalive',
            ]);
        } else {
            grunt.task.run([
                'run:checkNpm',
                'clean',
                'run:writeConfig',
                'run:build-js',
                'run:metalsmith_dist',
                'run:sass',
                'run:postcss',
                'connect:livereload',
                'concurrent:serve',
            ]);
        }
    });

    grunt.registerTask('test', (target) => {
        if (target !== 'fromWatch') {
            grunt.task.run([
                // 'clean'
            ]);
        }

        grunt.task.run([
            'connect:test',
            'mocha',
        ]);
    });

    grunt.registerTask('testServer', () => {
        grunt.task.run([
            'connect:testServer',
            'watch:testServer',
        ]);
    });

    grunt.registerTask('build', [
        'clean',
        'run:writeConfig',
        'concurrent:dist',
        'run:writeSitemap',
        'run:metalsmith_dist',
        'run:critical-css',
        'run:postcss-min',
        'filerev',
        'usemin',
        'run:inline',
        'htmlmin',
    ]);

    grunt.registerTask('default', [
        'test',
        'build',
    ]);

    grunt.registerTask('styles', [
        'run:sass',
        'run:critical-css',
        'run:postcss-min',
        'copy:dist',
        'filerev',
        'notify:styles',
    ]);

    grunt.registerTask('pages', [
        'run:metalsmith_dist',
        'usemin',
        'run:inline',
    ]);

    grunt.registerTask('dist', ['build', 'notify:dist']);

    grunt.registerTask('netlify', [
        'clean',
        'copy:dist',
        // 'build',
        // 'aws_s3:dl-asset-overlay',
        // 'copy:overlay',
        // 'build-search-index',
    ]);

    grunt.registerTask('build-search-index', [
        'aws_s3:search-index-json--get',
        'run:build-search-index',
        'aws_s3:search-index-json--put',
    ]);

    grunt.registerTask('build-search-index-docs', [
        'aws_s3:search-index-json--get',
        'aws_s3:search-index-docs--get-general',
        'run:build-search-index-docs',
        'aws_s3:search-index-json--put',
    ]);
};
