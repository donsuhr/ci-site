'use strict';

const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const AssetsPlugin = require('assets-webpack-plugin');

const production = process.env.NODE_ENV === 'production';
const mode = production ? 'production' : 'development';

const entry = {
    'cmc-site': [path.join(__dirname, 'app/scripts/index.js')],
    'page--home': [
        path.join(__dirname, 'node_modules/cmc-site/app/scripts/pages/home'),
    ],
    'page--contact-us': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/contact-us'
        ),
    ],
    'page--404': [
        path.join(__dirname, 'node_modules/cmc-site/app/scripts/pages/404'),
    ],
    'page--search': [
        path.join(__dirname, 'node_modules/cmc-site/app/scripts/pages/search'),
    ],
    'page--simple-form': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/simple-form'
        ),
    ],
    'page--simple-form--with-formalizer': [
        path.join(
            __dirname,
            'node_modules/cmc-site/app/scripts/pages/simple-form--with-formalizer'
        ),
    ],
    'page--sessions': [path.join(__dirname, 'app/scripts/pages/sessions')],
    'page--pct': [path.join(__dirname, 'app/scripts/pages/pct')],
    'page--become-a-sponsor': [
        path.join(__dirname, 'app/scripts/pages/become-a-sponsor'),
    ],
    'page--faq': [path.join(__dirname, 'app/scripts/pages/faq')],
    'page--passport': [path.join(__dirname, 'app/scripts/pages/passport')],
    'page--nomination-form': [
        path.join(__dirname, 'app/scripts/pages/nomination-form'),
    ],
    'page--schedule': [path.join(__dirname, 'app/scripts/pages/schedule')],
    'page--support-consultations': [
        path.join(__dirname, 'app/scripts/pages/support-consultations'),
    ],
    'page--support-consultations-grid': [
        path.join(__dirname, 'app/scripts/pages/support-consultations-grid'),
    ],
    'page--support-consultations-report': [
        path.join(__dirname, 'app/scripts/pages/support-consultations-report'),
    ],
    'page--executive-activities-registration': [
        path.join(
            __dirname,
            'app/scripts/pages/executive-activities-registration'
        ),
    ],
    'page--passport-report': [
        path.join(__dirname, 'app/scripts/pages/passport-report'),
    ],
    'page--schedule-report': [
        path.join(__dirname, 'app/scripts/pages/schedule-report'),
    ],
    'page--session-evaluation': [
        path.join(__dirname, 'app/scripts/pages/session-evaluation'),
    ],
    'page--evaluation-report': [
        path.join(__dirname, 'app/scripts/pages/evaluation-report'),
    ],
    'page--social-media-kiosk': [
        path.join(__dirname, 'app/scripts/pages/social-media-kiosk'),
    ],
};
const plugins = [
    new AssetsPlugin({
        entrypoints: false,
        prettyPrint: true,
        filename: 'webpack-assets.json',
    }),
    new AssetsPlugin({
        entrypoints: true,
        prettyPrint: true,
        filename: 'webpack-manifest.json',
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ProvidePlugin({
        'window.jQuery': 'jquery', // garlic, headroom, bootstrap
        jQuery: 'jquery', // bootstrap parsley superfish
    }),
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
        },
    }),
];
if (!production) {
    plugins.push(new webpack.HotModuleReplacementPlugin());
    if (!production) {
        entry['cmc-site'].unshift(
            'webpack/hot/dev-server',
            'webpack-hot-middleware/client?reload=true',
        );
    }

    const manifestFile = path.resolve(process.cwd(), 'dll-manifest.json');
    let manifestJson;
    try {
        manifestJson = JSON.parse(fs.readFileSync(manifestFile));
    } catch (e) {
        manifestJson = false;
    }
    if (manifestJson) {
        plugins.push(
            new webpack.DllReferencePlugin({
                context: '.',
                manifest: manifestJson,
            }),
        );
    }
}


module.exports = {
    stats: 'errors-only',
    devtool: production ? 'source-map' : '#eval-source-map',
    context: path.join(__dirname, 'app', 'scripts'),
    entry,
    mode,
    plugins,
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            maxInitialRequests: Infinity,
            maxAsyncRequests: Infinity,
            minSize: 11000,
            chunks: 'all',
        },
    },
    resolve: {
        modules: ['node_modules', path.resolve(__dirname, 'app')],
        alias: {
            config: path.join(__dirname, 'config.js'),
            underscore: path.join(
                __dirname,
                'node_modules/underscore/underscore'
            ),
            handlebars: 'handlebars/runtime',
        },
        extensions: ['.js', '.jsx'],
    },
    output: {
        path: path.join(__dirname, 'dist/'),
        filename: production
            ? 'scripts/[name]-[chunkhash].js'
            : 'scripts/[name].js',
        chunkFilename: production
            ? 'scripts/[name]-[chunkhash].js'
            : 'scripts/[name].js',
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /nls[/\\]\S*\.json/,
                use: [
                    {
                        loader: 'amdi18n-loader',
                    },
                ],
                include: [
                    path.join(__dirname, '/app/components/'),
                    path.join(__dirname, '/node_modules/cmc-site/app/'),
                ],
            },
            {
                test: /\.jsx?$/,
                use: ['babel-loader'],
                include: [
                    path.join(__dirname, '/app/'),
                    path.join(__dirname, '/config.js'),
                    path.join(__dirname, '/node_modules/autotrack'),
                    path.join(__dirname, '/node_modules/dom-utils'),
                    /\/node_modules\/cmc-/,
                ],
                exclude: [path.join(__dirname, '/app/metalsmith/helpers/')],
            },
            {
                test: /\.hbs$/,
                use: [
                    {
                        loader: 'handlebars-loader',
                        query: {
                            debug: false,
                            runtime: 'handlebars/runtime',
                            helperDirs: [
                                `${__dirname}/app/metalsmith/helpers`,
                                `${__dirname}/node_modules/cmc-site/app/metalsmith/helpers`,
                            ],
                            partialDirs: [
                                `${__dirname}/app/metalsmith/partials`,
                                `${__dirname}/node_modules/cmc-site/app/metalsmith/partials`,
                            ],
                        },
                    },
                ],
                include: [
                    path.join(__dirname, '/app/'),
                    path.join(__dirname, '/node_modules/cmc-site'),
                ],
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'underscore-template-loader',
                        query: {
                            variable: 'data',
                        },
                    },
                ],
            },
        ],
    },
};
